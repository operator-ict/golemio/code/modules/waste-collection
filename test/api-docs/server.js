// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("@golemio/core/dist/shared/express");
const { ContainerToken, OutputGatewayContainer } = require("@golemio/core/dist/output-gateway/ioc");

const app = express();
const server = http.createServer(app);
const postgresConnector = OutputGatewayContainer.resolve(ContainerToken.PostgresDatabase);

const start = async () => {
    await postgresConnector.connect();

    const { wasteCollectionLegacyRouter } = require("#og/routers/WasteCollectionLegacyRouter");
    const { v1BulkyWasteStationsRouter } = require("#og/routers/BulkyWasteStationsRouter");

    app.use("/v2/sortedwastestations", wasteCollectionLegacyRouter);
    app.use(v1BulkyWasteStationsRouter.getPath(), v1BulkyWasteStationsRouter.getRouter());

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await postgresConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
