import { ParseDateTimeHelper } from "#og/controllers/helpers/ParseDateTimeHelper";
import { expect } from "chai";

describe("ParseDateTimeHelper", () => {
    it("should parse utc format", () => {
        expect(ParseDateTimeHelper.getDateTimeSeparately(new Date("2020-01-01T00:00:00.000Z"))).to.deep.eq({
            date: "2020-01-01",
            time: "01:00:00",
        });
    });

    it("should parse utc format with day change", () => {
        expect(ParseDateTimeHelper.getDateTimeSeparately(new Date("2020-01-01T23:00:00.000Z"))).to.deep.eq({
            date: "2020-01-02",
            time: "00:00:00",
        });
    });

    it("should parse prague winter format", () => {
        expect(ParseDateTimeHelper.getDateTimeSeparately(new Date("2020-01-01T00:00:00+01:00"))).to.deep.eq({
            date: "2020-01-01",
            time: "00:00:00",
        });
    });

    it("should parse prague summer format", () => {
        expect(ParseDateTimeHelper.getDateTimeSeparately(new Date("2020-07-01T00:00:00+02:00"))).to.deep.eq({
            date: "2020-07-01",
            time: "00:00:00",
        });
    });
});
