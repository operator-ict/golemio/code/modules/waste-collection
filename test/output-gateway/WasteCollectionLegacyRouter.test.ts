import { wasteCollectionLegacyRouter } from "#og/routers/WasteCollectionLegacyRouter";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { legacyMeasurementsResponse } from "../data/legacy-measurements-response";
import { legacyPickDaysResponse } from "../data/legacy-pickdays-response";
import { legacyPicksResponse } from "../data/legacy-picks-response";
import { legacyStationsResponse } from "../data/legacy-stations-response";

chai.use(chaiAsPromised);

describe("WasteCollection Legacy Router", () => {
    const app = express();
    const sortedTrashTypes = [
        "Papír",
        "Plast",
        "Kovy",
        "Nápojové kartóny",
        "Barevné sklo",
        "Čiré sklo",
        "Jedlé tuky a oleje",
        "Elektrozařízení",
    ];

    before(async () => {
        app.use("/wastecollectionlegacy", wasteCollectionLegacyRouter);
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /wastecollectionlegacy", (done) => {
        request(app)
            .get("/wastecollectionlegacy")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                const filteredFeatures = response.body.features.map((feature: any) => {
                    const { updated_at, ...rest } = feature.properties;
                    return { ...feature, properties: rest };
                });

                expect(filteredFeatures).to.have.length(2);
                expect(filteredFeatures).to.deep.eq(legacyStationsResponse.features);
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });

    it("should correctly apply onlyMonitored filter to GET /wastecollectionlegacy", (done) => {
        request(app)
            .get("/wastecollectionlegacy?onlyMonitored=true")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                const filteredFeatures = response.body.features.map((feature: any) => {
                    const { updated_at, ...rest } = feature.properties;
                    return { ...feature, properties: rest };
                });
                expect(filteredFeatures).to.have.length(1);
                expect(filteredFeatures).to.deep.eq([legacyStationsResponse.features[1]]);
                done();
            })
            .catch((err) => done(err));
    });

    it("should correctly apply accessibility filter to GET /wastecollectionlegacy", (done) => {
        request(app)
            .get("/wastecollectionlegacy?accessibility=2&accessibility=3")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body.features).to.have.length(2);
                done();
            })
            .catch((err) => done(err));
    });

    it("GET /wastecollectionlegacy should respond with containers sorted by trash type", (done) => {
        request(app)
            .get("/wastecollectionlegacy")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                for (const feature of response.body.features) {
                    const containers = feature.properties.containers;
                    const trashTypes: string[] = containers.map(
                        (c: { trash_type: { description: string } }) => c.trash_type.description
                    );

                    const existingTrashTypes: Array<string | undefined> = sortedTrashTypes.map((st) =>
                        trashTypes.find((t) => t === st)
                    );

                    for (let i = 0; i < sortedTrashTypes.length; i++) {
                        expect(existingTrashTypes.findIndex((t) => t === sortedTrashTypes[i])).to.be.oneOf([i, -1]);
                    }
                }
                done();
            })
            .catch((err) => done(err));
    });

    it("GET /wastecollectionlegacy?id=23361 should response with a container", (done) => {
        request(app)
            .get("/wastecollectionlegacy?id=23361")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.have.property("features");
                expect(response.body.features).to.have.length(1);
                expect(response.body.features[0].properties).to.have.property("id", 1);
                done();
            })
            .catch((err) => done(err));
    });

    it("GET /wastecollectionlegacy?id=1&id=19 should respond with 400 Bad Request", (done) => {
        request(app)
            .get("/wastecollectionlegacy?id=1&id=19")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400)
            .then((response) => {
                expect(response.body).to.have.property("error_message", "Bad request");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionlegacy/measurements", (done) => {
        request(app)
            .get("/wastecollectionlegacy/measurements?containerId=9552&from=2022-11-29T16:57:42.000Z&to=2022-11-29T19:46:20.000Z")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq(legacyMeasurementsResponse.slice(1, 4));
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });

    it("/wastecollectionlegacy/measurements?containerId=9552&containerId=9553 should respond with 400 Bad Request", (done) => {
        request(app)
            .get("/wastecollectionlegacy/measurements?containerId=9552&containerId=9553")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400)
            .then((response) => {
                expect(response.body).to.have.property("error_message", "Bad request");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionlegacy/picks", (done) => {
        request(app)
            .get("/wastecollectionlegacy/picks?containerId=9552&from=2022-11-25T05:22:14.000Z&to=2022-11-28T07:25:57.000Z")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq(legacyPicksResponse.slice(0, 2));
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionlegacy/pickdays", (done) => {
        request(app)
            .get("/wastecollectionlegacy/pickdays?ksnkoId=9553&sensoneoCode=9553")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq([legacyPickDaysResponse[0]]);
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=21600, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });
});
