import { WasteCollectionContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { BulkyWasteStationsRepository } from "#og/repositories/BulkyWasteStationsRepository";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { SinonFakeTimers, useFakeTimers } from "sinon";
import { SCHEMA_NAME } from "src/constants";
import request from "supertest";

chai.use(chaiAsPromised);

describe("BulkyWasteStations Router", () => {
    const app = express();
    let clock: SinonFakeTimers;

    const now = new Date("2023-12-15T12:50:21.123Z");

    before(async () => {
        clock = useFakeTimers({
            now,
            shouldAdvanceTime: true,
        });
        await WasteCollectionContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).connect();

        const router = require("#og/routers/BulkyWasteStationsRouter");

        const repository = WasteCollectionContainer.resolve<BulkyWasteStationsRepository>(
            ModuleContainerToken.BulkyWasteStationsRepository
        );
        await repository.sequelizeModel.truncate(); // due to integration tests in #ie reset state of db
        await repository.sequelizeModel.sequelize?.query(
            // eslint-disable-next-line max-len
            `INSERT INTO ${SCHEMA_NAME}.ksnko_bulky_waste_stations (id,pick_date,pick_time_from,pick_time_to,street,payer,number_of_containers,service_id,service_code,service_name,trash_type_id,trash_type,trash_type_name,city_district_id,city_district_name,city_district_ruian,coordinate_lat,coordinate_lon,geom,created_at,created_by,update_batch_id,updated_at,updated_by,changed,city_district_slug,custom_id) VALUES (85293,'2023-12-30','09:00:00','13:00:00','U Rajské zahrady x Vlkova','municipality',1,1,'VOK','VOK',NULL,NULL,NULL,3,'Praha 3','500097',-741291.903159,-1043752.71104,'SRID=4326;POINT (14.443673271704977 50.08273347451379)','2023-10-25 22:00:10.288',NULL,NULL,'2023-10-25 22:00:10.288',NULL,'2023-10-25 14:41:27.000','praha-3','2023-12-30 09:00 ID:85293'),            (85291,'2023-12-28','14:00:00','18:00:00','Soběslavská x Hollarovo náměstí','municipality',1,1,'VOK','VOK',NULL,NULL,NULL,3,'Praha 3','500097',-739018.048305,-1044751.35408,'SRID=4326;POINT (14.477044689979657 50.076614242443746)','2023-10-25 22:00:10.288',NULL,NULL,'2023-10-25 22:00:10.288',NULL,'2023-10-25 14:41:27.000','praha-3','2023-12-28 14:00 ID:85291'),(85292,'2023-12-28','15:00:00','19:00:00','Sudoměřská x Křišťanova','municipality',1,1,'VOK','VOK',NULL,NULL,NULL,3,'Praha 3','500097',-740292.314752,-1044078.42435,'SRID=4326;POINT (14.45812939731576 50.081053810719084)','2023-10-25 22:00:10.288',NULL,NULL,'2023-10-25 22:00:10.288',NULL,'2023-10-25 14:41:27.000','praha-3','2023-12-28 15:00 ID:85292'),            (83217,'2023-12-28','14:00:00','18:00:00','Plzeňská x U Zámečnice','municipality',1,1,'VOK','VOK',NULL,NULL,NULL,6,'Praha 5','500143',-746766.020823,-1044388.20638,'SRID=4326;POINT (14.369120828676682 50.070353664285754)','2023-10-25 22:00:10.288',NULL,NULL,'2023-10-25 22:00:10.288',NULL,'2023-09-07 17:07:05.000','praha-5','2023-12-28 14:00 ID:83217');`
        );

        app.use("/bulky-waste", router.v1BulkyWasteStationsRouter.getRouter());
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    after(() => {
        clock.restore();
    });

    it("should respond with json to GET /bulky-waste/stations", (done) => {
        const expectedResponse = fs.readFileSync(__dirname + "/../data/ksnko-bulky-containers-og-output.json", "utf8");
        request(app)
            .get("/bulky-waste/stations")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq(JSON.parse(expectedResponse));
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=21600, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });
});
