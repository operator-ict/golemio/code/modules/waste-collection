export const legacyStationsResponse = {
    features: [
        {
            type: "Feature",
            geometry: {
                coordinates: [9.321902057986193, 52.37296489565013],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "obyvatelům domu",
                    id: 2,
                },
                containers: [
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                            pick_days: "Po",
                        },
                        container_type: "1100 normální HV",
                        trash_type: {
                            description: "Papír",
                            id: 5,
                        },
                        ksnko_id: 134,
                        container_id: 134,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: null,
                            frequency: 99,
                            id: 99,
                        },
                        container_type: "1100 normální HV",
                        trash_type: {
                            description: "Plast",
                            id: 6,
                        },
                        ksnko_id: 136,
                        container_id: 136,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: null,
                            frequency: null,
                            id: null,
                        },
                        container_type: "1100 mini H SV",
                        trash_type: {
                            description: "Kovy",
                            id: 3,
                        },
                        ksnko_id: 21128,
                        container_id: 21128,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 11,
                            pick_days: "Po",
                        },
                        container_type: "240 normální HV",
                        trash_type: {
                            description: "Nápojové kartóny",
                            id: 4,
                        },
                        ksnko_id: 133,
                        container_id: 133,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                            pick_days: "Po",
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Barevné sklo",
                            id: 1,
                        },
                        ksnko_id: 135,
                        container_id: 135,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                            pick_days: "Po",
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 132,
                        container_id: 132,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                            pick_days: "Po",
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 30414,
                        container_id: 30414,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                            pick_days: "Po",
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 30416,
                        container_id: 30416,
                        is_monitored: false,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 41,
                            pick_days: "Po",
                        },
                        container_type: "240 normální HV",
                        trash_type: {
                            description: "Jedlé tuky a oleje",
                            id: 8,
                        },
                        ksnko_id: 30415,
                        container_id: 30415,
                        is_monitored: false,
                    },
                ],
                district: null,
                id: 25,
                is_monitored: false,
                name: "Pstružná 821/2",
                station_number: "0022/ 002",
            },
        },
        {
            type: "Feature",
            geometry: {
                coordinates: [9.313658638379719, 52.379484438973755],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "neznámá dostupnost",
                    id: 3,
                },
                containers: [
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 41,
                            pick_days: "Po",
                        },
                        container_type: "240 normální HV",
                        trash_type: {
                            description: "Jedlé tuky a oleje",
                            id: 8,
                        },
                        last_measurement: {
                            measured_at_utc: "2023-11-24T08:48:16.000Z",
                            percent_calculated: 81,
                            prediction_utc: null,
                        },
                        last_pick: "2022-11-24T08:09:26.000Z",
                        ksnko_id: 23361,
                        container_id: 23361,
                        sensor_code: "23361",
                        sensor_supplier: "Sensoneo",
                        sensor_id: "Sensoneo_C01381",
                        is_monitored: true,
                    },
                ],
                district: null,
                id: 1,
                is_monitored: true,
                name: "Přátelství 356/61",
                station_number: "0022/ 001",
            },
        },
    ],
    type: "FeatureCollection",
};
