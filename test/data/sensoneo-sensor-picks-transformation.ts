import { TSensorPickUpdated } from "#ie/transformations";

export const sensoneoPicksTransformedDataFixture: TSensorPickUpdated[] = [
    {
        vendor_id: "Sensoneo_C00636",
        ksnko_container_id: 18699,
        pick_at: "2022-03-29T21:15:48Z",
        percent_before: 82,
        percent_now: 0,
        event_driven: false,
        updated_at: "2023-01-08T11:00:00.000Z",
    },
    {
        vendor_id: "Sensoneo_C01491",
        ksnko_container_id: 19328,
        pick_at: "2022-03-29T16:06:11Z",
        percent_before: 98,
        percent_now: 0,
        event_driven: false,
        updated_at: "2023-01-08T11:00:00.000Z",
    },
    {
        vendor_id: "Sensoneo_C10140",
        ksnko_container_id: 10140,
        pick_at: "2022-03-29T13:27:50Z",
        percent_before: 22,
        percent_now: 0,
        event_driven: false,
        updated_at: "2023-01-08T11:00:00.000Z",
    },
    {
        vendor_id: "Sensoneo_C00542",
        ksnko_container_id: 18739,
        pick_at: "2022-03-29T13:26:59Z",
        percent_before: 100,
        percent_now: 12,
        event_driven: false,
        updated_at: "2023-01-08T11:00:00.000Z",
    },
];
