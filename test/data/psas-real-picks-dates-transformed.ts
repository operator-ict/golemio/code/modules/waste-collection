import { IPsasRealPicksDates } from "#sch/definitions/PsasRealPicksDates";

export const psasRealPickDatesTransformed: IPsasRealPicksDates[] = [
    {
        internal_pick_id: "73d29eb7-0044-5292-ae51-79bd08927eb3",
        subject_id: 105476032,
        trash_type: "C",
        trash_type_code: "sc",
        trash_type_name: "Čiré sklo",
        frequency: "061",
        container_volume: 3350,
        container_type: "3350 L Atomium Reflex - SV",
        volume_ratio: 0.3,
        valid_from: new Date("2024-05-01T00:00:00+02:00"),
        valid_to: null,
        station_number: "0011/ 028",
        station_address: "Ledvinova 1709/2, Praha 11",
        psas_planned_pick_date: new Date("2024-10-30T00:00:00+01:00"),
        psas_real_pick_date: new Date("2024-09-18T00:00:00+02:00"),
    },
];
