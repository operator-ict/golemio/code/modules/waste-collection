import { IContainerChange } from "#sch/definitions/models/interfaces/IContainerChange";

export const rawContainerChangesTransformation: IContainerChange = {
    subject_id: 102406979,
    task_id: "0809650",
    changed_at: "2023-06-07T00:00:00.000+02:00",
    task_type: "Náhrada nádoby za vyhořelou",
    container_type: "1100 L normální",
    trash_type: "U",
    trash_type_code: "u",
    trash_type_name: "Plast",
    station_number: "0015/ 002",
    company: "Pražské služby, a.s.",
    removed_containers_count: null,
    added_containers_count: 1,
    street: "Boloňská 306/0",
    city_district: "Praha 15",
    created_at: "2023-06-07T00:00:00.000+02:00",
};
