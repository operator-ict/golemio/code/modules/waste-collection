import PsasRealPicksDatesTask from "#ie/workers/tasks/PsasRealPicksDatesTask";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { psasRealPickDatesTransformed } from "../../../data/psas-real-picks-dates-transformed";
import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import fs from "fs";
import path from "path";

describe("PsasRealPicksDatesTask", () => {
    let task: PsasRealPicksDatesTask;
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let transformArrayStub: SinonStub;
    let getDataStub: SinonStub;
    let saveDataStub: SinonStub;

    before(() => {
        sandbox = sinon.createSandbox();

        transformArrayStub = sandbox.stub().callsFake(() => psasRealPickDatesTransformed);
        getDataStub = sandbox
            .stub()
            .callsFake(() =>
                JSON.parse(fs.readFileSync(path.join(__dirname, "../../../data/psas-real-picks-dates-data.json"), "utf-8"))
            );
        saveDataStub = sandbox.stub().resolves();

        const dataSource = class DummyClass {
            getAll = getDataStub;
        };

        testContainer = WasteCollectionContainer.createChildContainer();
        testContainer
            .registerSingleton(
                ModuleContainerToken.PsasRealPicksDatesDataSource,
                class DummyClass {
                    getDataSource = () => new dataSource();
                }
            )
            .registerSingleton(
                ModuleContainerToken.PsasRealPicksDatesRepository,
                class DummyClass {
                    saveData = saveDataStub;
                }
            )
            .registerSingleton(
                ModuleContainerToken.PsasRealPicksDatesTransformation,
                class DummyClass {
                    transformArray = transformArrayStub;
                }
            );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.PsasRealPicksDatesTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    it("calls correct methods", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(getDataStub);
        sandbox.assert.calledOnce(transformArrayStub);
        sandbox.assert.calledOnce(saveDataStub);
    });
});
