import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("VenzeoDataTask", () => {
    let sandbox: SinonSandbox;
    let task: any;
    let transformArray: SinonStub,
        sendLogs: SinonStub,
        find: SinonStub,
        deactivateBulkyStation: SinonStub,
        getActiveContainersFromVenzeoApi: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = WasteCollectionContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        sandbox = sinon.createSandbox();
        transformArray = sandbox
            .stub()
            .callsFake(() =>
                JSON.parse(fs.readFileSync(path.join(__dirname, "../../../data/venzeo-transformed-data.json"), "utf-8"))
            );
        find = sandbox
            .stub()
            .callsFake(() =>
                JSON.parse(fs.readFileSync(path.join(__dirname, "../../../data/venzeo-incoming-data.json"), "utf-8"))
            );
        getActiveContainersFromVenzeoApi = sandbox
            .stub()
            .callsFake(() =>
                JSON.parse(fs.readFileSync(path.join(__dirname, "../../../data/venzeo-active-containers-data.json"), "utf-8"))
            );

        sendLogs = sandbox.stub().resolves();
        deactivateBulkyStation = sandbox.stub().resolves();

        WasteCollectionContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                module: {
                    "waste-collection": {
                        venzeo: {
                            url: "",
                            token: "",
                        },
                    },
                },
            })
        );

        testContainer = WasteCollectionContainer.createChildContainer();
        testContainer
            .registerSingleton(
                ModuleContainerToken.KsnkoBulkyStationsRepository,
                class DummyFactory {
                    find = find;
                }
            )
            .registerSingleton(
                ModuleContainerToken.VenzeoTransformation,
                class DummyFactory {
                    transformArray = transformArray;
                }
            )
            .registerSingleton(
                ModuleContainerToken.VenzeoDataSource,
                class DummyFactory {
                    getActiveContainersFromVenzeoApi = getActiveContainersFromVenzeoApi;
                    deactivateBulkyStation = deactivateBulkyStation;
                    sendDataToVenzeoApi = sendLogs;
                }
            );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SendDataToVenzeoTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    it("should refresh data", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(getActiveContainersFromVenzeoApi);
        sandbox.assert.calledTwice(deactivateBulkyStation);

        sandbox.assert.calledOnce(find);
        sandbox.assert.calledOnce(transformArray);
        sandbox.assert.calledTwice(sendLogs);
    });
});
