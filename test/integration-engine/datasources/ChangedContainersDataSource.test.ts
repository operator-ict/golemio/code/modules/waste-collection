import { ContainerChangesDatasource } from "#ie/datasources/psas/ContainerChangesDatasource";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("ContainerChangesDatasource", () => {
    let testDatasource: ContainerChangesDatasource;

    before(() => {
        testDatasource = new ContainerChangesDatasource("", "", "");
    });

    it("should have getDataSource method", async () => {
        expect(testDatasource.getData).not.to.be.undefined;
    });

    it("should validate data", async () => {
        expect(
            await testDatasource["validator"].Validate(
                JSON.parse(fs.readFileSync(__dirname + "/../../data/raw-containerChanges-datasource.json").toString("utf8")).data
            )
        ).to.be.true;
    });

    it("validation should fail", async () => {
        await expect(testDatasource["validator"].Validate([{ a: "b" }])).rejectedWith(
            Error,
            // eslint-disable-next-line max-len
            `validation failed [{"keyword":"required","dataPath":"[0]","schemaPath":"#/items/required","params":{"missingProperty":"cislo_subjektu"},"message":"should have required property 'cislo_subjektu'"}]`
        );
    });
});
