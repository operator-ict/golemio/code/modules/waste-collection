import { ITopDatasource } from "#ie/datasources/itop/ItopDatasource";
import ITopInputDataSchema from "#sch/datasources/itop/ITopInputDataSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("ITopDatasource", () => {
    let sandbox: SinonSandbox;
    let testDatasource: ITopDatasource;

    before(() => {
        sandbox = sinon.createSandbox();
        testDatasource = new ITopDatasource({
            datasources: { "waste-collection": { itop: { url: "", username: "", password: "" } } },
        } as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("data validated and loaded", async () => {
        sandbox
            .stub(testDatasource, <any>"getAll")
            .resolves(JSON.parse(fs.readFileSync(__dirname + "/../../../data/raw-itop-input-data.json").toString("utf8")));
        const result = await testDatasource.getData();

        expect((result as any).message).to.equal("Found: 1");
        expect(result.objects["Sensor::1384"].fields).to.deep.equal({
            name: "1F834A3",
            description:
                "Reinstalace IS + Očištění snímačů + Restart senzoru, kontrola měření v aplikaci Sensoneo (WMS) - 16% - Ok; ",
            status: "production",
            location_name: "C20614",
            brand_name: "Sensoneo",
            model_name: "Single sensor v3",
            ksnkoid: "20614",
            ismonitored: "monitored",
            snetworkid: "sigfox",
            loraid: "",
            nbiotid: "",
            sigfoxid: "1F834A3",
            serialnumber: "1F834A3",
            asset_number: "1F834A3",
            purchase_date: "2021-05-11",
            installdate: "2021-01-15",
            move2production: "2021-01-15",
            end_of_warranty: "2023-05-10",
            end_of_warranty_mhmp: "2023-05-11",
            servisdate: "2023-01-24",
        });
    });

    it("validation should fail", async () => {
        const vaidator = new JSONSchemaValidator("test", ITopInputDataSchema.jsonSchemaRaw);
        return expect(
            vaidator.Validate(
                JSON.parse(fs.readFileSync(__dirname + "/../../../data/raw-itop-input-data-invalid.json").toString("utf8"))
            )
        ).rejectedWith(
            // eslint-disable-next-line max-len
            `validation failed [{"keyword":"type","dataPath":".objects['Sensor::1384'].key","schemaPath":"#/properties/objects/patternProperties/%5ES/properties/key/type","params":{"type":"string"},"message":"should be string"}]`
        );
    });
});
