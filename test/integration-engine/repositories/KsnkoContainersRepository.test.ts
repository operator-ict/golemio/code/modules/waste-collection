import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { KsnkoContainersRepository } from "#ie/repositories/KsnkoContainersRepository";
import { IKsnkoStationContainer } from "#sch/definitions/KsnkoStationsContainers";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { Op } from "@golemio/core/dist/shared/sequelize";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("KsnkoContainersRepository", () => {
    let repository: KsnkoContainersRepository;

    before(() => {
        PostgresConnector.connect();
        repository = new KsnkoContainersRepository();
    });

    after(async () => {
        await repository.update({ active: true }, { where: { active: false } });
    });

    it("should have name", async () => {
        expect(repository.name).not.to.be.undefined;
        expect(repository.name).is.equal("WasteCollectionKsnkoContainersRepository");
    });

    it("should have updateActive method", async () => {
        expect(repository.updateActive).not.to.be.undefined;
    });

    it("should get container id by trash type and station number", async () => {
        const resultId = await repository.getIdByTypeAndStation("sb", "0007/ 022");
        expect(resultId?.length).to.equal(1);
        expect(resultId![0]).to.equal(9553);
    });

    it("saving data with identical hash", async () => {
        const processingDate = new Date();
        const data: IKsnkoStationContainer[] = [
            {
                id: 111,
                code: "code1",
                volume_ratio: 0.5,
                trash_type: "plastic",
                station_id: 22,
                valid_from: processingDate.toISOString(),
                container_volume: 0,
                container_brand: "",
                container_dump: "",
                cleaning_frequency_code: "",
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b6",
            },
            {
                id: 222,
                code: "code2",
                volume_ratio: 0.7,
                trash_type: "glass",
                station_id: 22,
                valid_from: processingDate.toISOString(),
                container_volume: 0,
                container_brand: "",
                container_dump: "",
                cleaning_frequency_code: "",
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b6",
            },
        ];

        const result1 = await repository.updateActive(data);
        await repository.updateValidTo(result1, processingDate, TableIdEnum.KsnkoId);

        const result = await repository.updateActive(data);
        await repository.updateValidTo(result, processingDate, TableIdEnum.KsnkoId);

        const updatedData = await repository.find({
            where: {
                id: {
                    [Op.in]: ["111", "222"],
                },
            },
        });
        console.log(updatedData);
        expect(updatedData).to.be.an("array");
        expect(updatedData).to.have.lengthOf(2);
        expect(updatedData.filter((item: IKsnkoStationContainer) => item.valid_to)).to.have.lengthOf(0);
    });

    it("create new items with different hash", async () => {
        const processingDate = new Date();
        const data: IKsnkoStationContainer[] = [
            {
                id: 111,
                code: "code1",
                volume_ratio: 0.5,
                trash_type: "plastic",
                station_id: 22,
                valid_from: processingDate.toISOString(),
                container_volume: 0,
                container_brand: "",
                container_dump: "",
                cleaning_frequency_code: "",
                active: true,
                form_hash: "333300f1-00c7-d974-8a85-a945ad4610b6",
            },
            {
                id: 222,
                code: "code2",
                volume_ratio: 0.7,
                trash_type: "glass",
                station_id: 22,
                valid_from: processingDate.toISOString(),
                container_volume: 0,
                container_brand: "",
                container_dump: "",
                cleaning_frequency_code: "",
                active: true,
                form_hash: "333300f1-00c7-d974-8a85-a945ad4610b6",
            },
        ];

        const result1 = await repository.updateActive(data);
        await repository.updateValidTo(result1, processingDate, TableIdEnum.KsnkoId);

        const result = await repository.updateActive(data);
        await repository.updateValidTo(result, processingDate, TableIdEnum.KsnkoId);

        const updatedData = await repository.find({
            where: {
                id: {
                    [Op.in]: ["111", "222"],
                },
            },
        });

        expect(updatedData).to.be.an("array");
        expect(updatedData).to.have.lengthOf(4);
        expect(updatedData.filter((item: IKsnkoStationContainer) => item.valid_to)).to.have.lengthOf(2);
    });

    it("create 2 new items with different hash", async () => {
        const processingDate = new Date();
        const data: IKsnkoStationContainer[] = [
            {
                id: 111,
                code: "code1",
                volume_ratio: 0.5,
                trash_type: "plastic",
                station_id: 22,
                valid_from: processingDate.toISOString(),
                container_volume: 0,
                container_brand: "",
                container_dump: "",
                cleaning_frequency_code: "",
                active: true,
                form_hash: "433300f1-00c7-d974-8a85-a945ad4610b6",
            },
            {
                id: 222,
                code: "code2",
                volume_ratio: 0.7,
                trash_type: "glass",
                station_id: 22,
                valid_from: processingDate.toISOString(),
                container_volume: 0,
                container_brand: "",
                container_dump: "",
                cleaning_frequency_code: "",
                active: true,
                form_hash: "433300f1-00c7-d974-8a85-a945ad4610b6",
            },
        ];

        const result1 = await repository.updateActive(data);
        await repository.updateValidTo(result1, processingDate, TableIdEnum.KsnkoId);

        const result = await repository.updateActive(data);
        await repository.updateValidTo(result, processingDate, TableIdEnum.KsnkoId);

        const updatedData = await repository.find({
            where: {
                id: {
                    [Op.in]: ["111", "222"],
                },
            },
        });

        expect(updatedData).to.be.an("array");
        expect(updatedData).to.have.lengthOf(6);
        expect(updatedData.filter((item: IKsnkoStationContainer) => item.valid_to)).to.have.lengthOf(4);
    });
});
