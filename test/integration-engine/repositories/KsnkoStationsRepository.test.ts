import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { KsnkoStationsRepository } from "#ie/repositories/KsnkoStationsRepository";
import { IKsnkoStation } from "#sch/definitions/KsnkoStations";
import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { KsnkoStationModel } from "#sch/definitions/models/KsnkoStationModel";

chai.use(chaiAsPromised);

describe("KsnkoStationsRepository", () => {
    let repository: KsnkoStationsRepository;

    before(() => {
        PostgresConnector.connect();
        repository = new KsnkoStationsRepository();
    });

    it("should have name", async () => {
        expect(repository.name).not.to.be.undefined;
        expect(repository.name).is.equal("WasteCollectionKsnkoStationsRepository");
    });

    it("should have updateActive method", async () => {
        expect(repository.updateActive).not.to.be.undefined;
    });

    it("should update records and mark old ones inactive", async () => {
        const processingDate = new Date();
        const stations = [
            {
                id: 1,
                number: "0022/ 001",
                name: "Přátelství 356/61",
                access: "neznámá dostupnost",
                location: "outdoor",
                has_sensor: false,
                changed: "2021-09-30T06:22:27.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: "-731175.14",
                coordinate_lon: "-1050805.71",
                geom: {
                    type: "Point",
                    coordinates: [9.313658638379719, 52.379484438973755],
                },
                active: true,
                form_hash: "09fdf61c-abde-333f-20b1-443c63889c64",
                valid_from: processingDate.toISOString(),
            },
            {
                id: 25,
                number: "0022/ 002",
                name: "Pstružná 821/2",
                access: "obyvatelům domu",
                location: "outdoor",
                has_sensor: true,
                changed: "2021-08-03T16:23:55.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: "-731999.974327",
                coordinate_lon: "-1050404.54492",
                geom: {
                    type: "Point",
                    coordinates: [9.321902057986193, 52.37296489565013],
                },
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                valid_from: processingDate.toISOString(),
            },
        ];

        const changedRecords = await repository.updateActive(stations);
        await repository.updateValidTo(changedRecords, processingDate, TableIdEnum.KsnkoId);
        const records = await repository.find({ where: { id: { [Op.in]: [1, 25] } }, raw: true });

        const activeRecordIds = records.filter((el: IKsnkoStation) => !el.valid_to).map((el: IKsnkoStation) => el.id);
        expect(activeRecordIds.length).to.equal(2);
        expect(activeRecordIds).to.have.members([1, 25]);
    });

    it("when I add 1 item with different hash it should create 1 new item 2 should remain active", async () => {
        const processingDate = new Date();
        const stations = [
            {
                id: 1,
                number: "0022/ 001",
                name: "Přátelství 356/61",
                access: "neznámá dostupnost",
                location: "outdoor",
                has_sensor: false,
                changed: "2021-09-30T06:22:27.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: "-731175.14",
                coordinate_lon: "-1050805.71",
                geom: {
                    type: "Point",
                    coordinates: [9.313658638379719, 52.379484438973755],
                },
                active: true,
                form_hash: "11fdf61c-abde-333f-20b1-443c63889c64",
                valid_from: processingDate.toISOString(),
            },
            {
                id: 25,
                number: "0022/ 002",
                name: "Pstružná 821/2",
                access: "obyvatelům domu",
                location: "outdoor",
                has_sensor: true,
                changed: "2021-08-03T16:23:55.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: "-731999.974327",
                coordinate_lon: "-1050404.54492",
                geom: {
                    type: "Point",
                    coordinates: [9.321902057986193, 52.37296489565013],
                },
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                valid_from: processingDate.toISOString(),
            },
        ];

        const changedRecords = await repository.updateActive(stations);
        await repository.updateValidTo(changedRecords, processingDate, TableIdEnum.KsnkoId);
        const records = await repository.find({ where: { id: { [Op.in]: [1, 25] } }, raw: true });

        const validRecords = records.filter((el: IKsnkoStation) => !el.valid_to).map((el: IKsnkoStation) => el.id);
        expect(validRecords.length).to.equal(2);
    });

    it("we should have 2 active items", async () => {
        const processingDate = new Date();
        const stations = [
            {
                id: 1,
                number: "0022/ 001",
                name: "Přátelství 356/61",
                access: "neznámá dostupnost",
                location: "outdoor",
                has_sensor: false,
                changed: "2021-09-30T06:22:27.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: "-731175.14",
                coordinate_lon: "-1050805.71",
                geom: {
                    type: "Point",
                    coordinates: [9.313658638379719, 52.379484438973755],
                },
                active: true,
                form_hash: "11fdf61c-abde-333f-20b1-443c63889c64",
                valid_from: processingDate.toISOString(),
            },
            {
                id: 25,
                number: "0022/ 002",
                name: "Pstružná 821/2",
                access: "obyvatelům domu",
                location: "outdoor",
                has_sensor: true,
                changed: "2021-08-03T16:23:55.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: "-731999.974327",
                coordinate_lon: "-1050404.54492",
                geom: {
                    type: "Point",
                    coordinates: [9.321902057986193, 52.37296489565013],
                },
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                valid_from: processingDate.toISOString(),
            },
        ];

        const changedRecords = await repository.updateActive(stations);
        await repository.updateValidTo(changedRecords, processingDate, TableIdEnum.KsnkoId);
        const records = await repository.find({ where: { id: { [Op.in]: [1, 25] } }, raw: true });
        console.log(records);
        const activeRecords = records.filter((el: IKsnkoStation) => !el.valid_to).map((el: IKsnkoStation) => el.id);
        const historicalRecords = records
            .filter((el: IKsnkoStation & { is_historical: boolean }) => el.is_historical)
            .map((el: IKsnkoStation) => el.id);
        expect(activeRecords.length).to.equal(2);
        expect(historicalRecords.length).to.equal(3);
    });
});
