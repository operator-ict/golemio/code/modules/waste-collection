import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { SensorPicksRepository } from "#ie/repositories/SensorPicksRepository";

chai.use(chaiAsPromised);

describe("SensorPicksRepository", () => {
    let model: SensorPicksRepository;

    before(() => {
        PostgresConnector.connect();
        model = new SensorPicksRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionSensorPicksRepository");
    });
});
