import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { SensorHistoryRepository } from "#ie/repositories/SensorHistoryRepository";

chai.use(chaiAsPromised);

describe("SensorHistoryRepository", () => {
    let model: SensorHistoryRepository;

    before(() => {
        PostgresConnector.connect();
        model = new SensorHistoryRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionSensorHistoryRepository");
    });

    it("should have updateHistory method", async () => {
        expect(model.updateHistory).not.to.be.undefined;
    });
});
