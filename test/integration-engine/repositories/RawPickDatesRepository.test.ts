import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { RawPickDatesRepository } from "#ie/repositories/RawPickDatesRepository";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { Op } from "@golemio/core/dist/shared/sequelize";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("RawPickDatesRepository", () => {
    let repository: RawPickDatesRepository;
    const processingDate = new Date();
    const mockupData = [
        {
            date: "2022-08-09T12:42:17.291Z",
            subject_id: 999998,
            count: 0.0,
            container_type: "abc",
            container_volume: 0,
            frequency: "1",
            street_name: "Dělnická 2",
            trash_type: "p",
            trash_type_code: "P",
            trash_type_name: "Papír",
            orientation_number: 123,
            address_char: "abcd",
            conscription_number: 123.0,
            valid_from: processingDate.toISOString(),
            station_number: "456789",
            year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
            year: 2022,
            days: "Po",
            isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
            company: "Pražské služby",
            form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
            psas_valid_from: "2022-08-09T12:42:17.291Z",
            psas_valid_to: "2022-08-09T12:42:17.291Z",
        } as IRawPickDates,
    ];
    before(() => {
        PostgresConnector.connect();
        repository = new RawPickDatesRepository();
    });

    after(async () => {
        await repository["sequelizeModel"].destroy({
            where: {
                subject_id: {
                    [Op.in]: [111, 222],
                },
            },
        });
    });

    it("should have name", async () => {
        expect(repository.name).not.to.be.undefined;
        expect(repository.name).is.equal("WasteCollectionRawPickDatesRepository");
    });

    it("should load example item", async () => {
        const result = await repository["sequelizeModel"].findOne<RawPickDates>({
            where: { subject_id: 9553 },
        });
        expect(result?.street_name).to.equal("Dělnická");
    });

    it("should validate item", async () => {
        expect(await repository.validate(mockupData)).to.equal(true);
    });

    it("should save item", async () => {
        await repository.saveBulk(mockupData);
        const result = await repository["sequelizeModel"].findOne<RawPickDates>({
            where: { subject_id: 999998 },
        });
        expect(result?.street_name).to.equal("Dělnická 2");
    });

    it("saving data with identical hash", async () => {
        const processingDate1 = new Date();
        const processingDate2 = new Date();
        const data1: IRawPickDates[] = [
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 111,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate1.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: "2022-08-09T12:42:17.291Z",
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 222,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate1.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "30de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: "2022-08-09T12:42:17.291Z",
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
        ];
        const data2: IRawPickDates[] = [
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 111,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate2.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: processingDate2.toISOString(),
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 222,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate2.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "30de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: processingDate2.toISOString(),
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
        ];

        const result1 = await repository.saveBulk(data1);
        const result2 = await repository.saveBulk(data2);

        await repository.updateValidTo(result1!, processingDate1, TableIdEnum.RawPickDatesId);
        await repository.updateValidTo(result2!, processingDate2, TableIdEnum.RawPickDatesId);

        const updatedData = await repository.find({
            where: {
                subject_id: {
                    [Op.in]: ["111", "222"],
                },
            },
        });

        expect(updatedData).to.be.an("array");
        expect(updatedData).to.have.lengthOf(2);
        expect(updatedData.filter((item: IRawPickDates) => item.valid_to)).to.have.lengthOf(0);
    });

    it("saving data with DIFFERENT hash (should create 2 new items)", async () => {
        const processingDate = new Date();
        const data: IRawPickDates[] = [
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 111,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "20de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: "2022-08-09T12:42:17.291Z",
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 222,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "10de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: "2022-08-09T12:42:17.291Z",
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
        ];

        const result = await repository.saveBulk(data);
        await repository.updateValidTo(result!, processingDate, TableIdEnum.RawPickDatesId);
        const updatedData = await repository.query(
            "SELECT * FROM waste_collection.raw_pick_dates_history WHERE subject_id IN (111, 222)"
        );

        expect(updatedData[0]).to.be.an("array");
        expect(updatedData[0]).to.have.lengthOf(4);
        expect((updatedData[0] as IRawPickDates[]).filter((item: IRawPickDates) => item.valid_to)).to.have.lengthOf(2);
    });

    it("check if the date is correct", async () => {
        const processingDate = new Date();
        const data: IRawPickDates[] = [
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 111,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "20de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: "2022-08-09T12:42:17.291Z",
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
            {
                date: "2022-08-09T12:42:17.291Z",
                subject_id: 222,
                count: 0.0,
                container_type: "abc",
                container_volume: 0,
                frequency: "1",
                street_name: "Dělnická 2",
                trash_type: "p",
                trash_type_code: "P",
                trash_type_name: "Papír",
                orientation_number: 123,
                address_char: "abcd",
                conscription_number: 123.0,
                valid_from: processingDate.toISOString(),
                station_number: "456789",
                year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
                year: 2022,
                days: "Po",
                isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
                company: "Pražské služby",
                form_hash: "10de00f1-00c7-d974-8a85-a945ad4610b5",
                psas_valid_from: "2022-08-09T12:42:17.291Z",
                psas_valid_to: "2022-08-09T12:42:17.291Z",
            },
        ];

        const result = await repository.saveBulk(data);
        await repository.updateValidTo(result!, processingDate, TableIdEnum.RawPickDatesId);
        const newItems = await repository.query(
            "SELECT * FROM waste_collection.raw_pick_dates_history WHERE subject_id = 111 AND valid_to IS NULL"
        );
        const oldItems = await repository.query(
            "SELECT * FROM waste_collection.raw_pick_dates_history WHERE subject_id = 111 AND valid_to IS NOT NULL"
        );

        expect((oldItems[0][0] as IRawPickDates).valid_to?.toString()).to.eq(
            (newItems[0][0] as IRawPickDates).valid_from?.toString()
        );
    });
});
