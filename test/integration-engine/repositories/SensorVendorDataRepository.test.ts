import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { SensorVendorDataRepository } from "#ie/repositories/SensorVendorDataRepository";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";
import { ISensorVendorData } from "#sch/definitions/SensorVendorData";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { Op } from "@golemio/core/dist/shared/sequelize";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("SensorVendorDataRepository", () => {
    let repository: SensorVendorDataRepository;

    before(() => {
        PostgresConnector.connect();
        repository = new SensorVendorDataRepository();
    });

    after(async () => {
        await repository.query(
            "DELETE FROM waste_collection.container_vendor_data_history WHERE vendor_id IN ('vendor_id1', 'vendor_id2');"
        );
    });

    it("should have name", async () => {
        expect(repository.name).not.to.be.undefined;
        expect(repository.name).is.equal("WasteCollectionSensorVendorDataRepository");
    });

    it("saving data with identical hash (0 new items)", async () => {
        const processingDate1 = new Date();
        const processingDate2 = new Date();
        const data: ISensorVendorData[] = [
            {
                vendor_id: "vendor_id1",
                sensor_id: "sensor_id1",
                network: "network1",
                bin_depth: 1,
                has_anti_noise: false,
                anti_noise_depth: 1,
                algorithm: "algorithm1",
                sensor_version: "sensor_version1",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type1",
                decrease_threshold: 1,
                pick_min_fill_level: 1,
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate1.toISOString(),
            },
            {
                vendor_id: "vendor_id2",
                sensor_id: "sensor_id2",
                network: "network2",
                bin_depth: 2,
                has_anti_noise: false,
                anti_noise_depth: 2,
                algorithm: "algorithm2",
                sensor_version: "sensor_version2",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type2",
                decrease_threshold: 2,
                pick_min_fill_level: 2,
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b6",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate1.toISOString(),
            },
        ];

        const data2: ISensorVendorData[] = [
            {
                vendor_id: "vendor_id1",
                sensor_id: "sensor_id1",
                network: "network1",
                bin_depth: 1,
                has_anti_noise: false,
                anti_noise_depth: 1,
                algorithm: "algorithm1",
                sensor_version: "sensor_version1",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type1",
                decrease_threshold: 1,
                pick_min_fill_level: 1,
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b5",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate2.toISOString(),
            },
            {
                vendor_id: "vendor_id2",
                sensor_id: "sensor_id2",
                network: "network2",
                bin_depth: 2,
                has_anti_noise: false,
                anti_noise_depth: 2,
                algorithm: "algorithm2",
                sensor_version: "sensor_version2",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type2",
                decrease_threshold: 2,
                pick_min_fill_level: 2,
                active: true,
                form_hash: "40de00f1-00c7-d974-8a85-a945ad4610b6",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate2.toISOString(),
            },
        ];

        const result1 = await repository.bulkSave(data);
        const result2 = await repository.bulkSave(data2);

        await repository.updateValidTo(result1, processingDate1, TableIdEnum.SensorVendorDataId);
        await repository.updateValidTo(result2, processingDate2, TableIdEnum.SensorVendorDataId);
        const updatedData = await repository.find({
            where: {
                vendor_id: {
                    [Op.in]: ["vendor_id1", "vendor_id2"],
                },
            },
        });

        expect(updatedData).to.be.an("array");
        expect(updatedData).to.have.lengthOf(2);
        expect(updatedData.filter((item: ISensorVendorData) => item.valid_to)).to.have.lengthOf(0);
    });

    it("saving data with DIFFERENT hash (should create 2 new items)", async () => {
        const processingDate = new Date();
        const data: ISensorVendorData[] = [
            {
                vendor_id: "vendor_id1",
                sensor_id: "sensor_id1",
                network: "network177",
                bin_depth: 1,
                has_anti_noise: false,
                anti_noise_depth: 1,
                algorithm: "algorithm1",
                sensor_version: "sensor_version1",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type1",
                decrease_threshold: 1,
                pick_min_fill_level: 1,
                active: true,
                form_hash: "30de00f1-00c7-d974-8a85-a945ad4610b5",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate.toISOString(),
            },
            {
                vendor_id: "vendor_id2",
                sensor_id: "sensor_id2",
                network: "network177",
                bin_depth: 2,
                has_anti_noise: false,
                anti_noise_depth: 2,
                algorithm: "algorithm2",
                sensor_version: "sensor_version2",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type2",
                decrease_threshold: 2,
                pick_min_fill_level: 2,
                active: true,
                form_hash: "30de00f1-00c7-d974-8a85-a945ad4610b6",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate.toISOString(),
            },
        ];

        const result = await repository.bulkSave(data, undefined, true);

        await repository.updateValidTo(result, processingDate, TableIdEnum.SensorVendorDataId);
        const updatedData = await repository.query(
            "SELECT * FROM waste_collection.container_vendor_data_history WHERE vendor_id IN ('vendor_id1', 'vendor_id2');"
        );

        expect(updatedData[0]).to.be.an("array");
        expect(updatedData[0]).to.have.lengthOf(4);
        expect((updatedData[0] as ISensorVendorData[]).filter((item: ISensorVendorData) => item.valid_to)).to.have.lengthOf(2);
    });

    it("check if the date is correct", async () => {
        const processingDate = new Date();
        const data: ISensorVendorData[] = [
            {
                vendor_id: "vendor_id1",
                sensor_id: "sensor_id1",
                network: "network177",
                bin_depth: 1,
                has_anti_noise: false,
                anti_noise_depth: 1,
                algorithm: "algorithm1",
                sensor_version: "sensor_version1",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type1",
                decrease_threshold: 1,
                pick_min_fill_level: 1,
                active: true,
                form_hash: "30de00f1-00c7-d974-8a85-a945ad4610b5",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate.toISOString(),
            },
            {
                vendor_id: "vendor_id2",
                sensor_id: "sensor_id2",
                network: "network177",
                bin_depth: 2,
                has_anti_noise: false,
                anti_noise_depth: 2,
                algorithm: "algorithm2",
                sensor_version: "sensor_version2",
                is_sensitive_to_pickups: true,
                pickup_sensor_type: "pickup_sensor_type2",
                decrease_threshold: 2,
                pick_min_fill_level: 2,
                active: true,
                form_hash: "30de00f1-00c7-d974-8a85-a945ad4610b6",
                ksnko_container_id: null,
                vendor: "",
                prediction: null,
                installed_at: null,
                installed_by: null,
                schedule: null,
                bin_brand: null,
                valid_from: processingDate.toISOString(),
            },
        ];

        const result = await repository.bulkSave(data, SensorVendorDataModel.attributeUpdateList, true);
        await repository.updateValidTo(result, processingDate, TableIdEnum.SensorVendorDataId);
        const newItems = await repository.query(
            "SELECT * FROM waste_collection.container_vendor_data_history WHERE vendor_id = 'vendor_id1' AND valid_to IS NULL"
        );
        const oldItems = await repository.query(
            "SELECT * FROM waste_collection.container_vendor_data_history WHERE vendor_id = 'vendor_id1' AND valid_to IS NOT NULL"
        );
        expect((oldItems[0][0] as ISensorVendorData).valid_to?.toString()).to.eq(
            (newItems[0][0] as ISensorVendorData).valid_from?.toString()
        );
    });
});
