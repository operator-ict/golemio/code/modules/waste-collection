import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ContainersChangesRepository } from "#ie/repositories/ContainersChangesRepository";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { rawContainerChangesTransformation } from "test/data/raw-containerChanges-transformation";

chai.use(chaiAsPromised);

describe("KsnkoItemsRepository", () => {
    let repository: ContainersChangesRepository;

    before(() => {
        repository = WasteCollectionContainer.resolve<ContainersChangesRepository>(ContainersChangesRepository);
    });

    it("should have tableName, schema, validator", async () => {
        expect(repository.tableName).not.to.be.undefined;
        expect(repository.schema).not.to.be.undefined;
        expect(repository.validator).not.to.be.undefined;
    });

    it("should have replaceWithNewData method", async () => {
        expect(repository.replaceWithNewData).not.to.be.undefined;
    });

    it("should validate data", async () => {
        expect(await repository.validator.Validate([rawContainerChangesTransformation])).to.be.true;
    });

    it("should err on validating data", async () => {
        await expect(repository.validator.Validate([{ a: "b" }])).to.be.rejectedWith(
            Error,
            // eslint-disable-next-line max-len
            `validation failed [{"keyword":"required","dataPath":"[0]","schemaPath":"#/items/required","params":{"missingProperty":"subject_id"},"message":"should have required property \'subject_id\'"}]`
        );
    });
});
