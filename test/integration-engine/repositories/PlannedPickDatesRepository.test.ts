import { PlannedPickDatesRepository } from "#ie/repositories/PlannedPickDatesRepository";
import IPlannedPickDates from "#sch/definitions/models/interfaces/IPlannedPickDates";
import PlannedPickDates from "#sch/definitions/models/PlannedPickDatesModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("PlannedPickDatesRepository", () => {
    let repo: PlannedPickDatesRepository;

    before(() => {
        PostgresConnector.connect();
        repo = new PlannedPickDatesRepository();
    });

    it("should have name", async () => {
        expect(repo.name).not.to.be.undefined;
        expect(repo.name).is.equal("WasteCollectionPlannedPickDatesRepository");
    });

    it("should load example item", async () => {
        const plannedPickDate = await repo["sequelizeModel"].findOne<PlannedPickDates>({
            where: { ksnko_container_id: 9553 },
        });
        expect((plannedPickDate?.pick_date as Date).toISOString()).to.equal("2022-08-09T08:00:00.000Z");
    });

    it("should validate item", async () => {
        expect(
            await repo.validate([
                {
                    ksnko_container_id: 1,
                    pick_date: new Date().toISOString(),
                } as IPlannedPickDates,
            ])
        ).to.equal(true);
        expect(
            repo.validate([
                {
                    ksnko_container_id: 1,
                    pick_date: "xxx",
                } as IPlannedPickDates,
            ])
        ).eventually.throws(GeneralError);
    });

    it("should save item", async () => {
        await repo.saveBulk([{ ksnko_container_id: 99999998, pick_date: "2022-01-01T08:00:00.000Z" }]);
        const loadedPickDate = await repo["sequelizeModel"].findOne<PlannedPickDates>({
            where: { ksnko_container_id: 99999998 },
        });
        expect((loadedPickDate?.pick_date as Date).toISOString()).to.equal("2022-01-01T08:00:00.000Z");
    });
});
