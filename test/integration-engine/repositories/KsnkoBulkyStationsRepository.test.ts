import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { KsnkoBulkyStationsRepository } from "#ie/repositories/KsnkoBulkyStationsRepository";

chai.use(chaiAsPromised);

describe("KsnkoBulkyStationsRepository", () => {
    let model: KsnkoBulkyStationsRepository;

    before(() => {
        PostgresConnector.connect();
        model = new KsnkoBulkyStationsRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionKsnkoBulkyWasteStationsRepository");
    });

    it("should have cleanFuturePickDates and saveBulk method", async () => {
        expect(model.cleanFuturePickDates).not.to.be.undefined;
        expect(model.bulkSave).not.to.be.undefined;
    });

    it("should save data in db", async () => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        const items = [
            {
                id: 83217,
                pick_date: "2023-12-28T00:00:00.000Z",
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Plzeňská x U Zámečnice",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 6,
                city_district_name: "Praha 5",
                city_district_ruian: "500143",
                coordinate_lat: -746766.020823,
                coordinate_lon: -1044388.20638,
                geom: {
                    type: "Point",
                    coordinates: [14.369120828676682, 50.070353664285754],
                },
                changed_at: "2023-09-07T15:07:05.000Z",
            },
            {
                id: 83216,
                pick_date: "2023-12-27T00:00:00.000Z",
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Pražského 608",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 6,
                city_district_name: "Praha 5",
                city_district_ruian: "500143",
                coordinate_lat: -746268.664021,
                coordinate_lon: -1048259.41445,
                geom: {
                    type: "Point",
                    coordinates: [14.38340279184206, 50.036486879626224],
                },
                changed_at: "2023-09-07T15:07:05.000Z",
            },
        ];
        await model.truncate();
        await model.cleanFuturePickDates(t);

        await model.bulkSave(items, undefined, false, false, t);
        await t.commit();
        const records = await model.find({ raw: true });

        expect(records.length).to.equal(2);
        const ids = items.map((item) => item.id);
        expect(ids).to.have.members([83217, 83216]);
    });

    it("should not delete old data in db base with an old pick date", async () => {
        const futureDate: string = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
        const today = new Date().toISOString();

        const connection = PostgresConnector.getConnection();

        const newData = [
            {
                id: 83217,
                customId: futureDate + " 14:00 ID:83217",
                pick_date: futureDate,
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Plzeňská x U Zámečnice",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 6,
                city_district_name: "Praha 5",
                city_district_ruian: "500143",
                coordinate_lat: -746766.020823,
                coordinate_lon: -1044388.20638,
                geom: {
                    type: "Point",
                    coordinates: [14.369120828676682, 50.070353664285754],
                },
                changed_at: today,
            },
            {
                id: 83216,
                customId: futureDate + " 14:00 ID:83216",
                pick_date: futureDate,
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Pražského 608",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 6,
                city_district_name: "Praha 5",
                city_district_ruian: "500143",
                coordinate_lat: -746268.664021,
                coordinate_lon: -1048259.41445,
                geom: {
                    type: "Point",
                    coordinates: [14.38340279184206, 50.036486879626224],
                },
                changed_at: today,
            },
        ];
        const oldData = [
            {
                id: 83217,
                customId: today + " 14:00 ID:83217",
                pick_date: today,
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Plzeňská x U Zámečnice",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 6,
                city_district_name: "Praha 5",
                city_district_ruian: "500143",
                coordinate_lat: -746766.020823,
                coordinate_lon: -1044388.20638,
                geom: {
                    type: "Point",
                    coordinates: [14.369120828676682, 50.070353664285754],
                },
                changed_at: today,
            },
            {
                id: 83216,
                customId: today + " 14:00 ID:83216",
                pick_date: today,
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Pražského 608",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 6,
                city_district_name: "Praha 5",
                city_district_ruian: "500143",
                coordinate_lat: -746268.664021,
                coordinate_lon: -1048259.41445,
                geom: {
                    type: "Point",
                    coordinates: [14.38340279184206, 50.036486879626224],
                },
                changed_at: today,
            },
        ];
        await model.truncate();
        try {
            const t1 = await connection.transaction();
            await model.cleanFuturePickDates(t1);
            await model.bulkSave(oldData, undefined, false, false, t1);
            await t1.commit();
            const records1 = await model.find({ raw: true });
            expect(records1.length).to.equal(2);

            const t2 = await connection.transaction();
            await model.cleanFuturePickDates(t2);
            await model.bulkSave(newData, undefined, false, false, t2);
            await t2.commit();
            const records2 = await model.find({ raw: true });
            expect(records2.length).to.equal(4);

            const t3 = await connection.transaction();
            await model.cleanFuturePickDates(t3);
            await model.bulkSave(newData, undefined, false, false, t3);
            await t3.commit();
            const records3 = await model.find({ raw: true });
            expect(records3.length).to.equal(4);
        } catch (e) {
            console.log(e);
        }
    });
});
