import { ITopDownloadHistoryRepository } from "#ie/repositories/itop/ITopDownloadHistory";
import { SensorCycleITopRepository } from "#ie/repositories/itop/SensorCycleITopRepository";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("ITopRepositories", () => {
    let historyRepository: ITopDownloadHistoryRepository;
    let sensorCycleRepository: SensorCycleITopRepository;

    before(() => {
        PostgresConnector.connect();
        historyRepository = new ITopDownloadHistoryRepository();
        sensorCycleRepository = new SensorCycleITopRepository();
    });

    it("should have name", async () => {
        expect(historyRepository.name).not.to.be.undefined;
        expect(historyRepository.name).is.equal("WasteCollectionITopDownloadHistoryRepository");
        expect(sensorCycleRepository.name).not.to.be.undefined;
        expect(sensorCycleRepository.name).is.equal("WasteCollectionSensorCycleITopRepository");
    });
});
