import { WasteCollection } from "#sch";
import IRawPickDatesInput from "#sch/datasources/psas/IRawPickDatesInput";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("Test validator for RawPickDates", () => {
    let inputData: {
        data: IRawPickDatesInput[];
    };

    before(() => {
        inputData = JSON.parse(fs.readFileSync(__dirname + "/../data/raw-pickdates-datasource.json").toString("utf8"));
    });

    it("input data should be valid", async () => {
        const inputDataValidator = new JSONSchemaValidator(
            WasteCollection.datasources.rawPickDates.name,
            WasteCollection.datasources.rawPickDates.jsonSchema
        );
        const data = inputData.data;
        const result = await inputDataValidator.Validate(data);
        expect(result).to.equal(true);
    });
});
