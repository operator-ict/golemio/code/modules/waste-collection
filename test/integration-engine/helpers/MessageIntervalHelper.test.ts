import { expect } from "chai";
import { createSandbox, SinonFakeTimers, SinonSandbox, useFakeTimers } from "sinon";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";

describe("MessageIntervalHelper", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    const now = new Date("2022-09-15T12:50:21.123Z");

    beforeEach(() => {
        sandbox = createSandbox();
        clock = useFakeTimers({
            now,
            shouldAdvanceTime: true,
        });
    });

    afterEach(() => {
        clock.restore();
        sandbox.restore();
    });

    describe("getDefaultInterval", () => {
        it("should return default 2h interval when called without parameters", () => {
            expect(MessageIntervalHelper.getDefaultInterval()).to.deep.equal({
                from: new Date("2022-09-15T10:50:21.123Z"),
                to: now,
            });
        });
    });

    describe("getMessageInterval", () => {
        it("should return correct time interval object", () => {
            expect(
                MessageIntervalHelper.getMessageInterval({ from: "2022-03-03", to: "2022-03-23", vendor: "unknown" })
            ).to.deep.equal({
                from: new Date("2022-03-03T00:00:00.000Z"),
                to: new Date("2022-03-23T00:00:00.000Z"),
            });
        });

        it("should fail for incorrect input", () => {
            const message = { from: "banana", to: "2022-03-23", vendor: "unknown" };
            expect(() => MessageIntervalHelper.getMessageInterval(message)).to.throw(/Error while parsing message input data/);
        });
    });

    describe("generateWeekIntervals", () => {
        it("should return one time interval if span is shorter than/equal to max duration", async () => {
            const intervals = MessageIntervalHelper.generateWeekIntervals(
                DateTime.fromISO("2022-09-14T12:23:12.123Z"),
                DateTime.fromISO("2022-09-16T02:57:43.321Z")
            );
            expect(intervals.length).equal(1);
            expect(intervals).to.eql([
                {
                    from: new Date("2022-09-14T12:23:12.123Z"),
                    to: new Date("2022-09-16T02:57:43.321Z"),
                },
            ]);
        });

        it("should return multiple time intervals if span is greater than max duration", async () => {
            const intervals = MessageIntervalHelper.generateWeekIntervals(
                DateTime.fromISO("2022-08-22T12:23:12.123Z"),
                DateTime.fromISO("2022-09-11T02:57:43.321Z")
            );
            expect(intervals.length).equal(3);
            expect(intervals).to.eql([
                {
                    from: new Date("2022-08-22T12:23:12.123Z"),
                    to: new Date("2022-08-29T12:23:12.123Z"),
                },
                {
                    from: new Date("2022-08-29T12:23:12.123Z"),
                    to: new Date("2022-09-05T12:23:12.123Z"),
                },
                {
                    from: new Date("2022-09-05T12:23:12.123Z"),
                    to: new Date("2022-09-11T02:57:43.321Z"),
                },
            ]);
        });
    });
});
