import fs from "fs";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { SensorVendorDataTransformation } from "#ie/transformations/SensorVendorDataTransformation";
import { ISensorVendorDataInput } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { sensoneoVendorDataTransformedDataFixture } from "../../data/sensoneo-sensor-vendor-data-transformation";

chai.use(chaiAsPromised);

describe("SensorVendorDataTransformation", () => {
    let transformation: SensorVendorDataTransformation;
    let sourceData: { container_vendor_data: ISensorVendorDataInput[] };

    before(() => {
        transformation = new SensorVendorDataTransformation("Sensoneo", new Date("2022-06-15T22:00:00.000Z"));
        sourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/sensoneo-sensor-vendor-data-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SensoneoSensorVendorDataDataSourceTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(sourceData.container_vendor_data);
        expect(data).to.deep.equal(sensoneoVendorDataTransformedDataFixture);
    });
});
