import { IKsnkoBulkyWasteStationsJsonSchema } from "#sch/datasources/ksnko/KsnkoBulkyWasteStationsJsonSchema";
import { KsnkoBulkyContainersTransformation } from "./../../../src/integration-engine/transformations/KsnkoBulkyContainersTransformation";
import fs from "fs";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { kSNKBulkyContainersTransformedData } from "../../data/knsko-bulky-containers-transformation";

chai.use(chaiAsPromised);

describe("Ksnko Bulky Containers Transformation", () => {
    let transformation: KsnkoBulkyContainersTransformation;
    let sourceData: { data: IKsnkoBulkyWasteStationsJsonSchema[] };

    before(() => {
        transformation = new KsnkoBulkyContainersTransformation();
        sourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/ksnko-bulky-containers-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("WasteCollectionKsnkoBulkyContainersTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(sourceData.data);

        expect(data).to.deep.equal(kSNKBulkyContainersTransformedData);
    });
});
