import fs from "fs";
import path from "path";
import { PsasRealPicksDatesTransformation } from "#ie/transformations/PsasRealPicksDatesTransformation";
import { psasRealPickDatesTransformed } from "../../data/psas-real-picks-dates-transformed";
import { expect } from "chai";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";

describe("PsasRealPicksDatesTransformation", () => {
    let transformation: PsasRealPicksDatesTransformation;

    before(async () => {
        const trashTypeMappingRepository = new (class DummyClass {
            public findByPsasCode(psasCode: string) {
                return {
                    code_ksnko: "sc",
                    name: "Čiré sklo",
                };
            }
        })() as unknown as TrashTypeMappingRepository;
        transformation = new PsasRealPicksDatesTransformation(trashTypeMappingRepository);
    });

    it("transforms data correctly", () => {
        const input = JSON.parse(fs.readFileSync(path.join(__dirname, "../../data/psas-real-picks-dates-data.json"), "utf-8"));
        const actual = transformation.transformArray(input.data);
        expect(actual).to.deep.eq(psasRealPickDatesTransformed);
    });
});
