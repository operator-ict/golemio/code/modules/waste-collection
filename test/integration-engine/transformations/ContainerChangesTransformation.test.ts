import { WasteCollectionContainer } from "#ie/ioc/Di";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { ContainerChangesTransformation } from "#ie/transformations/ContainerChangesTransformation";
import { IRawChangedContainersInput } from "#sch/datasources/psas/IRawChangedContainersInput";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { rawContainerChangesTransformation } from "test/data/raw-containerChanges-transformation";

chai.use(chaiAsPromised);

describe("ContainerChangesTransformation", () => {
    let transformation: ContainerChangesTransformation;
    let sourceData: {
        data: IRawChangedContainersInput[];
    };

    before(async () => {
        transformation = new ContainerChangesTransformation();
        await WasteCollectionContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).connect();
        await TrashTypeMappingRepository.getInstance().loadCache();
        sourceData = JSON.parse(fs.readFileSync(__dirname + "/../../data/raw-containerChanges-datasource.json").toString("utf8"));
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("ContainerChangesTransformation");
    });

    it("should properly transform data", async () => {
        const data = transformation.transformArray(sourceData.data);
        data[0].created_at = "2023-06-07T00:00:00.000+02:00";
        expect(data[0]).to.deep.equal(rawContainerChangesTransformation);
    });
});
