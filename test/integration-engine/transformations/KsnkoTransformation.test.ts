import fs from "fs";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { KsnkoTransformation } from "#ie/transformations/KsnkoTransformation";
import { IKsnkoStationInput } from "#sch/datasources/ksnko/KsnkoStationJsonSchema";
import { ksnkoStationsTransformedDataFixture } from "../../data/ksnko-stations-transformation";

chai.use(chaiAsPromised);

describe("KsnkoStationsTransformation", () => {
    let transformation: KsnkoTransformation;
    let sourceData: {
        data: IKsnkoStationInput[];
    };

    before(() => {
        transformation = new KsnkoTransformation(new Date("2025-01-16T10:41:20.011Z"));
        sourceData = JSON.parse(fs.readFileSync(__dirname + "/../../data/ksnko-stations-datasource.json").toString("utf8"));
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("KsnkoStationsDataSourceTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(sourceData.data);
        expect(data.stations).to.deep.equal(ksnkoStationsTransformedDataFixture.stations);
        expect(data.items).to.deep.equal(ksnkoStationsTransformedDataFixture.items);
        expect(data.containers).to.deep.equal(ksnkoStationsTransformedDataFixture.containers);
    });
});
