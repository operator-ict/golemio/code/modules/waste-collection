import { VenzeoTransformation } from "#ie/transformations/VenzeoTransformation";
import { ITransformedDataVenzeo } from "#sch/definitions/ITransformedDataVenzeo";
import { IInputDataVenezeo } from "#sch/definitions/IVenzeoInput";
import { expect } from "chai";

describe("VenzeoTransformation", () => {
    let transformation: VenzeoTransformation;

    beforeEach(() => {
        transformation = new VenzeoTransformation();
    });

    it("should transform valid input data correctly", () => {
        const input: IInputDataVenezeo[] = [
            {
                id: 85293,
                pick_date: "2023-12-30",
                pick_time_from: "09:00:00",
                pick_time_to: "13:00:00",
                street: "U Rajské zahrady x Vlkova",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 3,
                city_district_name: "Praha 3",
                city_district_ruian: "500097",
                city_district_slug: "praha-3",
                coordinate_lat: "-741291.903159",
                coordinate_lon: "-1043752.71104",
                geom: {
                    crs: { type: "name", properties: { name: "EPSG:4326" } },
                    type: "Point",
                    coordinates: [14.443673271704977, 50.08273347451379],
                },
                changed_at: "2023-10-25T12:41:27.000Z",
                custom_id: "2023-12-30 09:00 ID:85293",
                created_at: "2023-10-25T20:00:10.288Z",
                updated_at: "2023-10-25T20:00:10.288Z",
            },
            {
                id: 85291,
                pick_date: "2023-12-28",
                pick_time_from: "14:00:00",
                pick_time_to: "18:00:00",
                street: "Soběslavská x Hollarovo náměstí",
                payer: "municipality",
                number_of_containers: 1,
                service_id: 1,
                service_code: "VOK",
                service_name: "VOK",
                trash_type_id: null,
                trash_type: null,
                trash_type_name: null,
                city_district_id: 3,
                city_district_name: "Praha 3",
                city_district_ruian: "500097",
                city_district_slug: "praha-3",
                coordinate_lat: "-739018.048305",
                coordinate_lon: "-1044751.35408",
                geom: {
                    crs: { type: "name", properties: { name: "EPSG:4326" } },
                    type: "Point",
                    coordinates: [14.477044689979657, 50.076614242443746],
                },
                changed_at: "2023-10-25T12:41:27.000Z",
                custom_id: "2023-12-28 14:00 ID:85291",
                created_at: "2023-10-25T20:00:10.288Z",
                updated_at: "2023-10-25T20:00:10.288Z",
            },
        ];
        const expectedOutput: ITransformedDataVenzeo[] = [
            {
                customId: "2023-12-30 09:00 ID:85293",
                name: "VOK U Rajské zahrady x Vlkova 09:00-13:00",
                note: null,
                location: { latitude: 50.08273347451379, longitude: 14.443673271704977, altitude: null },
                address: {
                    street: "U Rajské zahrady x Vlkova",
                    city: "Praha 3",
                    country: "Czech Republic",
                    aa1: null,
                    aa2: null,
                },
            },
            {
                customId: "2023-12-28 14:00 ID:85291",
                name: "VOK Soběslavská x Hollarovo náměstí 14:00-18:00",
                note: null,
                location: { latitude: 50.076614242443746, longitude: 14.477044689979657, altitude: null },
                address: {
                    street: "Soběslavská x Hollarovo náměstí",
                    city: "Praha 3",
                    country: "Czech Republic",
                    aa1: null,
                    aa2: null,
                },
            },
        ];
        const transformedData = transformation.transformArray(input);
        expect(transformedData).deep.equal(expectedOutput);
    });
});
