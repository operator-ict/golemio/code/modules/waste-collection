import { ITopTransformation } from "#ie/transformations/ITopTransformation";
import { ISensorCycle } from "#sch/definitions/models/itop/ISensorCycle";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("ITopTransformation", () => {
    let sandbox: SinonSandbox;
    let testData: any;

    before(() => {
        sandbox = sinon.createSandbox();
        sandbox.useFakeTimers({ now: new Date("2023-03-30T14:31:16.726Z"), shouldAdvanceTime: false });
        testData = JSON.parse(fs.readFileSync(__dirname + "/../../data/raw-itop-input-data.json").toString("utf8"));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("data transformed", async () => {
        const result: ISensorCycle[] = await new ITopTransformation().transform(testData);

        expect(result.length).to.equal(1);
        expect(result[0]).to.deep.equal({
            sensor_id: "1F834A3",
            description:
                "Reinstalace IS + Očištění snímačů + Restart senzoru, kontrola měření v aplikaci Sensoneo (WMS) - 16% - Ok; ",
            status: "production",
            location: "C20614",
            vendor: "Sensoneo",
            model: "Single sensor v3",
            monitored: true,
            network: "sigfox",
            lora_id: "",
            nbiot_id: "",
            sigfox_id: "1F834A3",
            serial_number: "1F834A3",
            asset_number: "1F834A3",
            purchase_date: "2021-05-11",
            install_date: "2021-01-15",
            production_date: "2021-01-15",
            end_of_warranty: "2023-05-10",
            end_of_warranty_mhmp: "2023-05-11",
            service_date: "2023-01-24",
            record_date: "2023-03-30T14:31:16.726Z",
            external_form_id: 1384,
            form_hash: "9894e4fc8056574ba73e154de43366f60baddaeb83a7bcd68a0857b6138d3350",
        });
    });
});
