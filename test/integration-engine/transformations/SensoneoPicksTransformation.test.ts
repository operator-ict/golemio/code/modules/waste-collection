import fs from "fs";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { SensorPicksTransformation } from "#ie/transformations/SensorPicksTransformation";
import { ISensorPicksInput } from "#sch/datasources/sensor/SensorPicksJsonSchema";
import { sensoneoPicksTransformedDataFixture } from "../../data/sensoneo-sensor-picks-transformation";

chai.use(chaiAsPromised);

describe("SensorPicksTransformation", () => {
    let transformation: SensorPicksTransformation;
    let sourceData: { picks: ISensorPicksInput[] };

    before(() => {
        transformation = new SensorPicksTransformation("Sensoneo");
        sourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/sensoneo-sensor-picks-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SensoneoSensorPicksDataSourceTransformation");
    });

    it("should properly transform data", async () => {
        const updatedAt = "2023-01-08T11:00:00.000Z";
        const data = await transformation.transform({ data: sourceData.picks, updatedAt });
        expect(data).to.deep.equal(sensoneoPicksTransformedDataFixture);
    });
});
