import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("TrashTypeMapper", () => {
    let trashTypeMapper: TrashTypeMappingRepository;
    before(async () => {
        await PostgresConnector.connect();
        trashTypeMapper = TrashTypeMappingRepository.getInstance();
        await trashTypeMapper.loadCache();
    });

    it("test mapping", () => {
        const result = trashTypeMapper.findByPsasCode("P");
        expect(result).to.deep.equal({
            code_ksnko: "p",
            code_psas: "P",
            name: "Papír",
            legacy_id: 5,
        });
        expect(trashTypeMapper.findByPsasCode("p")).to.equal(undefined);
    });
});
