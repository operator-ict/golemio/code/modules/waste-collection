import { HashHelper } from "#ie/helpers/HashHelper";
import { IKsnkoStationContainerInputFlattened } from "#ie/helpers/interfaces/IKsnkoStationContainerInputFlattened";
import { IStationInfo } from "#ie/helpers/interfaces/IStationInfo";
import { WasteCollectionContainer } from "#og/ioc/Di";
import { SensorVendorDataRepository } from "#og/repositories/SensorVendorDataRepository";
import { StationContainersRepository } from "#og/repositories/StationContainersRepository";
import { IKsnkoStationContainerInput, IKsnkoStationInput } from "#sch/datasources/ksnko/KsnkoStationJsonSchema";
import { ISensorVendorDataInput } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { expect } from "chai";

describe("HashHelper", () => {
    before(async () => {
        await WasteCollectionContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).connect();
    });
    it("should create an UUID", () => {
        const input: ISensorVendorDataInput = {
            ksnko_container_id: 123,
            is_sensitive_to_pickups: false,
            network: "network1",
            prediction: null,
            installed_at: "2021-01-01",
            installed_by: "user1",
            vendor_id: "vendor_id1",
            sensor_id: "sensor_id1",
            bin_depth: 1,
            has_anti_noise: false,
            anti_noise_depth: 1,
            algorithm: "algorithm1",
            schedule: "schedule1",
            decrease_threshold: 1,
            pick_min_fill_level: 1,
            active: true,
            bin_brand: "brand1",
            version: "",
            pick_up_sensor_type: "",
        };

        const filterList: Array<keyof ISensorVendorDataInput | keyof { isHistorical: boolean }> = [
            "prediction",
            "installed_at",
            "installed_by",
            "network",
            "isHistorical",
        ];
        const hash = HashHelper.createCryptoHash({ ...input, isHistorical: false }, filterList);

        expect(hash).to.be.a("string");
        expect(hash.length).to.equal(36); // UUID length -> 32 of md5 + 4 dashes
    });

    it("should match SQL UUID with generated hash", async () => {
        const input: ISensorVendorDataInput = {
            vendor_id: "Sensoneo_C01381",
            ksnko_container_id: 23361,
            sensor_id: "70b3d50070007A58",
            network: "Lora",
            bin_depth: 150,
            has_anti_noise: false,
            anti_noise_depth: null,
            algorithm: "Standard",
            is_sensitive_to_pickups: true,
            decrease_threshold: 20,
            pick_min_fill_level: 55,
            active: true,
            bin_brand: "Bammens Memphis",
            prediction: null,
            installed_at: "2019-01-13 01:00:00.000",
            installed_by: "-",
            schedule: "-",
            version: "SSV3 Lite",
            pick_up_sensor_type: "event_and_data_driven",
        };

        const filterList: Array<keyof ISensorVendorDataInput | keyof { isHistorical: boolean }> = [
            "vendor_id",
            "ksnko_container_id",
            "sensor_id",
            "network",
            "bin_depth",
            "has_anti_noise",
            "anti_noise_depth",
            "algorithm",
            "version",
            "is_sensitive_to_pickups",
            "pick_up_sensor_type",
            "decrease_threshold",
            "pick_min_fill_level",
            "active",
            "bin_brand",
            "isHistorical",
        ];
        const hash = HashHelper.createCryptoHash({ ...input, isHistorical: false }, filterList);

        const repository = new SensorVendorDataRepository();

        const result = await repository.sequelizeModel.sequelize?.query(`
            SELECT form_hash FROM waste_collection.container_vendor_data_history WHERE vendor_id = 'Sensoneo_C01381'
        `);

        expect(hash).to.be.a("string");
        expect(hash.length).to.equal(36); // UUID length -> 32 of md5 + 4 dashes
        expect(hash).to.equal((result as any)?.[0]?.[0]?.form_hash);
    });

    it("ksnko_containers hashes should match", async () => {
        const input1: IKsnkoStationContainerInput = {
            id: 101,
            volumeRatio: 0.3,
            cleaningFrequency: { code: "61", days: "1" },

            code: "code1",
            sensorId: "sensorId1",
            trashType: { code: "code1", name: "trashType1" },
            container: { name: "container1", volume: 100, brand: "brand1", dump: "dump1" },
            currentPercentFullness: 50,
        };

        const input2: IStationInfo = {
            id: 19,
            number: "1",
        };

        const filterList: Array<keyof IKsnkoStationContainerInputFlattened> = [
            "id",
            "volumeRatio",
            "cleaningFrequencyCode",
            "stationInfoId",
            "active",
            "isHistorical",
        ];

        const hash = HashHelper.createCryptoHash(
            {
                ...input1,
                stationInfoId: input2.id,
                cleaningFrequencyCode: input1.cleaningFrequency.code,
                active: true,
                isHistorical: false,
            },
            filterList
        );

        const repository = new StationContainersRepository();
        const result = await repository.sequelizeModel.sequelize?.query(`
            SELECT form_hash FROM waste_collection.ksnko_containers_history WHERE id = '101'
         `);

        expect(hash).to.be.a("string");
        expect(hash.length).to.equal(36); // UUID length -> 32 of md5 + 4 dashes
        expect(hash).to.equal((result as any)?.[0]?.[0]?.form_hash);
    });

    it("ksnko_stations hashes should match", async () => {
        const input: IKsnkoStationInput = {
            id: 1,
            name: "Přátelství 356/61",
            location: "outdoor",
            number: "0022/ 001",
            access: "",
            cleaning: {
                code: null,
                days: null,
            },
            hasSensor: 0,
            cityDistrict: {
                id: 0,
                name: "",
                ruianCode: "",
            },
            coordinate: {
                lat: 0,
                lon: 0,
            },
            changed: "",
            items: [],
            containers: [],
        };

        const filterList: Array<keyof IKsnkoStationInput | keyof { active: boolean; isHistorical: boolean }> = [
            "id",
            "number",
            "name",
            "location",
            "active",
            "isHistorical",
        ];

        const hash = HashHelper.createCryptoHash({ ...input, active: true, isHistorical: false }, filterList);

        const repository = new StationContainersRepository();
        const result = await repository.sequelizeModel.sequelize
            ?.query(`SELECT form_hash FROM waste_collection.ksnko_stations WHERE id = '1'
         `);

        expect(hash).to.be.a("string");
        expect(hash.length).to.equal(36); // UUID length -> 32 of md5 + 4 dashes
        expect(hash).to.equal((result as any)?.[0]?.[0]?.form_hash);
    });
});
