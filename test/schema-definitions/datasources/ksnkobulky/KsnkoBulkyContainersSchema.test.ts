import KsnkoBulkyWasteStationsInputJsonSchema from "#sch/datasources/ksnko/KsnkoBulkyWasteStationsJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("Test validator for KSNKO Bulky Containers Datasource", () => {
    let inputData: any;

    it("input data should be valid", async () => {
        inputData = JSON.parse(
            fs.readFileSync(__dirname + "/../../../data/ksnko-bulky-containers-datasource.json").toString("utf8")
        );
        const inputDataValidator = new JSONSchemaValidator("TestKSNKOBulky", KsnkoBulkyWasteStationsInputJsonSchema.jsonSchema);

        const result = await inputDataValidator.Validate(inputData.data);
        expect(result).to.equal(true);
    });

    it("input data rejected if not correct", async () => {
        inputData = JSON.parse(
            fs.readFileSync(__dirname + "/../../../data/ksnko-bulky-containers-datasource-invalid.json").toString("utf8")
        );
        const inputDataValidator = new JSONSchemaValidator("TestKSNKOBulky", KsnkoBulkyWasteStationsInputJsonSchema.jsonSchema);

        expect(inputDataValidator.Validate(inputData.data)).rejectedWith(
            // eslint-disable-next-line max-len
            `validation failed [{"keyword":"type","dataPath":"[0].date","schemaPath":"#/items/properties/date/type","params":{"type":"string"},"message":"should be string"}]`
        );
    });
});
