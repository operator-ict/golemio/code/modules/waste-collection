# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.6.2] - 2025-03-11

### Added

- portman api test ([#10](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/10))

## [1.6.1] - 2025-03-04

### Changed

- cleaning frequency data now gathered from v_raw_pick_dates using code 99 for dynamic frequency ([p0149#398](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/398))

## [1.6.0] - 2025-02-25

-   data historization of container_vendor_data, ksnko_stations, ksnko_containers and raw_pick_dates ([p0149#386](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/386))

## [1.5.1] - 2025-02-20

### Changed

-  `qs` lib is imported from core

### Fixed

-  Fix stationsRepository to include only current pick dates ([p0149#392](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/392))

## [1.5.0] - 2025-02-06

### Added

-  PSAS real picks dates integration ([p0149#385](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/385))

## [1.4.4] - 2024-12-10

### Removed

-   remove RfidDownloadContainerVendorDataTask  and  RfidDownloadPicksTask  ([waste-collection#16](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/16))

## [1.4.3] - 2024-11-28

### Fixed

- put "payer" and "number_of_containers" to output ([#17](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/17))

## [1.4.2] - 2024-10-24

### Fixed

-   add prefixes to asyncAPI

## [1.4.1] - 2024-10-22

### Added

-   asyncapi documentation ([ie#260](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/260))

## [1.4.0] - 2024-08-29

### Added

-   sending ksnko_bulky_waste_stations data to venzeo ([p0149#308](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/308))

## [1.3.24] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.3.23] - 2024-08-13

### Fixed

-   reverted temporary hotfix from previous version

## [1.3.22] - 2024-07-23

### Fixed

-   temporary hotfix to prevent 3rd party from breaking

## [1.3.21] - 2024-07-17

### Fixed

-   api validations after adding checkExact in core ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [1.3.20] - 2024-07-15

### Fixed

-   previous version 1.3.19 caused internal errors with certain combination of parameters

## [1.3.19] - 2024-07-10

### Changed

-   mark id as deprecated, fix ksnkoid ([waste-collection#12](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/12))

## [1.3.18] - 2024-06-19

### Removed

-   remove ksnko_container_id from sensor_cycle_itop ([p0149#351]https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/351)

## [1.3.17] - 2024-06-17

### Changed

-   fixing open-data badge ([p0181#14](https://gitlab.com/operator-ict/golemio/projekty/oict/parkings/-/issues/14))

## [1.3.16] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.3.15] - 2024-04-29

### Added

-   add cache-control header to all responses ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

### Removed

-   remove redis useCacheMiddleware ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

## [1.3.14] - 2024-04-08

### Changed

-   migrate from axios to fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.3.13] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.3.12] - 2024-03-11

### Fixed

-   KSNKO stations datasource schema validation

## [1.3.11] - 2024-02-26

### Fixed

-   Fixing filter ([general#555](https://gitlab.com/operator-ict/golemio/code/general/-/issues/555))

## [1.3.10] - 2024-02-21

### Changed

-   Update express validator to 7.0.1 ([core#94](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/94))

## [1.3.9] - 2024-02-02

### Fixed

-   WasteCollection Legacy Router int query parameters validation

## [1.3.8] - 2024-01-29

### Changed

-   allow null for ksnko_container_id in SensorVendorData ([waste-collection#9](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/9))

## [1.3.7] - 2024-01-15

### Changed

-   removal of faulty mesurements for Sensority ([p0149#320](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/320))

## [1.3.6] - 2023-12-11

### Fixed

-   import_sensor_history & import_sensor_history_rfid function allows null sensor_id

## [1.3.5] - 2023-12-04

### Fixed

-   remove inactive containers from `/sortedwastestations` endpoint ([p0149#313](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/313))

## [1.3.4] - 2023-11-27

### Added

-   OG for bulky stations ([p0149#310](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/310))

### Fixed

-   data type in a column trash_type_code in a table container_changes was adjusted
-   new value in a look up table trash_type_mapping was added

## [1.3.3] - 2023-10-25

### Added

-   new datasource from ksnko regarding bulky containers ([p0149#309](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/309))

## [1.3.2] - 2023-10-16

### Changed

-   drop views in schema analytic

## [1.3.1] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.3.0] - 2023-08-02

### Changed

-   drop function analytic.waste_collection_last_correct

-   Change validation because of the new data source. ([p0149#306](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/306))

## [1.2.9] - 2023-07-26

### Changed

-   New sensoneo API ([p0149#284](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/284))

## [1.2.8] - 2023-07-12

### Added

-   new datasource from psas regarding container changes ([waste-collection#8](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/8))

## [1.2.7] - 2023-07-04

### Changed

-   change function analytic.waste_collection_last_correct (https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/296)

## [1.2.6] - 2023-06-14

### Fixed

-   Sensority datasource validation fix
-   `updateSensorsFromTo` job message validation fix

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.4] - 2023-05-29

### Changed

-   Update openapi docs

## [1.2.3] - 2023-05-22

### Changed

-   itop database changes

## [1.2.2] - 2023-05-17

### Changed

-   station number not unique, removed station_number constraints and duplicity columns ([p0149#285](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/285))

## [1.2.1] - 2023-04-19

### Added

-   integration of sensor cycle data from itop ([p0149#279](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/278))

## [1.2.0] - 2023-04-12

### Added

-   /wastecollectionlegacy - add `is_monitored` to container output

### Changed

-   /wastecollectionlegacy - sort by trash type ([p0149#267](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/267))

## [1.1.11] - 2023-04-05

### Changed

-   update rfid assigned picks view ([p0149#283](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/283))

## [1.1.10] - 2023-04-03

### Added

-   Accessibility filter for multiple values ([waste-collection#7](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/7))

## [1.1.9] - 2023-03-27

### Added

-   Parametrize sensor jobs by vendor name ([waste-collection#6](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/6))

## [1.1.8] - 2023-03-23

### Added

-   Add table and views regarding rfid picks assignment ([p0149#280](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/280))

## [1.1.7] - 2023-03-13

-   new view: v_waste_collection_containers_rfid (https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/277)

## [1.1.6] - 2023-03-13

### Added

-   Integration of second rfid vendor.

## [1.1.5] - 2023-03-06

### Changed

-   openapi docs updated

## [1.1.4] - 2023-03-01

### Changed

-   Update view: v_waste_collection_containers,v_waste_collection_containers_technical_info ([p0149#272](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/272))

## [1.1.3] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.2] - 2023-02-22

### Changed

-   Update city-districts

## [1.1.1] - 2023-02-20

### Fixed

-   VIEW analytic.v_waste_collection_containers_technical_info

## [1.1.0] - 2023-02-13

### Added

-   Rfid integration ([waste-collection#4](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/4))

## [1.0.1] - 2023-02-08

### Added

-   Extend legacy api with pick_days & next_picks attributes ([p0149#262](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/262))

## [1.0.0] - 2023-02-07

### Fixed

-   legacy output api returns district as slug ([p0149#268](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/268))

## [0.1.4] - 2023-02-06

### Added

-   Add analytic view ([p0149#266](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/266))

## [0.1.3] - 2023-02-01

### Fixed

-   Sensor picks strategy ([waste-collection#3](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/3))
-   Docs fixes

## [0.1.2] - 2023-01-30

### Fixed

-   Change analytic views ([p0149#264](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/264))

## [0.1.0] - 2023-01-23

### Fixed

-   Delete outdated pick data ([waste-collection#3](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/3))

### Changed

-   Migrate to npm

## [0.0.8] - 2023-01-16

### Fixed

-   Fix stations API sensor output
-   Fix analytic views ([p0149#255](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/255))

## [0.0.7] - 2023-01-04

### Added

-   New sensor data source (Sensority) ([p0149#254](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/254))
-   Update analytic views for all waste-collection dashboards:
    -   ([p0149#253](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/253))
    -   ([p0149#255](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/255))
-   Add analytic view that recommends changing the frequency of waste collection ([p0149#250](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/250))

### Fixed

-   Performance issues fixes
-   Ksnko has_sensor replaced with active sensor mapping

## [0.0.6] - 2022-11-29

### Added

-   Add analytic view ([p0149#249](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/249))

## [0.0.5] - 2022-11-14

### Added

Add analytic views.

## [0.0.4] - 2022-10-20

### Changed

-   Typescript upgrade to version 4.7.2
-   Pick dates are generated upto 6 months in future

### Fixed

-   Condition added to compute pick date only for future incl today.
-   trash type mapping table initialized before transformation

## [0.0.3] - 2022-09-21

### Added

-   Historic sensor data processing [p0149#232](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/232)

### Fixed

-   Generation of Planned pick dates for raw pick data without isoweeks

### Changed

-   Trash type lookup table added to database

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [0.0.2] - 2022-09-06

### Changed

-   Optimalization of import sensor history([waste-collection#1](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/1))

## [0.0.1] - 2022-08-24

### Added

-   PSAS integration ([p0149#226](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/226))
-   Sensoneo integration ([p0149#225](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/225))
-   KSNKO integration ([p0149#224](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/224))

