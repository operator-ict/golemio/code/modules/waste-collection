export const MODULE_NAME = "WasteCollection";
export const SCHEMA_NAME = "waste_collection";
export const TIMEZONE = "Europe/Prague";

export enum SensorVendorName {
    SENSONEO = "Sensoneo",
    SENSORITY = "Sensority",
}
