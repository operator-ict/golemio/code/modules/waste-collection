import { IKsnkoBulkyWasteStations } from "#sch/definitions/KsnkoBulkyWasteStations";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class BulkyWasteStationsTransformation extends AbstractTransformation<IKsnkoBulkyWasteStations, IGeoJSONFeature> {
    public name: string = "BulkyWasteStationsTransformation";

    public transformToFeatureCollection = (elements: IKsnkoBulkyWasteStations[]) => {
        try {
            return buildGeojsonFeatureCollection(this.transformArray(elements));
        } catch (err) {
            throw new GeneralError("Transformation error", this.name, err, 500);
        }
    };

    protected transformInternal = (element: IKsnkoBulkyWasteStations) => {
        const properties = {
            customId: element.custom_id,
            date: element.pick_date,
            timeFrom: element.pick_time_from,
            timeTo: element.pick_time_to,
            street: element.street,
            serviceName: element.service_name,
            payer: element.payer,
            numberOfContainers: element.number_of_containers,
            cityDistrict: element.city_district_slug,
        };

        return {
            geometry: {
                coordinates: element.geom.coordinates,
                type: element.geom.type,
            },
            properties: properties,
            type: "Feature",
        } as IGeoJSONFeature;
    };
}
