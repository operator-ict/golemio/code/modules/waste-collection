import { LegacyOutputMapper } from "#og/helpers";
import { WasteCollectionContainer } from "#og/ioc/Di";
import { PlannedPickDatesRepository } from "#og/repositories/PlannedPickDatesRepository";
import { repositories } from "#og/repositories/RepositoryManager";
import { SensorMeasurementsRepository } from "#og/repositories/SensorMeasurementsRepository";
import { SensorPicksRepository } from "#og/repositories/SensorPicksRepository";
import { StationsRepository } from "#og/repositories/StationsRepository";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { oneOf, query } from "@golemio/core/dist/shared/express-validator";

class WasteCollectionLegacyRouter extends BaseRouter {
    protected stationsRepository: StationsRepository;
    protected sensorMeasurementsRepository: SensorMeasurementsRepository;
    protected sensorPicksRepository: SensorPicksRepository;
    protected plannedPickDatesRepository: PlannedPickDatesRepository;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super();
        this.stationsRepository = repositories.stationsRepository;
        this.sensorMeasurementsRepository = repositories.sensorMeasurementsRepository;
        this.sensorPicksRepository = repositories.sensorPicksRepository;
        this.plannedPickDatesRepository = repositories.plannedPickDatesRepository;
        this.cacheHeaderMiddleware = WasteCollectionContainer.resolve<CacheHeaderMiddleware>(
            ContainerToken.CacheHeaderMiddleware
        );

        this.router.get(
            "/measurements",
            [
                query("ksnkoId").optional(),
                query("containerId").optional(),
                oneOf([
                    query("containerId").exists().isInt().not().isArray().toInt(),
                    query("ksnkoId").exists().isInt().not().isArray().toInt(),
                ]),
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            this.cacheHeaderMiddleware.getMiddleware(30 * 60, 60),
            this.GetMeasurements
        );
        this.router.get(
            "/picks",
            [
                query("ksnkoId").optional(),
                query("containerId").optional(),
                oneOf([
                    query("containerId").exists().isInt().not().isArray().toInt(),
                    query("ksnkoId").exists().isInt().not().isArray().toInt(),
                ]),
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            this.cacheHeaderMiddleware.getMiddleware(30 * 60, 60),
            this.GetPicks
        );

        this.router.get(
            "/pickdays",
            [
                query("sensoneoCode").optional().isInt().not().isArray().toInt(),
                query("ksnkoId").optional().isInt().not().isArray().toInt(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("LegacySortedWasteRouter"),
            this.cacheHeaderMiddleware.getMiddleware(6 * 60 * 60, 60),
            this.GetPickDays
        );

        this.router.get(
            "/",
            [
                query("id").optional().isInt().not().isArray().toInt(),
                query("ksnkoId").optional().isInt().not().isArray().toInt(),
                query("accessibility").optional().isInt().toInt(), // array is allowed
                query("onlyMonitored").optional().isBoolean().not().isArray(),
                query("districts").optional().not().isEmpty({ ignore_whitespace: true }), // array is allowed
                query("latlng").optional().isLatLong().not().isArray(),
                query("range").optional().isInt().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            this.cacheHeaderMiddleware.getMiddleware(30 * 60, 60),
            this.GetAll
        );
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        let districts: any = req.query.districts;
        let accessibility: any = req.query.accessibility;

        if (districts && !(districts instanceof Array)) {
            districts = districts.split(",");
        }

        if (accessibility) {
            accessibility = this.ConvertToArray(accessibility);
        }

        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);

            const data = await this.stationsRepository.GetAll({
                accessibility,
                districts,
                id: req.query.id as string,
                ksnkoId: req.query.ksnkoId as string,
                lat: coords.lat,
                lng: coords.lng,
                onlyMonitored: req.query.onlyMonitored === "true",
                range: coords.range,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
            });

            res.status(200).send(await LegacyOutputMapper.getAllMapper(data));
        } catch (err) {
            next(err);
        }
    };

    public GetMeasurements = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.sensorMeasurementsRepository.GetAll({
                containerId: req.query.containerId as string,
                ksnkoId: req.query.ksnkoId as string,
                from: req.query.from as string,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
                to: req.query.to as string,
            });

            res.status(200).send(LegacyOutputMapper.getAllMeasurementsMapper(data));
        } catch (err) {
            next(err);
        }
    };

    public GetPicks = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.sensorPicksRepository.GetAll({
                containerId: req.query.containerId as string,
                ksnkoId: req.query.ksnkoId as string,
                from: req.query.from as string,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
                to: req.query.to as string,
            });
            res.status(200).send(LegacyOutputMapper.getAllPicksMapper(data));
        } catch (err) {
            next(err);
        }
    };

    public GetPickDays = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.plannedPickDatesRepository.GetAll({
                sensoneoCode: req.query.sensoneoCode as string,
                ksnkoId: req.query.ksnkoId as string,
            });
            res.status(200).send(LegacyOutputMapper.getAllPlannedPickDatesMapper(data));
        } catch (err) {
            next(err);
        }
    };
}

const wasteCollectionLegacyRouter: Router = new WasteCollectionLegacyRouter().router;

export { wasteCollectionLegacyRouter };
