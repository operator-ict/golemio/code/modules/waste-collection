import { BulkyWasteStationsController } from "#og/controllers/BulkyWasteStationsController";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { WasteCollectionContainer } from "#og/ioc/Di";

export class BulkyWasteStationsRouter extends AbstractRouter {
    private controller: BulkyWasteStationsController;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super("v1", "bulky-waste");
        this.controller = new BulkyWasteStationsController();
        this.cacheHeaderMiddleware = WasteCollectionContainer.resolve<CacheHeaderMiddleware>(
            ContainerToken.CacheHeaderMiddleware
        );
        this.initRoutes();
    }

    protected initRoutes(expire?: string | number | undefined): void {
        this.router.get(
            "/stations",
            [
                query("latlng")
                    .optional()
                    .isString()
                    .matches(/^\d+\.?\d*(,\d+\.?\d*){1}$/)
                    .not()
                    .isArray(),
                query("range").optional().isInt().not().isArray(),
                query("districts").optional().not().isEmpty({ ignore_whitespace: true }),
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(6 * 60 * 60, 60),
            this.controller.getAll
        );
        this.router.get(
            "/stations/:customId",
            param("customId")
                .exists()
                .isString()
                .matches(/^\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{2} ID:\d+$/)
                .not()
                .isArray(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(6 * 60 * 60, 60),
            this.controller.getOne
        );
    }
}

const v1BulkyWasteStationsRouter: AbstractRouter = new BulkyWasteStationsRouter();

export { v1BulkyWasteStationsRouter };
