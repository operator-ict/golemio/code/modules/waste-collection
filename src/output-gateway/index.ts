import { v1BulkyWasteStationsRouter } from "./routers/BulkyWasteStationsRouter";

export * from "./repositories/RepositoryManager";
export * from "./routers/WasteCollectionLegacyRouter";

export const routers = [v1BulkyWasteStationsRouter];
