import { IBulkyWasteStationsParams } from "#og/helpers/params/IBulkyWasteStationsParams";
import { WasteCollectionContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { BulkyWasteStationsRepository } from "#og/repositories/BulkyWasteStationsRepository";
import { BulkyWasteStationsTransformation } from "#og/transformations/BulkyWasteStationsTransformation";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ParseDateTimeHelper } from "./helpers/ParseDateTimeHelper";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";

export class BulkyWasteStationsController {
    protected repository: BulkyWasteStationsRepository;
    protected transformation: BulkyWasteStationsTransformation;
    protected NUMBER_OF_FUTURE_DAYS = 60;
    protected DEFAULT_RANGE_IN_KM = 1;

    constructor() {
        this.repository = WasteCollectionContainer.resolve<BulkyWasteStationsRepository>(
            ModuleContainerToken.BulkyWasteStationsRepository
        );
        this.transformation = new BulkyWasteStationsTransformation();
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const params = await this.parseParams(req);
            const result = await this.repository.GetAll(params);
            const transformedResult = this.transformation.transformToFeatureCollection(result);

            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = req.params.customId;
            const result = await this.repository.GetOne(id);

            if (result === null) {
                throw new GeneralError("not_found", "BulkyWasteStationsController", undefined, 404);
            } else {
                const transformedResult = this.transformation.transformElement(result);
                res.json(transformedResult);
            }
        } catch (err) {
            next(err);
        }
    };

    private async parseParams(req: Request): Promise<Partial<IBulkyWasteStationsParams>> {
        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const result: Partial<IBulkyWasteStationsParams> = {
                districts: req.query.districts
                    ? req.query.districts instanceof Array
                        ? (req.query.districts as string[])
                        : [req.query.districts as string]
                    : undefined,
                lat: coords.lat,
                lng: coords.lng,
                range: coords.range ?? this.DEFAULT_RANGE_IN_KM,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
            };

            const fromDate = req.query.from ? new Date(req.query.from as string) : new Date();
            const fromSeparated = ParseDateTimeHelper.getDateTimeSeparately(fromDate);
            result.dateFrom = fromSeparated.date;
            result.timeFrom = fromSeparated.time;

            const toDate = req.query.to
                ? new Date(req.query.to as string)
                : dateTime(new Date()).add(this.NUMBER_OF_FUTURE_DAYS, "days").toDate();
            const toSeparated = ParseDateTimeHelper.getDateTimeSeparately(toDate);
            result.dateTo = toSeparated.date;
            result.timeTo = toSeparated.time;

            return result;
        } catch (err) {
            throw new GeneralError("Param parsing error", BulkyWasteStationsController.name, err, 500);
        }
    }
}
