import { LegacyAccessibilityMapper } from "#og/helpers";
import { TMeasurementsExtendedDataModel } from "#og/repositories/SensorMeasurementsRepository";
import { TPicksWithVendorDataModel } from "#og/repositories/SensorPicksRepository";
import { TContainersExtendedModel, TStationWithContainersModel } from "#og/repositories/StationsRepository";
import { TrashTypeMappingRepository } from "#og/repositories/TrashTypeMappingRepository";
import PlannedPickDatesModel from "#sch/definitions/models/PlannedPickDatesModel";
import { buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway";
import { GeoCoordinatesType, IGeoJSONFeature, IGeoJSONFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";

export interface IMeasurementsOutput {
    id: string;
    sensor_code: string;
    percent_calculated: number;
    upturned: number;
    temperature: number;
    battery_status: number;
    measured_at_utc: string;
    prediction_utc: string | null;
    firealarm: number;
    updated_at: number;
}

export interface IPicksOutput {
    id: string;
    sensor_code: string;
    pick_minfilllevel: number;
    decrease: number;
    pick_at_utc: string;
    percent_now: number;
    percent_before: number;
    event_driven: boolean;
    updated_at: number;
}

export interface IPlannedPickDatesOutput {
    ksnko_id: number;
    sensoneo_code: string;
    generated_dates: string[];
}

export class LegacyOutputMapper {
    public static getAllRecordMapper = async (record: TStationWithContainersModel): Promise<IGeoJSONFeature> => {
        const { containers, ...station } = record.get({ plain: true }) as unknown as TStationWithContainersModel;
        const containerMapped = await LegacyOutputMapper.getAllMapperContainers(containers);
        return {
            type: "Feature",
            geometry: {
                coordinates: station.geom.coordinates,
                type: station.geom.type as GeoCoordinatesType.Point,
            },
            properties: {
                accessibility: LegacyAccessibilityMapper.getAccessibilityByString(station.access),
                containers: containerMapped.data,
                district: station.city_district_slug,
                id: station.id,
                is_monitored: containerMapped.hasSensor,
                name: station.name,
                station_number: station.number,
                updated_at: station.updated_at,
            },
        };
    };

    public static getAllMapperContainers = async (containers: TContainersExtendedModel[]) => {
        let hasSensor = false;
        const result = [];
        const trashTypeMapping = await TrashTypeMappingRepository.getInstance();

        for (const el of containers) {
            if (!hasSensor) {
                hasSensor = !!el.vendor_data;
            }

            result.push({
                cleaning_frequency: this.getCleaningFrequency(el),
                container_type: `${el.container_volume} ${el.container_brand} ${el.container_dump}`,
                trash_type: await trashTypeMapping.getTrashTypeByKsnkoCode(el.trash_type),
                ...(el.vendor_data && {
                    last_measurement: {
                        measured_at_utc: el.vendor_data.last_measurements?.measured_at ?? null,
                        percent_calculated: el.vendor_data.last_measurements?.percent_smoothed ?? null,
                        prediction_utc: el.vendor_data.last_measurements?.prediction_bin_full ?? null,
                    },
                    last_pick: el.vendor_data.last_picks?.last_pick_at ?? null,
                }),
                ksnko_id: el.id,
                container_id: el.id,
                ...(el.vendor_data && {
                    sensor_code: el.id.toString(),
                    sensor_supplier: el.vendor_data?.vendor ?? null,
                    sensor_id: el.vendor_data?.vendor_id ?? null,
                }),
                is_monitored: !!el.vendor_data?.vendor_id,
            });
        }

        return { data: result, hasSensor };
    };

    private static getCleaningFrequency = (el: TContainersExtendedModel) => {
        const next_pick = el.next_planned_pick?.next_planned_pick_at_date;

        if (!el.raw_picks.length) {
            return {
                duration: null,
                frequency: null,
                id: null,
                next_pick,
            };
        }

        const frequency = el.raw_picks[0].frequency;

        return {
            duration: frequency === "007" ? null : "P" + Math.floor(+frequency % 10) + "W",
            frequency: frequency === "007" ? 99 : +frequency % 10,
            id: frequency === "007" ? 99 : +frequency,
            pick_days: el.raw_picks[0].days ?? undefined,
            next_pick,
        };
    };

    public static getAllMapper = async (records: TStationWithContainersModel[]): Promise<IGeoJSONFeatureCollection> => {
        const result: IGeoJSONFeature[] = [];
        for (const record of records) {
            result.push(await LegacyOutputMapper.getAllRecordMapper(record));
        }

        return buildGeojsonFeatureCollection(result);
    };

    public static getAllMeasurementsMapper = (records: TMeasurementsExtendedDataModel[]): IMeasurementsOutput[] => {
        const result: IMeasurementsOutput[] = [];
        for (const item of records) {
            result.push({
                id: item.ksnko_container_id.toString(),
                sensor_code: item.ksnko_container_id.toString(),
                percent_calculated: item.percent_smoothed || 0,
                upturned: 0,
                temperature: item.temperature || 0,
                battery_status: item.battery_status || 0,
                measured_at_utc: item.measured_at,
                prediction_utc: item.prediction_bin_full,
                firealarm: Number(item.firealarm),
                updated_at: item.updated_at
                    ? new Date(item.updated_at).getTime()
                    : item.created_at
                    ? new Date(item.created_at).getTime()
                    : 0,
            });
        }

        return result;
    };

    public static getAllPicksMapper = (records: TPicksWithVendorDataModel[]): IPicksOutput[] => {
        const result: IPicksOutput[] = [];
        for (const item of records) {
            result.push({
                id: item.ksnko_container_id.toString(),
                sensor_code: item.ksnko_container_id.toString(),
                pick_minfilllevel: item.vendor_data?.pick_min_fill_level || 0,
                decrease: item.vendor_data?.decrease_threshold || 0,
                pick_at_utc: item.pick_at,
                percent_now: item.percent_now || 0,
                percent_before: item.percent_before || 0,
                event_driven: item.event_driven,
                updated_at: item.updated_at
                    ? new Date(item.updated_at).getTime()
                    : item.created_at
                    ? new Date(item.created_at).getTime()
                    : 0,
            });
        }

        return result;
    };

    public static getAllPlannedPickDatesMapper = (records: PlannedPickDatesModel[]): IPlannedPickDatesOutput[] => {
        const result: Record<string, IPlannedPickDatesOutput> = {};
        for (const item of records) {
            if (!result[item.ksnko_container_id]) {
                result[item.ksnko_container_id] = {
                    ksnko_id: item.ksnko_container_id,
                    sensoneo_code: item.ksnko_container_id.toString(),
                    generated_dates: [],
                };
            }
            result[item.ksnko_container_id].generated_dates.push(item.pick_date as string);
        }

        return Object.values(result);
    };
}
