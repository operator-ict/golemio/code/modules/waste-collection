export interface IBulkyWasteStationsParams {
    districts: string[];
    lat: number;
    lng: number;
    range: number;
    limit: number;
    offset: number;
    dateFrom: string;
    timeFrom: string;
    dateTo: string;
    timeTo: string;
}
