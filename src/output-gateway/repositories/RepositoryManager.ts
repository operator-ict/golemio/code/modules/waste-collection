import { NextPlannedPicksRepository } from "#og/repositories/NextPlannedPicksRepository";
import { PlannedPickDatesRepository } from "#og/repositories/PlannedPickDatesRepository";
import { RawPickDatesRepository } from "#og/repositories/RawPickDatesRepository";
import { RawPickDatesViewRepository } from "#og/repositories/RawPickDatesViewRepository";
import { SensorLastMeasurementsRepository } from "#og/repositories/SensorLastMeasurementsRepository";
import { SensorLastPicksRepository } from "#og/repositories/SensorLastPicksRepository";
import { SensorMeasurementsRepository } from "#og/repositories/SensorMeasurementsRepository";
import { SensorPicksRepository } from "#og/repositories/SensorPicksRepository";
import { SensorVendorDataRepository } from "#og/repositories/SensorVendorDataRepository";
import { StationContainersRepository } from "#og/repositories/StationContainersRepository";
import { StationsRepository } from "#og/repositories/StationsRepository";

export interface IWasteCollectionRepositories {
    stationsRepository: StationsRepository;
    stationContainersRepository: StationContainersRepository;
    sensorVendorDataRepository: SensorVendorDataRepository;
    sensorMeasurementsRepository: SensorMeasurementsRepository;
    sensorLastMeasurementsRepository: SensorLastMeasurementsRepository;
    sensorPicksRepository: SensorPicksRepository;
    sensorLastPicksRepository: SensorLastPicksRepository;
    nextPlannedPicksRepository: NextPlannedPicksRepository;
    rawPickDatesRepository: RawPickDatesRepository;
    rawPickDatesViewRepository: RawPickDatesViewRepository;
    plannedPickDatesRepository: PlannedPickDatesRepository;
}

const repositories: IWasteCollectionRepositories = {
    stationsRepository: new StationsRepository(),
    stationContainersRepository: new StationContainersRepository(),
    sensorVendorDataRepository: new SensorVendorDataRepository(),
    sensorMeasurementsRepository: new SensorMeasurementsRepository(),
    sensorLastMeasurementsRepository: new SensorLastMeasurementsRepository(),
    sensorPicksRepository: new SensorPicksRepository(),
    nextPlannedPicksRepository: new NextPlannedPicksRepository(),
    sensorLastPicksRepository: new SensorLastPicksRepository(),
    rawPickDatesRepository: new RawPickDatesRepository(),
    rawPickDatesViewRepository: new RawPickDatesViewRepository(),
    plannedPickDatesRepository: new PlannedPickDatesRepository(),
};

for (const type of Object.keys(repositories)) {
    const model = (repositories as any)[type];
    if (model.hasOwnProperty("Associate")) {
        model.Associate(repositories);
    }
}

export { repositories };
