import { IBulkyWasteStationsParams } from "#og/helpers/params/IBulkyWasteStationsParams";
import { WasteCollection } from "#sch";
import { IKsnkoBulkyWasteStations } from "#sch/definitions/KsnkoBulkyWasteStations";
import { KsnkoBulkyWasteStationsModel } from "#sch/definitions/models/KsnkoBulkyWasteStationsModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";

export class BulkyWasteStationsRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.ksnkoBulkyWasteStations.name + "Repository",
            WasteCollection.definitions.ksnkoBulkyWasteStations.pgTableName,
            KsnkoBulkyWasteStationsModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public async GetAll(params: Partial<IBulkyWasteStationsParams>): Promise<IKsnkoBulkyWasteStations[]> {
        try {
            const { whereAttributes, order } = this.prepareWhereAndOrder(params);
            const bindParams = {
                dateFrom: params.dateFrom,
                timeFrom: params.timeFrom,
                dateTo: params.dateTo,
                timeTo: params.timeTo,
            };

            return this.sequelizeModel.findAll<KsnkoBulkyWasteStationsModel>({
                attributes: {
                    include: ["updated_at"],
                    exclude: ["service_id", "service_code", "trash_type_id", "trash_type"],
                },
                where: whereAttributes,
                limit: params.limit,
                offset: params.offset,
                order,
                bind: bindParams,
                raw: true,
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll Bulky Waste Stations", this.name, err, 500);
        }
    }

    public async GetOne(id: any): Promise<IKsnkoBulkyWasteStations | null> {
        try {
            return await this.sequelizeModel.findOne<KsnkoBulkyWasteStationsModel>({
                where: { custom_id: id },
                raw: true,
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetOne", this.name, err, 500);
        }
    }

    private prepareWhereAndOrder(params: Partial<IBulkyWasteStationsParams>) {
        let whereAttributes: Sequelize.WhereOptions = {};
        const order: Sequelize.OrderItem[] = [];

        order.push([Sequelize.col("id"), "asc"]);

        if (params.districts) {
            whereAttributes.city_district_slug = {
                [Sequelize.Op.in]: params.districts,
            };
        }

        if (params.lat && params.lng) {
            whereAttributes.range = Sequelize.where(
                Sequelize.fn(
                    "ST_DWithin",
                    Sequelize.col("geom"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", params.lng, params.lat), 4326),
                        "geography"
                    ),
                    params.range! * 1000,
                    true
                ),
                "true"
            );

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geom"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", params.lng, params.lat), 4326),
                        "geography"
                    )
                )
            );
        }
        const literalConditions = [];
        if (params.dateFrom) {
            literalConditions.push(
                Sequelize.literal(`(pick_date > $dateFrom or (pick_date = $dateFrom and pick_time_to >= $timeFrom))`)
            );
        }

        if (params.dateTo) {
            literalConditions.push(
                Sequelize.literal(`(pick_date < $dateTo or (pick_date = $dateTo and pick_time_to <= $timeTo))`)
            );
        }

        if (literalConditions.length > 0) {
            whereAttributes = {
                ...whereAttributes,
                [Op.and]: literalConditions,
            };
        }

        return { whereAttributes, order };
    }
}
