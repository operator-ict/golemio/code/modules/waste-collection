import { IWasteCollectionRepositories } from "#og/repositories/RepositoryManager";
import { TrashTypeMappingRepository } from "#og/repositories/TrashTypeMappingRepository";
import { WasteCollection } from "#sch";
import { KsnkoStationContainerModel } from "#sch/definitions/models/KsnkoStationContainerModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";

export class StationContainersRepository extends SequelizeModel {
    private trashTypeMapper: TrashTypeMappingRepository;

    constructor() {
        super(
            WasteCollection.definitions.ksnkoStationContainers.name + "Repository",
            WasteCollection.definitions.ksnkoStationContainers.pgTableNameOg,
            KsnkoStationContainerModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );

        this.trashTypeMapper = TrashTypeMappingRepository.getInstance();
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.hasOne(repositories.stationsRepository["sequelizeModel"], {
            sourceKey: "station_id",
            foreignKey: "id",
            as: "station",
        });

        this.sequelizeModel.hasOne(repositories.sensorVendorDataRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "vendor_data",
        });

        this.sequelizeModel.hasMany(repositories.sensorPicksRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "picks",
        });

        this.sequelizeModel.hasMany(repositories.sensorMeasurementsRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "measurements",
        });

        this.sequelizeModel.hasOne(repositories.nextPlannedPicksRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "next_planned_pick",
        });

        this.sequelizeModel.hasMany(repositories.rawPickDatesViewRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "container_id",
            as: "raw_picks",
        });

        this.sequelizeModel.hasOne(this.trashTypeMapper.sequelizeModel, {
            sourceKey: "trash_type",
            foreignKey: "code_ksnko",
            as: "trash_type_mapping",
        });
    };

    public GetAll = (): never => {
        throw new Error("Method not implemented.");
    };

    public GetOne = (): never => {
        throw new Error("Method not implemented.");
    };
}
