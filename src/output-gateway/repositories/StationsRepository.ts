import { LegacyAccessibilityMapper } from "#og/helpers";
import { IWasteCollectionRepositories, repositories } from "#og/repositories/RepositoryManager";
import { WasteCollection } from "#sch";
import { KsnkoStationContainerModel } from "#sch/definitions/models/KsnkoStationContainerModel";
import { KsnkoStationModel } from "#sch/definitions/models/KsnkoStationModel";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import { SensorMeasurementModel } from "#sch/definitions/models/SensorMeasurementModel";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { TrashTypeMappingRepository } from "./TrashTypeMappingRepository";

export type TContainersExtendedModel = KsnkoStationContainerModel & {
    next_planned_pick: { next_planned_pick_at_date: string };
    raw_picks: Array<Pick<RawPickDates, "days" | "frequency">>;
    vendor_data?: Pick<SensorVendorDataModel, "vendor" | "vendor_id"> & {
        last_picks?: { last_pick_at: string };
        last_measurements?: Pick<SensorMeasurementModel, "measured_at" | "percent_smoothed" | "prediction_bin_full">;
    };
};

export type TStationWithContainersModel = Omit<
    KsnkoStationModel,
    "location" | "has_sensor" | "changed_at" | "coordinate_lat" | "coordinate_lon" | "active"
> & { containers: TContainersExtendedModel[]; updated_at: string };

export class StationsRepository extends SequelizeModel {
    private trashTypeMapper: TrashTypeMappingRepository;

    constructor() {
        super(
            WasteCollection.definitions.ksnkoStations.name + "Repository",
            WasteCollection.definitions.ksnkoStations.pgTableNameOg,
            KsnkoStationModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );

        this.trashTypeMapper = TrashTypeMappingRepository.getInstance();
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.hasMany(repositories.stationContainersRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
            as: "containers",
        });
    };

    public async GetAll(
        options: {
            accessibility?: number[];
            districts?: string[];
            id?: string;
            ksnkoId?: string;
            onlyMonitored?: boolean;
            lat?: number;
            lng?: number;
            range?: number;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<TStationWithContainersModel[]> {
        const currentDateISO = new Date().toISOString();
        const whereAttributes: Sequelize.WhereOptions = {
            active: true,
        };

        const whereAttributesContainers: Sequelize.WhereOptions = {
            active: true,
        };

        const order = this.getInitialOrderAttributes();

        if (options.districts && options.districts.length) {
            whereAttributes.city_district_slug = {
                [Sequelize.Op.in]: options.districts,
            };
        }

        /**
         * @deprecated The parameter `id` is deprecated and should not be used.
         */
        if (options.id) {
            whereAttributesContainers.id = {
                [Sequelize.Op.eq]: options.id,
            };
        }

        if (options.ksnkoId) {
            whereAttributesContainers.id = {
                [Sequelize.Op.eq]: options.ksnkoId,
            };
        }

        if (options.accessibility && options.accessibility.length) {
            whereAttributes.access = {
                [Sequelize.Op.in]: options.accessibility.map((el) => LegacyAccessibilityMapper.getAccessibilityById(el)),
            };
        }

        if (options.lat && options.lng) {
            if (options.range) {
                whereAttributes.range = Sequelize.where(
                    Sequelize.fn(
                        "ST_DWithin",
                        Sequelize.col("geom"),
                        Sequelize.cast(
                            Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                            "geography"
                        ),
                        options.range,
                        true
                    ),
                    "true"
                );
            }

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geom"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        order.push([{ model: repositories.stationContainersRepository["sequelizeModel"], as: "containers" }, "id", "asc"]);

        return this.sequelizeModel.findAll({
            attributes: {
                include: ["updated_at"],
                exclude: ["location", "has_sensor", "changed_at", "coordinate_lat", "coordinate_lon", "active"],
            },
            include: [
                {
                    model: repositories.stationContainersRepository["sequelizeModel"],
                    as: "containers",
                    where: whereAttributesContainers,
                    required: options.id || options.ksnkoId ? true : false,
                    include: [
                        {
                            model: repositories.nextPlannedPicksRepository["sequelizeModel"],
                            as: "next_planned_pick",
                            attributes: [
                                [
                                    Sequelize.fn("DATE", Sequelize.literal("next_planned_pick_at AT TIME ZONE 'Europe/Prague'")),
                                    "next_planned_pick_at_date",
                                ],
                            ],
                            required: false,
                        },
                        {
                            model: repositories.rawPickDatesViewRepository["sequelizeModel"],
                            as: "raw_picks",
                            attributes: ["days", "frequency"],
                            required: false,
                            where: {
                                psas_valid_to: {
                                    [Sequelize.Op.or]: [null, { [Sequelize.Op.gte]: currentDateISO }],
                                },
                                psas_valid_from: {
                                    [Sequelize.Op.lte]: currentDateISO,
                                },
                            },
                        },
                        {
                            model: repositories.sensorVendorDataRepository["sequelizeModel"],
                            as: "vendor_data",
                            attributes: ["vendor", "vendor_id", "active"],
                            required: options.onlyMonitored ?? false,
                            subQuery: false,
                            where: {
                                active: true,
                            },
                            include: [
                                {
                                    model: repositories.sensorLastMeasurementsRepository["sequelizeModel"],
                                    as: "last_measurements",
                                    attributes: ["measured_at", "percent_smoothed", "prediction_bin_full"],
                                    required: false,
                                    subQuery: false,
                                },
                                {
                                    model: repositories.sensorLastPicksRepository["sequelizeModel"],
                                    as: "last_picks",
                                    attributes: ["last_pick_at"],
                                    required: false,
                                    subQuery: false,
                                },
                            ],
                        },
                        {
                            model: this.trashTypeMapper.sequelizeModel,
                            as: "trash_type_mapping",
                            attributes: [],
                            required: false,
                        },
                    ],
                },
            ],
            where: whereAttributes,
            // limit is applied only when id or ksnkoId is not set because of a conflict of the limit with the required: true
            limit: !options.id && !options.ksnkoId ? options?.limit : undefined,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        });
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }

    private getInitialOrderAttributes(): Sequelize.OrderItem[] {
        const order: Sequelize.OrderItem[] = [];

        order.push([
            { model: repositories.stationContainersRepository["sequelizeModel"], as: "containers" },
            { model: this.trashTypeMapper.sequelizeModel, as: "trash_type_mapping" },
            "sort_order",
            "asc",
        ]);

        order.push([Sequelize.col("id"), "asc"]);

        return order;
    }
}
