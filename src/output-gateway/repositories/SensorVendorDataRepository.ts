import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";
import { IWasteCollectionRepositories } from "#og";

export class SensorVendorDataRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorVendorData.name + "Repository",
            WasteCollection.definitions.sensorVendorData.pgTableNameOg,
            SensorVendorDataModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.hasMany(repositories.sensorPicksRepository["sequelizeModel"], {
            sourceKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "picks",
        });

        this.sequelizeModel.hasMany(repositories.sensorMeasurementsRepository["sequelizeModel"], {
            sourceKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "measurements",
        });

        this.sequelizeModel.hasMany(repositories.plannedPickDatesRepository["sequelizeModel"], {
            sourceKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "planned_picks",
        });

        this.sequelizeModel.belongsTo(repositories.stationContainersRepository["sequelizeModel"], {
            targetKey: "id",
            foreignKey: "ksnko_container_id",
        });

        this.sequelizeModel.hasOne(repositories.sensorLastMeasurementsRepository["sequelizeModel"], {
            sourceKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "last_measurements",
        });

        this.sequelizeModel.hasOne(repositories.sensorLastPicksRepository["sequelizeModel"], {
            sourceKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "last_picks",
        });
    };

    public GetAll = (): never => {
        throw new Error("Method not implemented.");
    };

    public GetOne = (): never => {
        throw new Error("Method not implemented.");
    };
}
