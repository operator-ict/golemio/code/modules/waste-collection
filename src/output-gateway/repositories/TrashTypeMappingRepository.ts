import { WasteCollection } from "#sch";
import ITrashTypeMapping from "#sch/definitions/models/interfaces/ITrashTypeMapping";
import TrashTypeMappingModel from "#sch/definitions/models/TrashTypeMapperModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";

export class TrashTypeMappingRepository extends SequelizeModel {
    private static instance: TrashTypeMappingRepository;
    private mappingTable: Record<string, ITrashTypeMapping> = {};

    public static getInstance() {
        if (!this.instance) {
            this.instance = new TrashTypeMappingRepository();
        }

        return this.instance;
    }

    private constructor() {
        super(
            WasteCollection.definitions.trashTypeMapper.name + "Repository",
            WasteCollection.definitions.trashTypeMapper.pgTableName,
            TrashTypeMappingModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public async getTrashTypeByKsnkoCode(ksnkoCode: string) {
        if (Object.keys(this.mappingTable).length === 0) {
            await this.loadCache();
        }

        const trashType = this.mappingTable[ksnkoCode];

        return {
            description: trashType?.name ?? "neznámý",
            id: trashType?.legacy_id ?? 0,
        };
    }

    public async loadCache() {
        const records = await this.GetAll();

        for (const record of records) {
            this.mappingTable[record.code_ksnko] = {
                name: record.name,
                code_ksnko: record.code_ksnko,
                code_psas: record.code_psas,
                legacy_id: record.legacy_id,
            };
        }
    }

    public GetAll = (): Promise<TrashTypeMappingModel[]> => {
        return this.sequelizeModel.findAll<TrashTypeMappingModel>({ raw: true });
    };

    public GetOne = (): never => {
        throw new Error("Method not implemented.");
    };
}
