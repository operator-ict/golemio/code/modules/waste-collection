import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import RawPickDatesView from "#sch/definitions/models/RawPickDatesViewModel";
import { IWasteCollectionRepositories } from "#og";

export class RawPickDatesViewRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.rawPickDatesView.name + "Repository",
            WasteCollection.definitions.rawPickDatesView.pgTableName,
            RawPickDatesView.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.stationContainersRepository["sequelizeModel"], {
            targetKey: "id",
            foreignKey: "container_id",
            as: "containers",
        });
    };

    public GetAll(id: string): never {
        throw new Error("Not implemented");
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
