import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import { IWasteCollectionRepositories } from "#og";
import { SensorLastMeasurementViewModel } from "#sch/definitions/models/SensorLastMeasurementViewModel";

export class SensorLastMeasurementsRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorLastMeasurementsView.name + "Repository",
            WasteCollection.definitions.sensorLastMeasurementsView.pgTableName,
            SensorLastMeasurementViewModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.sensorVendorDataRepository["sequelizeModel"], {
            targetKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "vendor_data",
        });
    };

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
