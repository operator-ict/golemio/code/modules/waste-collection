import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { BulkyWasteStationsRepository } from "#og/repositories/BulkyWasteStationsRepository";

//#region Initialization
const wasteCollectionContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();
//#endregion

//#region Models
wasteCollectionContainer.register(ModuleContainerToken.BulkyWasteStationsRepository, BulkyWasteStationsRepository);
//#endregion

export { wasteCollectionContainer as WasteCollectionContainer };
