export interface IITopRawObject {
    objects: Record<string, IITopSensor>; // key format Sensor::1234
}

export interface IITopSensor {
    key: string;
    fields: IItopFieldsData;
}

export interface IItopFieldsData {
    name: string;
    description: string;
    status: string;
    location_name: string;
    brand_name: string;
    model_name: string;
    ksnkoid: string;
    ismonitored: string;
    snetworkid: string;
    loraid: string;
    nbiotid: string;
    sigfoxid: string;
    serialnumber: string;
    asset_number: string;
    purchase_date: string;
    installdate: string;
    move2production: string;
    end_of_warranty: string;
    end_of_warranty_mhmp: string;
    servisdate: string;
}
