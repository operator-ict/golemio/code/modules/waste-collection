import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IITopRawObject, IITopSensor } from "./IITopInputData";

export default class ITopInputDataSchema {
    public static jsonSchemaElement: JSONSchemaType<IITopSensor> = {
        type: "object",
        properties: {
            key: { type: "string" },
            fields: {
                type: "object",
                properties: {
                    name: { type: "string" },
                    description: { type: "string" },
                    status: { type: "string" },
                    location_name: { type: "string" },
                    brand_name: { type: "string" },
                    model_name: { type: "string" },
                    ksnkoid: { type: "string" },
                    ismonitored: { type: "string" },
                    snetworkid: { type: "string" },
                    loraid: { type: "string" },
                    nbiotid: { type: "string" },
                    sigfoxid: { type: "string" },
                    serialnumber: { type: "string" },
                    asset_number: { type: "string" },
                    purchase_date: { type: "string", format: "date" },
                    installdate: { type: "string", format: "date" },
                    move2production: { type: "string", format: "date" },
                    end_of_warranty: { type: "string", format: "date" },
                    end_of_warranty_mhmp: { type: "string", format: "date" },
                    servisdate: { type: "string", format: "date" },
                },
                required: [
                    "name",
                    "description",
                    "status",
                    "location_name",
                    "brand_name",
                    "model_name",
                    "ksnkoid",
                    "ismonitored",
                    "snetworkid",
                    "loraid",
                    "nbiotid",
                    "sigfoxid",
                    "serialnumber",
                    "asset_number",
                    "purchase_date",
                    "installdate",
                    "move2production",
                    "end_of_warranty",
                    "end_of_warranty_mhmp",
                    "servisdate",
                ],
            },
        },
        required: ["key", "fields"],
    };

    // @ts-ignore
    public static jsonSchemaRaw: JSONSchemaType<IITopRawObject> = {
        $schema: "http://json-schema.org/draft-04/schema#",
        type: "object",
        properties: {
            objects: {
                type: "object",
                patternProperties: {
                    "^S": ITopInputDataSchema.jsonSchemaElement,
                },
            },
        },
        required: ["objects"],
    };
}
