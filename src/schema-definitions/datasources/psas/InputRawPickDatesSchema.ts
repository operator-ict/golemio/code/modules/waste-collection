import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import IRawPickDatesInput from "./IRawPickDatesInput";

export default class InputRawPickDatesSchema {
    public static jsonSchema: JSONSchemaType<IRawPickDatesInput[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                datum: { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                cislo_subjektu: { type: "integer" },
                pocet: { type: "number" },
                typ_nadoby: { type: "string" },
                objem: { type: "integer" },
                cetnost: { type: "string", maxLength: 3 },
                ulice: { type: "string" },
                typ_odpadu: { type: "string", maxLength: 3 },
                cor: { type: "integer" },
                pism: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                cpop: {
                    oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                },
                platnost_od: { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                platnost_do: {
                    oneOf: [
                        { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                        { type: "null", nullable: true },
                    ],
                },
                stanoviste: {
                    oneOf: [
                        { type: "string", maxLength: 9 },
                        { type: "null", nullable: true },
                    ],
                },
                tydny_roku: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                svoz_firma: { type: "string" },
            },
            required: ["cislo_subjektu"],
        },
    };
}
