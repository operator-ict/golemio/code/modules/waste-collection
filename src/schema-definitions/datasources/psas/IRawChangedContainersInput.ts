export interface IRawChangedContainersInput {
    cislo_subjektu: number;
    ukoly: string;
    poz_splneni: string;
    typ_ukolu: string;
    typ_nadoby: string;
    typ_odpadu: string;
    mag_pom: string;
    zpracovatel: string;
    odebrat: number | null;
    dodat: number | null;
    ulice: string;
    mestska_cast: string;
}
