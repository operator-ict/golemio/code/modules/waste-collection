import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPsasRealPicksDatesInput } from "#sch/datasources/psas/IPsasRealPicksDatesInput";

export default class PsasRealPickDatesInputSchema {
    public static jsonSchema: JSONSchemaType<IPsasRealPicksDatesInput[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                cislo_subjektu: { type: "integer" },
                typ_odpadu: { type: "string", maxLength: 3 },
                cetnost: { type: "string", maxLength: 3 },
                objem: { type: "integer" },
                nadoba: { type: "string" },
                pocet: { type: "number" },
                platnost_od: { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                platnost_do: {
                    oneOf: [
                        { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                        { type: "null", nullable: true },
                    ],
                },
                mag_pom: { type: "string" },
                stanoviste: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                datum_planovaneho_svozu: {
                    oneOf: [
                        { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                        { type: "null", nullable: true },
                    ],
                },
                datum_skut_plneni: {
                    oneOf: [
                        { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                        { type: "null", nullable: true },
                    ],
                },
            },
            required: ["cislo_subjektu", "stanoviste", "typ_odpadu", "datum_planovaneho_svozu"],
        },
    };
}
