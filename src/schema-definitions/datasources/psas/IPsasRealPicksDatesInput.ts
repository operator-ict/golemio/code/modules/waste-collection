export interface IPsasRealPicksDatesInput {
    cislo_subjektu: number;
    typ_odpadu: string;
    cetnost: string;
    objem: number;
    nadoba: string;
    pocet: number;
    platnost_od: string;
    platnost_do: string | null;
    mag_pom: string;
    stanoviste: string | null;
    datum_planovaneho_svozu: string | null;
    datum_skut_plneni: string | null;
}
