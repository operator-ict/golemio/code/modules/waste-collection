export default interface IRawPickDatesInput {
    datum: string;
    cislo_subjektu: number;
    pocet: number;
    typ_nadoby: string;
    objem: number;
    cetnost: string;
    ulice: string;
    typ_odpadu: string;
    cor: number;
    pism: string | null;
    cpop: number | null;
    platnost_od: string;
    platnost_do: string | null;
    stanoviste: string | null;
    tydny_roku: string | null;
    svoz_firma: string;
}
