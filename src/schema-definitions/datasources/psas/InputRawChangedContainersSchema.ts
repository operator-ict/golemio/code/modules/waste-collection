import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IRawChangedContainersInput } from "./IRawChangedContainersInput";

export default class InputRawChangedContainersSchema {
    public static jsonSchema: JSONSchemaType<IRawChangedContainersInput[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                cislo_subjektu: { type: "integer" },
                ukoly: { type: "string" },
                poz_splneni: { type: "string", pattern: "^\\d{4}-\\d{2}-\\d{2}T00:00:00$" },
                typ_ukolu: { type: "string" },
                typ_nadoby: { type: "string" },
                typ_odpadu: { type: "string" },
                mag_pom: { type: "string" },
                zpracovatel: { type: "string" },
                odebrat: {
                    oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                },
                dodat: {
                    oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                },
                ulice: { type: "string" },
                mestska_cast: { type: "string" },
            },
            required: [
                "cislo_subjektu",
                "ukoly",
                "poz_splneni",
                "typ_ukolu",
                "typ_nadoby",
                "typ_odpadu",
                "mag_pom",
                "zpracovatel",
                "odebrat",
                "dodat",
                "ulice",
                "mestska_cast",
            ],
        },
    };
}
