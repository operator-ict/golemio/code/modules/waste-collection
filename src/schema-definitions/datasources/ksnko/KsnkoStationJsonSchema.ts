import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IKsnkoStationItemInput {
    id: number;
    trashType: {
        code: string;
        name: string;
    };
    container: {
        name: string;
        volume: number;
        brand: string;
        dump: string;
    };
    cleaningFrequency: {
        code: string;
        days: string | null;
    };
    count: number;
}

export interface IKsnkoStationContainerInput {
    id: number;
    code: string;
    sensorId: string | null;
    volumeRatio: number;
    trashType: {
        code: string;
        name: string;
    };
    container: {
        name: string;
        volume: number;
        brand: string;
        dump: string;
    };
    cleaningFrequency: {
        code: string;
        days: string | null;
    };
    currentPercentFullness: number | null;
}

export interface IKsnkoStationInput {
    id: number;
    number: string;
    name: string;
    access: string;
    location: string;
    cleaning: {
        code: string | null;
        days: string | null;
    };
    hasSensor: number;
    cityDistrict: {
        id: number;
        name: string;
        ruianCode: string;
    };
    coordinate: {
        lat: number;
        lon: number;
    };
    changed: string; // timestamp
    items: IKsnkoStationItemInput[];
    containers: IKsnkoStationContainerInput[];
}

const ksnkoStationsJsonSchema: JSONSchemaType<IKsnkoStationInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "integer" },
            number: { type: "string" },
            name: { type: "string" },
            access: { type: "string" },
            location: { type: "string" },
            cleaning: {
                type: "object",
                properties: {
                    code: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    days: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                },
                required: ["code", "days"],
                additionalProperties: true,
            },
            hasSensor: { type: "integer" },
            cityDistrict: {
                type: "object",
                properties: {
                    id: { type: "integer" },
                    name: { type: "string" },
                    ruianCode: { type: "string" },
                },
                required: ["id", "name", "ruianCode"],
                additionalProperties: true,
            },
            coordinate: {
                type: "object",
                properties: {
                    lat: { type: "number" },
                    lon: { type: "number" },
                },
                required: ["lat", "lon"],
                additionalProperties: true,
            },
            changed: { type: "string" },
            items: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        id: { type: "integer" },
                        trashType: {
                            type: "object",
                            properties: {
                                code: { type: "string" },
                                name: { type: "string" },
                            },
                            required: ["code", "name"],
                            additionalProperties: true,
                        },
                        container: {
                            type: "object",
                            properties: {
                                name: { type: "string" },
                                volume: { type: "integer" },
                                brand: { type: "string" },
                                dump: { type: "string" },
                            },
                            required: ["name", "volume", "brand", "dump"],
                            additionalProperties: true,
                        },
                        cleaningFrequency: {
                            type: "object",
                            properties: {
                                code: { type: "string" },
                                days: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                            },
                            required: ["code", "days"],
                            additionalProperties: true,
                        },
                        count: { type: "number" },
                    },
                    required: ["id", "trashType", "container", "cleaningFrequency", "count"],
                    additionalProperties: true,
                },
            },
            containers: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        id: { type: "integer" },
                        code: { type: "string" },
                        sensorId: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        volumeRatio: { type: "number" },
                        trashType: {
                            type: "object",
                            properties: {
                                code: { type: "string" },
                                name: { type: "string" },
                            },
                            required: ["code", "name"],
                            additionalProperties: true,
                        },
                        container: {
                            type: "object",
                            properties: {
                                name: { type: "string" },
                                volume: { type: "integer" },
                                brand: { type: "string" },
                                dump: { type: "string" },
                            },
                            required: ["name", "volume", "brand", "dump"],
                            additionalProperties: true,
                        },
                        cleaningFrequency: {
                            type: "object",
                            properties: {
                                code: { type: "string" },
                                days: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                            },
                            required: ["code"],
                            additionalProperties: true,
                        },
                        currentPercentFullness: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                    },
                    required: [
                        "id",
                        "code",
                        "sensorId",
                        "volumeRatio",
                        "trashType",
                        "container",
                        "cleaningFrequency",
                        "currentPercentFullness",
                    ],
                    additionalProperties: true,
                },
            },
        },
        required: [
            "id",
            "number",
            "name",
            "access",
            "location",
            "cleaning",
            "hasSensor",
            "cityDistrict",
            "coordinate",
            "changed",
            "items",
            "containers",
        ],
        additionalProperties: true,
    },
};

export const ksnkoStationsDatasource: any = {
    name: "KsnkoStationsDataSource",
    jsonSchema: ksnkoStationsJsonSchema,
};
