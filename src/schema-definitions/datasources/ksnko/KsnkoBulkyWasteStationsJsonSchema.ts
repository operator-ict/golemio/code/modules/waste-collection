import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IKsnkoBulkyWasteStationsJsonSchema {
    id: number;
    date: string;
    timeFrom: string;
    timeTo: string;
    street: string;
    payer: string;
    numberOfContainers: number;
    service: {
        id: number;
        code: string;
        name: string;
    };
    trashType: {
        id: number | null;
        code: string | null;
        name: string | null;
    };
    cityDistrict: {
        id: number;
        name: string;
        ruianCode: string;
    };
    coordinate: {
        lat: number;
        lon: number;
    };
    changed: string;
}

export default class KsnkoBulkyWasteStationsInputJsonSchema {
    public static jsonSchema: JSONSchemaType<IKsnkoBulkyWasteStationsJsonSchema[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: {
                    type: "integer",
                },
                date: {
                    type: "string",
                    pattern: "^\\d{4}-\\d{2}-\\d{2}$",
                },
                timeFrom: {
                    type: "string",
                    pattern: "^\\d{2}:\\d{2}:\\d{2}$",
                },
                timeTo: {
                    type: "string",
                    pattern: "^\\d{2}:\\d{2}:\\d{2}$",
                },
                street: {
                    type: "string",
                },
                payer: {
                    type: "string",
                },
                numberOfContainers: {
                    type: "integer",
                },
                service: {
                    type: "object",
                    properties: {
                        id: {
                            type: "integer",
                        },
                        code: {
                            type: "string",
                        },
                        name: {
                            type: "string",
                        },
                    },
                    required: ["id", "code", "name"],
                    additionalProperties: false,
                },
                trashType: {
                    type: "object",
                    properties: {
                        id: {
                            oneOf: [{ type: "integer" }, { type: "null", nullable: true }],
                        },
                        code: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                        name: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                    },
                    required: ["id", "code", "name"],
                    additionalProperties: false,
                },
                cityDistrict: {
                    type: "object",
                    properties: {
                        id: {
                            type: "integer",
                        },
                        name: {
                            type: "string",
                        },
                        ruianCode: {
                            type: "string",
                        },
                    },
                    required: ["id", "name", "ruianCode"],
                    additionalProperties: false,
                },
                coordinate: {
                    type: "object",
                    properties: {
                        lat: {
                            type: "number",
                        },
                        lon: {
                            type: "number",
                        },
                    },
                    required: ["lat", "lon"],
                    additionalProperties: false,
                },
                changed: {
                    type: "string",
                    pattern: "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}",
                },
            },
            required: [
                "id",
                "date",
                "timeFrom",
                "timeTo",
                "street",
                "payer",
                "numberOfContainers",
                "service",
                "trashType",
                "cityDistrict",
                "coordinate",
            ],
            additionalProperties: false,
        },
    };
}
