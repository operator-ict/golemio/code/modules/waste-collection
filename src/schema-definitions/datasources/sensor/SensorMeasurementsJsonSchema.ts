import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface ISensorMeasurementsInput {
    ksnko_container_id: number;
    vendor_id: string;
    percent_smoothed: number;
    percent_raw: number;
    is_blocked: boolean;
    temperature: number;
    battery_status: number;
    measured_at: string;
    saved_at: string;
    updated_at: string;
    prediction_bin_full: string | null;
    firealarm: boolean;
}

const sensorMeasurementsJsonSchema: JSONSchemaType<ISensorMeasurementsInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            ksnko_container_id: { type: "integer" },
            vendor_id: { type: "string" },
            percent_smoothed: { type: "integer" },
            percent_raw: { type: "integer" },
            is_blocked: { type: "boolean" },
            temperature: { type: "integer" },
            battery_status: { type: "number" },
            measured_at: { type: "string" },
            saved_at: { type: "string" },
            updated_at: { type: "string" },
            prediction_bin_full: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            firealarm: { type: "boolean" },
        },
        required: [
            "ksnko_container_id",
            "vendor_id",
            "percent_smoothed",
            "percent_raw",
            "is_blocked",
            "temperature",
            "battery_status",
            "measured_at",
            "saved_at",
            "updated_at",
            "prediction_bin_full",
            "firealarm",
        ],
        additionalProperties: false,
    },
};

export const sensorMeasurementsDatasource: any = {
    name: "SensorMeasurementsDataSource",
    jsonSchema: sensorMeasurementsJsonSchema,
};
