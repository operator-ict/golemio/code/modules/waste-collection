import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface ISensorVendorDataInput {
    vendor_id: string;
    ksnko_container_id: number | null;
    sensor_id: string | null;
    network: string | null;
    prediction: string | null;
    algorithm: string | null;
    installed_at: string | null;
    installed_by: string | null;
    bin_brand: string | null;
    active: boolean;
    bin_depth: number | null;
    has_anti_noise: boolean;
    anti_noise_depth: number | null;
    schedule: string | null;
    version: string;
    is_sensitive_to_pickups: boolean;
    pick_up_sensor_type: string;
    decrease_threshold: number | null;
    pick_min_fill_level: number | null;
}

const sensorVendorDataJsonSchema: JSONSchemaType<ISensorVendorDataInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            ksnko_container_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            vendor_id: { type: "string" },
            sensor_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            network: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            prediction: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            algorithm: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            installed_at: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            installed_by: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            bin_brand: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            active: { type: "boolean" },
            bin_depth: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            has_anti_noise: { type: "boolean" },
            anti_noise_depth: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            schedule: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            version: { type: "string" },
            is_sensitive_to_pickups: { type: "boolean" },
            pick_up_sensor_type: { type: "string" },
            decrease_threshold: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            pick_min_fill_level: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
        },
        required: [
            "vendor_id",
            "ksnko_container_id",
            "sensor_id",
            "network",
            "prediction",
            "installed_at",
            "installed_by",
            "active",
            "bin_depth",
            "has_anti_noise",
            "anti_noise_depth",
            "schedule",
            "version",
            "is_sensitive_to_pickups",
            "pick_up_sensor_type",
            "decrease_threshold",
            "pick_min_fill_level",
        ],
        additionalProperties: false,
    },
};

export const sensorVendorDataDatasource: any = {
    name: "SensorVendorDataDataSource",
    jsonSchema: sensorVendorDataJsonSchema,
};
