export interface IInputDataVenezeo {
    id: number;
    pick_date: string;
    pick_time_from: string;
    pick_time_to: string;
    street: string;
    payer: string;
    number_of_containers: number;
    service_id: number;
    service_code: string;
    service_name: string;
    trash_type_id: number | null;
    trash_type: string | null;
    trash_type_name: string | null;
    city_district_id: number;
    city_district_name: string;
    city_district_ruian: string;
    city_district_slug: string;
    coordinate_lat: string;
    coordinate_lon: string;
    geom: {
        crs: {
            type: string;
            properties: {
                name: string;
            };
        };
        type: string;
        coordinates: [number, number];
    };
    changed_at: string;
    custom_id: string;
    created_at: string;
    updated_at: string;
}
