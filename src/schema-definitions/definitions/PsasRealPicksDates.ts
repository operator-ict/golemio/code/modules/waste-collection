export interface IPsasRealPicksDates {
    internal_pick_id: string;
    subject_id: number;
    trash_type: string;
    trash_type_code: string | null;
    trash_type_name: string | null;
    frequency: string;
    container_volume: number;
    container_type: string;
    volume_ratio: number;
    valid_from: Date;
    valid_to: Date | null;
    station_number: string;
    station_address: string | null;
    psas_planned_pick_date: Date;
    psas_real_pick_date: Date | null;
}
