import { MODULE_NAME } from "../../constants";

export interface ISensorVendorData {
    vendor_id: string;
    ksnko_container_id: number | null;
    sensor_id: string | null;
    vendor: string;
    prediction: string | null;
    installed_at: string | null;
    installed_by: string | null;
    network: string | null;
    bin_depth: number | null;
    has_anti_noise: boolean;
    anti_noise_depth: number | null;
    algorithm: string | null;
    schedule: string | null;
    sensor_version: string;
    is_sensitive_to_pickups: boolean;
    pickup_sensor_type: string;
    decrease_threshold: number | null;
    pick_min_fill_level: number | null;
    active: boolean;
    bin_brand: string | null;
    form_hash: string;
    valid_from?: string;
    valid_to?: string;
    is_historical?: boolean;
}

export const sensorVendorData = {
    name: MODULE_NAME + "SensorVendorData",
    pgTableNameOg: "container_vendor_data",
    pgTableNameIe: "container_vendor_data_history",
};
