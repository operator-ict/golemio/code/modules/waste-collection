import { MODULE_NAME } from "../../constants";

export interface IKsnkoStationItem {
    id: number;
    count: number;
    trash_type: string;
    container_volume: number;
    container_brand: string;
    container_dump: string;
    cleaning_frequency_code: string;
    station_id: number;
    active: boolean;
}

export const ksnkoStationItems = {
    name: MODULE_NAME + "KsnkoItems",
    pgTableName: "ksnko_items",
};
