import { MODULE_NAME } from "../../constants";

export interface ISensorHistory {
    vendor_id: string;
    sensor_id: string | null;
    recorded_at: string;
}

export const sensorHistory = {
    name: MODULE_NAME + "SensorHistory",
    pgTableName: "sensor",
};
