import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IContainerChange } from "./models/interfaces/IContainerChange";

export const ChangedContainersSchema: JSONSchemaType<IContainerChange[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            subject_id: { type: "integer" },
            task_id: { type: "string" },
            changed_at: { type: "string", format: "date-time" },
            task_type: { type: "string" },
            container_type: { type: "string" },
            trash_type: { type: "string" },
            trash_type_code: { type: "string", nullable: true },
            trash_type_name: { type: "string", nullable: true },
            station_number: { type: "string" },
            company: { type: "string" },
            removed_containers_count: {
                oneOf: [{ type: "number" }, { type: "null", nullable: true }],
            },
            added_containers_count: {
                oneOf: [{ type: "number" }, { type: "null", nullable: true }],
            },
            street: { type: "string" },
            city_district: { type: "string" },
            created_at: { type: "string", format: "date-time" },
        },
        required: [
            "subject_id",
            "task_id",
            "changed_at",
            "task_type",
            "container_type",
            "trash_type",
            "station_number",
            "company",
            "removed_containers_count",
            "added_containers_count",
            "street",
            "city_district",
        ],
    },
};
