import { MODULE_NAME } from "../../constants";

export interface ISensorLastPick {
    ksnko_container_id: number;
    last_pick_at: string;
}

export const sensorLastPicksView = {
    name: MODULE_NAME + "SensorLastPicksView",
    pgTableName: "v_last_picks",
};
