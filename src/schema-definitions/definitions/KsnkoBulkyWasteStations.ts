import { Point } from "geojson";
import { MODULE_NAME } from "../../constants";

export interface IKsnkoBulkyWasteStations {
    id: number;
    custom_id: string;
    pick_date: string;
    pick_time_from: string;
    pick_time_to: string;
    street: string;
    payer: string;
    number_of_containers: number;
    service_id: number;
    service_code: string;
    service_name: string;
    trash_type_id: number | null;
    trash_type: string | null;
    trash_type_name: string | null;
    city_district_id: number;
    city_district_name: string;
    city_district_ruian: string;
    city_district_slug: string | undefined;
    coordinate_lat: number;
    coordinate_lon: number;
    geom: Point;
    changed_at: string;
}

export const ksnkoBulkyWasteStations = {
    name: MODULE_NAME + "KsnkoBulkyWasteStations",
    pgTableName: "ksnko_bulky_waste_stations",
};
