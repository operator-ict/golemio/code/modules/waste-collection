import { MODULE_NAME } from "../../constants";

export interface IKsnkoStationContainer {
    id: number;
    code: string;
    volume_ratio: number;
    trash_type: string;
    container_volume: number;
    container_brand: string;
    container_dump: string;
    cleaning_frequency_code: string;
    station_id: number;
    active: boolean;
    form_hash: string;
    valid_from?: string;
    valid_to?: string;
    is_historical?: boolean;
}

export const ksnkoStationContainers = {
    name: MODULE_NAME + "KsnkoContainers",
    pgTableNameOg: "ksnko_containers",
    pgTableNameIe: "ksnko_containers_history",
};
