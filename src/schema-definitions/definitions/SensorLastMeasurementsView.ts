import { MODULE_NAME } from "../../constants";

export const sensorLastMeasurementsView = {
    name: MODULE_NAME + "SensorLastMeasurementsView",
    pgTableName: "v_last_measurements",
};
