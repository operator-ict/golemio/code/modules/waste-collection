import { MODULE_NAME } from "../../constants";

export interface ISensorMeasurement {
    vendor_id: string;
    ksnko_container_id: number;
    percent_smoothed: number;
    percent_raw: number;
    is_blocked: boolean;
    temperature: number;
    battery_status: number;
    measured_at: string;
    saved_at: string;
    vendor_updated_at: string;
    prediction_bin_full: string | null;
    firealarm: boolean;
}

export const sensorMeasurements = {
    name: MODULE_NAME + "SensorMeasurements",
    pgTableName: "measurements",
};
