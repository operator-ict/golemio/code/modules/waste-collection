export interface ITransformedDataVenzeo {
    customId: string;
    name: string;
    note: null;
    location: {
        latitude: number;
        longitude: number;
        altitude: null;
    };
    address: {
        street: string;
        city: string;
        country: string;
        aa1: null;
        aa2: null;
    };
}
