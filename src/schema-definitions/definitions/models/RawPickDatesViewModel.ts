import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import IRawPickDatesView from "#sch/definitions/models/interfaces/IRawPickDatesView";

export default class RawPickDatesView extends Model<Omit<RawPickDates, "valid_from" | "valid_to">> implements IRawPickDatesView {
    declare date: string;
    declare subject_id: number;
    declare count: number;
    declare container_type: string;
    declare container_volume: number;
    declare frequency: string;
    declare street_name: string;
    declare trash_type: string;
    declare trash_type_code: string;
    declare trash_type_name: string;
    declare orientation_number: number;
    declare address_char: string;
    declare conscription_number: number;
    declare psas_valid_from: string;
    declare psas_valid_to: string | undefined;
    declare station_number: string;
    declare year_days_isoweeks: string | undefined;
    declare year: number;
    declare days: string;
    declare isoweeks: string;
    declare company: string;
    declare container_id: number;
    declare form_hash: string;

    public static attributeModel: ModelAttributes<RawPickDatesView> = {
        container_id: DataTypes.INTEGER,
        ...RawPickDates.attributeModel,
    };
}
