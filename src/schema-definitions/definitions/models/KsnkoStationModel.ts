import { Point } from "geojson";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IKsnkoStation } from "#sch/definitions/KsnkoStations";

export class KsnkoStationModel extends Model<KsnkoStationModel> implements IKsnkoStation {
    declare id: number;
    declare number: string;
    declare name: string;
    declare access: string;
    declare location: string;
    declare has_sensor: boolean;
    declare changed_at: string;
    declare city_district_name: string;
    declare coordinate_lat: string;
    declare coordinate_lon: string;
    declare geom: Point;
    declare active: boolean;
    declare city_district_slug: string;
    declare form_hash: string;
    declare valid_from: string;
    declare valid_to: string;
    declare is_historical: boolean;

    public static attributeModel: ModelAttributes<KsnkoStationModel> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        number: DataTypes.STRING(50),
        name: DataTypes.STRING(255),
        access: DataTypes.STRING(50),
        location: DataTypes.STRING(50),
        has_sensor: DataTypes.BOOLEAN,
        changed_at: { type: DataTypes.DATE, field: "changed" },
        city_district_name: DataTypes.STRING(50),
        coordinate_lat: DataTypes.DECIMAL,
        coordinate_lon: DataTypes.DECIMAL,
        geom: DataTypes.GEOMETRY,
        active: DataTypes.BOOLEAN,
        city_district_slug: DataTypes.STRING(50),
        form_hash: { type: DataTypes.UUID, allowNull: false, primaryKey: true },
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
        is_historical: DataTypes.BOOLEAN,
    };

    public static updateAttributes = Object.keys(KsnkoStationModel.attributeModel)
        .filter((att) => !["valid_from", "id"].includes(att))
        .concat("updated_at") as Array<keyof IKsnkoStation>;

    public static jsonSchema: JSONSchemaType<IKsnkoStation[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                number: { type: "string" },
                name: { type: "string" },
                access: { type: "string" },
                location: { type: "string" },
                has_sensor: { type: "boolean" },
                changed: { type: "string" },
                city_district_name: { type: "string" },
                coordinate_lat: { type: "string" },
                coordinate_lon: { type: "string" },
                geom: { $ref: "#/definitions/geometry" },
                active: { type: "boolean" },
                city_district_slug: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                form_hash: { type: "string" },
                valid_from: { type: "string", nullable: true },
                valid_to: { type: "string", nullable: true },
                is_historical: { type: "boolean", nullable: true },
            },
            required: ["id"],
        },
        definitions: {
            // @ts-expect-error referenced definition from other file
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
