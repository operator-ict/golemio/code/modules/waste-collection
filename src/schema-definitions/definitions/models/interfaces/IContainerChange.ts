export interface IContainerChange {
    subject_id: number;
    task_id: string;
    changed_at: string;
    task_type: string;
    container_type: string;
    trash_type: string;
    trash_type_code?: string | null;
    trash_type_name?: string | null;
    station_number: string;
    company: string;
    removed_containers_count: number | null;
    added_containers_count: number | null;
    street: string;
    city_district: string;
    created_at: string;
}
