import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";

export default interface IRawPickDatesView extends IRawPickDates {
    container_id: number;
}
