export default interface IRawPickDates {
    date: string;
    subject_id: number;
    count: number;
    container_type: string;
    container_volume: number;
    frequency: string;
    street_name: string;
    trash_type: string;
    trash_type_code: string | undefined;
    trash_type_name: string | undefined;
    orientation_number: number;
    address_char: string | undefined;
    conscription_number: number | undefined;
    psas_valid_from: string;
    psas_valid_to: string | undefined;
    station_number: string | undefined;
    year_days_isoweeks: string | undefined;
    year: number | undefined;
    days: string | undefined;
    isoweeks: string | undefined;
    company: string;
    form_hash: string;
    valid_from?: string;
    valid_to?: string;
    is_historical?: boolean;
}
