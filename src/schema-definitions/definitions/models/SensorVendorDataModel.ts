import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorVendorData } from "#sch/definitions/SensorVendorData";

export class SensorVendorDataModel extends Model<SensorVendorDataModel> implements ISensorVendorData {
    declare vendor_id: string;
    declare ksnko_container_id: number | null;
    declare sensor_id: string | null;
    declare vendor: string;
    declare prediction: string | null;
    declare installed_at: string | null;
    declare installed_by: string | null;
    declare network: string | null;
    declare bin_depth: number | null;
    declare has_anti_noise: boolean;
    declare anti_noise_depth: number | null;
    declare algorithm: string | null;
    declare schedule: string | null;
    declare sensor_version: string;
    declare is_sensitive_to_pickups: boolean;
    declare pickup_sensor_type: string;
    declare decrease_threshold: number | null;
    declare pick_min_fill_level: number | null;
    declare active: boolean;
    declare bin_brand: string | null;
    declare form_hash: string;
    declare valid_from: string;
    declare valid_to: string;
    declare is_historical: boolean;

    public static attributeModel: ModelAttributes<SensorVendorDataModel, ISensorVendorData> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        sensor_id: DataTypes.STRING(255),
        vendor: DataTypes.STRING(255),
        prediction: DataTypes.DATE,
        installed_at: DataTypes.DATE,
        installed_by: DataTypes.STRING(255),
        network: DataTypes.STRING(255),
        bin_depth: DataTypes.INTEGER,
        has_anti_noise: DataTypes.BOOLEAN,
        anti_noise_depth: DataTypes.INTEGER,
        algorithm: DataTypes.STRING(255),
        schedule: DataTypes.STRING(255),
        sensor_version: DataTypes.STRING(255),
        is_sensitive_to_pickups: DataTypes.BOOLEAN,
        pickup_sensor_type: DataTypes.STRING(255),
        decrease_threshold: DataTypes.INTEGER,
        pick_min_fill_level: DataTypes.INTEGER,
        active: DataTypes.BOOLEAN,
        bin_brand: DataTypes.STRING(255),
        form_hash: { type: DataTypes.UUID, allowNull: false, primaryKey: true },
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
        is_historical: DataTypes.BOOLEAN,
    };

    public static jsonSchema: JSONSchemaType<ISensorVendorData[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                sensor_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                vendor: { type: "string" },
                prediction: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_at: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_by: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                network: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                bin_depth: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                has_anti_noise: { type: "boolean" },
                anti_noise_depth: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                algorithm: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                schedule: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                sensor_version: { type: "string" },
                is_sensitive_to_pickups: { type: "boolean" },
                pickup_sensor_type: { type: "string" },
                decrease_threshold: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                pick_min_fill_level: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                active: { type: "boolean" },
                bin_brand: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                form_hash: { type: "string" },
                valid_from: { type: "string", nullable: true },
                valid_to: { type: "string", nullable: true },
                is_historical: { type: "boolean", nullable: true },
            },
            required: ["vendor_id"],
        },
    };

    public static attributeUpdateList = Object.keys(SensorVendorDataModel.attributeModel)
        .filter((att) => att !== "valid_from")
        .concat("updated_at");
}
