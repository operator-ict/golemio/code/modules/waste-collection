export interface ISensorCycle {
    sensor_id: string;
    description: string;
    status: string;
    location: string;
    vendor: string;
    model: string;
    monitored: boolean;
    network: string;
    lora_id: string;
    nbiot_id: string;
    sigfox_id: string;
    serial_number: string;
    asset_number: string;
    purchase_date: string;
    install_date: string;
    production_date: string;
    end_of_warranty: string;
    end_of_warranty_mhmp: string;
    service_date: string;
    record_date: string;
    external_form_id: number;
    form_hash: string;
}
