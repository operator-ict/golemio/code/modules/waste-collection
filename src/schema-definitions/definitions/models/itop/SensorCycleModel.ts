import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorCycle } from "./ISensorCycle";

export class SensorCycleModel extends Model<ISensorCycle> implements ISensorCycle {
    declare sensor_id: string;
    declare description: string;
    declare status: string;
    declare location: string;
    declare vendor: string;
    declare model: string;
    declare monitored: boolean;
    declare network: string;
    declare lora_id: string;
    declare nbiot_id: string;
    declare sigfox_id: string;
    declare serial_number: string;
    declare asset_number: string;
    declare purchase_date: string;
    declare install_date: string;
    declare production_date: string;
    declare end_of_warranty: string;
    declare end_of_warranty_mhmp: string;
    declare service_date: string;
    declare record_date: string;
    declare external_form_id: number;
    declare form_hash: string;

    public static attributeModel: ModelAttributes<SensorCycleModel, ISensorCycle> = {
        sensor_id: { type: DataTypes.STRING(255) },
        description: { type: DataTypes.TEXT },
        status: DataTypes.STRING(255),
        location: DataTypes.STRING(255),
        vendor: DataTypes.STRING(255),
        model: DataTypes.STRING(255),
        monitored: DataTypes.BOOLEAN,
        network: DataTypes.STRING(255),
        lora_id: DataTypes.STRING(255),
        nbiot_id: DataTypes.STRING(255),
        sigfox_id: DataTypes.STRING(255),
        serial_number: DataTypes.STRING(255),
        asset_number: DataTypes.STRING(255),
        purchase_date: DataTypes.DATE,
        install_date: DataTypes.DATE,
        production_date: DataTypes.DATE,
        end_of_warranty: DataTypes.DATE,
        end_of_warranty_mhmp: DataTypes.DATE,
        service_date: { type: DataTypes.DATE },
        record_date: { type: DataTypes.DATE },
        external_form_id: { type: DataTypes.INTEGER, primaryKey: true },
        form_hash: { type: DataTypes.CHAR(64), primaryKey: true },
    };

    public static jsonSchema: JSONSchemaType<ISensorCycle[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                sensor_id: { type: "string" },
                description: { type: "string" },
                status: { type: "string" },
                location: { type: "string" },
                vendor: { type: "string" },
                model: { type: "string" },
                monitored: { type: "boolean" },
                network: { type: "string" },
                lora_id: { type: "string" },
                nbiot_id: { type: "string" },
                sigfox_id: { type: "string" },
                serial_number: { type: "string" },
                asset_number: { type: "string" },
                purchase_date: { type: "string" },
                install_date: { type: "string" },
                production_date: { type: "string" },
                end_of_warranty: { type: "string" },
                end_of_warranty_mhmp: { type: "string" },
                service_date: { type: "string" },
                record_date: { type: "string" },
                external_form_id: { type: "integer" },
                form_hash: { type: "string" },
            },
            required: ["external_form_id", "form_hash"],
        },
    };
}
