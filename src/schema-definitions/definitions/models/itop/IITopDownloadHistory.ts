export default interface IITopDownloadHistory {
    record_date: string;
    form_ids: number[];
}
