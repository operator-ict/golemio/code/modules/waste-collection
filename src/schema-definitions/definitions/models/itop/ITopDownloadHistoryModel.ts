import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import IITopDownloadHistory from "./IITopDownloadHistory";

export class ITopDownloadHistoryModel extends Model<IITopDownloadHistory> implements IITopDownloadHistory {
    declare record_date: string;
    declare form_ids: number[];

    public static attributeModel: ModelAttributes<ITopDownloadHistoryModel, IITopDownloadHistory> = {
        record_date: { type: DataTypes.DATE, primaryKey: true },
        form_ids: { type: DataTypes.ARRAY(DataTypes.INTEGER) },
    };

    public static jsonSchema: JSONSchemaType<IITopDownloadHistory> = {
        type: "object",
        properties: {
            record_date: { type: "string" },
            form_ids: { type: "array", items: { type: "integer" } },
        },
        required: ["record_date", "form_ids"],
    };
}
