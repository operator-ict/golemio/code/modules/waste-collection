import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IKsnkoStationContainer } from "#sch/definitions/KsnkoStationsContainers";
import { IKsnkoStation } from "../KsnkoStations";

export class KsnkoStationContainerModel extends Model<KsnkoStationContainerModel> implements IKsnkoStationContainer {
    declare id: number;
    declare code: string;
    declare volume_ratio: number;
    declare trash_type: string;
    declare container_volume: number;
    declare container_brand: string;
    declare container_dump: string;
    declare cleaning_frequency_code: string;
    declare station_id: number;
    declare active: boolean;
    declare form_hash: string;
    declare valid_from: string;
    declare valid_to: string;
    declare is_historical: boolean;

    public static attributeModel: ModelAttributes<KsnkoStationContainerModel> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        code: DataTypes.STRING(50),
        volume_ratio: DataTypes.FLOAT,
        trash_type: DataTypes.STRING(50),
        container_volume: DataTypes.INTEGER,
        container_brand: DataTypes.STRING(150),
        container_dump: DataTypes.STRING(50),
        cleaning_frequency_code: DataTypes.STRING(50),
        station_id: DataTypes.INTEGER,
        active: DataTypes.BOOLEAN,
        form_hash: { type: DataTypes.UUID, allowNull: false, primaryKey: true },
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
        is_historical: DataTypes.BOOLEAN,
    };

    public static updateAttributes = Object.keys(KsnkoStationContainerModel.attributeModel)
        .filter((att) => !["valid_from", "id"].includes(att))
        .concat("updated_at") as Array<keyof IKsnkoStationContainer>;

    public static jsonSchema: JSONSchemaType<IKsnkoStationContainer[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                code: { type: "string" },
                volume_ratio: { type: "number" },
                trash_type: { type: "string" },
                container_volume: { type: "integer" },
                container_brand: { type: "string" },
                container_dump: { type: "string" },
                cleaning_frequency_code: { type: "string" },
                station_id: { type: "integer" },
                active: { type: "boolean" },
                form_hash: { type: "string" },
                valid_from: { type: "string", nullable: true },
                valid_to: { type: "string", nullable: true },
                is_historical: { type: "boolean", nullable: true },
            },
            required: ["id"],
        },
    };
}
