import { IKsnkoBulkyWasteStations } from "./../KsnkoBulkyWasteStations";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { Point } from "geojson";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";

export class KsnkoBulkyWasteStationsModel extends Model<KsnkoBulkyWasteStationsModel> implements IKsnkoBulkyWasteStations {
    declare id: number;
    declare pick_date: string;
    declare pick_time_from: string;
    declare pick_time_to: string;
    declare street: string;
    declare payer: string;
    declare number_of_containers: number;
    declare service_id: number;
    declare service_code: string;
    declare service_name: string;
    declare trash_type_id: number;
    declare trash_type: string;
    declare trash_type_name: string;
    declare city_district_id: number;
    declare city_district_name: string;
    declare city_district_ruian: string;
    declare city_district_slug: string;
    declare coordinate_lat: number;
    declare coordinate_lon: number;
    declare geom: Point;
    declare changed_at: string;
    declare custom_id: string;

    public static attributeModel: ModelAttributes<KsnkoBulkyWasteStationsModel> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        pick_date: { type: DataTypes.DATE, primaryKey: true },
        pick_time_from: DataTypes.TIME,
        pick_time_to: DataTypes.TIME,
        street: DataTypes.STRING(50),
        payer: DataTypes.STRING(50),
        number_of_containers: DataTypes.INTEGER,
        service_id: DataTypes.INTEGER,
        service_code: DataTypes.STRING(3),
        service_name: DataTypes.STRING(3),
        trash_type_id: DataTypes.INTEGER,
        trash_type: DataTypes.STRING(3),
        trash_type_name: DataTypes.STRING(50),
        city_district_id: DataTypes.INTEGER,
        city_district_name: DataTypes.STRING(50),
        city_district_ruian: DataTypes.STRING(50),
        city_district_slug: DataTypes.STRING(50),
        coordinate_lat: DataTypes.DECIMAL,
        coordinate_lon: DataTypes.DECIMAL,
        geom: DataTypes.GEOMETRY,
        changed_at: { type: DataTypes.DATE, field: "changed" },
        custom_id: DataTypes.STRING(50),
    };

    public static updateAttributes = Object.keys(KsnkoBulkyWasteStationsModel.attributeModel).concat("updated_at") as Array<
        keyof IKsnkoBulkyWasteStations
    >;

    public static jsonSchema: JSONSchemaType<IKsnkoBulkyWasteStations[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                pick_date: { type: "string" },
                pick_time_from: { type: "string" },
                pick_time_to: { type: "string" },
                street: { type: "string" },
                payer: { type: "string" },
                number_of_containers: { type: "integer" },
                service_id: { type: "integer" },
                service_code: { type: "string" },
                service_name: { type: "string" },
                trash_type_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                trash_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                trash_type_name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                city_district_id: { type: "integer" },
                city_district_name: { type: "string" },
                city_district_ruian: { type: "string" },
                coordinate_lat: { type: "number" },
                coordinate_lon: { type: "number" },
                geom: { $ref: "#/definitions/geometry" },
                changed_at: { type: "string" },
            },
            required: ["id"],
        },
        definitions: {
            // @ts-expect-error referenced definition from other file
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
