import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorLastPick } from "#sch/definitions/SensorLastPicksView";

export class SensorLastPicksViewModel extends Model<SensorLastPicksViewModel> implements ISensorLastPick {
    declare ksnko_container_id: number;
    declare last_pick_at: string;

    public static attributeModel: ModelAttributes<SensorLastPicksViewModel, ISensorLastPick> = {
        ksnko_container_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        last_pick_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
    };
}
