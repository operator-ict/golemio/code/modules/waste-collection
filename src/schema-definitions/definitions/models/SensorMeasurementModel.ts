import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorMeasurement } from "#sch/definitions/SensorMeasurements";

export class SensorMeasurementModel extends Model<SensorMeasurementModel, ISensorMeasurement> implements ISensorMeasurement {
    declare vendor_id: string;
    declare ksnko_container_id: number;
    declare percent_smoothed: number;
    declare percent_raw: number;
    declare is_blocked: boolean;
    declare temperature: number;
    declare battery_status: number;
    declare measured_at: string;
    declare saved_at: string;
    declare vendor_updated_at: string;
    declare prediction_bin_full: string | null;
    declare firealarm: boolean;

    public static attributeModel: ModelAttributes<SensorMeasurementModel, ISensorMeasurement> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        percent_smoothed: DataTypes.INTEGER,
        percent_raw: DataTypes.INTEGER,
        is_blocked: DataTypes.BOOLEAN,
        temperature: DataTypes.INTEGER,
        battery_status: DataTypes.FLOAT,
        measured_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        saved_at: DataTypes.DATE,
        vendor_updated_at: DataTypes.DATE,
        prediction_bin_full: DataTypes.DATE,
        firealarm: DataTypes.BOOLEAN,
    };

    public static updateAttributes = Object.keys(SensorMeasurementModel.attributeModel)
        .filter((att) => att !== "vendor_id" && att !== "measured_at")
        .concat("updated_at") as Array<keyof ISensorMeasurement>;

    public static jsonSchema: JSONSchemaType<ISensorMeasurement[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                percent_smoothed: { type: "integer" },
                percent_raw: { type: "integer" },
                is_blocked: { type: "boolean" },
                temperature: { type: "integer" },
                battery_status: { type: "number" },
                measured_at: { type: "string" },
                saved_at: { type: "string" },
                vendor_updated_at: { type: "string" },
                prediction_bin_full: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                firealarm: { type: "boolean" },
            },
            required: ["vendor_id"],
        },
    };
}
