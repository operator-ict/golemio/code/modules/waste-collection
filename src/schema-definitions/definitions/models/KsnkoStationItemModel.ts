import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IKsnkoStationItem } from "#sch/definitions/KsnkoStationsItems";

export class KsnkoStationItemModel extends Model<KsnkoStationItemModel> implements IKsnkoStationItem {
    declare id: number;
    declare count: number;
    declare trash_type: string;
    declare container_volume: number;
    declare container_brand: string;
    declare container_dump: string;
    declare cleaning_frequency_code: string;
    declare station_id: number;
    declare active: boolean;

    public static attributeModel: ModelAttributes<KsnkoStationItemModel> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        count: DataTypes.FLOAT,
        trash_type: DataTypes.STRING,
        container_volume: DataTypes.INTEGER,
        container_brand: DataTypes.STRING(150),
        container_dump: DataTypes.STRING(50),
        cleaning_frequency_code: DataTypes.STRING(50),
        station_id: DataTypes.INTEGER,
        active: DataTypes.BOOLEAN,
    };

    public static updateAttributes = Object.keys(KsnkoStationItemModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof IKsnkoStationItem>;

    public static jsonSchema: JSONSchemaType<IKsnkoStationItem[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                count: { type: "number" },
                trash_type: { type: "string" },
                container_volume: { type: "integer" },
                container_brand: { type: "string" },
                container_dump: { type: "string" },
                cleaning_frequency_code: { type: "string" },
                station_id: { type: "integer" },
                active: { type: "boolean" },
            },
            required: ["id"],
        },
    };
}
