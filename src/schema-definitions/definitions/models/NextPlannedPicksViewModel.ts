import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { INextPlannedPick } from "#sch/definitions/NextPlannedPicksView";

export class NextPlannedPicksViewModel extends Model<NextPlannedPicksViewModel> implements INextPlannedPick {
    declare ksnko_container_id: number;
    declare next_planned_pick_at: string;

    public static attributeModel: ModelAttributes<NextPlannedPicksViewModel, INextPlannedPick> = {
        ksnko_container_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        next_planned_pick_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
    };
}
