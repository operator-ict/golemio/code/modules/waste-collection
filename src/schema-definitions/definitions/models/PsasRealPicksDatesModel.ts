import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPsasRealPicksDates } from "#sch/definitions/PsasRealPicksDates";

export class PsasRealPicksDatesModel extends Model<IPsasRealPicksDates> implements IPsasRealPicksDates {
    public static tableName = "psas_real_picks_dates";

    declare internal_pick_id: string;
    declare subject_id: number;
    declare trash_type: string;
    declare trash_type_code: string | null;
    declare trash_type_name: string | null;
    declare frequency: string;
    declare container_volume: number;
    declare container_type: string;
    declare volume_ratio: number;
    declare valid_from: Date;
    declare valid_to: Date | null;
    declare station_number: string;
    declare station_address: string | null;
    declare psas_planned_pick_date: Date;
    declare psas_real_pick_date: Date | null;

    public static attributeModel: ModelAttributes<PsasRealPicksDatesModel, IPsasRealPicksDates> = {
        internal_pick_id: {
            type: DataTypes.STRING(36),
            primaryKey: true,
        },
        subject_id: DataTypes.INTEGER,
        trash_type: DataTypes.STRING(3),
        trash_type_code: DataTypes.STRING(3),
        trash_type_name: DataTypes.STRING(50),
        frequency: DataTypes.STRING(3),
        container_volume: DataTypes.INTEGER,
        container_type: DataTypes.STRING(255),
        volume_ratio: DataTypes.FLOAT,
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
        station_number: DataTypes.STRING(9),
        station_address: DataTypes.STRING(255),
        psas_planned_pick_date: DataTypes.DATE,
        psas_real_pick_date: DataTypes.DATE,
    };

    public static jsonSchema: JSONSchemaType<IPsasRealPicksDates> = {
        type: "object",
        properties: {
            internal_pick_id: { type: "string", maxLength: 36 },
            subject_id: { type: "integer" },
            trash_type: { type: "string", maxLength: 3 },
            trash_type_code: {
                oneOf: [
                    { type: "string", maxLength: 3 },
                    { type: "null", nullable: true },
                ],
            },
            trash_type_name: {
                oneOf: [
                    { type: "string", maxLength: 50 },
                    { type: "null", nullable: true },
                ],
            },
            frequency: { type: "string", maxLength: 3 },
            container_volume: { type: "integer" },
            container_type: { type: "string" },
            volume_ratio: { type: "number" },
            valid_from: { type: "object", required: ["toISOString"] },
            valid_to: {
                oneOf: [
                    { type: "object", required: ["toISOString"] },
                    { type: "null", nullable: true },
                ],
            },
            station_number: { type: "string", maxLength: 9 },
            station_address: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            psas_planned_pick_date: { type: "object", required: ["toISOString"] },
            psas_real_pick_date: {
                oneOf: [
                    { type: "object", required: ["toISOString"] },
                    { type: "null", nullable: true },
                ],
            },
        },
        required: ["subject_id", "station_address", "trash_type", "psas_planned_pick_date"],
    };

    public static arraySchema: JSONSchemaType<IPsasRealPicksDates[]> = {
        type: "array",
        items: PsasRealPicksDatesModel.jsonSchema,
    };

    public static attributeUpdateList = Object.keys(PsasRealPicksDatesModel.attributeModel)
        .filter((att) => !["subject_id", "station_address", "trash_type", "psas_planned_pick_date"].includes(att))
        .concat("updated_at");
}
