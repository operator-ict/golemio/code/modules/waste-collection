import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import IRawPickDates from "./interfaces/IRawPickDates";

export default class RawPickDates extends Model<IRawPickDates> implements IRawPickDates {
    declare date: string;
    declare subject_id: number;
    declare count: number;
    declare container_type: string;
    declare container_volume: number;
    declare frequency: string;
    declare street_name: string;
    declare trash_type: string;
    declare trash_type_code: string;
    declare trash_type_name: string;
    declare orientation_number: number;
    declare address_char: string;
    declare conscription_number: number;
    declare valid_from: string;
    declare valid_to: string | undefined;
    declare station_number: string;
    declare year_days_isoweeks: string | undefined;
    declare year: number;
    declare days: string;
    declare isoweeks: string;
    declare company: string;
    declare form_hash: string;
    declare psas_valid_from: string;
    declare psas_valid_to: string;
    declare is_historical: boolean;

    public static attributeModel: ModelAttributes<RawPickDates> = {
        date: DataTypes.DATE,
        subject_id: { type: DataTypes.INTEGER, primaryKey: true },
        count: DataTypes.FLOAT,
        container_type: DataTypes.TEXT,
        container_volume: DataTypes.INTEGER,
        frequency: DataTypes.STRING(3),
        street_name: DataTypes.TEXT,
        trash_type: DataTypes.STRING(3),
        trash_type_code: DataTypes.STRING(3),
        trash_type_name: DataTypes.STRING,
        orientation_number: DataTypes.INTEGER,
        address_char: DataTypes.STRING(5),
        conscription_number: DataTypes.FLOAT,
        psas_valid_from: DataTypes.DATE,
        psas_valid_to: DataTypes.DATE,
        station_number: DataTypes.STRING(9),
        year_days_isoweeks: DataTypes.TEXT,
        year: DataTypes.INTEGER,
        days: DataTypes.TEXT,
        isoweeks: DataTypes.TEXT,
        company: DataTypes.TEXT,
        form_hash: { type: DataTypes.UUID, allowNull: false, primaryKey: true },
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
        is_historical: DataTypes.BOOLEAN,
    };

    public static jsonSchema: JSONSchemaType<IRawPickDates[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                date: { type: "string" },
                subject_id: { type: "integer" },
                count: { type: "number" },
                container_type: { type: "string" },
                container_volume: { type: "integer" },
                frequency: { type: "string", maxLength: 3 },
                street_name: { type: "string" },
                trash_type: { type: "string", maxLength: 3 },
                trash_type_code: { type: "string", maxLength: 3, nullable: true },
                trash_type_name: { type: "string", nullable: true },
                orientation_number: { type: "integer" },
                address_char: { type: "string", maxLength: 5, nullable: true },
                conscription_number: { type: "number", nullable: true },
                psas_valid_from: { type: "string" },
                psas_valid_to: { type: "string", nullable: true },
                station_number: { type: "string", maxLength: 9, nullable: true },
                year_days_isoweeks: { type: "string", nullable: true },
                year: { type: "integer", nullable: true },
                days: { type: "string", nullable: true },
                isoweeks: { type: "string", nullable: true },
                company: { type: "string" },
                form_hash: { type: "string" },
                valid_from: { type: "string", nullable: true },
                valid_to: { type: "string", nullable: true },
                is_historical: { type: "boolean", nullable: true },
            },
            required: ["subject_id"],
        },
    };
}
