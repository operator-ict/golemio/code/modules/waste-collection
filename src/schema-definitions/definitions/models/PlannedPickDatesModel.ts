import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import IPlannedPickDates from "./interfaces/IPlannedPickDates";
import RawPickDates from "./RawPickDatesModel";

export default class PlannedPickDatesModel extends Model<IPlannedPickDates> implements IPlannedPickDates {
    declare ksnko_container_id: number;
    declare pick_date: string | Date;

    public static attributeModel: ModelAttributes<RawPickDates> = {
        ksnko_container_id: { type: DataTypes.INTEGER, primaryKey: true },
        pick_date: { type: DataTypes.DATE, primaryKey: true },
    };

    public static jsonSchema: JSONSchemaType<IPlannedPickDates[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                ksnko_container_id: { type: "integer" },
                pick_date: { type: "string", format: "date-time" },
            },
            required: ["ksnko_container_id", "pick_date"],
        },
    };
}
