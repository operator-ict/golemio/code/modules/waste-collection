import { MODULE_NAME } from "../../constants";

export interface ISensorPick {
    vendor_id: string;
    ksnko_container_id: number;
    pick_at: string;
    percent_before: number;
    percent_now: number;
    event_driven: boolean;
}

export const sensorPicks = {
    name: MODULE_NAME + "SensorPicks",
    pgTableName: "picks",
};
