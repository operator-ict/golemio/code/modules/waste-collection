import { ksnkoStationsDatasource } from "#sch/datasources/ksnko/KsnkoStationJsonSchema";
import { sensorMeasurementsDatasource } from "#sch/datasources/sensor/SensorMeasurementsJsonSchema";
import { sensorPicksDatasource } from "#sch/datasources/sensor/SensorPicksJsonSchema";
import { sensorVendorDataDatasource } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { ksnkoStations } from "#sch/definitions/KsnkoStations";
import { ksnkoStationContainers } from "#sch/definitions/KsnkoStationsContainers";
import { ksnkoStationItems } from "#sch/definitions/KsnkoStationsItems";
import { nextPlannedPicksView } from "#sch/definitions/NextPlannedPicksView";
import { sensorHistory } from "#sch/definitions/SensorHistory";
import { sensorLastMeasurementsView } from "#sch/definitions/SensorLastMeasurementsView";
import { sensorLastPicksView } from "#sch/definitions/SensorLastPicksView";
import { sensorMeasurements } from "#sch/definitions/SensorMeasurements";
import { sensorPicks } from "#sch/definitions/SensorPicks";
import { sensorVendorData } from "#sch/definitions/SensorVendorData";
import { MODULE_NAME, SCHEMA_NAME } from "../constants";
import KsnkoBulkyWasteStationsInputJsonSchema from "./datasources/ksnko/KsnkoBulkyWasteStationsJsonSchema";
import InputRawChangedContainersSchema from "./datasources/psas/InputRawChangedContainersSchema";
import InputRawPickDatesSchema from "./datasources/psas/InputRawPickDatesSchema";
import { ksnkoBulkyWasteStations } from "./definitions/KsnkoBulkyWasteStations";
import PsasRealPickDatesInputSchema from "#sch/datasources/psas/PsasRealPickDatesInputSchema";

const forExport: any = {
    name: MODULE_NAME,
    pgSchema: SCHEMA_NAME,
    datasources: {
        ksnkoStationsDatasource,
        sensorVendorDataDatasource,
        sensorMeasurementsDatasource,
        sensorPicksDatasource,
        ksnkoStations: {
            name: "KsnkoStationsDataSource",
            jsonSchema: ksnkoStationsDatasource.ksnkoStationsJsonSchema,
        },
        ksnkoBulkyWasteStation: {
            name: "KsnkoBulkWasteStationsDataSource",
            jsonSchema: KsnkoBulkyWasteStationsInputJsonSchema.jsonSchema,
        },
        rawPickDates: {
            name: "RawPickDatesDataSource",
            jsonSchema: InputRawPickDatesSchema.jsonSchema,
        },
        changedContainers: {
            name: "ChangedContainersDataSource",
            jsonSchema: InputRawChangedContainersSchema.jsonSchema,
        },
        psasRealPickDates: {
            name: "PsasRealPickDatesDataSource",
            jsonSchema: PsasRealPickDatesInputSchema.jsonSchema,
        },
    },
    definitions: {
        ksnkoStations,
        ksnkoStationItems,
        ksnkoStationContainers,
        ksnkoBulkyWasteStations,
        sensorMeasurements,
        sensorHistory,
        sensorPicks,
        sensorVendorData,
        sensorLastMeasurementsView,
        sensorLastPicksView,
        nextPlannedPicksView,
        plannedPickDates: {
            name: MODULE_NAME + "PlannedPickDates",
            pgTableName: "planned_pick_dates",
        },
        rawPickDates: {
            name: MODULE_NAME + "RawPickDates",
            pgTableNameOg: "raw_pick_dates",
            pgTableNameIe: "raw_pick_dates_history",
        },
        rawPickDatesView: {
            name: MODULE_NAME + "RawPickDatesView",
            pgTableName: "v_raw_pick_dates",
        },
        trashTypeMapper: {
            name: MODULE_NAME + "TrashTypeMapping",
            pgTableName: "trash_type_mapping",
        },
        itop: {
            sensorCycle: {
                pgTableName: "sensor_cycle_itop",
            },
            downloadHistory: {
                pgTableName: "download_history_itop",
            },
        },
        psasRealPickDates: {
            name: MODULE_NAME + "psasRealPickDates",
            pgTableName: "psas_real_picks_dates",
        },
    },
};

export { forExport as WasteCollection };
