import { DateTime, DurationLike } from "@golemio/core/dist/shared/luxon";
import { ISensorDateRangeTaskMessage } from "#ie/workers/tasks/interfaces/ISensorDateRangeTaskMessage";

export interface IMessageInterval {
    from: Date;
    to: Date;
}

export default class MessageIntervalHelper {
    public static getDefaultInterval(): IMessageInterval {
        const now = new Date();
        return {
            from: new Date(new Date().setHours(now.getHours() - 2)),
            to: now,
        };
    }

    public static getMessageInterval(data: ISensorDateRangeTaskMessage): IMessageInterval {
        try {
            return MessageIntervalHelper.parseInterval(data);
        } catch (err) {
            throw new Error(`Error while parsing message input data: ${err}`);
        }
    }

    public static generateWeekIntervals(from: DateTime, to: DateTime): IMessageInterval[] {
        return this.generateCustomIntervals(from, to, { week: 1 });
    }

    public static generateCustomIntervals(from: DateTime, to: DateTime, interval: DurationLike): IMessageInterval[] {
        const breakpoints = [from];
        let tmpFrom = from.plus(interval);

        while (tmpFrom.diff(to, "milliseconds").milliseconds < 0) {
            breakpoints.push(tmpFrom);
            tmpFrom = tmpFrom.plus(interval);
        }

        if (tmpFrom.diff(to, "milliseconds").milliseconds >= 0) {
            breakpoints.push(to);
        }

        const intervalsOutput = [];
        for (let i = 0; i < breakpoints.length - 1; i++) {
            const current = breakpoints[i];
            const next = breakpoints[i + 1];
            intervalsOutput.push({ from: current.toJSDate(), to: next.toJSDate() });
        }

        return intervalsOutput;
    }

    private static parseInterval(data: ISensorDateRangeTaskMessage) {
        const { from, to } = data;

        if (isNaN(Date.parse(from)) || isNaN(Date.parse(to))) {
            throw new Error(`Invalid "from" or "to" properties in message: "${data}"`);
        }

        return {
            from: new Date(from),
            to: new Date(to),
        };
    }
}
