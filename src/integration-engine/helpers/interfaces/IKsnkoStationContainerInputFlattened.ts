import { IKsnkoStationContainerInput } from "#sch/datasources/ksnko/KsnkoStationJsonSchema";

export interface IKsnkoStationContainerInputFlattened extends IKsnkoStationContainerInput {
    stationInfoId: string;
    cleaningFrequencyCode: string;
    active: string;
    isHistorical: Boolean;
}
