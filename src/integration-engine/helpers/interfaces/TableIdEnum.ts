export enum TableIdEnum {
    SensorVendorDataId = "vendor_id",
    KsnkoId = "id",
    RawPickDatesId = "subject_id",
}
