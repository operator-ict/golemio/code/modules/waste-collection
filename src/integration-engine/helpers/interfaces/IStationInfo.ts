export interface IStationInfo {
    id: number;
    number: string;
}
