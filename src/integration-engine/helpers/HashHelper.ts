import crypto from "crypto";

export class HashHelper {
    public static createCryptoHash = <T>(element: T, acceptList: Array<keyof T>): string => {
        const relevantData = acceptList
            .filter((key) => element[key] !== null && element[key] !== undefined)
            .map((key) => element[key as keyof T])
            .join(",");

        return this.formatAsUuid(crypto.createHash("md5").update(relevantData).digest("hex"));
    };

    private static formatAsUuid(text: string) {
        if (text.length !== 32) throw new Error(`AlertsIdHelper: Unable to format string '${text}' as uuid.`);
        return `${text.slice(0, 8)}-${text.slice(8, 12)}-${text.slice(12, 16)}-${text.slice(16, 20)}-${text.slice(20)}`;
    }
}
