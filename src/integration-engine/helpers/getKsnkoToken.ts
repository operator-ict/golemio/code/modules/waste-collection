import { config } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";

export const getKsnkoToken = async (): Promise<string> => {
    try {
        const response = await new HTTPFetchProtocolStrategy({
            url: `${config.datasources.KSNKOApi.url}/login`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                password: config.datasources.KSNKOApi.password,
                username: config.datasources.KSNKOApi.user,
            }),
        }).getRawData();

        return response.data.token;
    } catch (err) {
        throw new Error(`Can not obtain KSNKO auth token: ${err}`);
    }
};
