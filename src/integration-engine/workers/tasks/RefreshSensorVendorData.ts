import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { ISensorVendorTaskMessage } from "#ie/workers/tasks/interfaces/ISensorVendorTaskMessage";
import { SensorVendorTaskMessageSchema } from "#ie/workers/tasks/schema/SensorVendorTaskMessageSchema";
import { BaseSensorDataSourceFactory, getSensorVendorDatasource } from "#ie/datasources/SensorDataSource";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SensorVendorDataTransformation } from "#ie/transformations";
import { ISensorVendorDataInput } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { SensorHistoryRepository, SensorVendorDataRepository } from "#ie/repositories";
import { MODULE_NAME } from "../../../constants";
import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";

export default class RefreshSensorVendorData extends AbstractTask<ISensorVendorTaskMessage> {
    public readonly queueName = "refreshSensorVendorData";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = SensorVendorTaskMessageSchema;
    private sensorVendorDataRepository: SensorVendorDataRepository;
    private sensorHistoryRepository: SensorHistoryRepository;

    constructor() {
        super(MODULE_NAME);
        this.sensorVendorDataRepository = new SensorVendorDataRepository();
        this.sensorHistoryRepository = new SensorHistoryRepository();
    }

    protected async execute(msg: ISensorVendorTaskMessage): Promise<void> {
        try {
            await this.processSensorVendorData(getSensorVendorDatasource(msg.vendor));

            const { from, to } = MessageIntervalHelper.getDefaultInterval();

            await Promise.all([
                QueueManager.sendMessageToExchange(this.queuePrefix, "refreshSensorMeasurements", {
                    from,
                    to,
                    vendor: msg.vendor,
                }),
                QueueManager.sendMessageToExchange(this.queuePrefix, "refreshSensorPicks", {
                    from,
                    to,
                    vendor: msg.vendor,
                }),
            ]);
        } catch (err) {
            throw new GeneralError(`Error while executing refreshSensorVendorData.`, this.constructor.name, err);
        }
    }

    private processSensorVendorData = async (sensorDataSourceFactory: BaseSensorDataSourceFactory) => {
        try {
            const processingDate = new Date();

            const datasource = await sensorDataSourceFactory.getVendorDataDataSource();
            const dataStream = await datasource.getAll(true);

            const sensorVendorDataTransformation = new SensorVendorDataTransformation(
                sensorDataSourceFactory.datasourceName,
                processingDate
            );

            await dataStream
                .setDataProcessor(async (data: ISensorVendorDataInput[]) => {
                    const batch = await sensorVendorDataTransformation.transform(data);
                    const result = await this.sensorVendorDataRepository.bulkSave(
                        batch,
                        SensorVendorDataModel.attributeUpdateList,
                        true
                    );

                    await this.sensorVendorDataRepository.updateValidTo(result, processingDate, TableIdEnum.SensorVendorDataId);
                    await this.sensorHistoryRepository.updateHistory(batch);
                })
                .proceed();
        } catch (err) {
            throw new GeneralError(
                `Error while processing SensorVendorData (${sensorDataSourceFactory.datasourceName}).`,
                this.constructor.name,
                err
            );
        }
    };
}
