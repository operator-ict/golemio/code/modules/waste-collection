import { RawPickDatesDataSource } from "#ie/datasources/psas/RawPickDatesDataSource";
import PsasParserHelper from "#ie/helpers/PsasParserHelper";
import { KsnkoContainersRepository } from "#ie/repositories/KsnkoContainersRepository";
import { PlannedPickDatesRepository } from "#ie/repositories/PlannedPickDatesRepository";
import { RawPickDatesRepository } from "#ie/repositories/RawPickDatesRepository";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { PsasTransformation } from "#ie/transformations/PsasTransformation";
import IPlannedPickDates from "#sch/definitions/models/interfaces/IPlannedPickDates";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import { AbstractEmptyTask, config } from "@golemio/core/dist/integration-engine";
import PickDateDetails from "../helpers/PickDateDetails";
import RawPickDatesDataProcessor from "../helpers/RawPickDatesDataProcessor";
import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";

export default class CollectPickDatesTask extends AbstractEmptyTask {
    public readonly queueName = "collectPickDatesFromPsas";
    public readonly queueTtl = 23 * 60 * 60 * 1000;
    private readonly MAX_RESULTS_IN_BUFFER = 1000;
    private psasDataSource: RawPickDatesDataSource;
    private plannedPickDatesRepository: PlannedPickDatesRepository;
    private rawPickDatesRepository: RawPickDatesRepository;
    private ksnkoContainersRepository: KsnkoContainersRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.psasDataSource = new RawPickDatesDataSource(
            config.datasources.PSASApi.url,
            config.datasources.PSASApi.user,
            config.datasources.PSASApi.password
        );
        this.plannedPickDatesRepository = new PlannedPickDatesRepository();
        this.rawPickDatesRepository = new RawPickDatesRepository();
        this.ksnkoContainersRepository = new KsnkoContainersRepository();
    }

    protected execute = async (): Promise<void> => {
        const processingDate = new Date();
        const transformer = new PsasTransformation(processingDate);
        const downloadedData = await this.getRelevantData();
        await TrashTypeMappingRepository.getInstance().loadCache();
        const transformedData = await transformer.transform(downloadedData);
        const result = await this.rawPickDatesRepository.saveBulk(transformedData);
        if (result) {
            await this.rawPickDatesRepository.updateValidTo(result, processingDate, TableIdEnum.RawPickDatesId);
        }

        await this.generatePlannedPicks(transformedData);
    };

    private generatePlannedPicks = async (rawData: IRawPickDates[]) => {
        let resultsBuffer = new Array<IPlannedPickDates>();
        await this.plannedPickDatesRepository.cleanFuturePickDates();

        for (const rawPickDate of rawData) {
            const pickDateDetails = await this.getPickDateDetails(rawPickDate);

            if (pickDateDetails.isPlannable()) {
                resultsBuffer.push(...pickDateDetails.generatePlannedPickDates());
                resultsBuffer = await this.saveIfBufferFull(resultsBuffer);
            }
        }

        if (resultsBuffer.length > 0) {
            await this.plannedPickDatesRepository.saveBulk(resultsBuffer);
        }
    };

    private getRelevantData = async () => {
        const datasource = await this.psasDataSource.getData();
        const dataStream = await datasource.getAll(true);
        const dataProcessor = new RawPickDatesDataProcessor();
        await dataStream.setDataProcessor(dataProcessor.processData).proceed();
        const downloadedData = dataProcessor.getData();

        return downloadedData;
    };

    private getPickDateDetails = async (rawPickDate: IRawPickDates) => {
        const isoweeksArray = rawPickDate.isoweeks?.split(PsasParserHelper.SEPARATOR);
        const weekdaysArray = rawPickDate.days?.split(PsasParserHelper.SEPARATOR);
        const containerIds = await this.ksnkoContainersRepository.getIdByTypeAndStation(
            rawPickDate.trash_type_code,
            rawPickDate.station_number
        );

        return new PickDateDetails(isoweeksArray, weekdaysArray, containerIds, rawPickDate.valid_to);
    };

    private saveIfBufferFull = async (resultsBuffer: IPlannedPickDates[]) => {
        if (resultsBuffer.length > this.MAX_RESULTS_IN_BUFFER) {
            await this.plannedPickDatesRepository.saveBulk(resultsBuffer);
            resultsBuffer = new Array<IPlannedPickDates>();
        }

        return resultsBuffer;
    };
}
