import { ContainerChangesDatasource } from "#ie/datasources/psas/ContainerChangesDatasource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ContainersChangesRepository } from "#ie/repositories/ContainersChangesRepository";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { ContainerChangesTransformation } from "#ie/transformations/ContainerChangesTransformation";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export default class ContainersChangeTask extends AbstractEmptyTask {
    public readonly queueName = "containersChangeTask";
    public readonly queueTtl = 23 * 60 * 60 * 1000;
    private datasource: ContainerChangesDatasource;

    constructor(
        @inject(CoreToken.SimpleConfig) readonly simpleConfig: ISimpleConfig,
        @inject(ModuleContainerToken.ContainerChangesTransformation)
        private readonly transformation: ContainerChangesTransformation,
        @inject(ModuleContainerToken.ContainersChangesRepository) private readonly repository: ContainersChangesRepository,
        @inject(CoreToken.Logger) private readonly logger: ILogger
    ) {
        super(MODULE_NAME);
        this.datasource = new ContainerChangesDatasource(
            simpleConfig.getValue("old.datasources.PSASApi.url"),
            simpleConfig.getValue("old.datasources.PSASApi.user"),
            simpleConfig.getValue("old.datasources.PSASApi.password")
        );
    }

    protected execute = async (): Promise<void> => {
        await TrashTypeMappingRepository.getInstance().loadCache();
        const data = await this.datasource.getData();
        const transformedData = this.transformation.transformArray(data);
        await this.repository.replaceWithNewData(transformedData);
    };
}
