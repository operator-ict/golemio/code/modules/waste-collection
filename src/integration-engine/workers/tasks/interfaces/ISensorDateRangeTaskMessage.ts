import { ISensorVendorTaskMessage } from "#ie/workers/tasks/interfaces/ISensorVendorTaskMessage";

export interface ISensorDateRangeTaskMessage extends ISensorVendorTaskMessage {
    from: string;
    to: string;
}
