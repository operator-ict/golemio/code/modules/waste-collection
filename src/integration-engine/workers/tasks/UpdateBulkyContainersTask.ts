import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { KsnkoBulkyStationsRepository } from "#ie/repositories/KsnkoBulkyStationsRepository";
import { IKsnkoBulkyWasteStations } from "#sch/definitions/KsnkoBulkyWasteStations";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";
import { KsnkoBulkWasteStationsDataSourceFactory } from "./../../datasources/KsnkoBulkWasteStationDataSource";
import { KsnkoBulkyContainersTransformation } from "./../../transformations/KsnkoBulkyContainersTransformation";

@injectable()
export default class UpdateBulkyContainersTask extends AbstractEmptyTask {
    public readonly queueName = "updateBulkyContainersTask";
    public readonly queueTtl = 23 * 60 * 60 * 1000;

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ModuleContainerToken.KsnkoBulkyContainersTransformation)
        private transformation: KsnkoBulkyContainersTransformation,
        @inject(ModuleContainerToken.KsnkoBulkyStationsRepository)
        private repository: KsnkoBulkyStationsRepository,
        @inject(ModuleContainerToken.CityDistrictsRepository) private cityDistrictRepository: CityDistrictsModel
    ) {
        super(MODULE_NAME);
    }

    protected execute = async (): Promise<void> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const dataSource = await KsnkoBulkWasteStationsDataSourceFactory.getDataSource();
            const result = await dataSource.getAll();
            let transformedResult = await this.transformation.transform(result);
            transformedResult = await this.addDistrictSlug(transformedResult);

            if (transformedResult.length) {
                await this.repository.cleanFuturePickDates(t);
                await this.repository.bulkSave(transformedResult, undefined, false, false, t);
                await t.commit();
            }
        } catch (error) {
            await t.rollback();
            this.logger.error("UpdateBulkyContainersTask: " + error.message);
        }
    };

    private async addDistrictSlug(stations: IKsnkoBulkyWasteStations[]) {
        for (let index = 0; index < stations.length; index++) {
            const element = stations[index];

            element.city_district_slug = await this.cityDistrictRepository.getDistrict(
                element.geom.coordinates[0],
                element.geom.coordinates[1]
            );
        }

        return stations;
    }
}
