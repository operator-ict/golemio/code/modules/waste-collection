import { BaseSensorDataSourceFactory, getSensorVendorDatasource } from "#ie/datasources/SensorDataSource";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";
import { SensorMeasurementsRepository } from "#ie/repositories";
import { SensorLastMeasurementsViewRepository } from "#ie/repositories/SensorLastMeasurementsViewRepository";
import { SensorMeasurementsTransformation } from "#ie/transformations";
import { ISensorDateRangeTaskMessage } from "#ie/workers/tasks/interfaces/ISensorDateRangeTaskMessage";
import { SensorDateRangeTaskMessageSchema } from "#ie/workers/tasks/schema/SensorTaskMessageSchema";
import { ISensorMeasurementsInput } from "#sch/datasources/sensor/SensorMeasurementsJsonSchema";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractTask, log } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MODULE_NAME, SensorVendorName } from "../../../constants";
import { ISensorMeasurement } from "#sch/definitions/SensorMeasurements";

export default class RefreshSensorMeasurements extends AbstractTask<ISensorDateRangeTaskMessage> {
    public readonly queueName = "refreshSensorMeasurements";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = SensorDateRangeTaskMessageSchema;
    private sensorMeasurementsRepository: SensorMeasurementsRepository;
    private sensorLastMeasurementsViewRepository: SensorLastMeasurementsViewRepository;
    private logger: ILogger;

    constructor() {
        super(MODULE_NAME);
        this.logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
        this.sensorMeasurementsRepository = new SensorMeasurementsRepository();
        this.sensorLastMeasurementsViewRepository = new SensorLastMeasurementsViewRepository();
    }

    protected async execute(msg: ISensorDateRangeTaskMessage): Promise<void> {
        try {
            await this.processSensorMeasurements(msg, getSensorVendorDatasource(msg.vendor));
            await this.sensorLastMeasurementsViewRepository.refreshView();
        } catch (err) {
            throw new GeneralError(`Error while executing refreshSensorMeasurements.`, this.constructor.name, err);
        }
    }

    private processSensorMeasurements = async (
        msg: ISensorDateRangeTaskMessage,
        sensorDataSourceFactory: BaseSensorDataSourceFactory
    ) => {
        const transaction = await this.sensorMeasurementsRepository.getTransaction();
        let updateFailed = false;
        const transactionStartDate = new Date();

        try {
            const { from, to } = MessageIntervalHelper.getMessageInterval(msg);
            const datasource = await sensorDataSourceFactory.getMeasurementsDataSource(from, to);
            const dataStream = await datasource.getAll(true);

            const sensorMeasurementsTransformation = new SensorMeasurementsTransformation(sensorDataSourceFactory.datasourceName);

            await dataStream
                .setDataProcessor(async (data: ISensorMeasurementsInput[]) => {
                    try {
                        if (!updateFailed) {
                            const batch = await sensorMeasurementsTransformation.transform(data);
                            // get unique data based on vendor_id and measured_at with using hashmap
                            const { uniqueData, duplicities } = this.getUniques(batch);

                            if (uniqueData.length !== batch.length) {
                                this.logger.warn(
                                    // eslint-disable-next-line max-len
                                    `Duplicate ${duplicities.length} measurements identified for vendor: ${
                                        sensorDataSourceFactory.datasourceName
                                    } between ${from.toISOString()} and ${to.toISOString()}. Examples: ${duplicities
                                        .slice(0, 5)
                                        .join(", ")}`
                                );
                            }

                            await this.sensorMeasurementsRepository.bulkUpdate(uniqueData, transaction);
                        }
                    } catch (err) {
                        this.logger.error(
                            new GeneralError(
                                `Unable to bulkupdate data for sensor measurements: ${
                                    sensorDataSourceFactory.datasourceName
                                } from: ${from.toISOString()} to: ${to.toISOString()}`,
                                this.constructor.name,
                                err
                            )
                        );
                        updateFailed = true;
                    }
                })
                .proceed();

            this.logger.debug(updateFailed ? "Update failed, skipping deleteOldData" : "Update successful, deleting old data");

            if (!updateFailed) {
                if (sensorDataSourceFactory.datasourceName === SensorVendorName.SENSORITY) {
                    await this.sensorMeasurementsRepository.removeOld(
                        from,
                        to,
                        transactionStartDate,
                        transaction,
                        sensorDataSourceFactory.datasourceName
                    );
                }
                await transaction.commit();
            } else {
                await transaction.rollback();
            }
        } catch (err) {
            await transaction.rollback();

            throw new GeneralError(
                `Error while processing SensorMeasurements (${sensorDataSourceFactory.datasourceName}).`,
                this.constructor.name,
                err
            );
        }
    };

    private getUniques(batch: ISensorMeasurement[]) {
        const duplicities: string[] = [];
        const uniqueData = Object.values(
            batch.reduce((acc: Record<string, ISensorMeasurement>, curr: ISensorMeasurement) => {
                const key = `${curr.vendor_id}_${curr.measured_at}`;
                if (!acc[key]) {
                    acc[key] = curr;
                } else if (!duplicities.includes(key)) {
                    duplicities.push(key);
                }

                return acc;
            }, {})
        );
        return { uniqueData, duplicities };
    }
}
