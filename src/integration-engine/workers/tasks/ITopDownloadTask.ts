import { ITopDatasource } from "#ie/datasources/itop/ItopDatasource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ITopDownloadHistoryRepository } from "#ie/repositories/itop/ITopDownloadHistory";
import { SensorCycleITopRepository } from "#ie/repositories/itop/SensorCycleITopRepository";
import { ITopTransformation } from "#ie/transformations/ITopTransformation";
import IITopDownloadHistory from "#sch/definitions/models/itop/IITopDownloadHistory";
import { ISensorCycle } from "#sch/definitions/models/itop/ISensorCycle";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export default class ITopDownloadTask extends AbstractEmptyTask {
    public readonly queueName = "iTopDownloadTask";
    public readonly queueTtl = 23 * 60 * 60 * 1000;

    constructor(
        @inject(ModuleContainerToken.ITopDatasource) private readonly datasource: ITopDatasource,
        @inject(ModuleContainerToken.ITopRepository) private readonly sensorCycleRepository: SensorCycleITopRepository,
        @inject(ModuleContainerToken.ITopDownloadHistoryRepository)
        private readonly downloadHistory: ITopDownloadHistoryRepository,
        @inject(ModuleContainerToken.ITopTransformation) private readonly iTopTransformation: ITopTransformation,
        @inject(ContainerToken.Logger) private readonly logger: ILogger
    ) {
        super(MODULE_NAME);
    }

    protected execute = async (): Promise<void> => {
        try {
            const result = await this.datasource.getData();

            const transformedResult: ISensorCycle[] = await this.iTopTransformation.transform(result);
            await this.sensorCycleRepository.bulkUpdate(transformedResult);

            const downloadMetadata: IITopDownloadHistory = {
                record_date: new Date().toISOString(),
                form_ids: transformedResult.map((element) => element.external_form_id),
            };
            await this.downloadHistory.save(downloadMetadata);
        } catch (error) {
            this.logger.error("ITopDownloadTask: " + error.message);
        }
    };
}
