import { AbstractTask, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { BaseSensorDataSourceFactory, getSensorVendorDatasource } from "#ie/datasources/SensorDataSource";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SensorPicksTransformation } from "#ie/transformations";
import { SensorPicksRepository } from "#ie/repositories";
import { MODULE_NAME } from "../../../constants";
import { SensorLastPicksViewRepository } from "#ie/repositories/SensorLastPicksViewRepository";
import { ISensorDateRangeTaskMessage } from "#ie/workers/tasks/interfaces/ISensorDateRangeTaskMessage";
import { SensorDateRangeTaskMessageSchema } from "#ie/workers/tasks/schema/SensorTaskMessageSchema";

export default class RefreshSensorPicks extends AbstractTask<ISensorDateRangeTaskMessage> {
    public readonly queueName = "refreshSensorPicks";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = SensorDateRangeTaskMessageSchema;
    private sensorPicksRepository: SensorPicksRepository;
    private sensorLastPicksViewRepository: SensorLastPicksViewRepository;

    constructor() {
        super(MODULE_NAME);
        this.sensorPicksRepository = new SensorPicksRepository();
        this.sensorLastPicksViewRepository = new SensorLastPicksViewRepository();
    }

    protected async execute(msg: ISensorDateRangeTaskMessage): Promise<void> {
        try {
            await this.processSensorPicks(msg, getSensorVendorDatasource(msg.vendor));
            await this.sensorLastPicksViewRepository.refreshView();
        } catch (err) {
            throw new GeneralError(`Error while executing refreshSensorPicks.`, this.constructor.name, err);
        }
    }

    private processSensorPicks = async (
        msg: ISensorDateRangeTaskMessage,
        sensorDataSourceFactory: BaseSensorDataSourceFactory
    ) => {
        const { from, to } = MessageIntervalHelper.getMessageInterval(msg);
        const updatedAt = new Date().toISOString();
        const datasource = await sensorDataSourceFactory.getPicksDataSource(from, to);
        const data = await datasource.getAll();

        const sensorPicksTransformation = new SensorPicksTransformation(sensorDataSourceFactory.datasourceName);
        const transformed = await sensorPicksTransformation.transform({ data, updatedAt });

        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            await this.sensorPicksRepository.bulkUpdate(transformed, t);
            await this.sensorPicksRepository.deleteOldData({ from, to }, updatedAt, sensorDataSourceFactory.datasourceName, t);
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw new GeneralError(
                `Error while processing SensorPicks (${sensorDataSourceFactory.datasourceName}).`,
                this.constructor.name,
                err
            );
        }
    };
}
