import { VenzeoDataSource } from "#ie/datasources/venzeo/VenzeoDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { KsnkoBulkyStationsRepository } from "#ie/repositories/KsnkoBulkyStationsRepository";
import { VenzeoTransformation } from "#ie/transformations/VenzeoTransformation";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export default class SendDataToVenzeoTask extends AbstractEmptyTask {
    public readonly queueName = "sendDataToVenzeoTask";
    public readonly queueTtl = 2 * 60 * 60 * 1000;

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ModuleContainerToken.VenzeoTransformation)
        private transformation: VenzeoTransformation,
        @inject(ModuleContainerToken.VenzeoDataSource)
        private venzeoDataSource: VenzeoDataSource,
        @inject(ModuleContainerToken.KsnkoBulkyStationsRepository)
        private repository: KsnkoBulkyStationsRepository
    ) {
        super(MODULE_NAME);
    }

    protected execute = async (): Promise<void> => {
        try {
            const activeContainers = await this.venzeoDataSource.getActiveContainersFromVenzeoApi();

            for (const activeContainer of activeContainers) {
                await this.venzeoDataSource.deactivateBulkyStation(activeContainer.customId);
            }

            const dbData = await this.repository.find({
                where: {
                    pick_date: {
                        [Op.lte]: new Date().setUTCHours(23, 59, 59, 999),
                        [Op.gte]: new Date().setUTCHours(0, 0, 0, 0),
                    },
                },
            });

            let transformedResults = await this.transformation.transformArray(dbData);

            if (transformedResults.length > 200) {
                this.logger.warn("SendDataToVenzeoTask: Venzeo API too many calls please check SendDataToVenzeoTask.");
            }
            //their api cannot accept an array of objects, we need to send one by one
            for (const transformedResult of transformedResults) {
                await this.venzeoDataSource.sendDataToVenzeoApi(transformedResult);
            }
        } catch (error) {
            this.logger.error("SendDataToVenzeoTask: " + error.message + this.constructor.name);
        }
    };
}
