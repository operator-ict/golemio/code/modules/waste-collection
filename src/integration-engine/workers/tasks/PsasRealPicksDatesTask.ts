import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { MODULE_NAME } from "src/constants";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { PsasRealPicksDatesTransformation } from "#ie/transformations/PsasRealPicksDatesTransformation";
import { PsasRealPicksDatesRepository } from "#ie/repositories/PsasRealPicksDatesRepository";
import { PsasRealPicksDatesDataSource } from "#ie/datasources/psas/PsasRealPicksDatesDataSource";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";

@injectable()
export default class PsasRealPicksDatesTask extends AbstractEmptyTask {
    readonly queueName = "psasRealPicksDates";

    constructor(
        @inject(ModuleContainerToken.PsasRealPicksDatesDataSource) private readonly datasource: PsasRealPicksDatesDataSource,
        @inject(ModuleContainerToken.PsasRealPicksDatesTransformation)
        private readonly transformation: PsasRealPicksDatesTransformation,
        @inject(ModuleContainerToken.PsasRealPicksDatesRepository) private readonly repository: PsasRealPicksDatesRepository
    ) {
        super(MODULE_NAME);
    }

    protected async execute(): Promise<void> {
        await TrashTypeMappingRepository.getInstance().loadCache();
        const dataSource = await this.datasource.getDataSource();
        const data = await dataSource.getAll();
        const transformed = this.transformation.transformArray(data);
        await this.repository.saveData(transformed);
    }
}
