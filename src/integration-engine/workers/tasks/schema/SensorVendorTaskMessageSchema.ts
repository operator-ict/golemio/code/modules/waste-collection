import { IsIn } from "@golemio/core/dist/shared/class-validator";
import { ISensorVendorTaskMessage } from "#ie/workers/tasks/interfaces/ISensorVendorTaskMessage";
import { SensorVendorName } from "../../../../constants";

export class SensorVendorTaskMessageSchema implements ISensorVendorTaskMessage {
    @IsIn(Object.values(SensorVendorName).map((el) => el.toLowerCase()))
    vendor!: string;
}
