import { IsISO8601, IsOptional, IsString } from "@golemio/core/dist/shared/class-validator";
import { ISensorDateRangeTaskMessage } from "#ie/workers/tasks/interfaces/ISensorDateRangeTaskMessage";
import { SensorVendorTaskMessageSchema } from "#ie/workers/tasks/schema/SensorVendorTaskMessageSchema";

export class SensorDateRangeTaskMessageSchema extends SensorVendorTaskMessageSchema implements ISensorDateRangeTaskMessage {
    @IsOptional()
    @IsString()
    @IsISO8601()
    from!: string;

    @IsOptional()
    @IsString()
    @IsISO8601()
    to!: string;
}
