import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SensorMeasurementsRepository } from "#ie/repositories";
import { MODULE_NAME } from "../../../constants";
import { SensorLastMeasurementsViewRepository } from "#ie/repositories/SensorLastMeasurementsViewRepository";
import { ISensorDateRangeTaskMessage } from "#ie/workers/tasks/interfaces/ISensorDateRangeTaskMessage";
import { SensorDateRangeTaskMessageSchema } from "#ie/workers/tasks/schema/SensorTaskMessageSchema";
import { DateTime } from "@golemio/core/dist/shared/luxon";

export default class UpdateSensorsFromTo extends AbstractTask<ISensorDateRangeTaskMessage> {
    public readonly queueName = "updateSensorsFromTo";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = SensorDateRangeTaskMessageSchema;
    private sensorMeasurementsRepository: SensorMeasurementsRepository;
    private sensorLastMeasurementsViewRepository: SensorLastMeasurementsViewRepository;

    constructor() {
        super(MODULE_NAME);
        this.sensorMeasurementsRepository = new SensorMeasurementsRepository();
        this.sensorLastMeasurementsViewRepository = new SensorLastMeasurementsViewRepository();
    }

    protected async execute(msg: ISensorDateRangeTaskMessage): Promise<void> {
        try {
            const { from, to } = MessageIntervalHelper.getMessageInterval(msg);
            return this.updateSensors(DateTime.fromJSDate(from), DateTime.fromJSDate(to), msg.vendor);
        } catch (err) {
            throw new GeneralError(`Error while executing refreshSensorMeasurements.`, this.constructor.name, err);
        }
    }

    private updateSensors = async (from: DateTime, to: DateTime, vendor: string): Promise<void> => {
        try {
            const intervals = MessageIntervalHelper.generateCustomIntervals(from, to, { days: 3 });

            for (const interval of intervals) {
                const { from, to } = interval;
                await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshSensorMeasurements", {
                    from,
                    to,
                    vendor,
                });
                await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshSensorPicks", { from, to, vendor });
            }
        } catch (err) {
            throw new GeneralError("Error while processing updateSensors.", this.constructor.name, err);
        }
    };
}
