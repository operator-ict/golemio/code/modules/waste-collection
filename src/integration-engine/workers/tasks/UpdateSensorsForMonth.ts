import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MODULE_NAME } from "../../../constants";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { ISensorVendorTaskMessage } from "#ie/workers/tasks/interfaces/ISensorVendorTaskMessage";
import { SensorVendorTaskMessageSchema } from "#ie/workers/tasks/schema/SensorVendorTaskMessageSchema";

export default class UpdateSensorsForMonth extends AbstractTask<ISensorVendorTaskMessage> {
    public readonly queueName = "updateSensorsForMonth";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = SensorVendorTaskMessageSchema;

    constructor() {
        super(MODULE_NAME);
    }

    protected async execute(msg: ISensorVendorTaskMessage): Promise<void> {
        try {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "updateSensorsFromTo", {
                from: DateTime.now().minus({ month: 1 }),
                to: DateTime.now(),
                vendor: msg.vendor,
            });
        } catch (err) {
            throw new GeneralError(`Error while executing updateSensorsForMonth.`, this.constructor.name, err);
        }
    }
}
