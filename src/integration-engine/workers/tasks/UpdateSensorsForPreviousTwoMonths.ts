import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SensorMeasurementsRepository } from "#ie/repositories";
import { MODULE_NAME } from "../../../constants";
import { SensorLastMeasurementsViewRepository } from "#ie/repositories/SensorLastMeasurementsViewRepository";
import { ISensorDateRangeTaskMessage } from "#ie/workers/tasks/interfaces/ISensorDateRangeTaskMessage";
import { SensorDateRangeTaskMessageSchema } from "#ie/workers/tasks/schema/SensorTaskMessageSchema";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { ISensorVendorTaskMessage } from "#ie/workers/tasks/interfaces/ISensorVendorTaskMessage";
import { SensorVendorTaskMessageSchema } from "#ie/workers/tasks/schema/SensorVendorTaskMessageSchema";

export default class UpdateSensorsForPreviousTwoMonths extends AbstractTask<ISensorVendorTaskMessage> {
    public readonly queueName = "updateSensorsForPreviousTwoMonths";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = SensorVendorTaskMessageSchema;

    constructor() {
        super(MODULE_NAME);
    }

    protected async execute(msg: ISensorVendorTaskMessage): Promise<void> {
        try {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "updateSensorsFromTo", {
                from: DateTime.now().minus({ month: 3 }),
                to: DateTime.now().minus({ month: 1 }),
                vendor: msg.vendor,
            });
        } catch (err) {
            throw new GeneralError(`Error while executing updateSensorsForMonth.`, this.constructor.name, err);
        }
    }
}
