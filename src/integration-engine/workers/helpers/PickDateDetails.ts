import PsasParserHelper from "#ie/helpers/PsasParserHelper";
import IPlannedPickDates from "#sch/definitions/models/interfaces/IPlannedPickDates";

export default class PickDateDetails {
    private isoweeksArray: string[] | undefined;
    private weekdaysArray: string[] | undefined;
    private containerIds: number[] | null;
    private validTo: string | undefined;

    constructor(
        isoweeksArray: string[] | undefined,
        weekdaysArray: string[] | undefined,
        containerIds: number[] | null,
        validTo: string | undefined
    ) {
        this.isoweeksArray = isoweeksArray;
        this.weekdaysArray = weekdaysArray;
        this.containerIds = containerIds;
        this.validTo = validTo;
    }

    public isPlannable = (): boolean => {
        return !!this.weekdaysArray && !!this.containerIds && this.containerIds.length > 0;
    };

    public *generatePlannedPickDates(): Generator<IPlannedPickDates> {
        if (this.isPlannable()) {
            const pickDates = Array.from(
                PsasParserHelper.generatePickDates(this.isoweeksArray ?? [], this.weekdaysArray!, this.validTo)
            );
            for (const containerId of this.containerIds!) {
                for (const date of pickDates) {
                    yield { ksnko_container_id: containerId, pick_date: date.toISO() };
                }
            }
        }
    }
}
