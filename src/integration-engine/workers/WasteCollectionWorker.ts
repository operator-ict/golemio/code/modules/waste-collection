import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import RefreshSensorMeasurements from "#ie/workers/tasks/RefreshSensorMeasurements";
import RefreshSensorPicks from "#ie/workers/tasks/RefreshSensorPicks";
import RefreshSensorVendorData from "#ie/workers/tasks/RefreshSensorVendorData";
import UpdateSensorsForMonth from "#ie/workers/tasks/UpdateSensorsForMonth";
import UpdateSensorsForPreviousTwoMonths from "#ie/workers/tasks/UpdateSensorsForPreviousTwoMonths";
import UpdateSensorsFromTo from "#ie/workers/tasks/UpdateSensorsFromTo";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";
import { MODULE_NAME } from "src/constants";
import CollectPickDatesTask from "./tasks/CollectPickDatesTask";
import ContainersChangeTask from "./tasks/ContainersChangeTask";
import ITopDownloadTask from "./tasks/ITopDownloadTask";
import UpdateBulkyContainersTask from "./tasks/UpdateBulkyContainersTask";
import SendDataToVenzeoTask from "./tasks/SendDataToVenzeoTask";
import PsasRealPicksDatesTask from "./tasks/PsasRealPicksDatesTask";

export class WasteCollectionWorker extends AbstractWorker {
    protected name = MODULE_NAME;

    constructor() {
        super();
        /* sensor data */
        this.registerTask(new RefreshSensorVendorData());
        this.registerTask(new RefreshSensorMeasurements());
        this.registerTask(new RefreshSensorPicks());
        this.registerTask(new UpdateSensorsFromTo());
        this.registerTask(new UpdateSensorsForMonth());
        this.registerTask(new UpdateSensorsForPreviousTwoMonths());

        /* psas data */
        this.registerTask(new CollectPickDatesTask(this.getQueuePrefix()));
        this.registerTask(WasteCollectionContainer.resolve<ContainersChangeTask>(ModuleContainerToken.ContainersChangeTask));

        /* itop data */
        this.registerTask(WasteCollectionContainer.resolve<ITopDownloadTask>(ModuleContainerToken.ITopDownloadTask));

        /* bulky containers */
        this.registerTask(
            WasteCollectionContainer.resolve<UpdateBulkyContainersTask>(ModuleContainerToken.UpdateBulkyContainersTask)
        );

        /* venzeo data */
        this.registerTask(WasteCollectionContainer.resolve<SendDataToVenzeoTask>(ModuleContainerToken.SendDataToVenzeoTask));

        this.registerTask(WasteCollectionContainer.resolve<PsasRealPicksDatesTask>(ModuleContainerToken.PsasRealPicksDatesTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
