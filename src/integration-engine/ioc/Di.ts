import { ITopDatasource } from "#ie/datasources/itop/ItopDatasource";
import { VenzeoDataSource } from "#ie/datasources/venzeo/VenzeoDataSource";
import { ContainersChangesRepository } from "#ie/repositories/ContainersChangesRepository";
import { ITopDownloadHistoryRepository } from "#ie/repositories/itop/ITopDownloadHistory";
import { SensorCycleITopRepository } from "#ie/repositories/itop/SensorCycleITopRepository";
import { ContainerChangesTransformation } from "#ie/transformations/ContainerChangesTransformation";
import { ITopTransformation } from "#ie/transformations/ITopTransformation";
import { VenzeoTransformation } from "#ie/transformations/VenzeoTransformation";
import ContainersChangeTask from "#ie/workers/tasks/ContainersChangeTask";
import ITopDownloadTask from "#ie/workers/tasks/ITopDownloadTask";
import SendDataToVenzeoTask from "#ie/workers/tasks/SendDataToVenzeoTask";
import UpdateBulkyContainersTask from "#ie/workers/tasks/UpdateBulkyContainersTask";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { KsnkoBulkWasteStationsDataSourceFactory } from "./../datasources/KsnkoBulkWasteStationDataSource";
import { KsnkoBulkyStationsRepository } from "./../repositories/KsnkoBulkyStationsRepository";
import { KsnkoBulkyContainersTransformation } from "./../transformations/KsnkoBulkyContainersTransformation";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { PsasRealPicksDatesTransformation } from "#ie/transformations/PsasRealPicksDatesTransformation";
import { PsasRealPicksDatesDataSource } from "#ie/datasources/psas/PsasRealPicksDatesDataSource";
import { PsasRealPicksDatesRepository } from "#ie/repositories/PsasRealPicksDatesRepository";
import PsasRealPicksDatesTask from "#ie/workers/tasks/PsasRealPicksDatesTask";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";

//#region Initialization
const WasteCollectionContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasource
WasteCollectionContainer.register(ModuleContainerToken.ITopDatasource, ITopDatasource)
    .register(ModuleContainerToken.VenzeoDataSource, VenzeoDataSource)
    .register(ModuleContainerToken.KsnkoBulkWasteStationDataSource, KsnkoBulkWasteStationsDataSourceFactory);
WasteCollectionContainer.register(ModuleContainerToken.PsasRealPicksDatesDataSource, PsasRealPicksDatesDataSource);
//#endregion

//#region Transformation
WasteCollectionContainer.register(ModuleContainerToken.ITopTransformation, ITopTransformation);
WasteCollectionContainer.register(ModuleContainerToken.ContainerChangesTransformation, ContainerChangesTransformation);
WasteCollectionContainer.register(ModuleContainerToken.KsnkoBulkyContainersTransformation, KsnkoBulkyContainersTransformation);
WasteCollectionContainer.register(ModuleContainerToken.VenzeoTransformation, VenzeoTransformation);
WasteCollectionContainer.register(ModuleContainerToken.PsasRealPicksDatesTransformation, {
    useFactory: (c) => new PsasRealPicksDatesTransformation(TrashTypeMappingRepository.getInstance()),
});
//#endregion

//#region Repositories
WasteCollectionContainer.register(ModuleContainerToken.ITopRepository, SensorCycleITopRepository)
    .register(ModuleContainerToken.ContainersChangesRepository, ContainersChangesRepository)
    .register(ModuleContainerToken.KsnkoBulkyStationsRepository, KsnkoBulkyStationsRepository)
    .registerSingleton<CityDistrictsModel>(ModuleContainerToken.CityDistrictsRepository, CityDistrictsModel)
    .register(ModuleContainerToken.PsasRealPicksDatesRepository, PsasRealPicksDatesRepository);
//#endregion

//#region Tasks
WasteCollectionContainer.register(ModuleContainerToken.ITopDownloadTask, ITopDownloadTask)
    .register(ModuleContainerToken.ITopDownloadHistoryRepository, ITopDownloadHistoryRepository)
    .register(ModuleContainerToken.ContainersChangeTask, ContainersChangeTask)
    .register(ModuleContainerToken.SendDataToVenzeoTask, SendDataToVenzeoTask)
    .register(ModuleContainerToken.UpdateBulkyContainersTask, UpdateBulkyContainersTask)
    .register(ModuleContainerToken.PsasRealPicksDatesTask, PsasRealPicksDatesTask);
//#endregion

export { WasteCollectionContainer };
