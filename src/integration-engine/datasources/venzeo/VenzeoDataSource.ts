import { ITransformedDataVenzeo } from "#sch/definitions/ITransformedDataVenzeo";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class VenzeoDataSource {
    private baseUrl = this.config.getValue<string>("module.waste-collection.venzeo.url");
    private token = this.config.getValue<string>("module.waste-collection.venzeo.token");
    private params = new URLSearchParams({
        page: "1",
        active: "true",
    });

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {}

    public async sendDataToVenzeoApi(ksnko_data: ITransformedDataVenzeo): Promise<void> {
        try {
            const response = await fetch(this.baseUrl, {
                method: "POST",
                headers: {
                    Authorization: `Bearer ${this.token}`,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(ksnko_data),
            });
            const body = await response.text();

            if (response.status < 200 || response.status >= 300) {
                this.logger.error(
                    new GeneralError(
                        `Error while sending ksnko payload to Venzeo. Status code: ${response.status} `,
                        this.constructor.name,
                        body.substring(0, 200)
                    )
                );
            }
        } catch (error) {
            if (error instanceof GeneralError) {
                throw error;
            }

            throw new GeneralError("Error while sending ksnko payload to Venzeo.", this.constructor.name, error);
        }
    }

    public async deactivateBulkyStation(id: string): Promise<void> {
        try {
            const response = await fetch(`${this.baseUrl}${id}`, {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${this.token}`,
                    "Content-Type": "application/json",
                },
            });
            const body = await response.text();

            if (response.status < 200 || response.status >= 300) {
                this.logger.error(
                    new GeneralError(
                        `Error while deleting ksnko payload from Venzeo. Status code: ${response.status} `,
                        this.constructor.name,
                        body.substring(0, 200)
                    )
                );
            }
        } catch (error) {
            if (error instanceof GeneralError) {
                throw error;
            }

            throw new GeneralError("Error while deleting ksnko payload from Venzeo.", this.constructor.name, error);
        }
    }

    public async getActiveContainersFromVenzeoApi(): Promise<ITransformedDataVenzeo[]> {
        try {
            const response = await fetch(`${this.baseUrl}?${this.params.toString()}`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${this.token}`,
                    "Content-Type": "application/json",
                },
            });
            const body = await response.text();

            if (response.status < 200 || response.status >= 300) {
                this.logger.error(
                    new GeneralError(
                        `Error while getting active containers from Venzeo. Status code: ${response.status} `,
                        this.constructor.name,
                        body.substring(0, 200)
                    )
                );
            }

            return JSON.parse(body)?.pois ?? [];
        } catch (error) {
            if (error instanceof GeneralError) {
                throw error;
            }

            throw new GeneralError("Error while getting active containers from Venzeo.", this.constructor.name, error);
        }
    }
}
