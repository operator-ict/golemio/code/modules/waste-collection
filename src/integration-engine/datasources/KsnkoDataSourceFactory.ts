import { getKsnkoToken } from "#ie/helpers/getKsnkoToken";
import { WasteCollection } from "#sch";
import { config, DataSourceStreamed, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import JSONStream from "JSONStream";

export class KsnkoDataSourceFactory {
    public static async getDataSource(): Promise<DataSourceStreamed> {
        const token = await getKsnkoToken();

        const ksnkoStrategy = new HTTPRequestProtocolStrategyStreamed({
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
            method: "GET",
            url: `${config.datasources.KSNKOApi.url}/stations?full=1&detail=1`,
        }).setStreamTransformer(JSONStream.parse("data.*"));

        return new DataSourceStreamed(
            WasteCollection.datasources.ksnkoStationsDatasource.name,
            ksnkoStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.ksnkoStationsDatasource.name,
                WasteCollection.datasources.ksnkoStationsDatasource.jsonSchema
            )
        );
    }
}
