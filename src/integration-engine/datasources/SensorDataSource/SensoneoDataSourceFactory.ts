import { config, IProtocolStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import JSONStream from "JSONStream";
import { SensorVendorName } from "src/constants";
import { BaseSensorDataSourceFactory } from "./BaseSensorDataSourceFactory";

export class SensoneoDataSourceFactory extends BaseSensorDataSourceFactory {
    public datasourceName: SensorVendorName = config.datasources.WasteCollectionSensorSources.sensoneoApi.name;

    protected getVendorDataProtocolStrategy = (): IProtocolStrategy => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensoneoApi;
        return new HTTPRequestProtocolStrategyStreamed({
            method: "POST",
            url: `${apiConfig.url}/vendor`,
            body: JSON.stringify({
                vendor_id: [],
                ksnko_container_id: [],
            }),
            headers: {
                "Content-Type": "application/json",
                "x-api-key": `${apiConfig.token}`,
            },
        }).setStreamTransformer(JSONStream.parse("container_vendor_data.*"));
    };

    protected getMeasurementsProtocolStrategy = (from: Date, to: Date): IProtocolStrategy => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensoneoApi;
        return new HTTPRequestProtocolStrategyStreamed({
            method: "POST",
            url: `${apiConfig.url}/measurements`,
            body: JSON.stringify({
                vendor_id: [],
                ksnko_container_id: [],
                measured_at: {
                    start: from,
                    end: to,
                },
            }),
            headers: {
                "Content-Type": "application/json",
                "x-api-key": `${apiConfig.token}`,
            },
        }).setStreamTransformer(JSONStream.parse("measurements.*"));
    };

    protected getPicksProtocolStrategy = (from: Date, to: Date): HTTPFetchProtocolStrategy => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensoneoApi;
        return new HTTPFetchProtocolStrategy({
            method: "POST",
            url: `${apiConfig.url}/picks`,
            body: JSON.stringify({
                vendor_id: [],
                ksnko_container_id: [],
                measured_at: {
                    start: from,
                    end: to,
                },
            }),
            headers: {
                "Content-Type": "application/json",
                "x-api-key": `${apiConfig.token}`,
            },
        });
    };
}
