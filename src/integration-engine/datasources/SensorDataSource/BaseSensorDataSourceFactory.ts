import { WasteCollection } from "#sch";
import { DataSource, DataSourceStreamed, IProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SensorVendorName } from "src/constants";

export abstract class BaseSensorDataSourceFactory {
    public abstract datasourceName: SensorVendorName;

    public async getVendorDataDataSource(): Promise<DataSourceStreamed> {
        const sourceProtocolStrategy = this.getVendorDataProtocolStrategy();

        return new DataSourceStreamed(
            WasteCollection.datasources.sensorVendorDataDatasource.name + ` (${this.datasourceName})`,
            sourceProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.sensorVendorDataDatasource.name + ` (${this.datasourceName})`,
                WasteCollection.datasources.sensorVendorDataDatasource.jsonSchema
            )
        );
    }

    public async getMeasurementsDataSource(from: Date, to: Date): Promise<DataSourceStreamed> {
        const sourceProtocolStrategy = this.getMeasurementsProtocolStrategy(from, to);

        return new DataSourceStreamed(
            WasteCollection.datasources.sensorMeasurementsDatasource.name + ` (${this.datasourceName})`,
            sourceProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.sensorMeasurementsDatasource.name + ` (${this.datasourceName})`,
                WasteCollection.datasources.sensorMeasurementsDatasource.jsonSchema
            )
        );
    }

    public async getPicksDataSource(from: Date, to: Date): Promise<DataSource> {
        const sourceProtocolStrategy = this.getPicksProtocolStrategy(from, to);

        return new DataSource(
            WasteCollection.datasources.sensorPicksDatasource.name + ` (${this.datasourceName})`,
            sourceProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "picks" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.sensorPicksDatasource.name + ` (${this.datasourceName})`,
                WasteCollection.datasources.sensorPicksDatasource.jsonSchema
            )
        );
    }

    protected abstract getVendorDataProtocolStrategy(): IProtocolStrategy;
    protected abstract getMeasurementsProtocolStrategy(from: Date, to: Date): IProtocolStrategy;
    protected abstract getPicksProtocolStrategy(from: Date, to: Date): IProtocolStrategy;
}
