import { BaseSensorDataSourceFactory } from "./BaseSensorDataSourceFactory";
import { SensoneoDataSourceFactory } from "./SensoneoDataSourceFactory";
import { SensorityDataSourceFactory } from "./SensorityDataSourceFactory";
import { SensorVendorName } from "../../../constants";

const sensorityDataSourceFactory = new SensorityDataSourceFactory();
const sensoneoDataSourceFactory = new SensoneoDataSourceFactory();

const sensorVendorMapDatasource = new Map<SensorVendorName, BaseSensorDataSourceFactory>([
    [SensorVendorName.SENSONEO.toLowerCase() as SensorVendorName, sensoneoDataSourceFactory],
    [SensorVendorName.SENSORITY.toLowerCase() as SensorVendorName, sensorityDataSourceFactory],
]);

const getSensorVendorDatasource = (vendor: string) => {
    return sensorVendorMapDatasource.get(<SensorVendorName>vendor.toLowerCase())!;
};

export { BaseSensorDataSourceFactory, getSensorVendorDatasource };
