import { getKsnkoToken } from "#ie/helpers/getKsnkoToken";
import { BulkyStationsParameters } from "#ie/helpers/interfaces/IKsnkoBulkyWasteStations";
import { WasteCollection } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class KsnkoBulkWasteStationsDataSourceFactory {
    public static async getDataSource(): Promise<DataSource> {
        const token = await getKsnkoToken();

        const params = new URLSearchParams({
            dateFrom: new Date().toISOString().substring(0, 10),
            dateTo: new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString().substring(0, 10),
            limit: BulkyStationsParameters.limit.toString(),
        });

        return new DataSource(
            WasteCollection.datasources.ksnkoBulkyWasteStation.name,
            new HTTPFetchProtocolStrategy({
                headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
                method: "GET",
                url: `${config.datasources.KSNKOApi.url}/bulky-wastes/stations?${params.toString()}`,
            }),
            new JSONDataTypeStrategy({ resultsPath: "data" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.ksnkoBulkyWasteStation.name,
                WasteCollection.datasources.ksnkoBulkyWasteStation.jsonSchema
            )
        );
    }
}
