import { IITopRawObject } from "#sch/datasources/itop/IITopInputData";
import ITopInputDataSchema from "#sch/datasources/itop/ITopInputDataSchema";
import { IntegrationEngineConfiguration } from "@golemio/core/dist/integration-engine/config/IntegrationEngineConfiguration";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources/DataSource";
import { JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources/datatype-strategy/JSONDataTypeStrategy";
import { HTTPRequestProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategy";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import qs from "@golemio/core/dist/shared/qs";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export class ITopDatasource {
    private url: string;
    private username: string;
    private password: string;
    private jsonQuery =
        // eslint-disable-next-line max-len
        '{"operation": "core/get", "class": "Sensor", "key": "SELECT Sensor", "output_fields": "name, description, status, location_name, brand_name, model_name, ksnkoid, ismonitored, snetworkid, loraid, nbiotid, sigfoxid, serialnumber, asset_number, purchase_date, installdate, move2production, end_of_warranty, end_of_warranty_mhmp, servisdate"}';

    constructor(@inject(ContainerToken.Config) config: IntegrationEngineConfiguration) {
        this.url = config.datasources["waste-collection"].itop.url;
        this.username = config.datasources["waste-collection"].itop.username;
        this.password = config.datasources["waste-collection"].itop.password;
    }

    public getData = async (): Promise<IITopRawObject> => {
        const data = qs.stringify({
            auth_user: this.username,
            auth_pwd: this.password,
            json_data: this.jsonQuery,
        });

        const result: IITopRawObject = await this.getAll(data);
        return result;
    };

    private async getAll(data: string) {
        const dataSource = new DataSource(
            MODULE_NAME + "ITopDatasource",
            new HTTPRequestProtocolStrategy({
                method: "POST",
                url: this.url,
                body: data,
                headers: { "Content-Type": "application/x-www-form-urlencoded" },
                rejectUnauthorized: false,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(MODULE_NAME + "ITopDatasourceValidator", ITopInputDataSchema.jsonSchemaRaw)
        );

        return await dataSource.getAll();
    }
}
