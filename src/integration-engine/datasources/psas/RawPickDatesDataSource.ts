import { WasteCollection } from "#sch";
import { DataSourceStreamed, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import JSONStream from "JSONStream";
import { MODULE_NAME } from "src/constants";
import AbstractPsasDataSource from "./AbstractPsasDataSource";

export class RawPickDatesDataSource extends AbstractPsasDataSource {
    protected validator: IValidator;
    protected dataPath: string = "/api/View/oict_nadoby";
    protected name: string = MODULE_NAME + "RawPickDatesDataSourceStreamed";

    constructor(url: string, user: string, password: string) {
        super(url, user, password);
        this.validator = new JSONSchemaValidator(
            WasteCollection.datasources.rawPickDates.name,
            WasteCollection.datasources.rawPickDates.jsonSchema
        );
    }

    public getData = async (): Promise<DataSourceStreamed> => {
        const token = await this.getAuthorization();
        const psasStrategy = new HTTPRequestProtocolStrategyStreamed({
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
            method: "GET",
            url: new URL(this.dataPath, this.url).href,
            rejectUnauthorized: false,
        }).setStreamTransformer(JSONStream.parse("data.*"));

        return new DataSourceStreamed(this.name, psasStrategy, new JSONDataTypeStrategy({ resultsPath: "" }), this.validator);
    };
}
