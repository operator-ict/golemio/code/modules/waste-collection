import { HTTPRequestProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategy";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IValidator } from "@golemio/core/dist/shared/golemio-validator";

export default abstract class AbstractPsasDataSource {
    protected abstract dataPath: string;
    protected abstract name: string;
    protected abstract validator: IValidator;
    protected url: string;
    protected loginPath = "/api/User/Login";
    protected user: string;
    protected password: string;

    constructor(url: string, user: string, password: string) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    protected getAuthorization = async (): Promise<string> => {
        try {
            const result = await new HTTPRequestProtocolStrategy({
                url: new URL(this.loginPath, this.url).href,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ login: this.user, password: this.password }),
                rejectUnauthorized: false,
            }).getRawData();

            return result.data.data.token;
        } catch (err) {
            throw new GeneralError("Unable to refresh token for PsasDatasource.", this.constructor.name, err);
        }
    };
}
