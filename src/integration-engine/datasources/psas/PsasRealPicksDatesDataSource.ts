import AbstractPsasDataSource from "#ie/datasources/psas/AbstractPsasDataSource";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MODULE_NAME } from "src/constants";
import { WasteCollection } from "#sch";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";

@injectable()
export class PsasRealPicksDatesDataSource extends AbstractPsasDataSource {
    protected validator: IValidator;
    protected dataPath: string = "/api/View/oict_nadoby_a_data_svozu";
    protected name: string = MODULE_NAME + "PsasRealPicksDatesDataSource";

    constructor(@inject(CoreToken.SimpleConfig) readonly config: ISimpleConfig) {
        super(
            config.getValue<string>("module.PSASApi.url"),
            config.getValue<string>("module.PSASApi.user"),
            config.getValue<string>("module.PSASApi.password")
        );
        this.validator = new JSONSchemaValidator(
            WasteCollection.datasources.psasRealPickDates.name,
            WasteCollection.datasources.psasRealPickDates.jsonSchema
        );
    }

    public getDataSource = async (): Promise<DataSource> => {
        const token = await this.getAuthorization();
        const psasStrategy = new HTTPFetchProtocolStrategy({
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
            method: "GET",
            url: new URL(this.dataPath, this.url).href,
        });

        return new DataSource(this.name, psasStrategy, new JSONDataTypeStrategy({ resultsPath: "data" }), this.validator);
    };
}
