import { WasteCollection } from "#sch";
import { IRawChangedContainersInput } from "#sch/datasources/psas/IRawChangedContainersInput";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources/DataSource";
import { JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources/datatype-strategy/JSONDataTypeStrategy";
import { HTTPRequestProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategy";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MODULE_NAME } from "src/constants";
import AbstractPsasDataSource from "./AbstractPsasDataSource";

export class ContainerChangesDatasource extends AbstractPsasDataSource {
    protected validator: IValidator;
    protected dataPath: string = "/api/View/oict_odebrane_nadoby";
    protected name: string = MODULE_NAME + "ChangedContainersDataSource";

    constructor(url: string, user: string, password: string) {
        super(url, user, password);
        this.validator = new JSONSchemaValidator(
            WasteCollection.datasources.changedContainers.name,
            WasteCollection.datasources.changedContainers.jsonSchema
        );
    }

    public getData = async (): Promise<IRawChangedContainersInput[]> => {
        try {
            const token = await this.getAuthorization();
            const psasStrategy = new HTTPRequestProtocolStrategy({
                headers: {
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json",
                },
                method: "GET",
                url: new URL(this.dataPath, this.url).href,
                rejectUnauthorized: false,
            });

            const dataSource = new DataSource(
                this.name,
                psasStrategy,
                new JSONDataTypeStrategy({ resultsPath: "data" }),
                this.validator
            );

            return dataSource.getAll();
        } catch (error) {
            throw new GeneralError(`Error while getting data for Changed Containers from PSAS.`, this.constructor.name, error);
        }
    };
}
