import { HashHelper } from "#ie/helpers/HashHelper";
import { WasteCollection } from "#sch";
import { ISensorVendorDataInput } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { ISensorVendorData } from "#sch/definitions/SensorVendorData";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";

export class SensorVendorDataTransformation extends BaseTransformation implements ITransformation {
    private acceptList: Array<keyof ISensorVendorDataInput | keyof { isHistorical: Boolean }> = [
        "vendor_id",
        "ksnko_container_id",
        "sensor_id",
        "network",
        "bin_depth",
        "has_anti_noise",
        "anti_noise_depth",
        "algorithm",
        "version",
        "is_sensitive_to_pickups",
        "pick_up_sensor_type",
        "decrease_threshold",
        "pick_min_fill_level",
        "active",
        "bin_brand",
        "isHistorical",
    ];
    public name: string;

    constructor(private vendorName: string, private transformationDate: Date) {
        super();
        this.name = vendorName + WasteCollection.datasources.sensorVendorDataDatasource.name + "Transformation";
    }

    public transform = async (data: ISensorVendorDataInput[]): Promise<ISensorVendorData[]> => {
        const res: ISensorVendorData[] = [];

        for (const item of data) {
            const transformedData = this.transformElement(item);
            res.push(transformedData);
        }

        return res;
    };

    protected transformElement = (element: ISensorVendorDataInput): ISensorVendorData => {
        return {
            vendor_id: element.vendor_id,
            ksnko_container_id: element.ksnko_container_id,
            sensor_id: element.sensor_id,
            vendor: this.vendorName,
            prediction: element.prediction,
            installed_at: element.installed_at,
            installed_by: element.installed_by,
            network: element.network,
            bin_depth: element.bin_depth,
            has_anti_noise: element.has_anti_noise,
            anti_noise_depth: element.anti_noise_depth,
            algorithm: element.algorithm,
            schedule: element.schedule,
            sensor_version: element.version,
            is_sensitive_to_pickups: element.is_sensitive_to_pickups,
            pickup_sensor_type: element.pick_up_sensor_type,
            decrease_threshold: element.decrease_threshold,
            pick_min_fill_level: element.pick_min_fill_level,
            active: element.active,
            bin_brand: element.bin_brand,
            form_hash: HashHelper.createCryptoHash({ ...element, isHistorical: false }, this.acceptList),
            valid_from: this.transformationDate.toISOString(),
        };
    };
}
