import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { IRawChangedContainersInput } from "#sch/datasources/psas/IRawChangedContainersInput";
import { IContainerChange } from "#sch/definitions/models/interfaces/IContainerChange";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime } from "@golemio/core/dist/shared/luxon";

export class ContainerChangesTransformation extends AbstractTransformation<IRawChangedContainersInput, IContainerChange> {
    public name: string = "ContainerChangesTransformation";

    constructor() {
        super();
    }

    protected transformInternal = (element: IRawChangedContainersInput): IContainerChange => {
        try {
            const trashTypeInfo = TrashTypeMappingRepository.getInstance().findByPsasCode(element.typ_odpadu);

            return {
                subject_id: element.cislo_subjektu,
                task_id: element.ukoly,
                changed_at: DateTime.fromISO(element.poz_splneni, { zone: "Europe/Prague" }).toISO(),
                task_type: element.typ_ukolu,
                container_type: element.typ_nadoby,
                trash_type: element.typ_odpadu,
                trash_type_code: trashTypeInfo?.code_ksnko,
                trash_type_name: trashTypeInfo?.name,
                station_number: element.mag_pom,
                company: element.zpracovatel,
                removed_containers_count: element.odebrat,
                added_containers_count: element.dodat,
                street: element.ulice,
                city_district: element.mestska_cast,
                created_at: new Date().toISOString(),
            };
        } catch (error) {
            throw new GeneralError("Error while transforming ContainerChanges", this.constructor.name, error);
        }
    };
}
