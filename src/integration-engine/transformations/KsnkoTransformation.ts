import { HashHelper } from "#ie/helpers/HashHelper";
import { IKsnkoStationContainerInputFlattened } from "#ie/helpers/interfaces/IKsnkoStationContainerInputFlattened";
import { IStationInfo } from "#ie/helpers/interfaces/IStationInfo";
import projection from "#ie/helpers/projections";
import { WasteCollection } from "#sch";
import {
    IKsnkoStationContainerInput,
    IKsnkoStationInput,
    IKsnkoStationItemInput,
} from "#sch/datasources/ksnko/KsnkoStationJsonSchema";
import { IKsnkoStation } from "#sch/definitions/KsnkoStations";
import { IKsnkoStationContainer } from "#sch/definitions/KsnkoStationsContainers";
import { IKsnkoStationItem } from "#sch/definitions/KsnkoStationsItems";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Point } from "geojson";

export interface IKsnkoStationsTransformationResult {
    stations: IKsnkoStation[];
    items: IKsnkoStationItem[];
    containers: IKsnkoStationContainer[];
}

export class KsnkoTransformation extends BaseTransformation implements ITransformation {
    private acceptListContainers: Array<keyof IKsnkoStationContainerInputFlattened> = [
        "id",
        "volumeRatio",
        "cleaningFrequencyCode",
        "stationInfoId",
        "active",
        "isHistorical",
    ];
    private acceptListStations: Array<keyof IKsnkoStationInput | keyof { active: Boolean; isHistorical: Boolean }> = [
        "id",
        "number",
        "name",
        "location",
        "active",
        "isHistorical",
    ];

    public name: string;

    constructor(private transformationDate: Date) {
        super();
        this.name = WasteCollection.datasources.ksnkoStationsDatasource.name + "Transformation";
    }

    private transformKsnkoItem = (items: IKsnkoStationItemInput[], stationInfo: IStationInfo): IKsnkoStationItem[] => {
        const result = [];
        for (const item of items) {
            result.push({
                id: item.id,
                count: item.count,
                trash_type: item.trashType.code,
                container_volume: item.container.volume,
                container_brand: item.container.brand,
                container_dump: item.container.dump,
                cleaning_frequency_code: item.cleaningFrequency.code,
                station_id: stationInfo.id,
                active: true,
            });
        }
        return result;
    };

    private transformKsnkoContainer = (
        containers: IKsnkoStationContainerInput[],
        stationInfo: IStationInfo
    ): IKsnkoStationContainer[] => {
        const result = [];
        for (const container of containers) {
            result.push({
                id: container.id,
                code: container.code,
                volume_ratio: container.volumeRatio,
                trash_type: container.trashType.code,
                container_volume: container.container.volume,
                container_brand: container.container.brand,
                container_dump: container.container.dump,
                cleaning_frequency_code: container.cleaningFrequency.code,
                station_id: stationInfo.id,
                active: true,
                form_hash: HashHelper.createCryptoHash(
                    {
                        ...container,
                        stationInfoId: stationInfo.id,
                        cleaningFrequencyCode: container.cleaningFrequency.code,
                        active: true,
                        isHistorical: false,
                    },
                    this.acceptListContainers
                ),
                valid_from: this.transformationDate.toISOString(),
            });
        }
        return result;
    };

    public transform = async (data: IKsnkoStationInput[]): Promise<IKsnkoStationsTransformationResult> => {
        const res: IKsnkoStationsTransformationResult = {
            stations: [],
            items: [],
            containers: [],
        };

        for (const item of data) {
            const data = this.transformElement(item);
            res.stations.push(...data.stations);
            res.items.push(...data.items);
            res.containers.push(...data.containers);
        }

        return res;
    };

    protected transformElement = (element: IKsnkoStationInput): IKsnkoStationsTransformationResult => {
        const stationInfo = {
            id: element.id,
            number: element.number,
        };

        const station = {
            ...stationInfo,
            name: element.name,
            access: element.access,
            location: element.location,
            has_sensor: !!element.hasSensor,
            changed_at: element.changed,
            city_district_name: element.cityDistrict.name,
            coordinate_lat: element.coordinate.lat.toString(),
            coordinate_lon: element.coordinate.lon.toString(),
            geom: {
                type: "Point",
                coordinates: projection("EPSG:5514").inverse([element.coordinate.lat, element.coordinate.lon]),
            } as Point,
            form_hash: HashHelper.createCryptoHash({ ...element, active: true, isHistorical: false }, this.acceptListStations),
            valid_from: this.transformationDate.toISOString(),
            active: true,
        };

        const items = this.transformKsnkoItem(element.items, stationInfo);
        const containers = this.transformKsnkoContainer(element.containers, stationInfo);

        return {
            stations: [station],
            items,
            containers,
        };
    };
}
