import { IITopRawObject } from "#sch/datasources/itop/IITopInputData";
import { ISensorCycle } from "#sch/definitions/models/itop/ISensorCycle";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import crypto from "crypto";
import { MODULE_NAME } from "src/constants";

@injectable()
export class ITopTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MODULE_NAME + "ITopTransformation";
    }

    protected transformElement = (input: IITopRawObject): ISensorCycle[] => {
        const fieldResults: ISensorCycle[] = [];

        for (const key in input.objects) {
            if (Object.prototype.hasOwnProperty.call(input.objects, key)) {
                const element = input.objects[key];
                fieldResults.push({
                    sensor_id: element.fields.name,
                    description: element.fields.description,
                    status: element.fields.status,
                    location: element.fields.location_name,
                    vendor: element.fields.brand_name,
                    model: element.fields.model_name,
                    monitored: element.fields.ismonitored.toLowerCase() === "monitored",
                    network: element.fields.snetworkid,
                    lora_id: element.fields.loraid,
                    nbiot_id: element.fields.nbiotid,
                    sigfox_id: element.fields.sigfoxid,
                    serial_number: element.fields.serialnumber,
                    asset_number: element.fields.asset_number,
                    purchase_date: element.fields.purchase_date,
                    install_date: element.fields.installdate,
                    production_date: element.fields.move2production,
                    end_of_warranty: element.fields.end_of_warranty,
                    end_of_warranty_mhmp: element.fields.end_of_warranty_mhmp,
                    service_date: element.fields.servisdate,
                    record_date: new Date().toISOString(),
                    external_form_id: Number.parseInt(element.key),
                    form_hash: crypto.createHash("sha256").update(JSON.stringify(element), "utf8").digest("hex"),
                });
            }
        }

        return fieldResults;
    };
}
