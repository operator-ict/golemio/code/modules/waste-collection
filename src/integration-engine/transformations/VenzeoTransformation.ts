import { ITransformedDataVenzeo } from "#sch/definitions/ITransformedDataVenzeo";
import { IInputDataVenezeo } from "#sch/definitions/IVenzeoInput";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Duration } from "@golemio/core/dist/shared/luxon";

export class VenzeoTransformation extends AbstractTransformation<IInputDataVenezeo, ITransformedDataVenzeo> {
    public name: string = "VenzeoTransformation";

    constructor() {
        super();
    }

    protected transformInternal = (element: IInputDataVenezeo): ITransformedDataVenzeo => {
        try {
            const pickDateFrom = Duration.fromISOTime(element.pick_time_from).toFormat("hh:mm").toString();
            const pickDateTo = Duration.fromISOTime(element.pick_time_to).toFormat("hh:mm").toString();

            return {
                customId: element.custom_id,
                name: `${element.service_name} ${element.street} ${pickDateFrom}-${pickDateTo}`,
                note: null,
                location: {
                    latitude: element.geom.coordinates[1],
                    longitude: element.geom.coordinates[0],
                    altitude: null,
                },
                address: {
                    street: element.street,
                    city: element.city_district_name,
                    country: "Czech Republic",
                    aa1: null,
                    aa2: null,
                },
            };
        } catch (error) {
            throw new GeneralError("Error while transforming bulky containers to Venzeo data", this.constructor.name, error);
        }
    };
}
