import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { WasteCollection } from "#sch";
import { ISensorMeasurementsInput } from "#sch/datasources/sensor/SensorMeasurementsJsonSchema";
import { ISensorMeasurement } from "#sch/definitions/SensorMeasurements";

export class SensorMeasurementsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor(vendor: string) {
        super();
        this.name = vendor + WasteCollection.datasources.sensorMeasurementsDatasource.name + "Transformation";
    }

    public transform = async (data: ISensorMeasurementsInput[]): Promise<ISensorMeasurement[]> => {
        const res: ISensorMeasurement[] = [];

        for (const item of data) {
            const transformedData = this.transformElement(item);
            res.push(transformedData);
        }

        return res;
    };

    protected transformElement = (element: ISensorMeasurementsInput): ISensorMeasurement => {
        return {
            vendor_id: element.vendor_id,
            ksnko_container_id: element.ksnko_container_id,
            percent_smoothed: element.percent_smoothed,
            percent_raw: element.percent_raw,
            is_blocked: element.is_blocked,
            temperature: element.temperature,
            battery_status: element.battery_status,
            measured_at: element.measured_at,
            saved_at: element.saved_at,
            vendor_updated_at: element.updated_at,
            prediction_bin_full: element.prediction_bin_full,
            firealarm: element.firealarm,
        };
    };
}
