import projection from "#ie/helpers/projections";
import { IKsnkoBulkyWasteStationsJsonSchema } from "#sch/datasources/ksnko/KsnkoBulkyWasteStationsJsonSchema";
import { IKsnkoBulkyWasteStations } from "#sch/definitions/KsnkoBulkyWasteStations";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Point } from "@golemio/core/dist/shared/geojson";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export class KsnkoBulkyContainersTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MODULE_NAME + "KsnkoBulkyContainersTransformation";
    }

    protected transformElement = (item: IKsnkoBulkyWasteStationsJsonSchema): IKsnkoBulkyWasteStations => {
        return {
            id: item.id,
            custom_id: item.date + " " + item.timeFrom.substring(0, 5) + " " + "ID:" + item.id,
            pick_date: item.date,
            pick_time_from: item.timeFrom,
            pick_time_to: item.timeTo,
            street: item.street,
            payer: item.payer,
            number_of_containers: item.numberOfContainers,
            service_id: item.service.id,
            service_code: item.service.code,
            service_name: item.service.name,
            trash_type_id: item.trashType.id,
            trash_type: item.trashType.code,
            trash_type_name: item.trashType.name,
            city_district_id: item.cityDistrict.id,
            city_district_name: item.cityDistrict.name,
            city_district_ruian: item.cityDistrict.ruianCode,
            city_district_slug: undefined, // is added later from common.citydistricts
            coordinate_lat: item.coordinate.lat,
            coordinate_lon: item.coordinate.lon,
            geom: {
                type: "Point",
                coordinates: projection("EPSG:5514").inverse([item.coordinate.lat, item.coordinate.lon]),
            } as Point,
            changed_at: item.changed,
        };
    };
}
