import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IPsasRealPicksDatesInput } from "#sch/datasources/psas/IPsasRealPicksDatesInput";
import { IPsasRealPicksDates } from "#sch/definitions/PsasRealPicksDates";
import { DateTime } from "@golemio/core/dist/helpers";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import getUuidByString from "uuid-by-string";

export class PsasRealPicksDatesTransformation extends AbstractTransformation<IPsasRealPicksDatesInput, IPsasRealPicksDates> {
    name = "PsasRealPicksDatesTransformation";
    private timeZone = "Europe/Prague";

    constructor(private trashTypeMapper: TrashTypeMappingRepository) {
        super();
    }

    public transformArray = (data: IPsasRealPicksDatesInput[]): IPsasRealPicksDates[] => {
        const filtered = data.filter((el) => el.datum_planovaneho_svozu !== null);
        return filtered.map(this.transformElement);
    };

    protected transformInternal = (element: IPsasRealPicksDatesInput): IPsasRealPicksDates => {
        const trashTypeDetails = this.trashTypeMapper.findByPsasCode(element.typ_odpadu);

        return {
            internal_pick_id: this.getPickId(element),
            container_type: element.nadoba,
            container_volume: element.objem,
            frequency: element.cetnost,
            psas_planned_pick_date: DateTime.fromISO(element.datum_planovaneho_svozu!, { timeZone: this.timeZone }).toDate(),
            psas_real_pick_date: element.datum_skut_plneni
                ? DateTime.fromISO(element.datum_skut_plneni, { timeZone: this.timeZone }).toDate()
                : null,
            station_address: element.stanoviste,
            station_number: element.mag_pom,
            subject_id: element.cislo_subjektu,
            trash_type: element.typ_odpadu,
            trash_type_code: trashTypeDetails ? trashTypeDetails.code_ksnko : null,
            trash_type_name: trashTypeDetails ? trashTypeDetails.name : null,
            valid_from: DateTime.fromISO(element.platnost_od, { timeZone: this.timeZone }).toDate(),
            valid_to: element.platnost_do ? DateTime.fromISO(element.platnost_do, { timeZone: this.timeZone }).toDate() : null,
            volume_ratio: element.pocet,
        };
    };

    private getPickId(element: IPsasRealPicksDatesInput): string {
        return getUuidByString(
            element.cislo_subjektu + element.typ_odpadu + element.mag_pom + element + element.datum_planovaneho_svozu
        );
    }
}
