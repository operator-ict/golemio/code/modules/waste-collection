import PsasParserHelper from "#ie/helpers/PsasParserHelper";
import { WasteCollection } from "#sch";
import IRawPickDatesInput from "#sch/datasources/psas/IRawPickDatesInput";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { TIMEZONE } from "src/constants";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { HashHelper } from "#ie/helpers/HashHelper";

export class PsasTransformation extends BaseTransformation implements ITransformation {
    private trashTypeMapper: TrashTypeMappingRepository;
    private acceptList: Array<keyof IRawPickDatesInput | keyof { isHistorical: Boolean }> = [
        "cislo_subjektu",
        "pocet",
        "cetnost",
        "ulice",
        "typ_odpadu",
        "cor",
        "platnost_od",
        "platnost_do",
        "stanoviste",
        "tydny_roku",
        "svoz_firma",
        "isHistorical",
    ];
    name: string;

    constructor(private transformationDate: Date) {
        super();
        this.name = WasteCollection.datasources.rawPickDates.name + "Transformation";
        this.trashTypeMapper = TrashTypeMappingRepository.getInstance();
    }

    protected transformElement = async (element: IRawPickDatesInput): Promise<IRawPickDates> => {
        const pickDateDetails = PsasParserHelper.parseYearIsoWeeks(element.tydny_roku);
        const trashTypeDetails = this.trashTypeMapper.findByPsasCode(element.typ_odpadu);

        return {
            date: this.formatDate(element.datum),
            subject_id: element.cislo_subjektu,
            count: element.pocet,
            container_type: element.typ_nadoby,
            container_volume: element.objem,
            frequency: element.cetnost,
            street_name: element.ulice,
            trash_type: element.typ_odpadu,
            trash_type_code: trashTypeDetails?.code_ksnko,
            trash_type_name: trashTypeDetails?.name,
            orientation_number: element.cor,
            address_char: element.pism?.slice(0, 5),
            conscription_number: element.cpop ? element.cpop : undefined,
            psas_valid_from: this.formatDate(element.platnost_od),
            psas_valid_to: element.platnost_do ? this.formatDate(element.platnost_do) : undefined,
            station_number: element.stanoviste ? element.stanoviste : undefined,
            year_days_isoweeks: element.tydny_roku ? element.tydny_roku : undefined,
            year: pickDateDetails.year,
            days: pickDateDetails.days && pickDateDetails.days.length > 0 ? pickDateDetails.days : undefined,
            isoweeks:
                pickDateDetails.weekNumbers && pickDateDetails.weekNumbers.length > 0 ? pickDateDetails.weekNumbers : undefined,
            company: element.svoz_firma,
            form_hash: HashHelper.createCryptoHash(
                {
                    ...element,
                    platnost_do: element.platnost_do ? this.formatDateTime(element.platnost_do) : undefined,
                    platnost_od: this.formatDateTime(element.platnost_od),
                    pocet: element.pocet.toString(),
                    isHistorical: false,
                },
                this.acceptList
            ),
            valid_from: this.transformationDate.toISOString(),
        };
    };

    private formatDate(dateWithoutTimezone: string): string {
        return DateTime.fromISO(dateWithoutTimezone, { zone: TIMEZONE }).toISO();
    }

    private formatDateTime(dateWithoutTimezone: string): string {
        return DateTime.fromISO(dateWithoutTimezone, { zone: TIMEZONE }).toISO().replace("T", " ").substring(0, 19);
    }
}
