import { KsnkoWorker } from "#ie/workers/KsnkoWorker";
import { WasteCollection } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: "Ksnko" + WasteCollection.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + WasteCollection.name.toLowerCase(),
        queues: [
            {
                name: "refreshKsnkoData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 10 * 60 * 1000, // 10 minutes
                },
                worker: KsnkoWorker,
                workerMethod: "refreshKsnkoData",
            },
        ],
    },
];

export { queueDefinitions };
