import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { KsnkoStationItemModel } from "#sch/definitions/models/KsnkoStationItemModel";
import { IKsnkoStationItem } from "#sch/definitions/KsnkoStationsItems";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";

export class KsnkoItemsRepository extends KsnkoPostgresRepository implements IModel {
    protected updateAttributes: Array<keyof IKsnkoStationItem>;

    constructor() {
        super(
            WasteCollection.definitions.ksnkoStationItems.name + "Repository",
            {
                outputSequelizeAttributes: KsnkoStationItemModel.attributeModel,
                pgTableName: WasteCollection.definitions.ksnkoStationItems.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.ksnkoStationItems.name + "Validator",
                KsnkoStationItemModel.jsonSchema
            )
        );

        this.updateAttributes = KsnkoStationItemModel.updateAttributes;
    }
}
