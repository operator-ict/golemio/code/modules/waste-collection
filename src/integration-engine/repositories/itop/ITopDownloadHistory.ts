import { WasteCollection } from "#sch";
import { ITopDownloadHistoryModel } from "#sch/definitions/models/itop/ITopDownloadHistoryModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export class ITopDownloadHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MODULE_NAME + "ITopDownloadHistoryRepository",
            {
                outputSequelizeAttributes: ITopDownloadHistoryModel.attributeModel,
                pgTableName: WasteCollection.definitions.itop.downloadHistory.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator(MODULE_NAME + "ITopDownloadHistoryRepositoryValidator", ITopDownloadHistoryModel.jsonSchema)
        );
    }
}
