import { WasteCollection } from "#sch";
import { ISensorCycle } from "#sch/definitions/models/itop/ISensorCycle";
import { SensorCycleModel } from "#sch/definitions/models/itop/SensorCycleModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export class SensorCycleITopRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MODULE_NAME + "SensorCycleITopRepository",
            {
                outputSequelizeAttributes: SensorCycleModel.attributeModel,
                pgTableName: WasteCollection.definitions.itop.sensorCycle.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MODULE_NAME + "SensorCycleITopRepositoryValidator", SensorCycleModel.jsonSchema)
        );
    }

    public bulkUpdate = async (data: ISensorCycle[]) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<SensorCycleModel>(data, {
            ignoreDuplicates: true,
        });
    };
}
