import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { KsnkoBulkyWasteStationsModel } from "#sch/definitions/models/KsnkoBulkyWasteStationsModel";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { Transaction } from "@golemio/core/dist/shared/sequelize";

export class KsnkoBulkyStationsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.ksnkoBulkyWasteStations.name + "Repository",
            {
                outputSequelizeAttributes: KsnkoBulkyWasteStationsModel.attributeModel,
                pgTableName: WasteCollection.definitions.ksnkoBulkyWasteStations.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.ksnkoBulkyWasteStations.name + "Validator",
                KsnkoBulkyWasteStationsModel.jsonSchema
            )
        );
    }
    public cleanFuturePickDates = async (t: Transaction) => {
        await this.sequelizeModel.destroy({
            where: {
                pick_date: {
                    [Op.gt]: DateTime.now().endOf("day").toISO(), //YYYY-MM-DDT23:59:59.999+02:00
                },
            },
            transaction: t,
        });
    };
}
