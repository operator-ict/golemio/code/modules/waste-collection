import { ChangedContainersSchema } from "#sch/definitions/ChangedContainersSchema";
import { IContainerChange } from "#sch/definitions/models/interfaces/IContainerChange";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractValidatableRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractValidatableRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { SCHEMA_NAME } from "src/constants";

@injectable()
export class ContainersChangesRepository extends AbstractValidatableRepository {
    public validator = new JSONSchemaValidator("ContainersChangesRepositoryValidator", ChangedContainersSchema);
    public schema: string = SCHEMA_NAME;
    public tableName: string = "container_changes";

    constructor(@inject(CoreToken.PostgresConnector) dbConnector: IDatabaseConnector, @inject(CoreToken.Logger) log: ILogger) {
        super(dbConnector, log);
    }

    public async replaceWithNewData(data: IContainerChange[]): Promise<void> {
        try {
            await this.validator.Validate(data);
            const sequelize = this.connector.getConnection();
            await sequelize.query(`select ${this.schema}.container_changes_replace($1)`, {
                bind: [JSON.stringify(data)],
            });
        } catch (error) {
            throw new GeneralError("Error while replacing data in ContainersChangesRepository", this.constructor.name, error);
        }
    }
}
