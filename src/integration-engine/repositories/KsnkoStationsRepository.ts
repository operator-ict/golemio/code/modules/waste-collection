import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { KsnkoStationModel } from "#sch/definitions/models/KsnkoStationModel";
import { IKsnkoStation } from "#sch/definitions/KsnkoStations";

export class KsnkoStationsRepository extends KsnkoPostgresRepository implements IModel {
    protected updateAttributes: Array<keyof IKsnkoStation>;

    constructor() {
        super(
            WasteCollection.definitions.ksnkoStations.name + "Repository",
            {
                outputSequelizeAttributes: KsnkoStationModel.attributeModel,
                pgTableName: WasteCollection.definitions.ksnkoStations.pgTableNameIe,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.ksnkoStations.name + "Validator", KsnkoStationModel.jsonSchema)
        );

        this.updateAttributes = KsnkoStationModel.updateAttributes;
    }
}
