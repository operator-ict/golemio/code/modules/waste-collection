import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { KsnkoStationsRepository } from "#ie/repositories/KsnkoStationsRepository";
import { WasteCollection } from "#sch";
import { IKsnkoStationContainer } from "#sch/definitions/KsnkoStationsContainers";
import { KsnkoStationContainerModel } from "#sch/definitions/models/KsnkoStationContainerModel";
import { KsnkoStationModel } from "#sch/definitions/models/KsnkoStationModel";
import { log } from "@golemio/core/dist/integration-engine";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class KsnkoContainersRepository extends KsnkoPostgresRepository implements IModel {
    private readonly ksnkoStationsRepository: KsnkoStationsRepository;
    protected updateAttributes: Array<keyof IKsnkoStationContainer>;

    constructor() {
        super(
            WasteCollection.definitions.ksnkoStationContainers.name + "Repository",
            {
                outputSequelizeAttributes: KsnkoStationContainerModel.attributeModel,
                pgTableName: WasteCollection.definitions.ksnkoStationContainers.pgTableNameIe,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.ksnkoStationContainers.name + "Validator",
                KsnkoStationContainerModel.jsonSchema
            )
        );

        this.updateAttributes = KsnkoStationContainerModel.updateAttributes;

        this.ksnkoStationsRepository = new KsnkoStationsRepository();

        this.ksnkoStationsRepository["sequelizeModel"].hasMany(this.sequelizeModel, {
            sourceKey: "id",
            foreignKey: "station_id",
            as: "containers",
        });

        this.sequelizeModel.hasOne(this.ksnkoStationsRepository["sequelizeModel"], {
            sourceKey: "station_id",
            foreignKey: "id",
        });
    }

    public getIdByTypeAndStation = async (
        trashType: string | undefined,
        stationNumber: string | undefined
    ): Promise<number[] | null> => {
        if (trashType && stationNumber) {
            const result = await this.sequelizeModel.findAll<KsnkoStationContainerModel & { ksnko_station: KsnkoStationModel }>({
                include: [
                    {
                        model: this.ksnkoStationsRepository["sequelizeModel"],
                        as: "ksnko_stations_history",
                        attributes: ["number"],
                        where: { number: stationNumber },
                    },
                ],
                where: { trash_type: trashType, active: true },
            });

            if (result) {
                return result.map((el) => el.id);
            }
        }

        log.warn(`Unable to find Ksnko Containers for station number ${stationNumber} and trash type: ${trashType}`);
        return null;
    };
}
