import { WasteCollection } from "#sch";
import { ISensorMeasurement } from "#sch/definitions/SensorMeasurements";
import { SensorMeasurementModel } from "#sch/definitions/models/SensorMeasurementModel";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op, Transaction, literal } from "@golemio/core/dist/shared/sequelize";

export class SensorMeasurementsRepository extends PostgresModel implements IModel {
    private MAX_DAYS_TO_REMOVE_IN_PAST = 14;
    private logger: ILogger;

    constructor() {
        super(
            WasteCollection.definitions.sensorMeasurements.name + "Repository",
            {
                outputSequelizeAttributes: SensorMeasurementModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorMeasurements.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.sensorMeasurements.name + "Validator",
                SensorMeasurementModel.jsonSchema
            )
        );
        this.logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
    }

    public async getTransaction(): Promise<Transaction> {
        return await this.sequelizeModel.sequelize!.transaction();
    }

    public async bulkUpdate(data: ISensorMeasurement[], t: Transaction) {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<SensorMeasurementModel>(data, {
            updateOnDuplicate: SensorMeasurementModel.updateAttributes,
            transaction: t,
        });
    }

    /**
     * Only for Sensority. Sometimes vendor removes some faulty measurement data in past and we need to remove them too.
     */
    public async removeOld(from: Date, to: Date, startDate: Date, t: Transaction, vendorPrefix: string) {
        const count = await this.sequelizeModel.destroy({
            where: {
                measured_at: {
                    [Op.between]: [from, to],
                },
                vendor_id: {
                    [Op.startsWith]: vendorPrefix,
                },
                updated_at: {
                    [Op.lt]: startDate,
                },
                [Op.and]: literal(`measured_at > CURRENT_DATE - INTERVAL '${this.MAX_DAYS_TO_REMOVE_IN_PAST} days'`),
            },
            transaction: t,
        });

        if (count > 0) {
            this.logger.warn(
                // eslint-disable-next-line max-len
                `Deleted ${count} old sensor measurements for vendor ${vendorPrefix} between ${from.toISOString()} and ${to.toISOString()}`
            );
        }
    }
}
