import { WasteCollection } from "#sch";
import ITrashTypeMapping from "#sch/definitions/models/interfaces/ITrashTypeMapping";
import TrashTypeMappingModel from "#sch/definitions/models/TrashTypeMapperModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class TrashTypeMappingRepository extends PostgresModel implements IModel {
    private static instance: TrashTypeMappingRepository;
    private mappingTable: ITrashTypeMapping[] = [];

    public static getInstance() {
        if (!this.instance) {
            this.instance = new TrashTypeMappingRepository();
        }

        return this.instance;
    }

    private constructor() {
        super(
            WasteCollection.definitions.trashTypeMapper.name + "Repository",
            {
                outputSequelizeAttributes: TrashTypeMappingModel.attributeModel,
                pgTableName: WasteCollection.definitions.trashTypeMapper.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("TrashTypeReadOnly", {}) // readonly db table
        );
    }

    public findByKsnkoCode(ksnkoCode: string) {
        if (this.mappingTable.length === 0) {
            throw new GeneralError("Unable to map trash type. Please load cache first.", this.constructor.name);
        }

        return this.mappingTable.find((el) => el.code_ksnko === ksnkoCode);
    }

    public findByPsasCode(psasCode: string) {
        if (this.mappingTable.length === 0) {
            throw new GeneralError("Unable to map trash type. Please load cache first.", this.constructor.name);
        }

        return this.mappingTable.find((el) => el.code_psas === psasCode);
    }

    public async loadCache() {
        const result = await this.sequelizeModel.findAll<TrashTypeMappingModel>({ raw: true });
        for (const mapping of result) {
            this.mappingTable.push({
                name: mapping.name,
                code_ksnko: mapping.code_ksnko,
                code_psas: mapping.code_psas,
                legacy_id: mapping.legacy_id,
            });
        }
    }
}
