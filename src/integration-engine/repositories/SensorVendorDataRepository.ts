import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { WasteCollection } from "#sch";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";
import { ISensorVendorData } from "#sch/definitions/SensorVendorData";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { TIMEZONE } from "src/constants";
export class SensorVendorDataRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorVendorData.name + "Repository",
            {
                outputSequelizeAttributes: SensorVendorDataModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorVendorData.pgTableNameIe,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.sensorVendorData.name + "Validator",
                SensorVendorDataModel.jsonSchema
            )
        );
    }

    public updateValidTo = async (items: ISensorVendorData[], processingDate: Date, idName: TableIdEnum): Promise<void> => {
        // update the valid_to column in the database just in case there is a new record with the same id
        const newlyCreatedRecordIds = items.map((item) => item[idName as keyof ISensorVendorData]);
        const formatedDate = DateTime.fromISO(processingDate.toISOString(), { zone: TIMEZONE }).toISO();

        await this.sequelizeModel.update(
            { valid_to: formatedDate, is_historical: true },
            {
                where: {
                    [idName]: {
                        [Op.in]: newlyCreatedRecordIds,
                    },
                    [Op.and]: [Sequelize.where(Sequelize.fn("timestamptz", Sequelize.col("updated_at")), "<", formatedDate)],
                    valid_to: {
                        [Op.is]: null,
                    },
                },
            }
        );
    };
}
