import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { WasteCollection } from "#sch";
import { SensorLastPicksViewModel } from "#sch/definitions/models/SensorLastPicksViewModel";

export class SensorLastPicksViewRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorLastPicksView.name + "Repository",
            {
                outputSequelizeAttributes: SensorLastPicksViewModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorLastPicksView.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.sensorLastPicksView.name + "Validator", {})
        );
    }

    public refreshView = async () => {
        const connection = PostgresConnector.getConnection();
        return connection.query(`REFRESH MATERIALIZED VIEW CONCURRENTLY "waste_collection".v_last_picks;`, {
            type: Sequelize.QueryTypes.SELECT,
        });
    };
}
