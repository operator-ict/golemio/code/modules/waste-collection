import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { WasteCollection } from "#sch";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { TIMEZONE } from "src/constants";

export class RawPickDatesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.rawPickDates.name + "Repository",
            {
                outputSequelizeAttributes: RawPickDates.attributeModel,
                pgTableName: WasteCollection.definitions.rawPickDates.pgTableNameIe,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.rawPickDates.name + "Validator", RawPickDates.jsonSchema)
        );
    }

    public saveBulk = async (data: IRawPickDates[]) => {
        if (await this.validate(data)) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => !["subject_id", "valid_from"].includes(el))
                .map<keyof IRawPickDates>((el) => el as keyof IRawPickDates);
            fieldsToUpdate.push("updated_at" as any);
            return await this.sequelizeModel.bulkCreate<RawPickDates>(data, {
                updateOnDuplicate: fieldsToUpdate,
                returning: true,
            });
        }
    };

    public updateValidTo = async (items: IRawPickDates[], processingDate: Date, idName: TableIdEnum): Promise<void> => {
        const newlyCreatedRecordIds = items.map((item) => item[idName as keyof IRawPickDates]);
        const formatedDate = DateTime.fromISO(processingDate.toISOString(), { zone: TIMEZONE }).toISO();

        await this.sequelizeModel.update(
            { valid_to: formatedDate, is_historical: true },
            {
                where: {
                    [idName]: {
                        [Op.in]: newlyCreatedRecordIds,
                    },
                    [Op.and]: [Sequelize.where(Sequelize.fn("timestamptz", Sequelize.col("updated_at")), "<", formatedDate)],
                    valid_to: {
                        [Op.is]: null,
                    },
                },
            }
        );
    };
}
