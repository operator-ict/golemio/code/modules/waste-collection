import { TableIdEnum } from "#ie/helpers/interfaces/TableIdEnum";
import { log, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { TIMEZONE } from "src/constants";
export abstract class KsnkoPostgresRepository extends PostgresModel implements IModel {
    protected abstract updateAttributes: string[];

    public updateActive = async (data: any[], hasValidTo = true) => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            await this.validate(data);
            const rows = await this.sequelizeModel.bulkCreate(data, {
                updateOnDuplicate: this.updateAttributes,
                transaction: t,
                returning: true,
            });

            await this.sequelizeModel.update(
                { active: false },
                {
                    transaction: t,
                    where: {
                        id: {
                            [Op.notIn]: rows.map((row) => row.id),
                        },
                        ...(hasValidTo && {
                            valid_to: {
                                [Op.is]: null,
                            },
                        }),
                        active: true,
                    },
                }
            );
            await t.commit();
            return rows;
        } catch (err) {
            log.error(JSON.stringify({ message: err.message, errors: err.errors, fields: err.fields }));
            await t.rollback();
            throw new GeneralError("Error while saving data", this.constructor.name, err);
        }
    };

    public updateValidTo = async <T extends { [key in TableIdEnum]: any }>(
        items: T[],
        processingDate: Date,
        idName: TableIdEnum
    ): Promise<void> => {
        // update the valid_to column in the database just in case there is a new record with the same id
        const newlyCreatedRecordIds = items.map((item) => item[idName]);
        const formatedDate = DateTime.fromISO(processingDate.toISOString(), { zone: TIMEZONE }).toISO();

        await this.sequelizeModel.update(
            { valid_to: formatedDate, is_historical: true },
            {
                where: {
                    [idName]: {
                        [Op.in]: newlyCreatedRecordIds,
                    },
                    [Op.and]: [Sequelize.where(Sequelize.fn("timestamptz", Sequelize.col("updated_at")), "<", formatedDate)],
                    valid_to: {
                        [Op.is]: null,
                    },
                },
            }
        );
    };
}
