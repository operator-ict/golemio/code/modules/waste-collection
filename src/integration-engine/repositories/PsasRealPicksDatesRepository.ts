import { AbstractValidatableRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractValidatableRepository";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SCHEMA_NAME } from "src/constants";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ILogger } from "@golemio/core/dist/helpers";
import { IPsasRealPicksDates } from "#sch/definitions/PsasRealPicksDates";
import { GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { PsasRealPicksDatesModel } from "#sch/definitions/models/PsasRealPicksDatesModel";

@injectable()
export class PsasRealPicksDatesRepository extends AbstractValidatableRepository {
    public validator: JSONSchemaValidator;
    public schema = SCHEMA_NAME;
    public tableName = PsasRealPicksDatesModel.tableName;
    private sequelizeModel: ModelStatic<PsasRealPicksDatesModel>;

    constructor(@inject(CoreToken.PostgresConnector) dbConnector: IDatabaseConnector, @inject(CoreToken.Logger) log: ILogger) {
        super(dbConnector, log);
        this.validator = new JSONSchemaValidator("PsasRealPicksDatesRepositoryValidator", PsasRealPicksDatesModel.arraySchema);
        this.sequelizeModel = dbConnector
            .getConnection()
            .define(this.tableName, PsasRealPicksDatesModel.attributeModel, { schema: this.schema });
    }

    async saveData(data: IPsasRealPicksDates[]): Promise<void> {
        try {
            await this.validator.Validate(data);
        } catch (err) {
            throw new ValidationError("PsasRealPicksDates validation error", this.constructor.name, err);
        }

        try {
            await this.sequelizeModel.bulkCreate(data, {
                updateOnDuplicate: PsasRealPicksDatesModel.attributeUpdateList as Array<keyof IPsasRealPicksDates>,
            });
        } catch (err) {
            throw new GeneralError("PsasRealPicksDates saving error", this.constructor.name, err);
        }
    }
}
