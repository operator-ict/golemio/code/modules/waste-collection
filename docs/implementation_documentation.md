# Implementační dokumentace modulu _WASTE COLLECTION_

## Záměr

Modul slouží k ukládání a poskytování informací o svozu tříděného odpadu.

## Vstupní data

### Data aktivně stahujeme

Základem integrace je KSNKO (Komplexní systém nakládání s komunálním odpadem) - zdroj pravdy o polohách, typech odpadu a zdroj ID.

#### _KSNKO_

-   zdroj dat
    -   url: [https://ksnko.praha.eu/api/stations?full=1&detail=1](https://ksnko.praha.eu/api/stations?full=1&detail=1)
    -   paramtery dotazu: Bearer token získaný z `POST https://ksnko.praha.eu/api/login` s parametry
        ```
        user: config.datasources.KSNKOApi.user,
        password: config.datasources.KSNKOApi.password,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: ksnkoStationsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/ksnko-stations-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.refreshKsnkoData` (`0 25 7 * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.refreshKsnkoData`


#### _Sensor data_

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/container_vendor_data?code={code}](https://sensoneotools.azurewebsites.net/api/container_vendor_data?code={code})
        ```
        code: config.datasources.SensoneoApi.code,
        ```
    -   url: [https://sensemanag.sensority.eu/external/golemio-container-vendor-data](https://sensemanag.sensority.eu/external/golemio-container-vendor-data)
        ```
        Bearer: config.datasources.sensorityApi.token,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: SensorVendorDataJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/sensoneo-sensor-vendor-data-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.refreshSensorVendorData` (`0 5 * * * *`)
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`
-   název rabbitmq fronty

    -   `dataplatform.wastecollection.refreshSensorVendorData`

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/measurements?code={code}](https://sensoneotools.azurewebsites.net/api/measurements?code={code})
        ```
        code: config.datasources.SensoneoApi.code,
        ```
    -   url: [https://sensemanag.sensority.eu/external/golemio-container-measurements](https://sensemanag.sensority.eu/external/golemio-container-measurements)
        ```
        Bearer: config.datasources.sensorityApi.token,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: SensorMeasurementsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/sensoneo-sensor-measurements-datasource.json)
-   frekvence stahování
    -   spouští se z `refreshSensorVendorData`
-   název rabbitmq fronty

    -   `dataplatform.wastecollection.refreshSensorMeasurements`

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/picks?code={code}](https://sensoneotools.azurewebsites.net/api/picks?code={code})
        ```
        code: config.datasources.SensoneoApi.code,
        ```
    -   url: [https://sensemanag.sensority.eu/external/golemio-container-picks](https://sensemanag.sensority.eu/external/golemio-container-picks)
        ```
        Bearer: config.datasources.sensorityApi.token,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: SensorPicksJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/sensoneo-sensor-measurements-datasource.json)
-   frekvence stahování
    -   spouští se z `refreshSensorVendorData`
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.refreshSensorPicks`

#### _PSAS_ - oict_nadoby

-   zdroj dat
    -   url: [https://rapi.psas.cz:7777/api/View/oict_nadoby](https://rapi.psas.cz:7777/api/View/oict_nadoby)
    -   paramtery dotazu: Bearer token získaný z `POST https://rapi.psas.cz:7777/api/User/Login` s parametry
        ```
        user: config.datasources.PSAS.user,
        password: config.datasources.PSAS.password,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/psas/InputRawPickDatesSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/raw-pickdates-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.refreshPsasData` (`0 40 7 * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.refreshPsasData`

#### _PSAS_ - oict_odebrane_nadoby

-   zdroj dat
    -   url: [https://rapi.psas.cz:7777/api/View/oict_nadoby](https://rapi.psas.cz:7777/api/View/oict_odebrane_nadoby)
    -   paramtery dotazu: Bearer token získaný z `POST https://rapi.psas.cz:7777/api/User/Login` s parametry
        ```
        user: config.datasources.PSAS.user,
        password: config.datasources.PSAS.password,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategy`
    -   datový typ: json data stream
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/psas/InputRawChangedContainersSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/data/raw-containerChanges-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.containersChangeTask` (`0 5 8 * * *`)
-   název rabbitmq fronty
    - `dataplatform.wastecollection.containersChangeTask`

#### _PSAS_ - psas_real_picks_dates

-   zdroj dat
    -   url: [https://rapi.psas.cz:7777/api/View/oict_nadoby](https://rapi.psas.cz:7777/api/View/oict_odebrane_nadoby)
    -   paramtery dotazu: Bearer token získaný z `POST https://rapi.psas.cz:7777/api/User/Login` s parametry
        ```
        user: config.datasources.PSAS.user,
        password: config.datasources.PSAS.password,
        ```
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategy`
    -   datový typ: json
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/psas/PsasRealPickDatesInputSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/data/psas-real-picks-dates-data.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.psasRealPicksDates` (`0 5 4 * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.psasRealPicksDates`

#### _ITOP_

-   zdroj dat
    -   url: [https://itopt.oict.cz/webservices/rest.php?version=1.3](https://itopt.oict.cz/webservices/rest.php?version=1.3)
    -   paramtery dotazu:
            POST `x-www-form-urlencoded`
            obsahuje klíčové hodnoty: `auth_user`,`auth_pwd`,`json_data`
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategy`
    -   datový typ: json
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/itop/ITopInputDataSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/raw-itop-input-data.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.iTopDownloadTask` (`0 30 6 * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.iTopDownloadTask`

### Data posíláme metodou POST

Data posíláme do aplikace Venzeo, aby posádky, které přistavují jednotlivé Biovoky, měly aktuální informace a mohli potvrdit přistavení a odvoz jednotlivých nádob v reálném čase, což je pak zobrazováno na odpady.mojepraha.eu.

#### _VENZEO_

-   endpoint
    -   url: [https://operator-ict.venzeo.com/external-api/v1/pois/](https://operator-ict.venzeo.com/external-api/v1/pois/)
    -   paramtery dotazu:
            POST `application/json`
            hlavička s Bearer tokenem `Authorization:` `Bearer xxxx`
-   formát dat
    -   protokol: `HTTPRequestProtocolStrategy`
    -   datový typ: json
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/extras/importers/-/blob/development/imports/waste_collection_high_volume_containers/transfer_data.py?ref_type=heads)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.sendDataToVenzeoTask` (`0 0 3 * * *`) každý den ve tři ráno
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.sendDataToVenzeoTask`


## Zpracování dat / transformace

### _KsnkoWorker_

Při ukládání data upsertujeme a pro starší data nastavujeme atribut active na false.

#### _refreshKsnkoData_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.refreshKsnkoData`
    -   bez parametrů
-   datové zdroje
    -   KsnkoDataSourceFactory (http),
-   transformace:

    -   [KsnkoTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/KsnkoStationsTransformation.ts) - mapování pro
        -   `ksnko_stations_history` - označují stanoviště kde stojí vícero kontejnerů s různými druhy odpadu. sloupec `city_district_slug` doplněn pomocí dotazu přes `geom` na modul `city-districts`
        -   `ksnko_containers_history` - označuje jednotlivé nádoby.
        -   `ksnko_items` - označuje agregátně jednotlivé typy odpadu na jednotlivých stanovištích.

-   data modely
    -   KsnkoStationsRepository -> (schéma waste-collection) `ksnko_stations_history`
    -   KsnkoContainerRepository -> (schéma waste-collection) `ksnko_containers_history`
    -   KsnkoItemsRepository -> (schéma waste-collection) `ksnko_items`

### _WasteCollectionSensorWorker_

Při ukládání data upsertujeme.

#### _refreshSensorVendorData_

-   vstupní rabbitmq fronta:
    -   název: `dataplatform.wastecollection.refreshSensorVendorData`
    -   parametry: `{ "vendor": [vendorName] }`
-   datové zdroje:
    -   SensorDataSourceFactory (http),
-   transformace:
    -   [SensorVendorDataTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/SensorVendorDataTransformation.ts) - mapování pro
        -   `container_vendor_data` - označují všechny instalované nádoby.
-   data modely:
    -   SensorVendorDataRepository -> (schéma waste-collection) `container_vendor_data`
    -   SensorHistoryRepository -> (schéma waste-collection) `sensor_history`

#### _refreshSensorMeasurements_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.refreshSensorMeasurements`
    -   parametry: `{ "from": [timestamp], "to": [timestamp], "vendor": [vendorName] }`
-   datové zdroje
    -   SensorDataSourceFactory (http),
-   transformace:
    -   [SensorMeasurementsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/SensorMeasurementsTransformation.ts) - mapování pro
        -   `measurements` - označují všechna měření.
-   data modely
    -   SensorMeasurementsRepository -> (schéma waste-collection) `measurements`

-   neaktualizovaná data se u vendora Sensority promazávají, max 14 dní zpětně.

#### _refreshSensorPicks_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.refreshSensorPicks`
    -   parametry: `{ "from": [timestamp], "to": [timestamp], "vendor": [vendorName] }`
-   datové zdroje
    -   SensorDataSourceFactory (http),
-   transformace:
    -   [SensorPicksTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/SensorPicksTransformation.ts) - mapování pro
        -   `picks` - označují informace o svozech.
-   data modely
    -   SensorPicksRepository -> (schéma waste-collection) `picks`

#### _updateSensorsForMonth_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.updateSensorsForMonth`
    -   parametry: `{ "vendor": [vendorName] }` - generuje týdenní intervaly pro stáhnutí sensorických měření od `now - month` do `now`.
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.updateSensorsForMonth` (`0 35 5 * * *`)

#### _updateSensorsForPreviousTwoMonths_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.updateSensorsForPreviousTwoMonths`
    -   parametry: `{ "vendor": [vendorName] }` - generuje týdenní intervaly pro stáhnutí sensorických měření od `now - 3 months` do `now - month`.
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.updateSensorsForPreviousTwoMonths` (`0 45 5 1,8,15,22 * *`)

#### _updateSensorsFromTo_

_pro stahování/zpětnou opravu sensorických dat v určitém časovém rozpětí_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.updateSensorsFromTo`
    -   parametry: `{ "from": [timestamp], "to": [timestamp], "vendor": [vendorName] }` - generuje týdenní intervaly pro stáhnutí sensorických měření
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`

### _WasteCollectionWorker_

Při ukládání data `raw_pick_dates_history` upsertujeme.
Při ukládání data `planned_pick_dates`, která mají `pick_date` v budoucnosti smažeme a následně dohrajeme nová.

#### task _CollectPickDatesTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.collectPickDatesFromPsas`
    -   bez parametrů
-   datové zdroje
    -   PsasDataSource (http),
-   transformace:
    -   [PsasTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/transformations/PsasTransformation.ts) -
-   mapování pro
    -   `raw_pick_dates_history` - obsahuje raw informace o planovaných svozech. `trash_type_code`, `trash_type_name` jsou doplněny dotazem do lookup tabulky `trash_type_mapping`
-   generovaná data
    -   `planned_pick_dates`:
        `pick_date`: - na základě dat z `raw_pick_dates_history` generujeme naplánované svozy - `isoweeks` definuje týdny v roce, kdy se svoz uskuteční, pokud je null tak se bere v potaz každý týden - `days` definuje dny v týdnu, kdy se svoz uskuteční - rok je definován současností a propertka `year` se ignoruje - `valid_to` pokud je definováno tak negenerovat datumy svozu po tomto datu - z proměných je sestaven iso string např. "2022-W01-1" a knihovna luxon ho přeloží na datum.
        `ksnko_container_id` - se získává z tabulky `ksnko_containers_history` dotazem na `trash_type` = `raw_pick_dates_history`.`trash_type_code` a `station_number` = `raw_pick_dates_history`.`station_number` - pokud se najde více výsledků je potřeba pro každý výsledek vygenerovat samostatný řádek
        [generatePickDates](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/helpers/PsasParserHelper.ts)
-   data modely
    -   RawPickDatesRepository -> (schéma waste-collection) `raw_pick_dates_history`
    -   PlannedPickDatesRepository -> (schéma waste-collection) `planned_pick_dates`

#### task _ITopDownloadTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.iTopDownloadTask`
    -   message prázdná
    -   datové zdroje ITOP (http),

-   transformace:
    -   data jsou obohacena o:
            `form_hash` ~ `sha256` hash příchozích dat k jednomu senzoru
            `record_date` ~ aktuální datum a čas
    -   zbytek transformace spočívá, pouze k přejmenování klíčových hodnot

-   data modely
    -   SensorCycleModel -> (schéma waste-collection) `sensor_cycle_itop`
        - data jsou pouze vkládána v případě existence primárního klíče `external_form_id` a `form_hash` už data uložená máme
    -   ITopDownloadHistoryModel -> (schéma waste-collection) `download_history_itop`
        - při každém stažení je uložen seznam stažených `external_form_id` a `record_date` kdy ke stažení došlo

#### task _ContainersChangeTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.containersChangeTask`
    -   message prázdná
    - datový zdroj PSAS - oict_odebrane_nadoby (http)

-   transformace:
    -   data jsou obohacena o: trash_type_code, trash_type_name z číselníku v tabulce `trash_type_mapping`
    -   `poz_splneni` je parsováno podle Pražské timezone a uloženo jako timestamptz
-   zbytek transformace spočívá, pouze k přejmenování klíčových hodnot

-   data
    -   uložené entity popisuje interface `IContainerChange`
    -   místo modelu s k nahrazení starých dat používá sql funkce `container_changes_replace` do které vstupuje json s novými daty

#### task _SendDataToVenzeoTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.sendDataToVenzeoTask`
    -   message prázdná
    -   datový zdroj Venzeo POST request - na endpoint posíláme údaje o bulky waste stations

-   transformace:
    -   data bereme z naši tabulky ksnko_bulky_waste_stations a transformujeme v `VenzeoTransformation` podle interfacu `ITransformedDataVenzeo`

-   data
    -  KsnkoBulkyStationsRepository -> data bereme z naši tabulky ksnko_bulky_waste_stations


#### task _UpdateBulkyContainersTask_
- vstupní rabbitmq fronta
    - název: `dataplatform.wastecollection.updateBulkyContainersTask`
    - message prázdná
    - datový zdroj KSNKO

- transformace:
    - pouze přejmenování hodnot

- data
    - uložené entity popisuje interface `IKsnkoBulkyWasteStations`
    - v tabulce držíme aktuální stav, t.j. svozy minulé se nemění a svozy plánované se updatují jednou denne
## Uložení dat

Lookup tabulka `trash_type_mapping` je naplněna migračním skriptem a její obsah si repository cachuje in memory v `TrashTypeMappingRepository`.

KSNKO: Pokud se objeví nová station/container/item je potřeba to zapracovat do existující databáze, ale pokud nepřijdou updatovaná data o dané položce, tak si ji držme, jen ji označme jako neaktivní (atribut `active: true`).

Psas: - `raw_pick_dates_history`: Vkládání se provádí UPSERTem přes atribut `subject_id`. Pokud přestane nádoba přestane na api chodit, záznam zůstane, jenom bude mít staré datum. - `planned_pick_dates`: Na základě atributů `days` a `isoweeks` z `raw_pick_dates_history` tabulky, se vygenerují budoucí datumy očekávaných svozů (prosté dva sloupce `ksnko_container_id` a date `pick_date`). Atribut `year` bude ignorován. Při updatu je třeba aktualizovat pouze datumy v budoucnosti. Minulé datumy je potřeba považovat za ukládanou historii.

ITOP: slouží k správě informací o životního cyklu o senzorech. Technicky obsahuje formulář s číselným id a možnost v čase změnit jakoukoliv hodnotu. Z toho důvodu ukládáme všechny změny ve formuláři a také které formuláře jsme stáhli. Pro přehled aktuálních stavů senzorů slouží pohled `sensor_cycle_itop_actual`

### Obecné

-   typ databáze
    -   PSQL
-   datábázové schéma
    -   ![waste-collection er diagram](assets/wastecollection_erd.png)
-   historizace
    -   u historizace se za pomocí metody updateValidTo označí staré záznamy jako neaktivní a připíše se k nim aktuální datum transformace do valid_to
    - záznamy se historizují na základě srovnání updated_at < transformationDate s tím že transformation date se tvoří dříve než updated_at tím tedy nebude platit pro aktuální záznam ale pro ty z předešlých transformací
    