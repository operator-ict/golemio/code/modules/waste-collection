CREATE TABLE sensor_cycle_itop (
	external_form_id int4 NOT NULL,
	form_hash char(64) NOT NULL,
	sensor_id varchar NULL,
	description text NULL,
	status varchar NULL,
	"location" varchar NULL,
	vendor varchar NULL,
	model varchar NULL,
	ksnko_container_id int4 NULL,
	monitored bool NULL,
	network varchar NULL,
	lora_id varchar NULL,
	nbiot_id varchar NULL,
	sigfox_id varchar NULL,
	serial_number varchar NULL,
	asset_number varchar NULL,
	purchase_date varchar NULL,
	install_date date NULL,
	production_date date NULL,
	end_of_warranty date NULL,
	end_of_warranty_mhmp date NULL,
	service_date date NULL,	
	record_date timestamptz NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	
	CONSTRAINT sensor_cycle_itop_pk PRIMARY KEY (external_form_id)
);

CREATE TABLE download_history_itop (
	record_date timestamptz NOT NULL,
	form_ids int4[] NOT NULL,
	CONSTRAINT download_history_itop_pk PRIMARY KEY (record_date)
);

CREATE VIEW sensor_cycle_itop_actual
AS
select 
	distinct on (external_form_id) external_form_id,
    sci.sensor_id,
    sci.description,
    sci.status,
    sci.location,
    sci.vendor,
    sci.model,
    sci.ksnko_container_id,
    sci.monitored,
    sci.network,
    sci.lora_id,
    sci.nbiot_id,
    sci.sigfox_id,
    sci.serial_number,
    sci.asset_number,
    sci.purchase_date,
    sci.install_date,
    sci.production_date,
    sci.end_of_warranty,
    sci.end_of_warranty_mhmp,
    sci.service_date,    
    sci.record_date,
	sci.form_hash,
    sci.create_batch_id,
    sci.created_at,
    sci.created_by,
    sci.update_batch_id,
    sci.updated_at,
    sci.updated_by
from sensor_cycle_itop as sci
inner join (
	select unnest(form_ids) as form_id, record_date  from download_history_itop
	where record_date = (
		select max(record_date) from download_history_itop
	)
) actualdownload on
	sci.external_form_id = actualdownload.form_id 	
order by external_form_id asc, sci.record_date desc;