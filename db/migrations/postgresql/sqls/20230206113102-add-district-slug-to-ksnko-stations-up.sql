ALTER TABLE ksnko_stations ADD city_district_slug varchar(50) NULL;

CREATE INDEX stations_district_slug_idx ON ksnko_stations USING btree (city_district_slug);
