-- drop dependant views
DROP VIEW IF EXISTS analytic.v_waste_collection_containers;
DROP VIEW IF EXISTS analytic.v_waste_collection_containers_technical_info;
DROP VIEW IF EXISTS analytic.v_waste_collection_containers_rfid;
DROP VIEW IF EXISTS analytic.v_waste_collection_measurements;


-- drop duplicate columns
ALTER TABLE ksnko_containers DROP CONSTRAINT ksnko_containers_station_number_fkey;
ALTER TABLE ksnko_containers DROP COLUMN station_number;

ALTER TABLE ksnko_items DROP CONSTRAINT ksnko_items_station_number_fkey;
ALTER TABLE ksnko_items DROP COLUMN station_number;

ALTER TABLE ksnko_stations DROP CONSTRAINT ksnko_stations_number_key;
CREATE UNIQUE INDEX ksnko_stations_number_key ON ksnko_stations (number) WHERE active = true;


-- recreate views without station_number dependency

 -- analytic.v_waste_collection_measurements source

CREATE OR REPLACE VIEW analytic.v_waste_collection_measurements
AS
SELECT m.ksnko_container_id,
	sta."number"  station_number,
    --k.station_number,
    m.percent_smoothed,
    m.percent_raw,
    m.measured_at,
    m.measured_at::date AS measured_at_date,
    split_part(m.measured_at::character varying(50)::text, ' '::text, 2) AS measured_at_time,
    COALESCE(m.percent_smoothed::numeric, 0::numeric) / 100::numeric AS percent_smoothed_calculated_decimal
   FROM waste_collection.measurements m
     JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
     JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id
     join waste_collection.ksnko_stations sta on sta.id = k.station_id ;



-- analytic.v_waste_collection_containers source

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers
AS
SELECT v.vendor_id,
    v.vendor,
    v.id,
    v.code,
    v.volume_ratio,
    v.trash_type,
    v.container_volume,
    v.container_dump,
    v.container_brand,
    v.cleaning_frequency_code,
    concat(v.cleaning_frequency_code, ' – ', COALESCE(v.days, 'NA'::text)) AS cleaning_frequency_displayed,
    v.station_id,
    v.station_number,
    v.active,
    v.create_batch_id,
    v.created_at,
    v.created_by,
    v.update_batch_id,
    v.updated_at,
    v.updated_by,
    v.city_district_name,
    split_part(v.city_district_name::text, ' '::text, 2) AS city_district_number,
    v.coordinate_lat,
    v.coordinate_lon,
    v.trash_type_name,
    v.days,
    v.street_name,
    v.frequency,
    v.name,
    v.network,
    v.company,
    v.bin_brand,
    v.frequency_text,
    v.row_number
   FROM ( SELECT cvd.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_dump,
            kc.container_volume,
                CASE
                    WHEN kc.container_brand::text = 'Podzemní'::text THEN 'Podzemní'::text
                    ELSE 'Nadzemní'::text
                END AS container_brand,
            kc.cleaning_frequency_code,
            kc.station_id,
            ks.number station_number,
            cvd.active,
            kc.create_batch_id,
            kc.created_at,
            kc.created_by,
            kc.update_batch_id,
            kc.updated_at,
            kc.updated_by,
            ks.city_district_name,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            rpd.company,
            ks.name,
            cvd.network,
            cvd.bin_brand,
            cvd.vendor,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data cvd
             JOIN waste_collection.ksnko_containers kc ON cvd.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
             LEFT JOIN waste_collection.raw_pick_dates rpd ON ks.number::text = rpd.station_number::text AND ks.active IS TRUE
             AND kc.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE) v
  WHERE v.row_number = 1;

 -- analytic.v_waste_collection_containers_rfid source

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_rfid
AS
WITH main AS (
         SELECT cvdr.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_dump,
            kc.container_volume,
            kc.cleaning_frequency_code,
            concat(kc.cleaning_frequency_code, ' – ', COALESCE(rpd.days, 'NA'::text)) AS cleaning_frequency_displayed,
            kc.station_id,
            ks.number station_number,
            cvdr.active,
            ks.city_district_name,
            split_part(ks.city_district_name::text, ' '::text, 2) AS city_district_number,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            rpd.company,
            ks.name,
            cvdr.network,
            cvdr.bin_brand,
            cvdr.vendor,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data_rfid cvdr
             JOIN waste_collection.ksnko_containers kc ON cvdr.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
             LEFT JOIN waste_collection.raw_pick_dates rpd ON ks.number::text = rpd.station_number::text AND kc.trash_type::text = rpd.trash_type_code::text AND ks.active IS TRUE
          WHERE cvdr.active IS TRUE
        ), last_pick AS (
         SELECT DISTINCT ON (pr.ksnko_container_id) pr.ksnko_container_id,
            pr.pick_at
           FROM waste_collection.picks_rfid pr
          ORDER BY pr.ksnko_container_id, pr.pick_at DESC
        )
 SELECT main.vendor_id,
    main.vendor,
    main.id,
    main.code,
    main.volume_ratio,
    main.trash_type,
    main.container_volume,
    main.container_dump,
    main.cleaning_frequency_code,
    main.cleaning_frequency_displayed,
    main.station_id,
    main.station_number,
    main.active,
    main.city_district_name,
    main.city_district_number,
    main.coordinate_lat,
    main.coordinate_lon,
    main.trash_type_name,
    main.days,
    main.street_name,
    main.frequency,
    main.name,
    main.network,
    main.company,
    main.bin_brand,
    main.frequency_text,
    main.row_number,
    last_pick.pick_at AS last_pick
   FROM main
     LEFT JOIN last_pick ON main.id = last_pick.ksnko_container_id
  WHERE main.row_number = 1;

 -- analytic.v_waste_collection_containers_technical_info source

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info
AS
WITH main AS (
         SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            ks_1.number station_number,
            ks_1.city_district_name,
            ks_1.name,
            initcap(rpd.trash_type_name::text)::character varying AS trash_type_name,
            cvd_1.network,
            cvd_1.anti_noise_depth,
            cvd_1.bin_depth,
            cvd_1.vendor,
            cvd_1.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed::double precision / 100::double precision AS percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd_1 ON m.vendor_id::text = cvd_1.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd_1.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks_1 ON k.station_id = ks_1.id
             JOIN waste_collection.raw_pick_dates rpd ON ks_1.number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text AND ks_1.active IS TRUE
          WHERE cvd_1.active IS TRUE AND m.measured_at >= (now() - '3 mons'::interval)
        ), min_percent AS (
         SELECT DISTINCT m.ksnko_container_id,
            max(m.measured_at) OVER (PARTITION BY m.ksnko_container_id) AS last_measured_at,
            m.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id) AS min_percent_smoothed
                   FROM main) m
          WHERE m.min_percent_smoothed = m.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
            main.trash_type_name,
            1::double precision - main.anti_noise_depth::double precision / main.bin_depth::double precision AS anti_noise_depth,
            min(main.battery_status) OVER (PARTITION BY main.ksnko_container_id) AS last_battery_status
           FROM main
          WHERE main.measured_at >= (now() - '14 days'::interval day)
        ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END AS daily_msg_expected,
                    30 AS count_days,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN
                            CASE
                                WHEN count(main.measured_at) > 4 THEN 4::bigint
                                ELSE count(main.measured_at)
                            END
                            ELSE
                            CASE
                                WHEN count(main.measured_at) > 7 THEN 7::bigint
                                ELSE count(main.measured_at)
                            END
                        END AS daily_msg
                   FROM main
                  WHERE main.measured_at >= (CURRENT_DATE::timestamp with time zone - '30 days'::interval) AND main.measured_at < CURRENT_DATE::timestamp with time zone
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.vendor, main.sensor_version, main.measured_at_day, (
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        ), max_measured_at AS (
         SELECT measurements.ksnko_container_id,
            max(measurements.measured_at) AS max_measured_at
           FROM waste_collection.measurements
          GROUP BY measurements.ksnko_container_id
        )
 SELECT max_measured_at.max_measured_at,
    cvd.ksnko_container_id,
    cvd.vendor_id,
    ks.number as station_number,
    ks.name,
    ks.city_district_name,
    lbs.trash_type_name,
    cvd.sensor_version,
    cvd.network,
    COALESCE(round((lbs.anti_noise_depth * 100::double precision)::numeric, 2)::character varying(255), 'není'::character varying) AS anti_noise_depth,
    mp.min_percent_smoothed * 100::double precision AS min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent * 100::double precision AS msg_transfer_in_percent
   FROM waste_collection.container_vendor_data cvd
     JOIN waste_collection.ksnko_containers kc ON kc.id = cvd.ksnko_container_id
     LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
     LEFT JOIN min_percent mp ON mp.ksnko_container_id = cvd.ksnko_container_id
     LEFT JOIN last_battery_status lbs ON cvd.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON cvd.ksnko_container_id = msg.ksnko_container_id
     LEFT JOIN max_measured_at ON cvd.ksnko_container_id = max_measured_at.ksnko_container_id
  WHERE cvd.active;
