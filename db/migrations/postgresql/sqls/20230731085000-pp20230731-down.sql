CREATE OR REPLACE FUNCTION analytic.waste_collection_last_correct(p_continer_code integer)
 RETURNS timestamp without time zone
 LANGUAGE plpgsql
AS $function$       
declare 
    posledni integer;
    vysledek timestamptz;
begin
    posledni = (
        select percent_smoothed   
        from waste_collection.measurements
        where ksnko_container_id = p_continer_code
        order by measured_at desc
        limit 1);
    vysledek=(
    select measured_at          
    from waste_collection.measurements
    where ksnko_container_id = p_continer_code
--  and not percent_smoothed between p_min and p_max
    and not percent_smoothed between posledni-3 and posledni+3
    order by measured_at desc
    limit 1);
   return coalesce (vysledek, (select min(measured_at) from waste_collection.measurements where ksnko_container_id = p_continer_code));
end;
$function$
;


CREATE OR REPLACE VIEW analytic.v_waste_collection_5_days_same
AS SELECT s.ksnko_container_id,
    s.min_percent,
    s.max_percent,
    analytic.waste_collection_last_correct(s.ksnko_container_id) AS last_correct
   FROM ( SELECT m.ksnko_container_id,
            min(m.percent_smoothed) AS min_percent,
            max(m.percent_smoothed) AS max_percent
           FROM waste_collection.measurements m
          WHERE m.measured_at > (CURRENT_DATE - 5) AND NOT (m.ksnko_container_id IN ( SELECT m_1.ksnko_container_id
                   FROM waste_collection.measurements m_1
                     JOIN waste_collection.container_vendor_data cc ON cc.vendor_id::text = m_1.vendor_id::text
                     JOIN waste_collection.ksnko_containers ks ON m_1.ksnko_container_id = ks.id
                  GROUP BY m_1.ksnko_container_id
                 HAVING max(m_1.measured_at) < (now() - '1 day'::interval) OR max(m_1.measured_at) IS NULL
                  ORDER BY (max(m_1.measured_at))))
          GROUP BY m.ksnko_container_id
         HAVING abs(min(m.percent_smoothed) - max(m.percent_smoothed)) <= 3) s;
