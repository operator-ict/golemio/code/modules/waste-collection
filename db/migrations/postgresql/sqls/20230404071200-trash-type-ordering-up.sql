alter table trash_type_mapping add column sort_order smallint not null default 100;

update trash_type_mapping set sort_order = 1 where code_ksnko = 'p';
update trash_type_mapping set sort_order = 2 where code_ksnko = 'u';
update trash_type_mapping set sort_order = 3 where code_ksnko = 'ko';
update trash_type_mapping set sort_order = 4 where code_ksnko = 'nk';
update trash_type_mapping set sort_order = 5 where code_ksnko = 'sb';
update trash_type_mapping set sort_order = 6 where code_ksnko = 'sc';
update trash_type_mapping set sort_order = 7 where code_ksnko = 'jt';
update trash_type_mapping set sort_order = 8 where code_ksnko = 'e';
