DROP INDEX IF EXISTS stations_geom_idx;
DROP INDEX IF EXISTS stations_district_idx;
DROP INDEX IF EXISTS stations_updated_at_idx;
DROP INDEX IF EXISTS container_vendor_data_ksnko_id_idx;
DROP INDEX IF EXISTS measurements_ksnko_id_idx;
DROP INDEX IF EXISTS picks_ksnko_id_idx;

DROP INDEX IF EXISTS v_last_picks_ksnko_id_idx;
DROP MATERIALIZED VIEW IF EXISTS v_last_picks;
