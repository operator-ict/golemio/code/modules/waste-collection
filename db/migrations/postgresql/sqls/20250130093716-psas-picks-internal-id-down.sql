COMMENT ON COLUMN psas_real_picks_dates.internal_pick_id IS NULL;
ALTER TABLE psas_real_picks_dates DROP CONSTRAINT psas_real_picks_dates_pk;
ALTER TABLE psas_real_picks_dates RENAME COLUMN internal_pick_id to pick_id;
ALTER TABLE psas_real_picks_dates ADD CONSTRAINT psas_real_picks_dates_pk PRIMARY KEY (pick_id);

