-- musím pseudo vytvořit schémata aby o nich migrace věděly
CREATE SCHEMA IF NOT EXISTS analytic;

-- view s doporučením nové frekvence
CREATE OR REPLACE VIEW analytic.v_waste_collection_frequency
AS WITH -- KALENDÁŘ za poslední 3 měsíce
        calendar AS (
         SELECT t.date::date AS date,
                CASE
                    WHEN to_char(t.date, 'dy'::text) = 'mon'::text THEN 'Po'::text
                    WHEN to_char(t.date, 'dy'::text) = 'tue'::text THEN 'Út'::text
                    WHEN to_char(t.date, 'dy'::text) = 'wed'::text THEN 'St'::text
                    WHEN to_char(t.date, 'dy'::text) = 'thu'::text THEN 'Čt'::text
                    WHEN to_char(t.date, 'dy'::text) = 'fri'::text THEN 'Pá'::text
                    WHEN to_char(t.date, 'dy'::text) = 'sat'::text THEN 'So'::text
                    WHEN to_char(t.date, 'dy'::text) = 'sun'::text THEN 'Ne'::text
                    ELSE NULL::text
                END AS day_cz,
            date_part('week'::text, t.date) AS week
           FROM generate_series(now() - '3 mons'::interval, now(), '1 day'::interval) t(date)
        ), -- kontejnery ktere meri
        containers AS (
         SELECT cvd.vendor_id,
            kc.id AS ksnko_container_id
           FROM waste_collection.container_vendor_data cvd
             JOIN waste_collection.ksnko_containers kc ON cvd.ksnko_container_id = kc.id
          WHERE cvd.active IS TRUE AND (kc.trash_type::text = ANY (ARRAY['sc'::character varying::text, 'sb'::character varying::text, 'nk'::character varying::text, 'ko'::character varying::text]))
        ), -- measurementy pro kontejneri ktere se meri za posledni 3 mesice s joinem na kalendar
        measurements AS (
         SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            c.week,
            m.percent_smoothed
           FROM waste_collection.measurements m
             LEFT JOIN calendar c ON m.measured_at::date = c.date
             JOIN containers k ON m.vendor_id::text = k.vendor_id::text AND m.ksnko_container_id = k.ksnko_container_id
          WHERE m.measured_at::date >= (now() - '3 mons'::interval)
        ), -- svozy podle prazskych sluzeb
        ps_picks AS (
         SELECT k.vendor_id,
            ppd.ksnko_container_id,
            ppd.pick_date,
            c.week
           FROM waste_collection.planned_pick_dates ppd
             JOIN containers k ON ppd.ksnko_container_id = k.ksnko_container_id
             JOIN calendar c ON ppd.pick_date::date = c.date
          WHERE ppd.pick_date::date >= (now() - '3 mons'::interval) AND ppd.pick_date <= now()
        ), -- denne maxima hladiny
        daily_max AS (
         SELECT measurements.vendor_id,
            measurements.ksnko_container_id,
            measurements.measured_at::date AS measured_at,
            max(measurements.percent_smoothed) AS daily_max
           FROM measurements
          GROUP BY measurements.vendor_id, measurements.ksnko_container_id, (measurements.measured_at::date)
        ), -- osetrenie ked senzor hlasi svoz post zvoz, denne maxima hladiny pre dany den a predchadzajuci den 
        daily_max_2 AS (
         SELECT m1.vendor_id,
            m1.ksnko_container_id,
            m1.measured_at,
            m1.daily_max,
            m2.daily_max AS previous_day_daily_max
           FROM daily_max m1
             LEFT JOIN daily_max m2 ON m1.vendor_id::text = m2.vendor_id::text AND m1.ksnko_container_id = m2.ksnko_container_id AND (m1.measured_at - 1) = m2.measured_at
        ), -- detekovane svozy, agregace tyden, min max zaplnenost, bere jen jeden prvni svoz v danem dnu
        sensor_picks AS (
         SELECT vv.ksnko_container_id,
            vv.vendor_id,
            vv.week,
            max(vv.calc_percent_smoothed) AS max_percent_smoothed,
            min(vv.pick_at) AS pick_at
           FROM ( SELECT m.vendor_id,
                    m.ksnko_container_id,
                    m.percent_smoothed,
                        CASE
                            WHEN dm.previous_day_daily_max > m.percent_smoothed THEN dm.previous_day_daily_max
                            WHEN dm.daily_max > m.percent_smoothed THEN dm.daily_max
                            ELSE m.percent_smoothed
                        END AS calc_percent_smoothed,
                    m.week,
                    p.pick_at
                   FROM measurements m
                     LEFT JOIN daily_max_2 dm ON dm.measured_at = m.measured_at::date AND dm.vendor_id::text = m.vendor_id::text AND dm.ksnko_container_id = m.ksnko_container_id
                     LEFT JOIN waste_collection.picks p ON m.vendor_id::text = p.vendor_id::text AND m.measured_at = p.pick_at) vv
          WHERE vv.pick_at IS NOT NULL AND vv.pick_at::date >= (now() - '3 mons'::interval)
          GROUP BY vv.ksnko_container_id, vv.vendor_id, vv.week
        ), -- beru jako vsechny zaznamy mereni a k nim vsechny planovane svozy a oznaceni jestli je to svoz nebo ne
        records AS (
         SELECT foo.vendor_id,
            foo.ksnko_container_id,
            foo.measured_at,
            foo.week,
            foo.is_pick_day,
                CASE
                    WHEN sum(foo.is_pick_day) OVER (PARTITION BY foo.vendor_id, foo.ksnko_container_id, foo.week) <> 0 THEN 1
                    ELSE 0
                END AS is_pick_week,
            foo.percent_smoothed,
            sp.max_percent_smoothed,
            max(foo.percent_smoothed) OVER (PARTITION BY foo.vendor_id, foo.ksnko_container_id, foo.week) - min(foo.percent_smoothed) OVER (PARTITION BY foo.vendor_id, foo.ksnko_container_id, foo.week) AS week_max_percent_smoothed,
                CASE
                    WHEN sp.vendor_id IS NOT NULL THEN 1
                    ELSE 0
                END AS is_assigned_pick
           FROM ( SELECT COALESCE(m.vendor_id, p.vendor_id) AS vendor_id,
                    COALESCE(m.ksnko_container_id, p.ksnko_container_id) AS ksnko_container_id,
                    COALESCE(m.measured_at, p.pick_date) AS measured_at,
                        CASE
                            WHEN p.pick_date IS NOT NULL THEN 1
                            ELSE 0
                        END AS is_pick_day,
                    COALESCE(m.week, p.week) AS week,
                    m.percent_smoothed
                   FROM ps_picks p
                     FULL JOIN measurements m ON m.ksnko_container_id = p.ksnko_container_id AND m.vendor_id::text = p.vendor_id::text AND m.measured_at::date = p.pick_date::date
                  ORDER BY (COALESCE(m.vendor_id, p.vendor_id)), (COALESCE(m.ksnko_container_id, p.ksnko_container_id)), (COALESCE(m.measured_at, p.pick_date))) foo
             LEFT JOIN sensor_picks sp ON foo.vendor_id::text = sp.vendor_id::text AND foo.ksnko_container_id = sp.ksnko_container_id AND foo.measured_at = sp.pick_at
          ORDER BY foo.vendor_id, foo.ksnko_container_id, foo.measured_at
        ), -- pridavam tyzdenny objem pre prvy tyzden
        level1 AS (
         SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            m.week,
            m.is_pick_day,
            m.is_pick_week,
            m.is_assigned_pick,
            m.percent_smoothed,
            m.max_percent_smoothed,
            n.week_max_percent_smoothed AS week1_max_percent_smoothed
           FROM records m
             LEFT JOIN ( SELECT r.vendor_id,
                    r.ksnko_container_id,
                    r.week,
                    max(r.week_max_percent_smoothed) AS week_max_percent_smoothed
                   FROM records r
                  GROUP BY r.vendor_id, r.ksnko_container_id, r.week) n ON m.vendor_id::text = n.vendor_id::text AND m.ksnko_container_id = n.ksnko_container_id AND (m.week + 1::double precision) = n.week
          ORDER BY m.vendor_id, m.ksnko_container_id, m.measured_at
        ), -- pridavam tyzdenny objem pre druhy tyzden
        level2 AS (
         SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            m.week,
            m.is_pick_day,
            m.is_pick_week,
            m.is_assigned_pick,
            m.percent_smoothed,
            m.max_percent_smoothed,
            m.week1_max_percent_smoothed,
            n.week1_max_percent_smoothed AS week2_max_percent_smoothed,
            (m.week1_max_percent_smoothed::double precision + n.week1_max_percent_smoothed::double precision) / 2::double precision AS avg_weekly_increase
           FROM level1 m
             LEFT JOIN ( SELECT level1.vendor_id,
                    level1.ksnko_container_id,
                    level1.week,
                    max(level1.week1_max_percent_smoothed) AS week1_max_percent_smoothed
                   FROM level1
                  GROUP BY level1.vendor_id, level1.ksnko_container_id, level1.week) n ON m.vendor_id::text = n.vendor_id::text AND m.ksnko_container_id = n.ksnko_container_id AND (m.week + 1::double precision) = n.week
          ORDER BY m.vendor_id, m.ksnko_container_id, m.measured_at
        ), -- ohodnotenie merani, kedy zacina plny kontajner, kedy je plny a kedy konci plna zaplnenost
        status_tbl AS (
         SELECT r.vendor_id,
            r.ksnko_container_id,
            r.measured_at,
            r.is_assigned_pick,
            r.percent_smoothed,
            r.max_percent_smoothed,
                CASE
                    WHEN r.percent_smoothed > 90 OR r.max_percent_smoothed > 90 THEN
                    CASE
                        WHEN lag(r.percent_smoothed, 1) OVER (PARTITION BY r.vendor_id, r.ksnko_container_id ORDER BY r.measured_at) < 91 THEN 'begin'::text
                        WHEN lead(r.percent_smoothed, 1) OVER (PARTITION BY r.vendor_id, r.ksnko_container_id ORDER BY r.measured_at) < 91 THEN 'end'::text
                        ELSE 'body'::text
                    END
                    ELSE 'ok'::text
                END AS status
           FROM records r
        ), -- definicia svozov, ktore su moc plne viac ako 5 dni pred svozom a pocas svozu
        full_picks AS (
         SELECT foo.vendor_id,
            foo.ksnko_container_id,
            foo.percent_smoothed,
            foo.full_started,
            foo.measured_at,
            date_part('epoch'::text, foo.measured_at - foo.full_started) / (3600 * 24)::double precision AS full_days
           FROM ( SELECT status_tbl.vendor_id,
                    status_tbl.ksnko_container_id,
                    status_tbl.percent_smoothed,
                    status_tbl.status,
                        CASE
                            WHEN lag(status_tbl.status) OVER (PARTITION BY status_tbl.vendor_id, status_tbl.ksnko_container_id ORDER BY status_tbl.measured_at) = 'begin'::text AND (status_tbl.status = ANY (ARRAY['end'::text, 'body'::text])) THEN lag(status_tbl.measured_at) OVER (PARTITION BY status_tbl.vendor_id, status_tbl.ksnko_container_id ORDER BY status_tbl.measured_at)
                            ELSE NULL::timestamp with time zone
                        END AS full_started,
                    status_tbl.measured_at
                   FROM status_tbl
                  WHERE status_tbl.is_assigned_pick = 1 AND (status_tbl.status = ANY (ARRAY['body'::text, 'end'::text])) OR status_tbl.is_assigned_pick = 0 AND status_tbl.status = 'begin'::text) foo
          WHERE foo.full_started IS NOT NULL AND (date_part('epoch'::text, foo.measured_at - foo.full_started) / (3600 * 24)::double precision) >= 5::double precision
        ), -- definicia svozov ktore su moc prazdne 
        empty_picks AS (
         SELECT l.vendor_id,
            l.ksnko_container_id,
            l.measured_at,
            l.week,
            l.is_pick_day,
            l.is_assigned_pick,
            l.max_percent_smoothed AS percent_smoothed,
            l.week1_max_percent_smoothed,
            l.week2_max_percent_smoothed
           FROM level2 l
          WHERE l.is_assigned_pick = 1 AND l.max_percent_smoothed < 85
        ), -- spojenie informacii o svozoch
        picks_summary AS (
         SELECT a.vendor_id,
            a.ksnko_container_id,
            a.pick_day_ps,
            a.pick_at AS assigned_pick,
                CASE
                    WHEN e.percent_smoothed IS NOT NULL THEN 1
                    ELSE 0
                END AS empty_pick,
            m.percent_smoothed,
            a.max_percent_smoothed,
            l2.week1_max_percent_smoothed,
            l2.week2_max_percent_smoothed,
            l2.avg_weekly_increase,
                CASE
                    WHEN (e.percent_smoothed + e.week1_max_percent_smoothed) < 90 THEN 1
                    ELSE 0
                END AS objem1,
                CASE
                    WHEN (e.percent_smoothed + e.week1_max_percent_smoothed + e.week2_max_percent_smoothed) < 90 THEN 1
                    ELSE 0
                END AS objem2,
                CASE
                    WHEN f.full_days IS NOT NULL THEN 1
                    ELSE 0
                END AS full_pick,
            f.full_days
           FROM ( SELECT p.vendor_id,
                    p.ksnko_container_id,
                    min(p.pick_date::date) AS pick_day_ps,
                    p.week,
                    i.pick_at,
                    i.max_percent_smoothed
                   FROM ps_picks p
                     LEFT JOIN sensor_picks i ON p.vendor_id::text = i.vendor_id::text AND p.ksnko_container_id = i.ksnko_container_id AND p.week = i.week
                  GROUP BY p.vendor_id, p.ksnko_container_id, p.week, i.pick_at, i.max_percent_smoothed) a
             LEFT JOIN empty_picks e ON a.vendor_id::text = e.vendor_id::text AND a.ksnko_container_id = e.ksnko_container_id AND a.pick_at = e.measured_at
             LEFT JOIN full_picks f ON a.vendor_id::text = f.vendor_id::text AND a.ksnko_container_id = f.ksnko_container_id AND a.pick_at = f.measured_at
             LEFT JOIN measurements m ON a.vendor_id::text = m.vendor_id::text AND a.ksnko_container_id = m.ksnko_container_id AND a.pick_at = m.measured_at
             LEFT JOIN level2 l2 ON a.vendor_id::text = l2.vendor_id::text AND a.ksnko_container_id = l2.ksnko_container_id AND a.pick_at = l2.measured_at
        ), -- sumarizacna tabulke pre kontajneri
        main AS (
         SELECT foo.vendor_id,
            foo.ksnko_container_id,
            avg(foo.max_percent_smoothed) AS avg_percent_smoothed,
            avg(foo.avg_weekly_increase) AS avg_weekly_increase,
            count(foo.pick_day_ps) AS ps_picks_count,
            count(foo.assigned_pick) AS assigned_picks_count,
            sum(foo.empty_pick) AS empty_picks_count,
            sum(foo.objem1) AS objem1_count,
            sum(foo.objem2) AS objem2_count,
            sum(foo.full_pick) AS full_picks_count
           FROM ( SELECT picks_summary.vendor_id,
                    picks_summary.ksnko_container_id,
                    picks_summary.pick_day_ps,
                    picks_summary.assigned_pick,
                    picks_summary.empty_pick,
                    picks_summary.max_percent_smoothed,
                    picks_summary.week1_max_percent_smoothed,
                    picks_summary.week2_max_percent_smoothed,
                    picks_summary.avg_weekly_increase,
                    picks_summary.objem1,
                    picks_summary.objem2,
                    picks_summary.full_pick,
                    picks_summary.full_days
                   FROM picks_summary
                  GROUP BY picks_summary.vendor_id, picks_summary.ksnko_container_id, picks_summary.pick_day_ps, picks_summary.assigned_pick, picks_summary.empty_pick, picks_summary.max_percent_smoothed, picks_summary.week1_max_percent_smoothed, picks_summary.week2_max_percent_smoothed, picks_summary.avg_weekly_increase, picks_summary.objem1, picks_summary.objem2, picks_summary.full_pick, picks_summary.full_days) foo
          GROUP BY foo.vendor_id, foo.ksnko_container_id
        )
-- doporucinie o zmene frekvencie zvozu
 SELECT main.vendor_id,
    main.ksnko_container_id,
    main.avg_percent_smoothed,
    main.avg_weekly_increase,
        CASE
            WHEN main.ps_picks_count = main.assigned_picks_count THEN 'ok'::text
            ELSE concat((main.ps_picks_count - main.assigned_picks_count)::text, ' z ', main.ps_picks_count::text)
        END AS status,
        CASE
            WHEN main.ps_picks_count = main.assigned_picks_count THEN
            CASE
                WHEN main.ps_picks_count = main.empty_picks_count THEN
                CASE
                    WHEN main.empty_picks_count = main.objem1_count AND main.empty_picks_count <> main.objem2_count THEN 'snižujeme 1x'::text
                    WHEN main.empty_picks_count = main.objem2_count THEN 'snižujeme 2x'::text
                    ELSE 'nic'::text
                END
                WHEN main.ps_picks_count::double precision <= ((3 / 2)::double precision * main.full_picks_count::double precision) THEN 'zvyšujeme'::text
                ELSE 'nic'::text
            END
            ELSE 'nelze určit'::text
        END AS change_interval
   FROM main;
