CREATE TABLE python.waste_collection_assigned_pick_dates_rfid (
	container_code varchar(16) NULL,
	pick_date date NULL,
	pick_at timestamptz NULL,
	is_exact_pick_date bool NULL,
	is_tolerated_pick_date bool NULL,
	is_pick_assigned bool NULL,
	tolerance interval NULL,
	trash_type varchar(20) NULL,
	category varchar(20) NULL,
	CONSTRAINT cnstr_waste_collection_assigned_pick_dates_1 UNIQUE (container_code,pick_date)
);

CREATE OR REPLACE VIEW analytic.v_waste_collection_assigned_picks_rfid
AS SELECT cp.container_code,
    cp.pick_at,
    cp.pick_date AS pickup_date,
    cp.category,
        CASE
            WHEN cp.category::text = 'exact'::text THEN 'Přesný'::text
            WHEN cp.category::text = 'tolerated_before'::text THEN 'Předjetý'::text
            WHEN cp.category::text = 'tolerated_after'::text THEN 'Dodatečný'::text
            ELSE 'Nepřiřazený'::text
        END AS category_cz
   FROM python.waste_collection_assigned_pick_dates_rfid cp
     JOIN waste_collection.ksnko_containers cc ON cp.container_code::integer = cc.id
     JOIN waste_collection.container_vendor_data_rfid cvd ON cc.id = cvd.ksnko_container_id
  WHERE cvd.active IS TRUE;
  
CREATE OR REPLACE VIEW analytic.v_waste_collection_unassigned_picks_rfid
AS SELECT cp.ksnko_container_id AS container_code,
    cp.pick_at,
    cp.pick_at::date AS pick_at_date
   FROM waste_collection.picks_rfid cp
     JOIN waste_collection.ksnko_containers cc ON cp.ksnko_container_id = cc.id
     JOIN waste_collection.container_vendor_data_rfid cvd ON cc.id = cvd.ksnko_container_id
     LEFT JOIN python.waste_collection_assigned_pick_dates_rfid wpd ON cp.ksnko_container_id = wpd.container_code::integer AND cp.pick_at = wpd.pick_at
  WHERE cvd.active IS TRUE AND wpd.category IS NULL;
  