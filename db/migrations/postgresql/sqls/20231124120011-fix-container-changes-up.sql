ALTER TABLE container_changes ALTER COLUMN trash_type_code TYPE varchar(3) USING trash_type_code::varchar;

-- Fix db state for local development. In other environments the value was added manually. For that reason there wont be any down script.
INSERT INTO trash_type_mapping (code_ksnko,code_psas,"name",create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,legacy_id,sort_order) VALUES
	 ('mks','A','Multikomoditní sběr',NULL,now(),NULL,NULL,NULL,NULL,9,9) on conflict do nothing;

