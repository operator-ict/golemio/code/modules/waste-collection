-- Drop dependant views
DROP VIEW v_raw_pick_dates;

-- Drop indexes

DROP INDEX container_vendor_data_history_valid_to_idx;
DROP INDEX  ksnko_containers_history_valid_to_idx;
DROP INDEX  ksnko_stations_history_id_idx;
DROP INDEX raw_pick_dates_history_valid_to_idx;

-- Table container_vendor_data;
DROP VIEW container_vendor_data;
ALTER TABLE container_vendor_data_history RENAME TO container_vendor_data;

ALTER TABLE container_vendor_data DROP COLUMN form_hash;
ALTER TABLE container_vendor_data DROP COLUMN is_historical;
ALTER TABLE container_vendor_data DROP COLUMN valid_from;
ALTER TABLE container_vendor_data DROP COLUMN valid_to;




-- Table ksnko_containers;
DROP VIEW ksnko_containers;
ALTER TABLE ksnko_containers_history RENAME TO ksnko_containers;

ALTER TABLE ksnko_containers DROP COLUMN form_hash;
ALTER TABLE ksnko_containers DROP COLUMN is_historical;
ALTER TABLE ksnko_containers DROP COLUMN valid_from;
ALTER TABLE ksnko_containers DROP COLUMN valid_to;

-- Table ksnko_stations;

DROP VIEW ksnko_stations;
ALTER TABLE ksnko_stations_history RENAME TO ksnko_stations;

CREATE INDEX IF NOT EXISTS ksnko_stations_number_key ON waste_collection.ksnko_stations (number);

ALTER TABLE ksnko_stations DROP COLUMN form_hash;
ALTER TABLE ksnko_stations DROP COLUMN is_historical;
ALTER TABLE ksnko_stations DROP COLUMN valid_from;
ALTER TABLE ksnko_stations DROP COLUMN valid_to;

-- Table raw_pick_dates;

DROP VIEW raw_pick_dates;
ALTER TABLE raw_pick_dates_history RENAME TO raw_pick_dates;

ALTER TABLE raw_pick_dates DROP COLUMN form_hash;
ALTER TABLE raw_pick_dates DROP COLUMN is_historical;

ALTER TABLE raw_pick_dates DROP COLUMN valid_from;
ALTER TABLE raw_pick_dates DROP COLUMN valid_to;

ALTER TABLE raw_pick_dates RENAME COLUMN psas_valid_from to valid_from; 
ALTER TABLE raw_pick_dates RENAME COLUMN psas_valid_to to valid_to; 

-- recreate  v_raw_pick_dates;

CREATE VIEW v_raw_pick_dates AS (
    SELECT kc.id as container_id, rpd.*
    FROM ksnko_containers kc
    LEFT JOIN ksnko_stations ks ON ks.id = kc.station_id
    INNER JOIN waste_collection.raw_pick_dates rpd ON ks."number" = rpd.station_number  AND kc.trash_type = rpd.trash_type_code
);


