ALTER TABLE ksnko_bulky_waste_stations ADD city_district_slug varchar(50) NULL;
ALTER TABLE ksnko_bulky_waste_stations ADD custom_id varchar(50) NULL;

COMMENT ON COLUMN ksnko_bulky_waste_stations.id IS 'externí id objednávky';

update ksnko_bulky_waste_stations set custom_id = pick_date || ' ' || to_char(pick_time_from,'HH24:MI') ||' ID:' || id;

CREATE INDEX ksnko_bulky_waste_stations_pick_date_idx ON ksnko_bulky_waste_stations (pick_date,pick_time_from,pick_time_to);
CREATE INDEX ksnko_bulky_waste_stations_city_district_slug_idx ON ksnko_bulky_waste_stations (city_district_slug);
CREATE UNIQUE INDEX ksnko_bulky_waste_stations_custom_id_idx ON ksnko_bulky_waste_stations (custom_id);

