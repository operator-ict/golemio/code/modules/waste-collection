-- analytic.v_waste_colection_containers_full source

CREATE OR REPLACE VIEW analytic.v_waste_colection_containers_full
AS WITH base AS (
         SELECT measurements.ksnko_container_id AS container_id,
            measurements.measured_at,
            measurements.percent_smoothed
           FROM waste_collection.measurements
        ), status_tbl AS (
         SELECT base.container_id,
            base.measured_at,
            base.percent_smoothed,
                CASE
                    WHEN base.percent_smoothed > 95 THEN
                    CASE
                        WHEN lag(base.percent_smoothed, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 THEN 'begin'::text
                        WHEN lag(base.percent_smoothed, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) IS NULL THEN 'begin'::text
                        WHEN lead(base.percent_smoothed, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) IS NULL THEN 'end'::text
                        ELSE 'body'::text
                    END
                    WHEN lag(base.percent_smoothed, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN 'end'::text
                    ELSE 'ok'::text
                END AS status
           FROM base
          ORDER BY base.container_id, base.measured_at
        ), start_end AS (
         SELECT status_tbl.container_id,
            status_tbl.measured_at,
            status_tbl.percent_smoothed,
            status_tbl.status,
                CASE
                    WHEN status_tbl.status = 'begin'::text THEN status_tbl.measured_at
                    ELSE NULL::timestamp with time zone
                END AS start_time,
                CASE
                    WHEN status_tbl.status = 'begin'::text THEN COALESCE(lead(status_tbl.measured_at, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at), date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval)
                    ELSE NULL::timestamp with time zone
                END AS end_time
           FROM status_tbl status_tbl
          WHERE status_tbl.status = ANY (ARRAY['begin'::text, 'end'::text])
          ORDER BY status_tbl.container_id, status_tbl.measured_at
        )
 SELECT start_end.container_id,
    date_trunc('day'::text, start_end.start_time) AS date,
    start_end.start_time,
    start_end.end_time
   FROM start_end
  WHERE start_end.status = 'begin'::text;


-- analytic.v_waste_colection_containers_full_days source

CREATE OR REPLACE VIEW analytic.v_waste_colection_containers_full_days
AS WITH base AS (
         SELECT m.ksnko_container_id AS container_id,
            m.measured_at,
            m.percent_smoothed AS percent_calculated
           FROM waste_collection.measurements m
        ), status_tbl AS (
         SELECT base.container_id,
            base.measured_at,
            base.percent_calculated,
                CASE
                    WHEN base.percent_calculated > 95 THEN
                    CASE
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 AND (base.measured_at::date - lead(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'begin_till_midnight'::text
                        WHEN (base.measured_at::date - lag(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 AND lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN 'begin_from_midnight'::text
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 THEN 'begin'::text
                        WHEN (base.measured_at::date - lead(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'end_at_midnight'::text
                        ELSE 'body'::text
                    END
                    WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN
                    CASE
                        WHEN (base.measured_at::date - lag(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'end_from_midnight'::text
                        ELSE 'end'::text
                    END
                    ELSE 'ok'::text
                END AS status
           FROM base
          ORDER BY base.container_id, base.measured_at
        ), start_end AS (
         SELECT status_tbl.container_id,
            status_tbl.measured_at,
            status_tbl.percent_calculated,
            status_tbl.status,
                CASE
                    WHEN status_tbl.status = 'end_from_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at)
                    WHEN status_tbl.status = 'begin'::text THEN status_tbl.measured_at
                    WHEN status_tbl.status = 'begin_till_midnight'::text THEN status_tbl.measured_at
                    WHEN status_tbl.status = 'begin_from_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at)
                    ELSE NULL::timestamp without time zone::timestamp with time zone
                END AS start_time,
                CASE
                    WHEN status_tbl.status ~~ 'begin%'::text AND (status_tbl.measured_at::date - lead(status_tbl.measured_at::date, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at)) <> 0 THEN date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval
                    WHEN status_tbl.status ~~ 'begin%'::text AND lead(status_tbl.status, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at) = 'end_at_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval
                    WHEN status_tbl.status ~~ 'begin%'::text AND lead(status_tbl.status, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at) = 'end'::text THEN lead(status_tbl.measured_at, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at)
                    WHEN status_tbl.status = 'end_from_midnight'::text THEN status_tbl.measured_at
                    ELSE NULL::timestamp without time zone::timestamp with time zone
                END AS end_time
           FROM status_tbl
          WHERE status_tbl.status = ANY (ARRAY['begin_till_midnight'::text, 'begin_from_midnight'::text, 'begin'::text, 'end_at_midnight'::text, 'end_from_midnight'::text, 'end'::text])
          ORDER BY status_tbl.container_id, status_tbl.measured_at
        )
 SELECT start_end.container_id,
    date_trunc('day'::text, start_end.start_time) AS date,
    start_end.start_time,
    start_end.end_time
   FROM start_end
  WHERE start_end.status = 'end_from_midnight'::text OR start_end.status ~~ 'begin%'::text;


-- analytic.v_waste_collection_24_hr_missing source

CREATE OR REPLACE VIEW analytic.v_waste_collection_24_hr_missing
AS SELECT ks.id AS ksnko_container_id,
    max(m.measured_at) AS measured_at
   FROM waste_collection.measurements m
     JOIN waste_collection.container_vendor_data cc ON cc.vendor_id::text = m.vendor_id::text AND cc.active IS TRUE
     JOIN waste_collection.ksnko_containers ks ON m.ksnko_container_id = ks.id
  GROUP BY ks.id
 HAVING max(m.measured_at) < (now() - '1 day'::interval) OR max(m.measured_at) IS NULL
  ORDER BY (max(m.measured_at));


-- analytic.v_waste_collection_assigned_picks source

CREATE OR REPLACE VIEW analytic.v_waste_collection_assigned_picks
AS SELECT cp.ksnko_container_id AS container_code,
    cc.container_volume AS total_volume,
        CASE
            WHEN cc.trash_type::text = 'sb'::text THEN 411
            WHEN cc.trash_type::text = 'ko'::text THEN 120
            WHEN cc.trash_type::text = 'nk'::text THEN 55
            WHEN cc.trash_type::text = 'p'::text THEN 76
            WHEN cc.trash_type::text = 'u'::text THEN 39
            WHEN cc.trash_type::text = 'sc'::text THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cp.pick_at,
    cp.pick_at::date AS pick_at_date,
    date_part('hour'::text, cp.pick_at) AS hour,
    cp.percent_before,
    cp.percent_now,
    cp.event_driven,
    wpd.category,
        CASE
            WHEN wpd.category::text = 'exact'::text THEN 'Přesný'::text
            WHEN wpd.category::text = 'tolerated_before'::text THEN 'Předjetý'::text
            WHEN wpd.category::text = 'tolerated_after'::text THEN 'Dodatečný'::text
            ELSE 'Nepřiřazený'::text
        END AS category_cz
   FROM waste_collection.picks cp
     JOIN waste_collection.ksnko_containers cc ON cp.ksnko_container_id = cc.id
     JOIN waste_collection.container_vendor_data cvd ON cc.id = cvd.ksnko_container_id
     LEFT JOIN python.waste_collection_assigned_pick_dates wpd ON cp.ksnko_container_id = wpd.container_code::integer AND cp.pick_at = wpd.pick_at
  WHERE cvd.active IS TRUE;


-- analytic.v_waste_collection_assigned_picks_rfid source

CREATE OR REPLACE VIEW analytic.v_waste_collection_assigned_picks_rfid
AS SELECT cp.container_code,
    cp.pick_at,
    cp.pick_date AS pickup_date,
    cp.category,
        CASE
            WHEN cp.category::text = 'exact'::text THEN 'Přesný'::text
            WHEN cp.category::text = 'tolerated_before'::text THEN 'Předjetý'::text
            WHEN cp.category::text = 'tolerated_after'::text THEN 'Dodatečný'::text
            ELSE 'Nepřiřazený'::text
        END AS category_cz,
    pr.cu_id
   FROM python.waste_collection_assigned_pick_dates_rfid cp
     LEFT JOIN waste_collection.picks_rfid pr ON cp.container_code::integer = pr.ksnko_container_id AND cp.pick_at = pr.pick_at
     JOIN waste_collection.ksnko_containers cc ON cp.container_code::integer = cc.id
     JOIN waste_collection.container_vendor_data_rfid cvd ON cc.id = cvd.ksnko_container_id
  WHERE cvd.active IS TRUE;


-- analytic.v_waste_collection_containers source

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers
AS SELECT v.vendor_id,
    v.vendor,
    v.id,
    v.code,
    v.volume_ratio,
    v.trash_type,
    v.container_volume,
    v.container_dump,
    v.container_brand,
    v.cleaning_frequency_code,
    concat(v.cleaning_frequency_code, ' – ', COALESCE(v.days, 'NA'::text)) AS cleaning_frequency_displayed,
    v.station_id,
    v.station_number,
    v.active,
    v.create_batch_id,
    v.created_at,
    v.created_by,
    v.update_batch_id,
    v.updated_at,
    v.updated_by,
    v.city_district_name,
    split_part(v.city_district_name::text, ' '::text, 2) AS city_district_number,
    v.coordinate_lat,
    v.coordinate_lon,
    v.trash_type_name,
    v.days,
    v.street_name,
    v.frequency,
    v.name,
    v.network,
    v.company,
    v.bin_brand,
    v.frequency_text,
    v.row_number
   FROM ( SELECT cvd.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_dump,
            kc.container_volume,
                CASE
                    WHEN kc.container_brand::text = 'Podzemní'::text THEN 'Podzemní'::text
                    ELSE 'Nadzemní'::text
                END AS container_brand,
            kc.cleaning_frequency_code,
            kc.station_id,
            ks.number AS station_number,
            cvd.active,
            kc.create_batch_id,
            kc.created_at,
            kc.created_by,
            kc.update_batch_id,
            kc.updated_at,
            kc.updated_by,
            ks.city_district_name,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            rpd.company,
            ks.name,
            cvd.network,
            cvd.bin_brand,
            cvd.vendor,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data cvd
             JOIN waste_collection.ksnko_containers kc ON cvd.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
             LEFT JOIN waste_collection.raw_pick_dates rpd ON ks.number::text = rpd.station_number::text AND ks.active IS TRUE AND kc.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE) v
  WHERE v.row_number = 1;


-- analytic.v_waste_collection_containers_rfid source

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_rfid
AS WITH main AS (
         SELECT cvdr.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_dump,
            kc.container_volume,
            kc.cleaning_frequency_code,
            concat(kc.cleaning_frequency_code, ' – ', COALESCE(rpd.days, 'NA'::text)) AS cleaning_frequency_displayed,
            kc.station_id,
            ks.number AS station_number,
            cvdr.active,
            ks.city_district_name,
            split_part(ks.city_district_name::text, ' '::text, 2) AS city_district_number,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            rpd.company,
            ks.name,
            cvdr.network,
            cvdr.bin_brand,
            cvdr.vendor,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data_rfid cvdr
             JOIN waste_collection.ksnko_containers kc ON cvdr.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
             LEFT JOIN waste_collection.raw_pick_dates rpd ON ks.number::text = rpd.station_number::text AND kc.trash_type::text = rpd.trash_type_code::text AND ks.active IS TRUE
          WHERE cvdr.active IS TRUE
        ), last_pick AS (
         SELECT DISTINCT ON (pr.ksnko_container_id) pr.ksnko_container_id,
            pr.pick_at
           FROM waste_collection.picks_rfid pr
          ORDER BY pr.ksnko_container_id, pr.pick_at DESC
        )
 SELECT main.vendor_id,
    main.vendor,
    main.id,
    main.code,
    main.volume_ratio,
    main.trash_type,
    main.container_volume,
    main.container_dump,
    main.cleaning_frequency_code,
    main.cleaning_frequency_displayed,
    main.station_id,
    main.station_number,
    main.active,
    main.city_district_name,
    main.city_district_number,
    main.coordinate_lat,
    main.coordinate_lon,
    main.trash_type_name,
    main.days,
    main.street_name,
    main.frequency,
    main.name,
    main.network,
    main.company,
    main.bin_brand,
    main.frequency_text,
    main.row_number,
    last_pick.pick_at AS last_pick
   FROM main
     LEFT JOIN last_pick ON main.id = last_pick.ksnko_container_id
  WHERE main.row_number = 1;


-- analytic.v_waste_collection_containers_technical_info source

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info
AS WITH main AS (
         SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            ks_1.number AS station_number,
            ks_1.city_district_name,
            ks_1.name,
            initcap(rpd.trash_type_name::text)::character varying AS trash_type_name,
            cvd_1.network,
            cvd_1.anti_noise_depth,
            cvd_1.bin_depth,
            cvd_1.vendor,
            cvd_1.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed::double precision / 100::double precision AS percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd_1 ON m.vendor_id::text = cvd_1.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd_1.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks_1 ON k.station_id = ks_1.id
             JOIN waste_collection.raw_pick_dates rpd ON ks_1.number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text AND ks_1.active IS TRUE
          WHERE cvd_1.active IS TRUE AND m.measured_at >= (now() - '3 mons'::interval)
        ), min_percent AS (
         SELECT DISTINCT m.ksnko_container_id,
            max(m.measured_at) OVER (PARTITION BY m.ksnko_container_id) AS last_measured_at,
            m.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id) AS min_percent_smoothed
                   FROM main) m
          WHERE m.min_percent_smoothed = m.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
            main.trash_type_name,
            1::double precision - main.anti_noise_depth::double precision / main.bin_depth::double precision AS anti_noise_depth,
            min(main.battery_status) OVER (PARTITION BY main.ksnko_container_id) AS last_battery_status
           FROM main
          WHERE main.measured_at >= (now() - '14 days'::interval day)
        ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END AS daily_msg_expected,
                    30 AS count_days,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN
                            CASE
                                WHEN count(main.measured_at) > 4 THEN 4::bigint
                                ELSE count(main.measured_at)
                            END
                            ELSE
                            CASE
                                WHEN count(main.measured_at) > 7 THEN 7::bigint
                                ELSE count(main.measured_at)
                            END
                        END AS daily_msg
                   FROM main
                  WHERE main.measured_at >= (CURRENT_DATE::timestamp with time zone - '30 days'::interval) AND main.measured_at < CURRENT_DATE::timestamp with time zone
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.vendor, main.sensor_version, main.measured_at_day, (
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        ), max_measured_at AS (
         SELECT measurements.ksnko_container_id,
            max(measurements.measured_at) AS max_measured_at
           FROM waste_collection.measurements
          GROUP BY measurements.ksnko_container_id
        ), counts AS (
         SELECT sensor_cycle_itop.sensor_id,
            count(DISTINCT sensor_cycle_itop.service_date) AS services_count
           FROM waste_collection.sensor_cycle_itop
          WHERE sensor_cycle_itop.service_date >= (now() - '6 mons'::interval)
          GROUP BY sensor_cycle_itop.sensor_id
        )
 SELECT max_measured_at.max_measured_at,
    cvd.ksnko_container_id,
    cvd.vendor_id,
    cvd.sensor_id,
    ks.number AS station_number,
    ks.name,
    ks.city_district_name,
    lbs.trash_type_name,
    cvd.sensor_version,
    cvd.network,
    COALESCE(round((lbs.anti_noise_depth * 100::double precision)::numeric, 2)::character varying(255), 'není'::character varying) AS anti_noise_depth,
    mp.min_percent_smoothed * 100::double precision AS min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent * 100::double precision AS msg_transfer_in_percent,
    itop.ksnko_container_id AS ksnko_container_id_itop,
    itop.service_date,
    itop.description,
    (itop.service_date + '180 days'::interval)::date AS contract_service_date,
    COALESCE(c.services_count, 0::bigint) AS services_count,
    itop.install_date,
    itop.production_date,
    itop.end_of_warranty_mhmp
   FROM waste_collection.container_vendor_data cvd
     JOIN waste_collection.ksnko_containers kc ON kc.id = cvd.ksnko_container_id
     LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
     LEFT JOIN min_percent mp ON mp.ksnko_container_id = cvd.ksnko_container_id
     LEFT JOIN last_battery_status lbs ON cvd.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON cvd.ksnko_container_id = msg.ksnko_container_id
     LEFT JOIN max_measured_at ON cvd.ksnko_container_id = max_measured_at.ksnko_container_id
     LEFT JOIN waste_collection.sensor_cycle_itop_actual itop ON upper(itop.sensor_id::text) = upper(cvd.sensor_id::text)
     LEFT JOIN counts c ON itop.sensor_id::text = c.sensor_id::text
  WHERE cvd.active;


-- analytic.v_waste_collection_frequency source

CREATE OR REPLACE VIEW analytic.v_waste_collection_frequency
AS WITH calendar AS (
         SELECT t.date::date AS date,
                CASE
                    WHEN to_char(t.date, 'dy'::text) = 'mon'::text THEN 'Po'::text
                    WHEN to_char(t.date, 'dy'::text) = 'tue'::text THEN 'Út'::text
                    WHEN to_char(t.date, 'dy'::text) = 'wed'::text THEN 'St'::text
                    WHEN to_char(t.date, 'dy'::text) = 'thu'::text THEN 'Čt'::text
                    WHEN to_char(t.date, 'dy'::text) = 'fri'::text THEN 'Pá'::text
                    WHEN to_char(t.date, 'dy'::text) = 'sat'::text THEN 'So'::text
                    WHEN to_char(t.date, 'dy'::text) = 'sun'::text THEN 'Ne'::text
                    ELSE NULL::text
                END AS day_cz,
            date_part('week'::text, t.date) AS week
           FROM generate_series(now() - '3 mons'::interval, now(), '1 day'::interval) t(date)
        ), containers AS (
         SELECT cvd.vendor_id,
            kc.id AS ksnko_container_id
           FROM waste_collection.container_vendor_data cvd
             JOIN waste_collection.ksnko_containers kc ON cvd.ksnko_container_id = kc.id
          WHERE cvd.active IS TRUE AND (kc.trash_type::text = ANY (ARRAY['sc'::character varying::text, 'sb'::character varying::text, 'nk'::character varying::text, 'ko'::character varying::text]))
        ), measurements AS (
         SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            c.week,
            m.percent_smoothed
           FROM waste_collection.measurements m
             LEFT JOIN calendar c ON m.measured_at::date = c.date
             JOIN containers k ON m.vendor_id::text = k.vendor_id::text AND m.ksnko_container_id = k.ksnko_container_id
          WHERE m.measured_at::date >= (now() - '3 mons'::interval)
        ), ps_picks AS (
         SELECT k.vendor_id,
            ppd.ksnko_container_id,
            ppd.pick_date,
            c.week
           FROM waste_collection.planned_pick_dates ppd
             JOIN containers k ON ppd.ksnko_container_id = k.ksnko_container_id
             JOIN calendar c ON ppd.pick_date::date = c.date
          WHERE ppd.pick_date::date >= (now() - '3 mons'::interval) AND ppd.pick_date <= now()
        ), daily_max AS (
         SELECT measurements.vendor_id,
            measurements.ksnko_container_id,
            measurements.measured_at::date AS measured_at,
            max(measurements.percent_smoothed) AS daily_max
           FROM measurements
          GROUP BY measurements.vendor_id, measurements.ksnko_container_id, (measurements.measured_at::date)
        ), daily_max_2 AS (
         SELECT m1.vendor_id,
            m1.ksnko_container_id,
            m1.measured_at,
            m1.daily_max,
            m2.daily_max AS previous_day_daily_max
           FROM daily_max m1
             LEFT JOIN daily_max m2 ON m1.vendor_id::text = m2.vendor_id::text AND m1.ksnko_container_id = m2.ksnko_container_id AND (m1.measured_at - 1) = m2.measured_at
        ), sensor_picks AS (
         SELECT vv.ksnko_container_id,
            vv.vendor_id,
            vv.week,
            max(vv.calc_percent_smoothed) AS max_percent_smoothed,
            min(vv.pick_at) AS pick_at
           FROM ( SELECT m.vendor_id,
                    m.ksnko_container_id,
                    m.percent_smoothed,
                        CASE
                            WHEN dm.previous_day_daily_max > m.percent_smoothed THEN dm.previous_day_daily_max
                            WHEN dm.daily_max > m.percent_smoothed THEN dm.daily_max
                            ELSE m.percent_smoothed
                        END AS calc_percent_smoothed,
                    m.week,
                    p.pick_at
                   FROM measurements m
                     LEFT JOIN daily_max_2 dm ON dm.measured_at = m.measured_at::date AND dm.vendor_id::text = m.vendor_id::text AND dm.ksnko_container_id = m.ksnko_container_id
                     LEFT JOIN waste_collection.picks p ON m.vendor_id::text = p.vendor_id::text AND m.measured_at = p.pick_at) vv
          WHERE vv.pick_at IS NOT NULL AND vv.pick_at::date >= (now() - '3 mons'::interval)
          GROUP BY vv.ksnko_container_id, vv.vendor_id, vv.week
        ), records AS (
         SELECT foo.vendor_id,
            foo.ksnko_container_id,
            foo.measured_at,
            foo.week,
            foo.is_pick_day,
                CASE
                    WHEN sum(foo.is_pick_day) OVER (PARTITION BY foo.vendor_id, foo.ksnko_container_id, foo.week) <> 0 THEN 1
                    ELSE 0
                END AS is_pick_week,
            foo.percent_smoothed,
            sp.max_percent_smoothed,
            max(foo.percent_smoothed) OVER (PARTITION BY foo.vendor_id, foo.ksnko_container_id, foo.week) - min(foo.percent_smoothed) OVER (PARTITION BY foo.vendor_id, foo.ksnko_container_id, foo.week) AS week_max_percent_smoothed,
                CASE
                    WHEN sp.vendor_id IS NOT NULL THEN 1
                    ELSE 0
                END AS is_assigned_pick
           FROM ( SELECT COALESCE(m.vendor_id, p.vendor_id) AS vendor_id,
                    COALESCE(m.ksnko_container_id, p.ksnko_container_id) AS ksnko_container_id,
                    COALESCE(m.measured_at, p.pick_date) AS measured_at,
                        CASE
                            WHEN p.pick_date IS NOT NULL THEN 1
                            ELSE 0
                        END AS is_pick_day,
                    COALESCE(m.week, p.week) AS week,
                    m.percent_smoothed
                   FROM ps_picks p
                     FULL JOIN measurements m ON m.ksnko_container_id = p.ksnko_container_id AND m.vendor_id::text = p.vendor_id::text AND m.measured_at::date = p.pick_date::date
                  ORDER BY (COALESCE(m.vendor_id, p.vendor_id)), (COALESCE(m.ksnko_container_id, p.ksnko_container_id)), (COALESCE(m.measured_at, p.pick_date))) foo
             LEFT JOIN sensor_picks sp ON foo.vendor_id::text = sp.vendor_id::text AND foo.ksnko_container_id = sp.ksnko_container_id AND foo.measured_at = sp.pick_at
          ORDER BY foo.vendor_id, foo.ksnko_container_id, foo.measured_at
        ), level1 AS (
         SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            m.week,
            m.is_pick_day,
            m.is_pick_week,
            m.is_assigned_pick,
            m.percent_smoothed,
            m.max_percent_smoothed,
            n.week_max_percent_smoothed AS week1_max_percent_smoothed
           FROM records m
             LEFT JOIN ( SELECT r.vendor_id,
                    r.ksnko_container_id,
                    r.week,
                    max(r.week_max_percent_smoothed) AS week_max_percent_smoothed
                   FROM records r
                  GROUP BY r.vendor_id, r.ksnko_container_id, r.week) n ON m.vendor_id::text = n.vendor_id::text AND m.ksnko_container_id = n.ksnko_container_id AND (m.week + 1::double precision) = n.week
          ORDER BY m.vendor_id, m.ksnko_container_id, m.measured_at
        ), level2 AS (
         SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            m.week,
            m.is_pick_day,
            m.is_pick_week,
            m.is_assigned_pick,
            m.percent_smoothed,
            m.max_percent_smoothed,
            m.week1_max_percent_smoothed,
            n.week1_max_percent_smoothed AS week2_max_percent_smoothed,
            (m.week1_max_percent_smoothed::double precision + n.week1_max_percent_smoothed::double precision) / 2::double precision AS avg_weekly_increase
           FROM level1 m
             LEFT JOIN ( SELECT level1.vendor_id,
                    level1.ksnko_container_id,
                    level1.week,
                    max(level1.week1_max_percent_smoothed) AS week1_max_percent_smoothed
                   FROM level1
                  GROUP BY level1.vendor_id, level1.ksnko_container_id, level1.week) n ON m.vendor_id::text = n.vendor_id::text AND m.ksnko_container_id = n.ksnko_container_id AND (m.week + 1::double precision) = n.week
          ORDER BY m.vendor_id, m.ksnko_container_id, m.measured_at
        ), status_tbl AS (
         SELECT r.vendor_id,
            r.ksnko_container_id,
            r.measured_at,
            r.is_assigned_pick,
            r.percent_smoothed,
            r.max_percent_smoothed,
                CASE
                    WHEN r.percent_smoothed > 90 OR r.max_percent_smoothed > 90 THEN
                    CASE
                        WHEN lag(r.percent_smoothed, 1) OVER (PARTITION BY r.vendor_id, r.ksnko_container_id ORDER BY r.measured_at) < 91 THEN 'begin'::text
                        WHEN lead(r.percent_smoothed, 1) OVER (PARTITION BY r.vendor_id, r.ksnko_container_id ORDER BY r.measured_at) < 91 THEN 'end'::text
                        ELSE 'body'::text
                    END
                    ELSE 'ok'::text
                END AS status
           FROM records r
        ), full_picks AS (
         SELECT foo.vendor_id,
            foo.ksnko_container_id,
            foo.percent_smoothed,
            foo.full_started,
            foo.measured_at,
            date_part('epoch'::text, foo.measured_at - foo.full_started) / (3600 * 24)::double precision AS full_days
           FROM ( SELECT status_tbl.vendor_id,
                    status_tbl.ksnko_container_id,
                    status_tbl.percent_smoothed,
                    status_tbl.status,
                        CASE
                            WHEN lag(status_tbl.status) OVER (PARTITION BY status_tbl.vendor_id, status_tbl.ksnko_container_id ORDER BY status_tbl.measured_at) = 'begin'::text AND (status_tbl.status = ANY (ARRAY['end'::text, 'body'::text])) THEN lag(status_tbl.measured_at) OVER (PARTITION BY status_tbl.vendor_id, status_tbl.ksnko_container_id ORDER BY status_tbl.measured_at)
                            ELSE NULL::timestamp with time zone
                        END AS full_started,
                    status_tbl.measured_at
                   FROM status_tbl
                  WHERE status_tbl.is_assigned_pick = 1 AND (status_tbl.status = ANY (ARRAY['body'::text, 'end'::text])) OR status_tbl.is_assigned_pick = 0 AND status_tbl.status = 'begin'::text) foo
          WHERE foo.full_started IS NOT NULL AND (date_part('epoch'::text, foo.measured_at - foo.full_started) / (3600 * 24)::double precision) >= 5::double precision
        ), empty_picks AS (
         SELECT l.vendor_id,
            l.ksnko_container_id,
            l.measured_at,
            l.week,
            l.is_pick_day,
            l.is_assigned_pick,
            l.max_percent_smoothed AS percent_smoothed,
            l.week1_max_percent_smoothed,
            l.week2_max_percent_smoothed
           FROM level2 l
          WHERE l.is_assigned_pick = 1 AND l.max_percent_smoothed < 85
        ), picks_summary AS (
         SELECT a.vendor_id,
            a.ksnko_container_id,
            a.pick_day_ps,
            a.pick_at AS assigned_pick,
                CASE
                    WHEN e.percent_smoothed IS NOT NULL THEN 1
                    ELSE 0
                END AS empty_pick,
            m.percent_smoothed,
            a.max_percent_smoothed,
            l2.week1_max_percent_smoothed,
            l2.week2_max_percent_smoothed,
            l2.avg_weekly_increase,
                CASE
                    WHEN (e.percent_smoothed + e.week1_max_percent_smoothed) < 90 THEN 1
                    ELSE 0
                END AS objem1,
                CASE
                    WHEN (e.percent_smoothed + e.week1_max_percent_smoothed + e.week2_max_percent_smoothed) < 90 THEN 1
                    ELSE 0
                END AS objem2,
                CASE
                    WHEN f.full_days IS NOT NULL THEN 1
                    ELSE 0
                END AS full_pick,
            f.full_days
           FROM ( SELECT p.vendor_id,
                    p.ksnko_container_id,
                    min(p.pick_date::date) AS pick_day_ps,
                    p.week,
                    i.pick_at,
                    i.max_percent_smoothed
                   FROM ps_picks p
                     LEFT JOIN sensor_picks i ON p.vendor_id::text = i.vendor_id::text AND p.ksnko_container_id = i.ksnko_container_id AND p.week = i.week
                  GROUP BY p.vendor_id, p.ksnko_container_id, p.week, i.pick_at, i.max_percent_smoothed) a
             LEFT JOIN empty_picks e ON a.vendor_id::text = e.vendor_id::text AND a.ksnko_container_id = e.ksnko_container_id AND a.pick_at = e.measured_at
             LEFT JOIN full_picks f ON a.vendor_id::text = f.vendor_id::text AND a.ksnko_container_id = f.ksnko_container_id AND a.pick_at = f.measured_at
             LEFT JOIN measurements m ON a.vendor_id::text = m.vendor_id::text AND a.ksnko_container_id = m.ksnko_container_id AND a.pick_at = m.measured_at
             LEFT JOIN level2 l2 ON a.vendor_id::text = l2.vendor_id::text AND a.ksnko_container_id = l2.ksnko_container_id AND a.pick_at = l2.measured_at
        ), main AS (
         SELECT foo.vendor_id,
            foo.ksnko_container_id,
            avg(foo.max_percent_smoothed) AS avg_percent_smoothed,
            avg(foo.avg_weekly_increase) AS avg_weekly_increase,
            count(foo.pick_day_ps) AS ps_picks_count,
            count(foo.assigned_pick) AS assigned_picks_count,
            sum(foo.empty_pick) AS empty_picks_count,
            sum(foo.objem1) AS objem1_count,
            sum(foo.objem2) AS objem2_count,
            sum(foo.full_pick) AS full_picks_count
           FROM ( SELECT picks_summary.vendor_id,
                    picks_summary.ksnko_container_id,
                    picks_summary.pick_day_ps,
                    picks_summary.assigned_pick,
                    picks_summary.empty_pick,
                    picks_summary.max_percent_smoothed,
                    picks_summary.week1_max_percent_smoothed,
                    picks_summary.week2_max_percent_smoothed,
                    picks_summary.avg_weekly_increase,
                    picks_summary.objem1,
                    picks_summary.objem2,
                    picks_summary.full_pick,
                    picks_summary.full_days
                   FROM picks_summary
                  GROUP BY picks_summary.vendor_id, picks_summary.ksnko_container_id, picks_summary.pick_day_ps, picks_summary.assigned_pick, picks_summary.empty_pick, picks_summary.max_percent_smoothed, picks_summary.week1_max_percent_smoothed, picks_summary.week2_max_percent_smoothed, picks_summary.avg_weekly_increase, picks_summary.objem1, picks_summary.objem2, picks_summary.full_pick, picks_summary.full_days) foo
          GROUP BY foo.vendor_id, foo.ksnko_container_id
        )
 SELECT main.vendor_id,
    main.ksnko_container_id,
    main.avg_percent_smoothed,
    main.avg_weekly_increase,
        CASE
            WHEN main.ps_picks_count = main.assigned_picks_count THEN 'ok'::text
            ELSE concat((main.ps_picks_count - main.assigned_picks_count)::text, ' z ', main.ps_picks_count::text)
        END AS status,
        CASE
            WHEN main.ps_picks_count = main.assigned_picks_count THEN
            CASE
                WHEN main.ps_picks_count = main.empty_picks_count THEN
                CASE
                    WHEN main.empty_picks_count = main.objem1_count AND main.empty_picks_count <> main.objem2_count THEN 'snižujeme 1x'::text
                    WHEN main.empty_picks_count = main.objem2_count THEN 'snižujeme 2x'::text
                    ELSE 'nic'::text
                END
                WHEN main.ps_picks_count::double precision <= ((3 / 2)::double precision * main.full_picks_count::double precision) THEN 'zvyšujeme'::text
                ELSE 'nic'::text
            END
            ELSE 'nelze určit'::text
        END AS change_interval
   FROM main;


-- analytic.v_waste_collection_measurements source

CREATE OR REPLACE VIEW analytic.v_waste_collection_measurements
AS SELECT m.ksnko_container_id,
    sta.number AS station_number,
    m.percent_smoothed,
    m.percent_raw,
    m.measured_at,
    m.measured_at::date AS measured_at_date,
    split_part(m.measured_at::character varying(50)::text, ' '::text, 2) AS measured_at_time,
    COALESCE(m.percent_smoothed::numeric, 0::numeric) / 100::numeric AS percent_smoothed_calculated_decimal
   FROM waste_collection.measurements m
     JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
     JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id
     JOIN waste_collection.ksnko_stations sta ON sta.id = k.station_id;


-- analytic.v_waste_collection_measurements_last_3_months source

CREATE OR REPLACE VIEW analytic.v_waste_collection_measurements_last_3_months
AS SELECT measurements.ksnko_container_id,
    measurements.percent_smoothed,
    measurements.measured_at
   FROM waste_collection.measurements
  WHERE measurements.measured_at >= (now() - '3 mons'::interval);


-- analytic.v_waste_collection_pick_dates_ps source

CREATE OR REPLACE VIEW analytic.v_waste_collection_pick_dates_ps
AS SELECT vv.ksnko_container_id,
    vv.measured_at,
    vv.measured_at::date AS measured_at_date,
    vv.percent_smoothed,
        CASE
            WHEN vv.row_number = 2 AND vv.pick_date IS NOT NULL THEN 'Svoz PS'::text
            ELSE NULL::text
        END AS picks,
        CASE
            WHEN vv.row_number = 2 AND vv.pick_date IS NOT NULL THEN vv.pick_date
            ELSE NULL::timestamp with time zone
        END AS pick_date_svoz
   FROM ( SELECT v.ksnko_container_id,
            v.measured_at,
            v.percent_smoothed,
            v.pick_date,
            row_number() OVER (PARTITION BY v.ksnko_container_id, (v.pick_date::date) ORDER BY v.measured_at) AS row_number
           FROM ( SELECT COALESCE(m.ksnko_container_id, p.ksnko_container_id) AS ksnko_container_id,
                    COALESCE(m.measured_at, p.pick_date) AS measured_at,
                    COALESCE(m.percent_smoothed, 0) AS percent_smoothed,
                    p.pick_date
                   FROM waste_collection.measurements m
                     FULL JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
                  WHERE m.measured_at <= (( SELECT max(measurements.measured_at) AS max
                           FROM waste_collection.measurements)) AND m.measured_at <= now()) v) vv
  ORDER BY vv.measured_at DESC;


-- analytic.v_waste_collection_pick_dates_sensoneo source

CREATE OR REPLACE VIEW analytic.v_waste_collection_pick_dates_sensoneo
AS SELECT vv.ksnko_container_id,
    vv.measured_at,
    vv.measured_at::date AS measured_at_date,
    vv.percent_smoothed,
    vv.pick_at,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN 'Svoz sensoneo'::text
            ELSE NULL::text
        END AS picks,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_before
            ELSE NULL::integer
        END AS percent_before,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_now
            ELSE NULL::integer
        END AS percent_now,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.event_driven
            ELSE NULL::boolean
        END AS event_driven,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_before
            ELSE NULL::integer
        END -
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_now
            ELSE NULL::integer
        END AS size
   FROM ( SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            m.percent_smoothed,
            p.pick_at,
            p.event_driven,
            p.percent_before,
            p.percent_now,
            row_number() OVER (PARTITION BY m.ksnko_container_id, (p.pick_at::date) ORDER BY p.pick_at) AS row_number
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.picks p ON m.vendor_id::text = p.vendor_id::text AND m.measured_at = p.pick_at) vv;


-- analytic.v_waste_collection_unassigned_picks_rfid source

CREATE OR REPLACE VIEW analytic.v_waste_collection_unassigned_picks_rfid
AS SELECT cp.ksnko_container_id AS container_code,
    cp.pick_at,
    cp.pick_at::date AS pick_at_date
   FROM waste_collection.picks_rfid cp
     JOIN waste_collection.ksnko_containers cc ON cp.ksnko_container_id = cc.id
     JOIN waste_collection.container_vendor_data_rfid cvd ON cc.id = cvd.ksnko_container_id
     LEFT JOIN python.waste_collection_assigned_pick_dates_rfid wpd ON cp.ksnko_container_id = wpd.container_code::integer AND cp.pick_at = wpd.pick_at
  WHERE cvd.active IS TRUE AND wpd.category IS NULL;