CREATE OR REPLACE VIEW analytic.v_waste_collection_measurements_last_3_months
AS SELECT measurements.ksnko_container_id,
    measurements.percent_smoothed,
    measurements.measured_at
   FROM measurements
  WHERE measurements.measured_at >= (now() - '3 mons'::interval);
  