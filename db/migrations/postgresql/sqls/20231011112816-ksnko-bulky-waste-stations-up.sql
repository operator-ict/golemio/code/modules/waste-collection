
CREATE TABLE IF NOT EXISTS ksnko_bulky_waste_stations ( 
id int NOT NULL,
pick_date date NOT NULL,
pick_time_from time ,
pick_time_to time , 
street varchar(50),
payer  varchar(50),
number_of_containers int ,
service_id int ,
service_code varchar(50),
service_name varchar(50),
trash_type_id integer NULL,
trash_type varchar(50) NULL,
trash_type_name varchar(50) NULL,
city_district_id int,
city_district_name varchar(50),
city_district_ruian varchar(50),
coordinate_lat decimal,
coordinate_lon decimal,
geom geometry,
created_at timestamptz NULL,
created_by varchar(150) NULL,
update_batch_id int8 NULL,
updated_at timestamptz NULL,
updated_by varchar(150) NULL,
changed timestamptz ,
     CONSTRAINT ksnko_bulky_waste_stations_pkey PRIMARY KEY (id, pick_date)
);