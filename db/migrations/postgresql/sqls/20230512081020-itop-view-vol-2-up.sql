DROP VIEW analytic.v_waste_collection_containers_technical_info;

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info AS
WITH main AS (
         SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            ks_1.number AS station_number,
            ks_1.city_district_name,
            ks_1.name,
            initcap(rpd.trash_type_name::text)::character varying AS trash_type_name,
            cvd_1.network,
            cvd_1.anti_noise_depth,
            cvd_1.bin_depth,
            cvd_1.vendor,
            cvd_1.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed::double precision / 100::double precision AS percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd_1 ON m.vendor_id::text = cvd_1.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd_1.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks_1 ON k.station_id = ks_1.id
             JOIN waste_collection.raw_pick_dates rpd ON ks_1.number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text AND ks_1.active IS TRUE
          WHERE cvd_1.active IS TRUE AND m.measured_at >= (now() - '3 mons'::interval)
        ), min_percent AS (
         SELECT DISTINCT m.ksnko_container_id,
            max(m.measured_at) OVER (PARTITION BY m.ksnko_container_id) AS last_measured_at,
            m.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id) AS min_percent_smoothed
                   FROM main) m
          WHERE m.min_percent_smoothed = m.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
            main.trash_type_name,
            1::double precision - main.anti_noise_depth::double precision / main.bin_depth::double precision AS anti_noise_depth,
            min(main.battery_status) OVER (PARTITION BY main.ksnko_container_id) AS last_battery_status
           FROM main
          WHERE main.measured_at >= (now() - '14 days'::interval day)
        ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END AS daily_msg_expected,
                    30 AS count_days,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN
                            CASE
                                WHEN count(main.measured_at) > 4 THEN 4::bigint
                                ELSE count(main.measured_at)
                            END
                            ELSE
                            CASE
                                WHEN count(main.measured_at) > 7 THEN 7::bigint
                                ELSE count(main.measured_at)
                            END
                        END AS daily_msg
                   FROM main
                  WHERE main.measured_at >= (CURRENT_DATE::timestamp with time zone - '30 days'::interval) AND main.measured_at < CURRENT_DATE::timestamp with time zone
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.vendor, main.sensor_version, main.measured_at_day, (
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        ), max_measured_at AS (
         SELECT measurements.ksnko_container_id,
            max(measurements.measured_at) AS max_measured_at
           FROM waste_collection.measurements
          GROUP BY measurements.ksnko_container_id
        ), counts as 
        (select sensor_id, count(distinct service_date) services_count from waste_collection.sensor_cycle_itop
		where service_date >= (now() - '6 mons'::interval)
	group by sensor_id)
 SELECT max_measured_at.max_measured_at,
    cvd.ksnko_container_id,
    cvd.vendor_id,
    cvd.sensor_id,
    ks.number AS station_number,
    ks.name,
    ks.city_district_name,
    lbs.trash_type_name,
    cvd.sensor_version,
    cvd.network,
    COALESCE(round((lbs.anti_noise_depth * 100::double precision)::numeric, 2)::character varying(255), 'není'::character varying) AS anti_noise_depth,
    mp.min_percent_smoothed * 100::double precision AS min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent * 100::double precision AS msg_transfer_in_percent,
	itop.ksnko_container_id as ksnko_container_id_itop,
	itop.service_date,
	itop.description,
	(itop.service_date::date + '18 mons'::interval)::date as contract_service_date,
	coalesce(c.services_count, 0) as services_count,
	itop.install_date,
	itop.production_date,
	itop.end_of_warranty_mhmp
   FROM waste_collection.container_vendor_data cvd
     JOIN waste_collection.ksnko_containers kc ON kc.id = cvd.ksnko_container_id
     LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_id = ks.id
     LEFT JOIN min_percent mp ON mp.ksnko_container_id = cvd.ksnko_container_id
     LEFT JOIN last_battery_status lbs ON cvd.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON cvd.ksnko_container_id = msg.ksnko_container_id
     LEFT JOIN max_measured_at ON cvd.ksnko_container_id = max_measured_at.ksnko_container_id
     left join waste_collection.sensor_cycle_itop_actual itop on upper(itop.sensor_id) = upper(cvd.sensor_id)
     left join counts c on itop.sensor_id = c.sensor_id
  WHERE cvd.active;
