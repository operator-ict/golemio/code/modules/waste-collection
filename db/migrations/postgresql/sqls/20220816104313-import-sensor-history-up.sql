ALTER TABLE sensor_history ADD COLUMN is_actual BOOLEAN;

-- set correct is_actual value for existing data
WITH latest_record AS (
    SELECT vendor_id, MAX("recorded_at") AS "max_recorded_at" FROM sensor_history GROUP BY vendor_id
)
UPDATE sensor_history
SET is_actual = true
FROM latest_record
WHERE sensor_history.vendor_id = latest_record.vendor_id AND sensor_history.recorded_at = latest_record.max_recorded_at;


CREATE OR REPLACE FUNCTION import_sensor_history(p_data json, p_time timestamp with time zone)
    RETURNS void
    LANGUAGE plpgsql
AS $function$

DECLARE
    xvendor text;
    xsensor text;
    query_rec record;
BEGIN
    FOR query_rec IN SELECT * FROM (
        SELECT json_array_elements (p_data)->>'vendor_id' vendor_id, json_array_elements (p_data)->>'sensor_id' sensor_id
    ) a
    LOOP
        xvendor = query_rec.vendor_id ;
        xsensor = query_rec.sensor_id ;
        IF NOT EXISTS
            ( SELECT 1
              FROM waste_collection.sensor_history
              WHERE vendor_id = xvendor
            ) THEN
            INSERT INTO waste_collection.sensor_history (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
            VALUES(xvendor, xsensor, p_time, true, 0, now(), now());
        ELSE
            IF NOT EXISTS (
                SELECT 1
                FROM waste_collection.sensor_history
                WHERE xvendor = vendor_id
                  AND xsensor = sensor_id
                  AND is_actual
                )
            THEN
                UPDATE waste_collection.sensor_history
                SET is_actual = false
                WHERE xvendor = vendor_id;
                INSERT INTO waste_collection.sensor_history (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
                VALUES(xvendor, xsensor, p_time, true, 0, now(), now());
            ELSE
                UPDATE waste_collection.sensor_history
                SET updated_at = now ()
                WHERE xvendor = vendor_id AND sensor_id = xsensor;
            END IF;
        END IF;
    END LOOP;
END;
$function$
