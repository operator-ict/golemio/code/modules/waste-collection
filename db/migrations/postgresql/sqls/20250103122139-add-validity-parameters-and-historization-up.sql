-- Drop FK
ALTER TABLE ksnko_containers DROP CONSTRAINT IF EXISTS ksnko_containers_station_id_fkey;
ALTER TABLE ksnko_items DROP CONSTRAINT IF EXISTS ksnko_items_station_id_fkey;
ALTER TABLE planned_pick_dates DROP CONSTRAINT IF EXISTS planned_pick_dates_fk;

ALTER TABLE picks DROP CONSTRAINT IF EXISTS picks_vendor_id_fkey;
ALTER TABLE sensor_history DROP CONSTRAINT IF EXISTS sensor_history_vendor_id_fkey;
ALTER TABLE measurements DROP CONSTRAINT IF EXISTS measurements_vendor_id_fkey;

-- Table container_vendor_data;
ALTER TABLE container_vendor_data ADD COLUMN form_hash uuid;
ALTER TABLE container_vendor_data ADD COLUMN is_historical boolean DEFAULT false;

UPDATE container_vendor_data
SET form_hash =  
md5(concat_ws(',',
vendor_id,
ksnko_container_id,
sensor_id,
network,
bin_depth,
has_anti_noise::text,
anti_noise_depth,
algorithm,
sensor_version,
is_sensitive_to_pickups::text,
pickup_sensor_type,
decrease_threshold,
pick_min_fill_level,
active::text,
bin_brand,
is_historical::text
))::uuid WHERE form_hash IS NULL; 

ALTER TABLE container_vendor_data DROP CONSTRAINT IF EXISTS container_vendor_data_pkey;
ALTER TABLE container_vendor_data ADD CONSTRAINT container_vendor_data_pkey PRIMARY KEY (vendor_id,form_hash);

ALTER TABLE container_vendor_data ADD COLUMN valid_from timestamptz; --created_at
UPDATE container_vendor_data SET valid_from = COALESCE(valid_from, created_at);

ALTER TABLE container_vendor_data ADD COLUMN valid_to timestamptz NULL;

ALTER TABLE container_vendor_data RENAME TO container_vendor_data_history;

CREATE INDEX  container_vendor_data_history_valid_to_idx ON container_vendor_data_history USING btree (valid_to);

CREATE VIEW container_vendor_data
AS
select 
    *
from container_vendor_data_history
where valid_to is null	
order by vendor_id asc;

-- Table ksnko_containers;

ALTER TABLE ksnko_containers ADD COLUMN form_hash uuid;
ALTER TABLE ksnko_containers ADD COLUMN is_historical boolean DEFAULT false;

UPDATE ksnko_containers
SET form_hash =  
md5(concat_ws(',',
    id,
    volume_ratio,
    cleaning_frequency_code,
    station_id,
    active::text,
    is_historical::text 
))::uuid WHERE form_hash IS NULL; 


ALTER TABLE ksnko_containers DROP CONSTRAINT IF EXISTS ksnko_containers_pkey;
ALTER TABLE ksnko_containers ADD CONSTRAINT ksnko_containers_pkey PRIMARY KEY (id,form_hash);

ALTER TABLE ksnko_containers ADD COLUMN valid_from timestamptz DEFAULT CURRENT_TIMESTAMP; --created_at
ALTER TABLE ksnko_containers ADD COLUMN valid_to timestamptz NULL;

ALTER TABLE ksnko_containers RENAME TO ksnko_containers_history;

CREATE INDEX  ksnko_containers_history_valid_to_idx ON ksnko_containers_history USING btree (valid_to);

CREATE VIEW ksnko_containers
AS
select 
    *
from ksnko_containers_history
where valid_to is null	
order by id asc;

-- Table ksnko_stations;
DROP INDEX IF EXISTS ksnko_stations_number_key;
ALTER TABLE ksnko_stations DROP CONSTRAINT IF EXISTS ksnko_stations_id_key;
ALTER TABLE ksnko_stations ADD COLUMN form_hash uuid;
ALTER TABLE ksnko_stations ADD COLUMN is_historical boolean DEFAULT false;

UPDATE ksnko_stations
SET form_hash =  
md5(concat_ws(',',
id,
number,
name,
location,
active::text,
is_historical::text
))::uuid WHERE form_hash IS NULL; 

ALTER TABLE ksnko_stations DROP CONSTRAINT IF EXISTS ksnko_stations_pkey;
ALTER TABLE ksnko_stations ADD CONSTRAINT ksnko_stations_pkey PRIMARY KEY (id,form_hash);

ALTER TABLE ksnko_stations ADD COLUMN valid_from timestamptz; 
UPDATE ksnko_stations SET valid_from = COALESCE(valid_from, created_at);

ALTER TABLE ksnko_stations ADD COLUMN valid_to timestamptz NULL;



ALTER TABLE ksnko_stations RENAME TO ksnko_stations_history;

CREATE INDEX  ksnko_stations_history_id_idx ON ksnko_stations_history USING btree (valid_to);

CREATE VIEW ksnko_stations
AS
select 
    *
from ksnko_stations_history
where valid_to is null	
order by id asc;


-- Table raw_pick_dates;

ALTER TABLE raw_pick_dates ADD COLUMN form_hash uuid;
ALTER TABLE raw_pick_dates ADD COLUMN is_historical boolean DEFAULT false;

ALTER TABLE raw_pick_dates RENAME COLUMN valid_from to psas_valid_from; 
ALTER TABLE raw_pick_dates RENAME COLUMN valid_to to psas_valid_to; 

UPDATE raw_pick_dates
SET form_hash =  
md5(concat_ws(',',
subject_id,
count,
frequency,
street_name,
trash_type,
orientation_number,
psas_valid_from::timestamp,
psas_valid_to::timestamp,
station_number,
year_days_isoweeks,
company,
is_historical::text
))::uuid WHERE form_hash IS NULL; 

ALTER TABLE raw_pick_dates DROP CONSTRAINT IF EXISTS raw_pick_dates_pk;
ALTER TABLE raw_pick_dates ADD CONSTRAINT raw_pick_dates_pk PRIMARY KEY (subject_id,form_hash);

ALTER TABLE raw_pick_dates ADD COLUMN valid_from timestamptz; 
UPDATE raw_pick_dates SET valid_from = COALESCE(valid_from, created_at);

ALTER TABLE raw_pick_dates ADD COLUMN valid_to timestamptz NULL;

ALTER TABLE raw_pick_dates RENAME TO raw_pick_dates_history;

CREATE INDEX raw_pick_dates_history_valid_to_idx ON raw_pick_dates_history USING btree (valid_to);

CREATE VIEW raw_pick_dates
AS
select 
    *
from raw_pick_dates_history
where valid_to is null	
order by subject_id asc;

-- recreate  v_raw_pick_dates;
DROP VIEW v_raw_pick_dates;

CREATE VIEW v_raw_pick_dates AS (
    SELECT kc.id as container_id, rpd.*
    FROM ksnko_containers kc
    LEFT JOIN ksnko_stations ks ON ks.id = kc.station_id
    INNER JOIN waste_collection.raw_pick_dates rpd ON ks."number" = rpd.station_number  AND kc.trash_type = rpd.trash_type_code
);
