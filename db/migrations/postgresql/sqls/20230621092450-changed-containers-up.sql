CREATE TABLE container_changes (
	subject_id integer NOT NULL,
	task_id text NOT NULL,
	changed_at timestamptz NOT NULL,
	task_type text NOT NULL,
	container_type text NOT NULL,
	trash_type char(1) NOT NULL,
	trash_type_code varchar(2) NOT NULL,
	trash_type_name text NOT NULL,
	station_number text NOT NULL,
	company text NOT NULL,
	removed_containers_count float8 NULL,
	added_containers_count float8 NULL,
	street text NOT NULL,
	city_district text NOT NULL,		
	created_at timestamptz NULL,
	updated_at timestamptz NULL,	
	CONSTRAINT container_changes_pk PRIMARY KEY (subject_id,task_id)
);

CREATE OR REPLACE FUNCTION container_changes_replace(data jsonb)
    RETURNS void
    LANGUAGE 'plpgsql'
    SET search_path FROM CURRENT
AS $BODY$
begin	
	truncate table container_changes;   	
    INSERT INTO container_changes
    SELECT * from jsonb_populate_recordset(null::container_changes, data);
END;
$BODY$;
