CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info
AS 
WITH main AS (
        SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            k.station_number,
            ks.city_district_name,
            ks.name,
            initcap(rpd.trash_type_name::text)::character varying AS trash_type_name,
            cvd.network,
            cvd.anti_noise_depth,
            cvd.bin_depth,
            cvd.vendor,
            cvd.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed::double precision / 100::double precision AS percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks ON k.station_number::text = ks.number::text
             JOIN waste_collection.raw_pick_dates rpd ON k.station_number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE AND m.measured_at >= (now() - '3 mons'::interval)
--          ORDER BY m.ksnko_container_id, m.vendor_id, k.station_number, m.measured_at
        ), min_percent AS (
         SELECT DISTINCT m.ksnko_container_id,
            max(m.measured_at) OVER (PARTITION BY m.ksnko_container_id) AS last_measured_at,
            m.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id) AS min_percent_smoothed
                   FROM main) m
          WHERE m.min_percent_smoothed = m.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
         	main.trash_type_name,
            1::double precision - main.anti_noise_depth::double precision / main.bin_depth::double precision AS anti_noise_depth,
            min(main.battery_status) OVER (PARTITION BY main.ksnko_container_id) AS last_battery_status
           FROM main
         WHERE main.measured_at >= (now() - '14 days'::interval day)
          ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END AS daily_msg_expected,
                    30 AS count_days,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN
                            CASE
                                WHEN count(main.measured_at) > 4 THEN 4::bigint
                                ELSE count(main.measured_at)
                            END
                            ELSE
                            CASE
                                WHEN count(main.measured_at) > 7 THEN 7::bigint
                                ELSE count(main.measured_at)
                            END
                        END AS daily_msg
                   FROM main
                  WHERE main.measured_at >= (CURRENT_DATE::timestamp with time zone - '30 days'::interval) AND main.measured_at < CURRENT_DATE::timestamp with time zone
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.vendor, main.sensor_version, main.measured_at_day, (
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        )
 SELECT cvd.ksnko_container_id,
    cvd.vendor_id,
    kc.station_number,
    ks.name,
    ks.city_district_name,
    lbs.trash_type_name,
    cvd.sensor_version,
    cvd.network,
    COALESCE(round((lbs.anti_noise_depth * 100::double precision)::numeric, 2)::character varying(255), 'není'::character varying) AS anti_noise_depth,
    mp.min_percent_smoothed * 100::double precision AS min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent * 100::double precision AS msg_transfer_in_percent
   from waste_collection.container_vendor_data cvd
	join waste_collection.ksnko_containers kc on kc.id = cvd.ksnko_container_id
	LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_number = ks.number
   	left join min_percent mp on mp.ksnko_container_id = cvd.ksnko_container_id 
     LEFT JOIN last_battery_status lbs ON cvd.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON cvd.ksnko_container_id = msg.ksnko_container_id
	where cvd.active 
;
