
DROP VIEW IF EXISTS sensor_cycle_itop_actual;

ALTER TABLE sensor_cycle_itop DROP COLUMN ksnko_container_id;

CREATE VIEW sensor_cycle_itop_actual
AS
select 
	distinct on (external_form_id) external_form_id,
    sci.sensor_id,
    sci.description,
    sci.status,
    sci.location,
    sci.vendor,
    sci.model,
    sci.monitored,
    sci.network,
    sci.lora_id,
    sci.nbiot_id,
    sci.sigfox_id,
    sci.serial_number,
    sci.asset_number,
    sci.purchase_date,
    sci.install_date,
    sci.production_date,
    sci.end_of_warranty,
    sci.end_of_warranty_mhmp,
    sci.service_date,    
    sci.record_date,
	sci.form_hash,
    sci.create_batch_id,
    sci.created_at,
    sci.created_by,
    sci.update_batch_id,
    sci.updated_at,
    sci.updated_by
from sensor_cycle_itop as sci
inner join (
	select unnest(form_ids) as form_id, record_date  from download_history_itop
	where record_date = (
		select max(record_date) from download_history_itop
	)
) actualdownload on
	sci.external_form_id = actualdownload.form_id 	
order by external_form_id asc, sci.record_date desc;