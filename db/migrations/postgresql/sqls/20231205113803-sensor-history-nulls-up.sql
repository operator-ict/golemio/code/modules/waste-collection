CREATE OR REPLACE FUNCTION import_sensor_history(p_data json, p_time timestamp with time zone)
    RETURNS void
    LANGUAGE plpgsql
AS $function$

DECLARE
    xvendor text;
    xsensor text;
    query_rec record;
BEGIN
    FOR query_rec IN SELECT * FROM (
        SELECT json_array_elements (p_data)->>'vendor_id' vendor_id, json_array_elements (p_data)->>'sensor_id' sensor_id
    ) a
    LOOP
        xvendor = query_rec.vendor_id ;
        xsensor = query_rec.sensor_id ;
        IF NOT EXISTS
            ( SELECT 1
              FROM waste_collection.sensor_history
              WHERE vendor_id = xvendor
            ) THEN
            INSERT INTO waste_collection.sensor_history (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
            VALUES (xvendor, xsensor, p_time, true, 0, now(), now());
        ELSE
            IF NOT EXISTS (
                SELECT 1
                FROM waste_collection.sensor_history
                WHERE vendor_id = xvendor
                  AND coalesce(sensor_id, '') = coalesce(xsensor, '' )
                  AND is_actual
                )
            THEN
                UPDATE waste_collection.sensor_history
                SET is_actual = false
                WHERE vendor_id = xvendor;
                INSERT INTO waste_collection.sensor_history (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
                VALUES (xvendor, xsensor, p_time, true, 0, now(), now());
            ELSE
                UPDATE waste_collection.sensor_history
                SET updated_at = now ()
                WHERE vendor_id = xvendor AND coalesce(sensor_id, '') = coalesce(xsensor, '' );
            END IF;
        END IF;
    END LOOP;
END;
$function$
;

CREATE OR REPLACE FUNCTION import_sensor_history_rfid(p_data json, p_time timestamp with time zone)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
    xvendor text;
    xsensor text;
    query_rec record;
BEGIN
    FOR query_rec IN SELECT * FROM (
        SELECT json_array_elements (p_data)->>'vendor_id' vendor_id, json_array_elements (p_data)->>'sensor_id' sensor_id
    ) a
    LOOP
        xvendor = query_rec.vendor_id ;
        xsensor = query_rec.sensor_id ;
        IF NOT EXISTS
            ( SELECT 1
              FROM waste_collection.sensor_history_rfid
              WHERE vendor_id = xvendor
            ) THEN
            INSERT INTO waste_collection.sensor_history_rfid (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
            VALUES (xvendor, xsensor, p_time, true, 0, now(), now());
        ELSE
            IF NOT EXISTS (
                SELECT 1
                FROM waste_collection.sensor_history_rfid
                WHERE vendor_id = xvendor
                  AND coalesce(sensor_id, '') = coalesce(xsensor, '' )
                  AND is_actual
                )
            THEN
                UPDATE waste_collection.sensor_history_rfid
                SET is_actual = false
                WHERE vendor_id = xvendor;
                INSERT INTO waste_collection.sensor_history_rfid (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
                VALUES (xvendor, xsensor, p_time, true, 0, now(), now());
            ELSE
                UPDATE waste_collection.sensor_history_rfid
                SET updated_at = now ()
                WHERE vendor_id = xvendor AND coalesce(sensor_id, '') = coalesce(xsensor, '' );
            END IF;
        END IF;
    END LOOP;
END;
$function$
;

