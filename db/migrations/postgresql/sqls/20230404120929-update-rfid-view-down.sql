DROP VIEW analytic.v_waste_collection_assigned_picks_rfid;

CREATE OR REPLACE VIEW analytic.v_waste_collection_assigned_picks_rfid
AS SELECT cp.container_code,
    cp.pick_at,
    cp.pick_date AS pickup_date,
    cp.category,
        CASE
            WHEN cp.category::text = 'exact'::text THEN 'Přesný'::text
            WHEN cp.category::text = 'tolerated_before'::text THEN 'Předjetý'::text
            WHEN cp.category::text = 'tolerated_after'::text THEN 'Dodatečný'::text
            ELSE 'Nepřiřazený'::text
        END AS category_cz
   FROM python.waste_collection_assigned_pick_dates_rfid cp
     JOIN waste_collection.ksnko_containers cc ON cp.container_code::integer = cc.id
     JOIN waste_collection.container_vendor_data_rfid cvd ON cc.id = cvd.ksnko_container_id
  WHERE cvd.active IS TRUE;
  