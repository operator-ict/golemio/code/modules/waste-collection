SELECT v.vendor_id,
    v.id,
    v.code,
    v.volume_ratio,
    v.trash_type,
    v.container_volume,
    v.container_brand,
    v.container_dump,
    v.cleaning_frequency_code::character varying(50) AS cleaning_frequency_code,
    concat(v.cleaning_frequency_code, ' – ', v.days) AS cleaning_frequency_displayed,
    v.station_id,
    v.station_number,
    v.active,
    v.create_batch_id,
    v.created_at,
    v.created_by,
    v.update_batch_id,
    v.updated_at,
    v.updated_by,
    v.city_district_name,
    split_part(v.city_district_name::text, ' '::text, 2) AS city_district_number,
    v.coordinate_lat,
    v.coordinate_lon,
    v.trash_type_name,
    v.days,
    v.street_name,
    v.frequency,
    v.name,
    v.network,
    v.bin_brand,
    v.frequency_text,
    v.row_number
   FROM ( SELECT cvd.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_volume,
            kc.container_brand,
            replace(rpd.frequency::text, '0'::text, ''::text) AS cleaning_frequency_code,
            kc.container_dump,
            kc.station_id,
            kc.station_number,
            cvd.active,
            kc.create_batch_id,
            kc.created_at,
            kc.created_by,
            kc.update_batch_id,
            kc.updated_at,
            kc.updated_by,
            ks.city_district_name,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            ks.name,
            cvd.network,
            cvd.bin_brand,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data cvd
             JOIN waste_collection.ksnko_containers kc ON cvd.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_number::text = ks.number::text
             JOIN waste_collection.raw_pick_dates rpd ON kc.station_number::text = rpd.station_number::text AND kc.trash_type::text = rpd.trash_type_code::text and rpd.days IS NOT NULL
          WHERE cvd.active IS TRUE) v
  WHERE v.row_number = 1;

DROP VIEW analytic.v_waste_collection_containers_technical_info;
CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info
AS WITH main AS (
         SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            k.station_number,
            ks.city_district_name,
            ks.name,
            rpd.trash_type_name,
            cvd.network,
            cvd.anti_noise_depth,
            cvd.bin_depth,
            cvd.vendor,
            cvd.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed::double precision / 100::double precision AS percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks ON k.station_number::text = ks.number::text
             JOIN waste_collection.raw_pick_dates rpd ON k.station_number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE AND m.measured_at >= (now() - '3 mons'::interval)
          ORDER BY m.ksnko_container_id, m.vendor_id, k.station_number, m.measured_at
        ), min_percent AS (
         SELECT DISTINCT foo.ksnko_container_id,
            foo.vendor_id,
            foo.station_number,
            last_value(foo.measured_at) OVER (PARTITION BY foo.ksnko_container_id, foo.vendor_id, foo.station_number) AS last_measured_at,
            foo.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id, main.vendor_id, main.station_number) AS min_percent_smoothed
                   FROM main
                  ORDER BY main.ksnko_container_id, main.vendor_id, main.station_number, main.measured_at) foo
          WHERE foo.min_percent_smoothed = foo.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
            main.vendor_id,
            main.station_number,
            main.name,
            main.city_district_name,
            main.trash_type_name,
            main.sensor_version,
            main.network,
            1::double precision - main.anti_noise_depth::double precision / main.bin_depth::double precision AS anti_noise_depth,
            min(main.battery_status) OVER (PARTITION BY main.ksnko_container_id, main.vendor_id, main.station_number) AS last_battery_status
           FROM main
          WHERE main.measured_at >= (now() - '14 days'::interval day)
        ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END AS daily_msg_expected,
                    30 AS count_days,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN
                            CASE
                                WHEN count(main.measured_at) > 4 THEN 4::bigint
                                ELSE count(main.measured_at)
                            END
                            ELSE
                            CASE
                                WHEN count(main.measured_at) > 7 THEN 7::bigint
                                ELSE count(main.measured_at)
                            END
                        END AS daily_msg
                   FROM main
                  WHERE main.measured_at >= (CURRENT_DATE::timestamp with time zone - '30 days'::interval) AND main.measured_at < CURRENT_DATE::timestamp with time zone
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.vendor, main.sensor_version, main.measured_at_day, (
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        )
 SELECT lbs.ksnko_container_id,
    lbs.vendor_id,
    lbs.station_number,
    lbs.name,
    lbs.city_district_name,
    lbs.trash_type_name,
    lbs.sensor_version,
    lbs.network,
    COALESCE(round((lbs.anti_noise_depth * 100::double precision)::numeric, 2)::character varying(255), 'není'::character varying) AS anti_noise_depth,
    mp.min_percent_smoothed * 100::double precision AS min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent * 100::double precision AS msg_transfer_in_percent
   FROM last_battery_status lbs
     LEFT JOIN min_percent mp ON mp.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON lbs.ksnko_container_id = msg.ksnko_container_id;

CREATE OR REPLACE VIEW analytic.v_waste_collection_24_hr_missing
AS SELECT ks.id AS ksnko_container_id,
    max(m.measured_at) AS measured_at
   FROM waste_collection.measurements m
     JOIN waste_collection.container_vendor_data cc ON cc.vendor_id::text = m.vendor_id::text AND cc.active IS TRUE
     JOIN waste_collection.ksnko_containers ks ON m.ksnko_container_id = ks.id
  GROUP BY ks.id
 HAVING max(m.measured_at) < (now() - '1 day'::interval) OR max(m.measured_at) IS NULL
  ORDER BY (max(m.measured_at));
  