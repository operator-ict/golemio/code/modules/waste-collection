CREATE OR REPLACE FUNCTION analytic.waste_collection_last_correct(p_continer_code integer)
 RETURNS timestamp without time zone
 LANGUAGE plpgsql
AS $function$       
declare 
    posledni integer;
    vysledek timestamptz;
begin
    posledni = (
        select percent_smoothed   
        from waste_collection.measurements
        where ksnko_container_id = p_continer_code
        order by measured_at desc
        limit 1);
    vysledek=(
    select measured_at          
    from waste_collection.measurements
    where ksnko_container_id = p_continer_code
--  and not percent_smoothed between p_min and p_max
    and not percent_smoothed between posledni-3 and posledni+3
    order by measured_at desc
    limit 1);
   return coalesce (vysledek, (select min(measured_at) from waste_collection.measurements where ksnko_container_id = p_continer_code));
end;
$function$
;

