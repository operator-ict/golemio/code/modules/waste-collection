CREATE VIEW v_raw_pick_dates AS (
    SELECT kc.id as container_id, rpd.*
    FROM ksnko_containers kc
    LEFT JOIN ksnko_stations ks ON ks.id = kc.station_id
    INNER JOIN waste_collection.raw_pick_dates rpd ON ks."number" = rpd.station_number  AND kc.trash_type = rpd.trash_type_code
);
