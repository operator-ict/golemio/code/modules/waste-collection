CREATE INDEX IF NOT EXISTS stations_geom_idx ON ksnko_stations USING gist ("geom");
CREATE INDEX IF NOT EXISTS stations_district_idx ON ksnko_stations USING btree ("city_district_name");
CREATE INDEX IF NOT EXISTS stations_updated_at_idx ON ksnko_stations USING btree ("updated_at");
CREATE INDEX IF NOT EXISTS container_vendor_data_ksnko_id_idx ON container_vendor_data USING btree (ksnko_container_id);
CREATE INDEX IF NOT EXISTS measurements_ksnko_id_idx ON measurements USING btree (ksnko_container_id);
CREATE INDEX IF NOT EXISTS picks_ksnko_id_idx ON picks USING btree (ksnko_container_id);

CREATE MATERIALIZED VIEW IF NOT EXISTS v_last_picks AS
SELECT ksnko_container_id, MAX(pick_at) last_pick_at
FROM picks
GROUP BY 1;

CREATE UNIQUE INDEX IF NOT EXISTS v_last_picks_ksnko_id_idx ON v_last_picks USING btree (ksnko_container_id);

