-- musím pseudo vytvořit schémata aby o nich migrace věděly
CREATE SCHEMA IF NOT EXISTS analytic;

-- view s technickými informacemi
CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info
AS WITH main AS (
         SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            k.station_number,
            ks.city_district_name,
            ks.name,
            rpd.trash_type_name,
            cvd.network,
            cvd.anti_noise_depth,
            cvd.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks ON k.station_number::text = ks.number::text
             JOIN waste_collection.raw_pick_dates rpd ON k.station_number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE AND m.measured_at > (CURRENT_DATE - '3 mons'::interval)
          ORDER BY m.ksnko_container_id, m.vendor_id, k.station_number, m.measured_at
        ), min_percent AS (
         SELECT DISTINCT foo.ksnko_container_id,
            foo.vendor_id,
            foo.station_number,
            last_value(foo.measured_at) OVER (PARTITION BY foo.ksnko_container_id, foo.vendor_id, foo.station_number) AS last_measured_at,
            foo.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id, main.vendor_id, main.station_number) AS min_percent_smoothed
                   FROM main
                  WHERE main.measured_at > (now() - '3 mons'::interval)
                  ORDER BY main.ksnko_container_id, main.vendor_id, main.station_number, main.measured_at) foo
          WHERE foo.min_percent_smoothed = foo.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
            main.vendor_id,
            main.station_number,
            main.name,
            main.city_district_name,
            main.trash_type_name,
            main.sensor_version,
            main.network,
            main.anti_noise_depth,
            last_value(main.battery_status) OVER (PARTITION BY main.ksnko_container_id, main.vendor_id, main.station_number) AS last_battery_status
           FROM main
        ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN (main.sensor_version::text = ANY (ARRAY['v3'::character varying::text, 'v3lite'::character varying::text])) AND main.pick_day IS NOT NULL THEN 10
                            WHEN (main.sensor_version::text = ANY (ARRAY['v3'::character varying::text, 'v3lite'::character varying::text])) AND main.pick_day IS NULL THEN 9
                            ELSE 6
                        END AS daily_msg_expected,
                    CURRENT_DATE - (CURRENT_DATE - '3 mons'::interval)::date AS count_days,
                    count(main.measured_at) AS daily_msg
                   FROM main
                  WHERE main.measured_at_day <> now()::date
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.measured_at_day, (
                        CASE
                            WHEN (main.sensor_version::text = ANY (ARRAY['v3'::character varying::text, 'v3lite'::character varying::text])) AND main.pick_day IS NOT NULL THEN 10
                            WHEN (main.sensor_version::text = ANY (ARRAY['v3'::character varying::text, 'v3lite'::character varying::text])) AND main.pick_day IS NULL THEN 9
                            ELSE 6
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        )
 SELECT lbs.ksnko_container_id,
    lbs.vendor_id,
    lbs.station_number,
    lbs.name,
    lbs.city_district_name,
    lbs.trash_type_name,
    lbs.sensor_version,
    lbs.network,
    lbs.anti_noise_depth,
    mp.min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent
   FROM last_battery_status lbs
     LEFT JOIN min_percent mp ON mp.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON lbs.ksnko_container_id = msg.ksnko_container_id;
     