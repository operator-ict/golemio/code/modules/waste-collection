ALTER TABLE ksnko_items DROP CONSTRAINT ksnko_items_station_number_fkey;
ALTER TABLE ksnko_items ADD CONSTRAINT ksnko_items_station_number_fkey FOREIGN KEY ("station_number")
    REFERENCES ksnko_stations (number)
    ON UPDATE CASCADE
    ON DELETE NO ACTION;

ALTER TABLE ksnko_containers DROP CONSTRAINT ksnko_containers_station_number_fkey;
ALTER TABLE ksnko_containers ADD CONSTRAINT ksnko_containers_station_number_fkey FOREIGN KEY ("station_number")
    REFERENCES ksnko_stations (number)
    ON UPDATE CASCADE
    ON DELETE NO ACTION;

ALTER TABLE trash_type_mapping ADD COLUMN legacy_id INTEGER;
ALTER TABLE trash_type_mapping ADD CONSTRAINT trash_type_mapping_legacy_id_key UNIQUE (legacy_id);

TRUNCATE TABLE trash_type_mapping;

INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('sb', 'S', 'Barevné sklo', 1);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('e', '-', 'Elektrozařízení', 2);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('ko', 'E', 'Kovy', 3);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('nk', 'K', 'Nápojové kartóny', 4);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('p', 'P', 'Papír', 5);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('u', 'U', 'Plast', 6);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('sc', 'C', 'Čiré sklo', 7);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('jt', '-', 'Jedlé tuky a oleje', 8);
