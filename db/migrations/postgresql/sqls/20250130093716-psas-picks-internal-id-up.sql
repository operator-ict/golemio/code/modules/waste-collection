ALTER TABLE psas_real_picks_dates DROP CONSTRAINT psas_real_picks_dates_pk;
ALTER TABLE psas_real_picks_dates RENAME COLUMN pick_id to internal_pick_id;
ALTER TABLE psas_real_picks_dates ADD CONSTRAINT psas_real_picks_dates_pk PRIMARY KEY (internal_pick_id);
COMMENT ON COLUMN psas_real_picks_dates.internal_pick_id IS 'Hash generovaný ze sloupců subject_id, trash_type, station_number a psas_planned_pick_date.';
