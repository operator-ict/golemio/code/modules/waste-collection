/* Replace with your SQL commands */

CREATE TABLE container_vendor_data_rfid (
	vendor_id varchar(255) NOT NULL,
	ksnko_container_id int4 NULL,
    vendor varchar(255) NULL,
	sensor_id varchar(255) NULL,	
	network varchar(255) NULL,
    installed_at timestamptz NULL,
	installed_by varchar(255) NULL,
	bin_brand varchar(255) NULL,
    active bool NOT NULL,
    sensor_version varchar(255) NULL,    
    create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT container_vendor_data_rfid_pkey PRIMARY KEY (vendor_id)
);
CREATE INDEX container_vendor_data_rfid_ksnko_id_idx ON container_vendor_data_rfid USING btree (ksnko_container_id);

-- waste_collection.picks definition

CREATE TABLE picks_rfid (
	vendor_id varchar(255) NOT NULL,
	ksnko_container_id int4 NOT NULL,
	pick_at timestamptz NOT NULL,
	event_driven bool NULL,
    cu_id varchar(255) NOT NULL,
    pick_lat numeric NOT NULL,
    pick_lon numeric NOT NULL,
    geometry geometry NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT picks_rfid_pkey PRIMARY KEY (vendor_id, pick_at)
);

CREATE INDEX picks_rfid_id_idx ON picks_rfid USING btree (ksnko_container_id);

CREATE TABLE sensor_history_rfid (
	vendor_id varchar(255) NOT NULL,
	sensor_id varchar(255) NULL,
	recorded_at timestamptz NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	is_actual bool NULL,
	CONSTRAINT sensor_history_rfid_pkey PRIMARY KEY (vendor_id, recorded_at)
);



ALTER TABLE sensor_history_rfid ADD CONSTRAINT sensor_history_rfid_vendor_id_fkey FOREIGN KEY (vendor_id) REFERENCES container_vendor_data_rfid(vendor_id);

CREATE OR REPLACE FUNCTION import_sensor_history_rfid(p_data json, p_time timestamp with time zone)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
    xvendor text;
    xsensor text;
    query_rec record;
BEGIN
    FOR query_rec IN SELECT * FROM (
        SELECT json_array_elements (p_data)->>'vendor_id' vendor_id, json_array_elements (p_data)->>'sensor_id' sensor_id
    ) a
    LOOP
        xvendor = query_rec.vendor_id ;
        xsensor = query_rec.sensor_id ;
        IF NOT EXISTS
            ( SELECT 1
              FROM waste_collection.sensor_history_rfid
              WHERE vendor_id = xvendor
            ) THEN
            INSERT INTO waste_collection.sensor_history_rfid (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
            VALUES(xvendor, xsensor, p_time, true, 0, now(), now());
        ELSE
            IF NOT EXISTS (
                SELECT 1
                FROM waste_collection.sensor_history_rfid
                WHERE xvendor = vendor_id
                  AND xsensor = sensor_id
                  AND is_actual
                )
            THEN
                UPDATE waste_collection.sensor_history_rfid
                SET is_actual = false
                WHERE xvendor = vendor_id;
                INSERT INTO waste_collection.sensor_history_rfid (vendor_id, sensor_id, recorded_at, is_actual, create_batch_id, created_at, updated_at)
                VALUES(xvendor, xsensor, p_time, true, 0, now(), now());
            ELSE
                UPDATE waste_collection.sensor_history_rfid
                SET updated_at = now ()
                WHERE xvendor = vendor_id AND sensor_id = xsensor;
            END IF;
        END IF;
    END LOOP;
END;
$function$
;
