CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_rfid
AS WITH main AS (
         SELECT cvdr.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_dump,
            kc.container_volume,
            kc.cleaning_frequency_code,
            concat(kc.cleaning_frequency_code, ' – ', COALESCE(rpd.days, 'NA'::text)) AS cleaning_frequency_displayed,
            kc.station_id,
            kc.station_number,
            cvdr.active,
            ks.city_district_name,
            split_part(ks.city_district_name::text, ' '::text, 2) AS city_district_number,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            rpd.company,
            ks.name,
            cvdr.network,
            cvdr.bin_brand,
            cvdr.vendor,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data_rfid cvdr
             JOIN waste_collection.ksnko_containers kc ON cvdr.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_number::text = ks.number::text
             LEFT JOIN waste_collection.raw_pick_dates rpd ON kc.station_number::text = rpd.station_number::text AND kc.trash_type::text = rpd.trash_type_code::text
          WHERE cvdr.active IS TRUE
        ), last_pick AS (
         SELECT DISTINCT ON (pr.ksnko_container_id) pr.ksnko_container_id,
            pr.pick_at
           FROM waste_collection.picks_rfid pr
          ORDER BY pr.ksnko_container_id, pr.pick_at DESC
        )
 SELECT main.vendor_id,
    main.vendor,
    main.id,
    main.code,
    main.volume_ratio,
    main.trash_type,
    main.container_volume,
    main.container_dump,
    main.cleaning_frequency_code,
    main.cleaning_frequency_displayed,
    main.station_id,
    main.station_number,
    main.active,
    main.city_district_name,
    main.city_district_number,
    main.coordinate_lat,
    main.coordinate_lon,
    main.trash_type_name,
    main.days,
    main.street_name,
    main.frequency,
    main.name,
    main.network,
    main.company,
    main.bin_brand,
    main.frequency_text,
    main.row_number,
    last_pick.pick_at AS last_pick
   FROM main
     LEFT JOIN last_pick ON main.id = last_pick.ksnko_container_id
  WHERE main.row_number = 1;
  