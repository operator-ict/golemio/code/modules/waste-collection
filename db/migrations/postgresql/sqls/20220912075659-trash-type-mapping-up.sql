CREATE TABLE trash_type_mapping (
	code_ksnko varchar(3) NOT NULL,
	code_psas varchar(3) NOT NULL,
	"name" varchar NOT NULL,
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,
    CONSTRAINT trash_type_mapping_pk PRIMARY KEY (code_ksnko)
);

INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('p', 'P', 'Papír');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('u', 'U', 'Plast');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('sb', 'S', 'Barevné Sklo');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('sc', 'C', 'Čiré sklo');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('nk', 'K', 'Nápojové kartóny');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('ko', 'E', 'Kovy');
