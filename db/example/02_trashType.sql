INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('sb', 'S', 'Barevné sklo', 1, 5);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('e', '-', 'Elektrozařízení', 2, 8);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('ko', 'E', 'Kovy', 3, 3);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('nk', 'K', 'Nápojové kartóny', 4, 4);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('p', 'P', 'Papír', 5, 1);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('u', 'U', 'Plast', 6, 2);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('sc', 'C', 'Čiré sklo', 7, 6);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id, sort_order) VALUES('jt', '-', 'Jedlé tuky a oleje', 8, 7);
